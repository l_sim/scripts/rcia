#!/usr/bin/env python3

import h5py
import sys
import numpy

from optparse import OptionParser, OptionGroup

def parse():
  parser = OptionParser("usage: zapToPNG.py [options] [file]")
  parser.add_option("-g", "--group", dest="group", type="int", default = "0",
                    help="select the i-th group in the file.")
  parser.add_option("-d", "--detector", dest="detector", type="string", default = "pico",
                    help="name of the detector")
  parser.add_option("-o", "--output", dest="output", type="string", default = None,
                    help="name of the output file")
  parser.add_option("-i", "--interpolate", dest="interpol", action="store_true",
                    help="use real sy, sz position and interpolate to get a regular grid")
  parser.add_option("-n", "--normalise", dest="normalise", action="store_true",
                    help="normalise data by removing the average value of the last 5 pixels per line and by multiplying by the storage ring current variations")
  parser.add_option("-r", "--export-raw", dest="raw", action="store_true",
                    help="export raw data")
  return parser

def readZap(filename, grp = 0, detector = "pico", line_length = 0):
  with h5py.File(filename, "r") as f:
    if grp < 1:
      groupId = "1.1"
      groupSr = "1.1"
      for k in f.keys():
        if "measurement" in f[k].keys() and detector in f[k]["measurement"]:
          groupId = k
        if "measurement" in f[k].keys() and "srcur" in f[k]["measurement"]:
          groupSr = k
    else:
      groupId = "%d.1" % grp
      groupSr = "%d.1" % (grp + 1)
    measure = f[groupId]["measurement"]
    sy = numpy.array(measure["sy"])
    sz = numpy.array(measure["sz"])
    data = numpy.array(measure[detector])
    if "instrument" in f[groupId] and "fscan_parameters" in f[groupId]["instrument"]:
      params = f[groupId]["instrument"]["fscan_parameters"]
      ny = numpy.array(params["fast_npoints"])
      nz = numpy.array(params["slow_npoints"])
    else:
      if line_length < 1:
        ny = numpy.unique(sz).size
      else:
        ny = line_length
      nz = int(data.size / ny)
    if ny * nz != data.size:
      raise ValueError
    tics = numpy.array(measure["epoch_trig"])
    measure = f[groupSr]["measurement"]
    epoch = numpy.array(measure["epoch"])
    srcur = numpy.array(measure["srcur"])
    if len(srcur) > 1:
      from scipy import interpolate
      I = interpolate.interp1d(epoch, srcur, kind = "linear")
      srcurs = I(tics)
    else:
      srcurs = None
    return sy, sz, data, ny, nz, srcurs
  return 0, 0, None, None, None, None

def jetScale(img):
  img = img * 4.
  red1 = img - 382
  red2 = -img + 1147
  red = numpy.minimum(red1, red2).clip(min = 0, max = 255).astype(numpy.dtype('u1'))
  green1 = img - 127
  green2 = -img + 892
  green = numpy.minimum(green1, green2).clip(min = 0, max = 255).astype(numpy.dtype('u1'))
  blue1 = img + 127
  blue2 = -img + 637
  blue = numpy.minimum(blue1, blue2).clip(min = 0, max = 255).astype(numpy.dtype('u1'))
  return numpy.vstack((red, green, blue)).T.reshape(-1)

def export(filename, doInterpolate, normalise, X, Y, data, nx, ny, srcur):
  if normalise:
    data -= numpy.hstack(numpy.repeat(data.reshape(ny, nx)[:, -5:].mean(1).T, nx))
    data *= srcur.max() / srcur
  if doInterpolate:
    from scipy import interpolate
    I = interpolate.interp2d(X, Y, data, kind = "linear")
    data = I(numpy.linspace(X.min(), X.max(), nx), numpy.linspace(Y.min(), Y.max(), ny))
  #from PIL import Image
  #img = Image.fromarray(data.reshape(ny, nx), "F").convert("L")
  #img.save(filename)
  import gi
  gi.require_version('GdkPixbuf', '2.0')
  from gi.repository import GdkPixbuf, GLib
  img = GLib.Bytes.new(jetScale(256. * (data - data.min()) / data.ptp()))
  pixbuf = GdkPixbuf.Pixbuf.new_from_bytes(img, GdkPixbuf.Colorspace.RGB,
                                          False, 8, nx, ny, nx * 3).flip(False)
  pixbuf.savev(filename, filename[-3:], (), ())

def exportRaw(X, Y, data, nx, ny, srcur):
  for line in data.reshape(ny, nx):
    for val in line:
      print(val)
    print()

if __name__ == "__main__":
  parser = parse()
  (options, args) = parser.parse_args()

  if (options.raw):
    exportRaw(*readZap(args[0], options.group, options.detector))
  else:
    output = options.output
    if output is None:
      output = args[0].replace(".h5", ".png")
    export(output, options.interpol, options.normalise,
           *readZap(args[0], options.group, options.detector))
