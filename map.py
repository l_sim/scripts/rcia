#!/usr/bin/env python

import sys
import numpy
import scipy
from scipy import ndimage
import rcv, edf

rcvs = rcv.RockingCurveSet()
for path in sys.argv[1:]:
  dt = rcv.RockingCurve()
  dt.read(path)
  rcvs.add(dt)

## for i in range(1309, 1349):
##   data = rcvs.getDataAt(i, 1080)
##   print data

## (integral, fwhm, peakpos) = rcvs.calcMap(ptpMax = 10)
## integral.save("map_integral.edf")
## fwhm.save("map_FWHM.edf")
## peakpos.save("map_peakpos.edf")

mp = edf.RawImage()
mp.read("map_peakpos.edf")

data = mp.getData(flat = False)
# For 75
dx = 31.
# For 76
## dx = 26.
# For 77
## dx = 5.
angle = numpy.arctan(dx /2048.) * 180 / numpy.pi
data_r = ndimage.interpolation.rotate(data, -angle)[:2048, :2048]

#mask = (data == 9.03800011)
mask = (data == rcvs.lst[0].origAng).astype(numpy.dtype('f4'))
mask_r = (ndimage.interpolation.rotate(mask, -angle, cval = 1.)[:2048, :2048] > 0.33)
#mask = ((data == rcvs.lst[0].origAng) | (data < 9.95))
#mask = ((data == rcvs.lst[0].origAng) | (data < 9.95))
filtered = data_r.copy()
filtered[mask_r] = 0.
nf = 2048 - mask_r.sum(0)
nf[nf == 0] = 1
s = filtered.sum(0) / nf

## for val in s:
##   print val

# -0.87; -0.75  | 0.12
# -0.67; -0.55  | 0.12
# -0.47; -0.43  | 0.04
# -0.30; -0.26  | 0.04

# Slits are:
# For 75
slits = ((67, 139),
         (680, 742),
         (1278, 1348),
         (1875, 1954))
# For 76
## slits = ((93, 157),
##          (693, 763),
##          (1280, 1364),
##          (1872, 1967))
# For 77
## slits = ((0, 44),
##          (533, 639),
##          (1147, 1219),
##          (1731, 1806))

deltas = []
for (m, M) in slits:
  avg = s[m:M].sum() / (M - m)
  deltas.append(avg)
#print deltas

# Smallest FWHM areas are:
# For 75
refs = ((96, 1754, 141, 1828),
        (704, 416, 746, 458),
        (1309, 233, 1352, 327),
        (1901, 195, 1943, 323))
# For 76
## refs = ((138, 590, 157, 656),
##         (741, 1094, 768, 1180),
##         (1324, 1419, 1356, 1539),
##         (1925, 249, 1957, 316))
# For 77
## refs = ((8, 103, 28, 213),
##         (565, 1321, 591, 1379),
##         (1181, 678, 1203, 733),
##         (1748, 1630, 1786, 1672))

deltas2 = []
for (x1, y1, x2, y2) in refs:
  avg = data_r[y1:y2, x1:x2].sum() / ((x2 - x1) * (y2 - y1))
  deltas2.append(avg)
#print deltas2

# Generate the mask for the FWHM.
mask_FWHM = numpy.ones((2048, 2048), dtype = numpy.bool8)
for (x1, y1, x2, y2) in refs:
  mask_FWHM[y1:y2, x1:x2] = False
FWHM = edf.RawImage()
FWHM.read("map_FWHM_rotated.edf")

dt = FWHM.getData(flat = False)
mask_FWHM_r = ((dt < 0.0015) & (mask_r == False))
pix = FWHM.getPixbuf(scale = (0.001, 0.01), logScale = True,
                     colorMap = edf.jetScale, mask = mask_FWHM_r)
pix.save("map_FWHM_rotated_zoned.png", "png")

delta_all = data_r.copy()
delta_all[~mask_FWHM_r] == 0.
nf = mask_FWHM_r.sum(0)
nf[nf == 0] = 1
s = delta_all.sum(0) / nf
deltas3 = []
slit = None
for (i, n, val) in zip(range(len(s)), nf, s):
  # print i, n, val
  if n > 100:
    if slit is None:
      slit = []
    slit.append((i, val, n))
  else:
    if slit is not None and len(slit) > 50:
      deltas3.append(numpy.array(slit))
    slit = None
print deltas3
fghj

x = 0
for delta in deltas2:
  data_r[:, x:x + 512] += (9.28959910219027 - delta)
  x += 512

a0 = 5.43102
data = (12398.41930 / 20000 / 2 * numpy.sqrt(8) / numpy.sin(data_r * numpy.pi / 180) - a0) / a0

eps = edf.RawImage()
eps.set(data)
eps.store(kind = "map")
eps.save("map_epsilon_rotated.edf")
pix = eps.getPixbuf(scale = (-0.001, 0.001), logScale = False,
                    colorMap = edf.jetScale, mask = mask_r)
pix.save("map_epsilon_rotated_zoned.png", "png")

filtered = data.copy()
filtered[mask_r] = 0.
nf = 2048 - mask_r.sum(0)
nf[nf == 0] = 1
s = filtered.sum(0) / nf
mslits = []
slit = None
for (i, n, val) in zip(range(len(s)), nf, s):
  #print i, n, val
  if n > 1000:
    if slit is None:
      slit = []
    slit.append((i, val, n))
  else:
    if slit is not None and len(slit) > 50:
      mslits.append(numpy.array(slit))
    slit = None

# Surface limit for each slit
# for 75
surfs = (135, 745, 1349, 1955)
# for 75
## surfs = (135, 745, 1349, 1955)
# for 77
## surfs = (25, 611, 1204, 1795)

mask = numpy.zeros(2048)
for (surf, slit) in zip(surfs, mslits):
  s = 0
  for (i, val, n) in slit:
    print i, val, n
    if i > surf:
      mask[i] = 1.
      #print i, val, n
      s += (val * n)
  print "# ", s
  print

pix = eps.getPixbuf(scale = (-0.001, 0.001), logScale = False,
                    colorMap = edf.jetScale, mask = (mask < 0.5).repeat(2048).reshape(2048, 2048).T)
pix.save("map_epsilon_rotated_surf.png", "png")

## print 'plot [] [-0.015:0.015] "./epsilon" w l, "" u 0:((($0 > 67 && $0 < 139) || ($0 > 680 && $0 < 742) || ($0 > 1278 && $0 < 1348) || ($0 > 1875 && $0 < 1954))?$1:1/0) w l'
