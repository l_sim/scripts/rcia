#!/usr/bin/env python3

import edf, sys, numpy
from scipy.optimize import leastsq
from scipy import ndimage
from optparse import OptionParser, OptionGroup

def getDevLin(u, data, x, y):
  (a, b, c) = u
  return data - (a * x + b * y + c)

def getDevQuad(u, data, x, y):
  (a, b, c, q1, q2, q3) = u
  return data - (a * x + b * y + c + q1 * x * x + q2 * y * y + q3 * x * y)
  
def getDelta(p, data, mask, x, y, fitX, fitY):
  if not fitX:
    p[1] = 0.
  if not fitY:
    p[0] = 0.
  if len(p) == 6:
    err = getDevQuad(p, data, x, y)
  else:
    err = getDevLin(p, data, x, y)
  err[~mask] = 0.
  print(p, (err**2).sum())
  return (err**2).reshape(data.size)

class DispersiveCorrection:
  def __init__(self, filename, kind = "raw", outputMask = False, lbound = None, ubound = None, leftbound = None, rightbound = None):
    # Read peakpos file.
    data = edf.RawImage()
    data.read(filename)
    self.prefix = data.prefix
    self.data = data.getData(flat = False, dtype = numpy.float64)

    suffix = "_peakpos_map_%s.edf" % kind
    if not(filename.endswith(suffix)):
      suffix = "_peakpos_map.edf"
    if not(filename.endswith(suffix)):
      suffix = ".edf"
    self.radical = filename[:-len(suffix)]
    if self.radical != "":
      self.radical +=  "_%s" % kind
    else:
      self.radical = filename[:-4]
    if leftbound is not None or rightbound is not None:
      l = leftbound if leftbound is not None else 0
      r = rightbound if rightbound is not None else data.width
      self.radical += "_X%04dx%04d" % (l, r)
    if lbound is not None or ubound is not None:
      l = lbound if lbound is not None else 0
      u = ubound if ubound is not None else data.height
      self.radical += "_Y%04dx%04d" % (l, u)

    # Try to find peakpos hot pixels by calculating a divergence of data.
    div = numpy.zeros(self.data.shape)
    div[:-1,:] += abs(self.data[1:,:] - self.data[:-1,:])
    div[1:,:] += abs(self.data[:-1,:] - self.data[1:,:])
    div[:,:-1] += abs(self.data[:,1:] - self.data[:,:-1])
    div[:,1:] += abs(self.data[:,:-1] - self.data[:,1:])
    div /= 4.
    self.mask = (div < 0.0015)
    self.mask = ndimage.binary_erosion(self.mask, structure=numpy.ones((2,2)))
    #mask &= (data < -19.6)
    #mask &= (data > -19.6)
    if lbound is not None:
      self.mask[:lbound,:] = False
    if ubound is not None:
      self.mask[ubound:,:] = False
    if leftbound is not None:
      self.mask[:,:leftbound] = False
    if rightbound is not None:
      self.mask[:,rightbound:] = False
    if outputMask:
      out = edf.RawImage()
      out.set(self.mask.astype(numpy.float32))
      out.prefix = self.prefix + "_mask"
      out.save(self.radical + "_MaskDiv.edf")

  def correctLinear(self, fitX, fitY):
    p0 = (3.28050259e-07 if fitY else 0., 7.20711485e-08 if fitX else 0., 1.95682444e+01)
    x = numpy.arange(self.data.shape[0]).repeat(self.data.shape[1]).reshape(self.data.shape).astype(self.data.dtype)
    y = numpy.arange(self.data.shape[1]).repeat(self.data.shape[0]).reshape(self.data.T.shape).T.astype(self.data.dtype)
    (p, info) = leastsq(getDelta, p0, args = (self.data, self.mask, x, y, fitX, fitY)) #, xtol=5e-3, maxfev=25) #, q1, q2, q3
    #(a,b,c) = (p[0], p[1], -19.648826881666524)
    print("#", getDelta(p, self.data, self.mask, x, y, fitX, fitY).sum() / self.mask.sum()) #, q1, q2, q3

    self.err = getDevLin(p, self.data, x, y)
    self.average = p[2]

    out = edf.RawImage()
    out.set(self.err + self.average)
    out.dict["mean_angle"] = self.average
    out.prefix = self.prefix + "_corr"
    out.save(self.radical + "_lin.edf")

    return out

  def correctBragg(self, miscut, braggAngle, braggMono, dist, pixelSize):
    y = numpy.arange(self.data.shape[1]).repeat(self.data.shape[0]).reshape(self.data.T.shape).astype(self.data.dtype)
    b = numpy.sin((braggAngle + miscut) * numpy.pi / 180.) / numpy.sin((braggAngle - miscut) * numpy.pi / 180.)
    self.err = (1. - numpy.tan(braggAngle * numpy.pi / 180.) / numpy.tan(braggMono * numpy.pi / 180.)) * y * pixelSize / b / dist * 180. / numpy.pi

    out = edf.RawImage()
    out.set(self.data + self.err)
    #out.dict["mean_angle"] = self.average
    out.prefix = self.prefix + "_corr"
    out.save(self.radical + "_bragg.edf")

  def extract(self, hpix = None, ypix = None):
    if hpix is not None:
      f = open("hpix%04d" % hpix, "w")
      for vals in zip(data[:,hpix], mask[:,hpix], err[:,hpix]):
        f.write("%g %d %g\n" % vals)
        f.close()
    if ypix is not None:
      f = open("ypix%04d" % hpix, "w")
      for vals in zip(data[hpix,:], mask[hpix,:], err[hpix,:]):
        f.write("%g %d %g\n" % vals)
        f.close()

def parse():
  parser = OptionParser("usage: corr.py [options] file")
  parser.add_option("-l", "--lower-bound", dest="lbound", type="int", default = None,
                    help="lower bound to define Y axis window.")
  parser.add_option("-u", "--upper-bound", dest="ubound", type="int", default = None,
                    help="upper bound to define Y axis window.")
  parser.add_option("-L", "--left-bound", dest="leftbound", type="int", default = None,
                    help="lower bound to define X axis window.")
  parser.add_option("-R", "--right-bound", dest="rightbound", type="int", default = None,
                    help="upper bound to define X axis window.")
  parser.add_option("-I", "--intensity-map", dest="intensity", type="string", default = None,
                    help="intensity map to be used as a mask.")
  parser.add_option("-d", "--dump", dest="dump", action="store_true",
                    help="export the corrected peak position as a PNG")
  parser.add_option("-s", "--scale", dest="scale", default = None,
                    help="scale when exporting in the form l:U.")
  parser.add_option("-m", "--method", dest="method", default = "regression",
                    help="the correction method (regression or bragg).")
  parser.add_option("", "--miscut", dest="miscut", default = None,
                    help="the miscut angle in degrees used by the Bragg method.")
  parser.add_option("", "--bragg-angle", dest="braggAngle", default = None,
                    help="the Bragg angle in degrees used by the Bragg method.")
  parser.add_option("", "--bragg-mono", dest="braggMono", default = None,
                    help="the Bragg angle of the monochromater in degrees used by the Bragg method.")
  parser.add_option("", "--distance", dest="dist", default = None,
                    help="the distance to the source in meters used by the Bragg method.")
  parser.add_option("", "--pixel-size", dest="pixelSize", default = None,
                    help="the pixel size in micrometers used by the Bragg method.")
  parser.add_option("-x", "--with-regression-x", dest="fitX", default = False, action="store_true",
                    help="adjust along X axis, used by the regression method.")
  parser.add_option("-y", "--without-regression-y", dest="fitY", default = True, action="store_false",
                    help="disable adjustment along Y axis, used by the regression method.")
  
  return parser

if __name__ == "__main__":
  parser = parse()
  (options, args) = parser.parse_args()

  slits = []
  if options.intensity is not None:
    intensity = edf.RawImage()
    intensity.read(options.intensity)
    data = intensity.getData(flat = False).sum(1)
    # Detect slit positions or profile case
    if numpy.average((data-data.min()) / data.ptp()) < 0.2:
      slits = []
      threshold = data.min() + 0.5 * data.ptp()
      start = None
      for i, v in enumerate(data):
        if start is None and v > threshold:
          start = i
        elif start is not None and v < threshold:
          if i > start + 50:
            slits.append((start, i))
          start = None

  if len(slits) == 0:
    slits = [(options.lbound, options.ubound)]
  for i, (lbound, ubound) in enumerate(slits):
    print("# ", lbound, ubound, i + 1, "/", len(slits))
    corr = DispersiveCorrection(args[0], outputMask = False,
                                lbound = lbound,
                                ubound = ubound,
                                leftbound = options.leftbound,
                                rightbound = options.rightbound)
    if options.method == "bragg":
      out = corr.correctBragg(float(options.miscut),
                              float(options.braggAngle),
                              float(options.braggMono),
                              float(options.dist),
                              float(options.pixelSize) * 1.e-6)
    else:
      out = corr.correctLinear(options.fitX, options.fitY)

    if options.dump:
      if options.scale is None:
        std = 4 * numpy.std(corr.err * corr.mask) * 3600
        scale = numpy.log10(std)
        ubound = 10 ** int(scale)
        r = std / ubound
        if r < 2:
          ubound *= 1
        elif r < 5:
          ubound *= 2
        else:
          ubound *= 5
        lbound = -ubound
      else:
        vals = options.scale.split(":")
        lbound = float(vals[0])
        ubound = float(vals[1])
      out.exportPDF(corr.radical + "_lin.pdf",
                    scale = (lbound, ubound),
                    title = corr.prefix,
                    logScale = False,
                    offset = corr.average * 3600,
                    legend = "peak position dispersion (arcseconds)",
                    format = "%.2f",
                    factor = 3600.)
  
