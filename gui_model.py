#!/usr/bin/env python
#
# Copyright (C) 2012-2024 Damien Caliste <damien.caliste@cea.fr>,
#                         Thu Nhi Tran Thi <thu-nhi.tran-thi@esrf.fr>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA

import re, os, time
import numpy
import edf, rcv
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GObject, GdkPixbuf
#from meliae import scanner

class EDF_model(Gtk.TreeStore):
  COL_FILE      =  0
  COL_DATE      =  1
  COL_PATH      =  2
  COL_EDF_RAW   =  3
  COL_THUMB     =  4
  COL_MEAN      =  5
  COL_EDF_NORM  =  6
  COL_EDF_AVG   =  7
  COL_EDF_CORR  =  8
  COL_EDF_RESC  =  9
  COL_MEANVAL   = 10
  COL_DIR_TYPE  = 11
  COL_FILE_TYPE = 12
  COL_SPAN_RAW  = 13
  COL_SPAN_NORM = 14
  COL_SPAN_CORR = 15
  COL_RCV_RAW   = 16
  COL_RCV_NORM  = 17
  COL_RCV_DARK  = 18
  COL_RCV_FLAT  = 19
  COL_EDF_FRAME = 20
  COL_EDF_GROUP = 21

  DIR_NONE  =  0
  DIR_IMG   =  1
  DIR_FLAT  =  2
  DIR_DARK  =  3
  DIR_OTHER = 99

  KIND_RAW  = 0
  KIND_DARK = 1
  KIND_FLAT = 2
  KIND_NORM = 3
  KIND_OTHER = 99
  KIND_DICT_LBL = {"" : KIND_RAW,
                   "_raw" : KIND_RAW,
                   "_norm" : KIND_NORM,
                   "_dark_corr" : KIND_DARK,
                   "_flat_corr" : KIND_FLAT}
  KIND_DICT_COL_EDF = {KIND_RAW : COL_EDF_RAW,
                       KIND_NORM : COL_EDF_NORM,
                       KIND_DARK : COL_EDF_CORR,
                       KIND_FLAT : COL_EDF_RESC,
                       KIND_OTHER : COL_EDF_RAW}
  KIND_DICT_COL_RCV = {KIND_RAW : COL_RCV_RAW,
                       KIND_NORM : COL_RCV_NORM,
                       KIND_DARK : COL_RCV_DARK,
                       KIND_FLAT : COL_RCV_FLAT,
                       KIND_OTHER : COL_RCV_RAW}
  KIND_DICT_NAME = {KIND_RAW : "raw",
                    KIND_NORM : "norm",
                    KIND_DARK : "dark_corr",
                    KIND_FLAT : "flat_corr",
                    KIND_OTHER : ""}
  COUNTER_NAMES = {"srcur": "storage ring current"}
  
  def __init__(self, normKey = "srcur", detector = "pcogold"):
    super(EDF_model, self).__init__(GObject.TYPE_STRING,
                                    GObject.TYPE_STRING,
                                    GObject.TYPE_STRING,
                                    GObject.TYPE_PYOBJECT,
                                    GdkPixbuf.Pixbuf,
                                    GObject.TYPE_BOOLEAN,
                                    GObject.TYPE_PYOBJECT,
                                    GObject.TYPE_PYOBJECT,
                                    GObject.TYPE_PYOBJECT,
                                    GObject.TYPE_PYOBJECT,
                                    GObject.TYPE_PYOBJECT,
                                    GObject.TYPE_UINT,
                                    GObject.TYPE_UINT,
                                    GObject.TYPE_PYOBJECT,
                                    GObject.TYPE_PYOBJECT,
                                    GObject.TYPE_PYOBJECT,
                                    GObject.TYPE_PYOBJECT,
                                    GObject.TYPE_PYOBJECT,
                                    GObject.TYPE_PYOBJECT,
                                    GObject.TYPE_PYOBJECT,
                                    GObject.TYPE_UINT,
                                    GObject.TYPE_UINT)
    self.filePattern = re.compile(r"(?P<prefix>[()a-zA-Z_0-9]+(([0-9]+)|(_map_[a-zA-Z]+)))(?P<lbl>((_raw)|(_norm)|(_dark_corr)|(_flat_corr))?)\.((edf)|(EDF)|(rcv)|(tif)|(tiff)|(TIF))$")
    self.mapPattern = re.compile(r"(?P<prefix>[a-zA-Z_0-9]+)_(?P<group>[0-9]+)_[A-Za-z]+_map((_raw)|(_norm)|(_dark_corr)|(_flat_corr))?\.edf")
    self.corrPattern = re.compile(r"(?P<prefix>[a-zA-Z_0-9]+)_(?P<group>[0-9]+)_((raw)|(norm)|(dark_corr)|(flat_corr))?(_[A-Za-z0-9]+)?_lin\.edf")
    # Adding a filter to our model.
    self.filter = self.filter_new()
    self.filter.set_visible_func(self.hideFile)
    self.setFilterPattern(".*\.((edf)|(EDF)|(h5)|(tif)|(tiff)|(TIF))$")
    # Creating a treeview to render our model.
    self.view = None
    self.renderMean = None
    # Creating a combobox on dark directories from our model.
    self.cbDark = None
    self.cbFlat = None
    # Normalising key
    self.normKey = normKey
    self.detector = detector

  #####################
  ## The filter part ##
  #####################
  def setFilterPattern(self, patStr):
    if (patStr[-1] != "$"):
      patStr += "$"
    pat = re.compile(patStr)
    if pat is not None:
      self.filterPattern = pat
      self.filter.refilter()

  def hideFile(self, model, iter, closure):
    (date, ) = model.get(iter, EDF_model.COL_DATE)
    if date is None:
      return True
    (file, ) = model.get(iter, EDF_model.COL_FILE)
    if (file is None):
      return False
    return (re.match(self.filterPattern, file) is not None)

  #########################
  ## The filesystem part ##
  #########################
  def kindDir(self, dirname):
    if dirname.lower().find("dark") >= 0:
      return self.DIR_DARK
    elif dirname.lower().find("flat") >= 0:
      return self.DIR_FLAT
    else:
      return self.DIR_IMG

  def kindFile(self, filename):
    parsed = self.filePattern.match(filename)
    if parsed is not None:
      return (EDF_model.KIND_DICT_LBL[parsed.group("lbl")], parsed.group("prefix"))
    else:
      return (EDF_model.KIND_OTHER, filename)

  def __addDataFile(self, parent, filename, entry, kind, timestamp):
    try:
      data = edf.RawImage().read(filename, detector = self.detector)
    except ValueError:
      print("File '%s' is not an experimental data file" % filename)
      return
    for iGroup in data.groups:
      if iGroup > 0:
        try:
          data = edf.RawImage().read(filename, grp = iGroup, detector = self.detector)
        except (KeyError, ValueError):
          continue
      iter = self.append(parent)
      self.set(iter,
               EDF_model.COL_FILE, entry,
               EDF_model.COL_PATH, filename,
               EDF_model.COL_EDF_GROUP, iGroup,
               EDF_model.COL_DATE, timestamp,
               EDF_model.COL_FILE_TYPE, kind)
      if len(data.groups) > 1 or data.nFrames > 1:
        self.set(iter, EDF_model.COL_DIR_TYPE, EDF_model.DIR_IMG)
        lst = rcv.RockingCurveSet()
        self.set(iter, EDF_model.KIND_DICT_COL_RCV[kind], lst)
        rc = rcv.RockingCurve()
        rc.read(filename, grp = iGroup, detector = self.detector)
        lst.add(rc)
        
        for i in range(data.nFrames):
          iter2 = self.append(iter)
          self.set(iter2,
                   EDF_model.COL_FILE, entry,
                   EDF_model.COL_PATH, filename,
                   EDF_model.COL_EDF_GROUP, iGroup,
                   EDF_model.COL_EDF_FRAME, i + 1,
                   EDF_model.COL_FILE_TYPE, kind)
  
  def parseDir(self, dirname, parent = None, infoFunc = None):
    entries = os.listdir(dirname)
    natsort = lambda s: [int(t) if t.isdigit() else t.lower() for t in re.split('(\d+)', s)]
    entries.sort(key = natsort)
    for entry in entries:
      if infoFunc is not None:
        infoFunc("add file %s from %s" % (entry, dirname))
      filename = os.path.join(dirname, entry)
      if (os.path.isfile(filename)):
        # File case.
        tm = time.localtime(os.stat(filename)[8])
        tm = "%d-%02d-%02d\n  %02d:%02d" % tm[:5]
        (kind, prefix) = self.kindFile(entry)
        if not(kind == EDF_model.KIND_RAW) and len(prefix) > 0:
          entry = prefix
        if filename.endswith(".rcv"):
          iter = self.append(parent)
          self.set(iter,
                   EDF_model.COL_FILE, entry,
                   EDF_model.COL_PATH, filename,
                   EDF_model.COL_DATE, tm,
                   EDF_model.COL_FILE_TYPE, kind)
        else:
          self.__addDataFile(parent, filename, entry, kind, tm)
      elif os.path.isdir(filename) and not(entry.startswith(".")) and not(entry.startswith("scan")):
        # Directory case.
        iter = self.append(parent)
        self.set(iter,
                 EDF_model.COL_FILE, entry,
                 EDF_model.COL_PATH, filename,
                 EDF_model.COL_DIR_TYPE, self.kindDir(entry))
        kind = self.parseDir(filename, parent = iter)
        if parent is not None and not(kind == self.DIR_IMG):
          self.set(parent, EDF_model.COL_DIR_TYPE, self.DIR_OTHER)
    return self.kindDir(dirname)

  def setDirectory(self, dirname, lazy = False, infoFunc = None):
    # Repopulate the listview.
    self.clear()
    self.set_sort_column_id(-2, Gtk.SortType.DESCENDING)
    self.parseDir(dirname)
    # Load RCV if any.
    self.foreach(self.loadRCV)
    reorder = []
    self.filter.foreach(self.loadEDF, (reorder, lazy, infoFunc))
    if infoFunc is not None:
      infoFunc("Reordering")
    for it in reorder:
      self.move_after(it, None)
    # Load and gather additionnal EDF files.
    if infoFunc is not None:
      infoFunc("Gathering")
    self.foreach(self.gather, infoFunc)
    #self.set_sort_column_id(EDF_model.COL_DIR_TYPE, gtk.SORT_DESCENDING)
    return False

  def gather(self, model, path, iter, infoFunc):
    (filename, entry, kind) = model.get(iter, EDF_model.COL_PATH,
                                        EDF_model.COL_FILE, EDF_model.COL_FILE_TYPE)
    parent = model.iter_parent(iter)
    if infoFunc is not None:
      infoFunc("Gathering %s (%d)" % (entry, kind))
    if filename.endswith("rcv"):
      if parent is None:
        parent = self.filter.convert_iter_to_child_iter(self.filter.iter_children(None))
      (lst, ) = model.get(parent, EDF_model.KIND_DICT_COL_RCV[kind])
      if lst is None:
        lst = rcv.RockingCurveSet()
        model.set(parent, EDF_model.KIND_DICT_COL_RCV[kind], lst)
      (rc, ) = model.get(iter, EDF_model.KIND_DICT_COL_RCV[kind])
      lst.add(rc)
    elif filename.endswith(".edf") and "_map" in filename:
      parsed = self.mapPattern.match(entry)
      if parsed is not None and os.path.isfile(parsed.group("prefix") + ".h5"):
        lst = rcv.RockingCurveSet()
        model.set(iter, EDF_model.KIND_DICT_COL_RCV[kind], lst)
        rc = rcv.RockingCurve()
        rc.read(parsed.group("prefix") + ".h5", int(parsed.group("group")), detector = self.detector)
        lst.add(rc)
    elif filename.endswith("_lin.edf"):
      parsed = self.corrPattern.match(entry)
      if parsed is not None and os.path.isfile(parsed.group("prefix") + ".h5"):
        lst = rcv.RockingCurveSet()
        model.set(iter, EDF_model.KIND_DICT_COL_RCV[kind], lst)
        rc = rcv.RockingCurve()
        rc.read(parsed.group("prefix") + ".h5", int(parsed.group("group")), detector = self.detector)
        lst.add(rc)
    elif ((filename.endswith("edf") or filename.endswith("EDF")) and
          (kind == EDF_model.KIND_NORM or
           kind == EDF_model.KIND_DARK or
           kind == EDF_model.KIND_FLAT)):
      data = edf.RawImage()
      data.read(filename)
      if parent is not None:
        parent = self.filter.convert_child_iter_to_iter(parent)
      it = self.getIterFromName(entry + ".edf", parent)
      if it is not None:
        it = self.filter.convert_iter_to_child_iter(it)
      else:
        it = iter
        self.set(it, EDF_model.COL_FILE, os.path.basename(filename))
      if kind == EDF_model.KIND_NORM:
        self.set(it, EDF_model.COL_EDF_NORM, data)
      elif kind == EDF_model.KIND_DARK:
        self.set(it, EDF_model.COL_EDF_CORR, data)
      elif kind == EDF_model.KIND_FLAT:
        self.set(it, EDF_model.COL_EDF_RESC, data)

  def loadRCV(self, model, path, iter):
    (kind, iDir, iGrp, filename) = model.get(iter, EDF_model.COL_FILE_TYPE, EDF_model.COL_DIR_TYPE, EDF_model.COL_EDF_GROUP, EDF_model.COL_PATH)
    if filename.endswith("rcv"):
      rc = rcv.RockingCurve()
      rc.read(filename, store = False)
      model.set(iter, EDF_model.KIND_DICT_COL_RCV[kind], rc)
    elif filename.endswith(".h5") and iDir == EDF_model.DIR_IMG:
      kind = EDF_model.KIND_RAW
      rc = rcv.RockingCurve()
      rc.read(filename, grp = iGrp, store = False, detector = self.detector)
      lst = rcv.RockingCurveSet()
      lst.add(rc)
      model.set(iter, EDF_model.KIND_DICT_COL_RCV[kind], lst)

  def loadEDF(self, model, path, iter, options):
    (reorder, lazy, infoFunc) = options
    (filename, entry, iGroup, iFrame, iDir) = model.get(iter, EDF_model.COL_PATH, EDF_model.COL_FILE, EDF_model.COL_EDF_GROUP, EDF_model.COL_EDF_FRAME, EDF_model.COL_DIR_TYPE)
    if not(os.path.isfile(filename)):
      return False
    if infoFunc is not None:
      infoFunc("parsing %s" % entry)
    data = edf.RawImage()
    data.read(filename, nr = iFrame - 1, grp = iGroup, detector = self.detector)
    it = model.convert_iter_to_child_iter(iter)
    thumb = None
    if not(lazy):
      thumb = data.getPixbuf().scale_simple(48, 48, GdkPixbuf.InterpType.BILINEAR)
    if len(data.average) > 0: # and "srcur" in data.counter:
      reorder.append(it)
      self.set(it, EDF_model.COL_EDF_NORM, data,
               EDF_model.COL_EDF_AVG, data,
               EDF_model.COL_THUMB, thumb)
      minMax = data.getMinMax()
      mean = (len(data.average), float(data.counter.get(self.normKey, "0.")))
      parent = self.iter_parent(it)
      if parent is not None:
        self.set(parent, EDF_model.COL_EDF_AVG, data,
                 EDF_model.COL_EDF_NORM, data,
                 EDF_model.COL_THUMB, thumb,
                 EDF_model.COL_SPAN_NORM, minMax,
                 EDF_model.COL_MEANVAL, mean)
    elif iDir == EDF_model.DIR_NONE:
      self.set(it, EDF_model.COL_EDF_RAW, data,
               EDF_model.COL_THUMB, thumb)
    return False

  ########################
  ## Some get routines. ##
  ########################
  def getIterFromName(self, filename, iterFilter):
    iterFilter = self.filter.iter_children(iterFilter)
    while (iterFilter):
      (entry, ) = self.filter.get(iterFilter, EDF_model.COL_FILE)
      if entry == filename:
        return iterFilter
      iterFilter = self.filter.iter_next(iterFilter)
    return None

  def getMean(self, iterFilter):
    parent = self.getIterParent(iterFilter)
    if parent is None:
      parent = self.filter.iter_children(None)
    vmean = 0.
    nmean = 0
    (mean, kind) = self.filter.get(parent, EDF_model.COL_MEANVAL,
                                   EDF_model.COL_DIR_TYPE)
    if mean is not None:
      (nmean, vmean) = mean
    avgRequired = (kind == EDF_model.DIR_DARK or
                   kind == EDF_model.DIR_FLAT)
    return (nmean, vmean, avgRequired)

  def getNorm(self, iterFilter):
    norm = None
    parent = self.filter.iter_parent(iterFilter)
    if parent is None:
      iterFilter = self.filter.iter_children(iterFilter)
    if iterFilter is not None:
      (norm, ) = self.filter.get(iterFilter, EDF_model.COL_EDF_NORM)
    return norm

  def getAvg(self, iterFilter):
    avg = None
    parent = self.filter.iter_parent(iterFilter)
    if parent is None:
      iterFilter = self.filter.iter_children(iterFilter)
    if iterFilter is not None:
      (avg, ) = self.filter.get(iterFilter, EDF_model.COL_EDF_AVG)
    return avg
    
  def getDark(self):
    dark = None
    it = self.cbDark.get_active_iter()
    model = self.cbDark.get_model()
    if it is not None:
      (dark, ) = model.get(it, EDF_model.COL_EDF_AVG)
    return dark

  def getFlat(self):
    flat = None
    it = self.cbFlat.get_active_iter()
    model = self.cbFlat.get_model()
    if it is not None:
      (flat, ) = model.get(it, EDF_model.COL_EDF_AVG)
    return flat

  def saveAll(self, iterFilter = None, infoFunc = None):
    if iterFilter is None:
      (model, iterFilter) = self.view.get_selection().get_selected()
    # We iterate on all children of the parent of current selection.
    parent = self.filter.iter_parent(iterFilter)
    if parent is None:
      parent = iterFilter
    (dirname, ) = self.filter.get(parent, EDF_model.COL_FILE)
    dirname += "_DarkCorr"
    if infoFunc is not None:
      infoFunc("create directory %s" % dirname)
    if not(os.path.isdir(dirname)):
      os.mkdir(dirname)
    iterFilter = self.filter.iter_children(parent)
    while (iterFilter):
      (corr, check) = self.filter.get(iterFilter, EDF_model.COL_EDF_CORR,
                                      EDF_model.COL_MEAN)
      if corr is not None and check:
        if infoFunc is not None:
          infoFunc("export run %04d" % corr.run)
        corr.path = dirname
        corr.save(os.path.join(dirname, "%s%04d_%s.edf" % (corr.prefix,
                                                           corr.run, "corr")))
      iterFilter = self.filter.iter_next(iterFilter)

  def select(self, id):
    parent = self.getIterParent()
    if self.view is not None:
      self.view.get_selection().select_iter(self.filter.iter_nth_child(parent, id))

  ########################
  ## The operation part ##
  ########################
  def calcMean(self, key, iterFilter = None):
    vmean = 0.
    nmean = 0
    nfile = 0
    iterFilter = self.getIterFirst()
    while (iterFilter):
      (data, check) = self.filter.get(iterFilter,
                                      EDF_model.COL_EDF_RAW,
                                      EDF_model.COL_MEAN)
      if check and data is not None and key in data.counter:
        nmean += 1
        vmean += float(data.counter[key])
      if check:
        nfile += 1
      iterFilter = self.filter.iter_next(iterFilter)
    if nmean > 0:
      vmean /= float(nmean)
    else:
      vmean = 0.
    mean = (nfile, vmean)
    parent = self.getIterParent(iterFilter)
    if parent is None:
      parent = self.filter.iter_children(None)
      #self.set(self.filter.convert_iter_to_child_iter(parent),
      #         EDF_model.COL_DIR_TYPE, EDF_model.DIR_DARK)
    self.set(self.filter.convert_iter_to_child_iter(parent),
             EDF_model.COL_MEANVAL, mean)
    return mean

  def applyNormalisation(self, iterFilter = None, infoFunc = None):
    parent = self.getIterParent(iterFilter)
    if parent is None:
      parent = self.filter.iter_children(None)
      iterFilter = parent.copy()
    else:
      iterFilter = self.filter.iter_children(parent)
    ((nmean, vmean), ) = self.filter.get(parent, EDF_model.COL_MEANVAL)
    minMax = [65536, 0]
    while (iterFilter):
      (data, check) = self.filter.get(iterFilter,
                                      EDF_model.COL_EDF_CORR,
                                      EDF_model.COL_MEAN)
      if data is None:
        (data, check) = self.filter.get(iterFilter,
                                        EDF_model.COL_EDF_RAW,
                                        EDF_model.COL_MEAN)
      it = self.filter.convert_iter_to_child_iter(iterFilter)
      if check:
        # Calculate the normalisation
        if not(vmean == 0.) and self.normKey in data.counter and \
             not(data.counter[self.normKey] == 0.):
          self.calcNorm(vmean, it, data, infoFunc, minMax)
        else:
          self.set(it, EDF_model.COL_EDF_NORM, data)
      elif data is not None:
        # Reset the calculated EDF associated to the raw data.
        self.set(it,
                 EDF_model.COL_EDF_NORM, None,
                 EDF_model.COL_EDF_AVG,  None)
      iterFilter = self.filter.iter_next(iterFilter)
    self.set(self.filter.convert_iter_to_child_iter(parent),
             EDF_model.COL_SPAN_NORM, minMax)
        
  def calcNorm(self, mean, iter, data, infoFunc = None, minMax = None):
    if infoFunc is not None:
      infoFunc("apply normalisation on %04d" % data.run)
    norm = data.rescaleFromCounterId(self.normKey, mean)
    if minMax is not None:
      (m, M) = norm.getMinMax()
      minMax[0] = min(minMax[0], m)
      minMax[1] = max(minMax[1], M)
    self.set(iter,
             EDF_model.COL_EDF_NORM, norm,
             EDF_model.COL_THUMB,
             norm.getPixbuf().scale_simple(48, 48, GdkPixbuf.InterpType.BILINEAR))
    # We save norm on disk, and remove it from memory.
    norm.save(os.path.join(norm.path, "%s%04d_norm.edf" % (norm.prefix, norm.run)))

  def calcAverage(self, iterFilter = None, infoFunc = None):
    parent = self.getIterParent(iterFilter)
    if parent is None:
      parent = self.filter.iter_children(None)
      parentHook = None
      it = parent.copy()
    else:
      parentHook = self.filter.convert_iter_to_child_iter(parent)
      it = self.filter.iter_children(parent)
    ((nmean, vmean), kind) = self.filter.get(parent, EDF_model.COL_MEANVAL,
                                             EDF_model.COL_DIR_TYPE)
    avgRequired = (kind == EDF_model.DIR_DARK or
                   kind == EDF_model.DIR_FLAT)
    if not(avgRequired):
      return
    # We do the calculation in a new RawImage.
    avg = edf.RawImage()
    iterFilter = it.copy()
    while (iterFilter):
      (norm, check) = self.filter.get(iterFilter,
                                      EDF_model.COL_EDF_RAW,
                                      EDF_model.COL_MEAN)
      if check:
        if infoFunc is not None:
          infoFunc("add run %04d for average" % norm.run)
        avg.add(norm)
      iterFilter = self.filter.iter_next(iterFilter)
    avg.multiply(1. / float(nmean))
    avg.store(kind = "avrg")
    if not(vmean == 0.):
      avg.counter[self.normKey] = str(vmean)
    # We set this average data on every entries.
    iterFilter = it.copy()
    while (iterFilter):
      (check, ) = self.filter.get(iterFilter, EDF_model.COL_MEAN)
      if check:
        self.set(self.filter.convert_iter_to_child_iter(iterFilter),
                 EDF_model.COL_EDF_AVG, avg)
      iterFilter = self.filter.iter_next(iterFilter)
    # We add this average as first child.
    tm = time.localtime()
    pixbuf = avg.getPixbuf().scale_simple(48, 48, GdkPixbuf.InterpType.BILINEAR)
    self.set(self.filter.convert_iter_to_child_iter(parent),
             EDF_model.COL_EDF_RAW, avg,
             EDF_model.COL_EDF_AVG, avg,
             EDF_model.COL_THUMB, pixbuf)
    self.set(self.prepend(parentHook),
             EDF_model.COL_FILE, "%savrg.edf" % avg.prefix,
             EDF_model.COL_PATH, os.curdir,
             EDF_model.COL_DATE, "%d-%02d-%02d\n  %02d:%02d" % tm[:5],
             EDF_model.COL_EDF_AVG, avg,
             EDF_model.COL_EDF_RAW, avg,
             EDF_model.COL_THUMB, pixbuf)
    # We save avg on disk.
    avg.save(os.path.join(avg.path, "%savrg.edf" % avg.prefix))

  def applyCorrection(self, iterFilter = None, infoFunc = None, shift = None):
    parent = self.getIterParent(iterFilter)
    if parent is None:
      parent = self.filter.iter_children(None)
      iterFilter = parent.copy()
    else:
      iterFilter = self.filter.iter_children(parent)
    #((nmean, vmean), ) = self.filter.get(parent, EDF_model.COL_MEANVAL)
    # We get dark average.
    dark = self.getDark()
    if dark is None:
      raise ValueError("No dark average.")
    dark.setStored(True)
    # We rescale dark to fit srcur of the data.
    # dark = dark.rescaleFromCounterId("srcur", vmean)
    # We get flat average.
    flat = self.getFlat()
    # if flat is None:
    #   raise ValueError("No flat average.")
    # We rescale flat to fit srcur of the data.
    # flat = flat.rescaleFromCounterId("srcur", vmean)
    # We create the (flat_avg - dark_avg)
    if flat is not None:
      flatminusdark = edf.RawImage()
      flatminusdark.add(dark)
      flatminusdark.multiply(-1.)
      flatminusdark.add(flat)
      flatminusdark.store(kind = "flatminusdark")
    else:
      flatminusdark = None
    minMax = [65536., 0.]
    # We iterate on all children of the parent of current selection.
    while (iterFilter):
      (check, raw) = self.filter.get(iterFilter,
                                     EDF_model.COL_MEAN,
                                     EDF_model.COL_EDF_RAW)
      it = self.filter.convert_iter_to_child_iter(iterFilter)
      if check:
        # Calculate the correction
        corr = self.calcCorr(raw, dark, infoFunc, minMax, shift)
        resc = self.calcResc(corr, flatminusdark, infoFunc)
        self.set(it,
                 EDF_model.COL_EDF_CORR, corr,
                 EDF_model.COL_EDF_RESC, resc,
                 EDF_model.COL_THUMB,
                 corr.getPixbuf(logScale = True).scale_simple(48, 48, GdkPixbuf.InterpType.BILINEAR))
      else:
        # Reset the calculated EDF associated to the raw data.
        self.set(it, EDF_model.COL_EDF_CORR, None,
                 EDF_model.COL_EDF_RESC, None)
      iterFilter = self.filter.iter_next(iterFilter)
    self.set(self.filter.convert_iter_to_child_iter(parent),
             EDF_model.COL_EDF_CORR, flatminusdark,
             EDF_model.COL_SPAN_CORR, minMax)
    dark.setStored(False)

  def calcCorr(self, raw, dark, infoFunc = None, minMax = None, shift = None):
    if infoFunc is not None:
      infoFunc("apply dark correction on %04d" % raw.run)
    corr = edf.RawImage()
    corr.add(dark)
    corr.multiply(-1.)
    corr.add(raw)
    if minMax is not None:
      (m, M) = corr.getMinMax()
      minMax[0] = min(minMax[0], m)
      minMax[1] = max(minMax[1], M)
    if shift is not None:
      corr.add(shift)
    # Copy some info from raw and reset some others.
    corr.endian = "<"
    corr.dtype = "u2"
    corr.run = raw.run
    corr.average = []
    corr.counter = raw.counter.copy()
    corr.motor   = raw.motor.copy()
    corr.prefix  = raw.prefix
    corr.path    = raw.path
    # Flatten it to export.
    corr.store()
    # We save corr on disk, and remove it from memory.
    corr.save(os.path.join(corr.path, "%s%04d_dark_corr.edf" % (corr.prefix, corr.run)))
    return corr

  def calcResc(self, norm, flat, infoFunc = None, minMax = None, shift = None):
    if flat is None:
      return None
    if infoFunc is not None:
      infoFunc("apply flat correction on %04d" % norm.run)
    resc = edf.RawImage()
    resc.add(norm)
    resc.divide(flat)
    if minMax is not None:
      (m, M) = resc.getMinMax()
      minMax[0] = min(minMax[0], m)
      minMax[1] = max(minMax[1], M)
    if shift is not None:
      resc.add(shift)
    # Copy some info from norm and reset some others.
    resc.endian  = "<"
    resc.dtype   = "u2"
    resc.run     = norm.run
    resc.average = []
    resc.counter = norm.counter.copy()
    resc.motor   = norm.motor.copy()
    resc.prefix  = norm.prefix
    resc.path    = norm.path
    # Flatten it to export.
    resc.store()
    # We save resc on disk, and remove it from memory.
    resc.save(os.path.join(resc.path, "%s%04d_flat_corr.edf" % (resc.prefix, resc.run)))
    return resc

  def getCheckItems(self, kind, iterFilter = None):
    datas = []
    col = EDF_model.KIND_DICT_COL_EDF[kind]
    iterFilter = self.getIterFirst(iterFilter)
    while (iterFilter):
      (data, check) = self.filter.get(iterFilter, col, EDF_model.COL_MEAN)
      if data is not None and check:
        datas.append(data)
      iterFilter = self.filter.iter_next(iterFilter)
    return datas

  def setRockingCurves(self, kind, lst, iterFilter = None):
    parent = self.getIterParent(iterFilter)
    if parent is None:
      parent = self.filter.iter_children(None)
    rcvs = rcv.RockingCurveSet()
    for rc in lst:
      rcvs.add(rc)
    self.set(self.filter.convert_iter_to_child_iter(parent),
             EDF_model.KIND_DICT_COL_RCV[kind], rcvs)

  #######################
  ## The TreeView part ##
  #######################
  def getIterParent(self, iterFilter = None):
    if iterFilter is None:
      (model, iterFilter) = self.view.get_selection().get_selected()
    if iterFilter is None:
      return None
    # We iterate on all children of the parent of current selection.
    parent = self.filter.iter_parent(iterFilter)
    if parent is None and self.filter.iter_children(iterFilter) is not None:
      parent = iterFilter
    return parent

  def getIterFirst(self, iterFilter = None):
    return self.filter.iter_children(self.getIterParent(iterFilter))

  def getView(self):
    if self.view is None:
      self.view = Gtk.TreeView(self.filter)
      self.view.set_rules_hint(True)

      render = Gtk.CellRendererText()
      col = Gtk.TreeViewColumn("File", render)
      col.set_cell_data_func(render, self.displayEDFData)
      render = Gtk.CellRendererPixbuf()
      render.set_property("width", 16)
      col.pack_end(render, expand = False)
      col.set_cell_data_func(render, self.displayKind)
      col.set_expand(True)
      self.view.insert_column(col, -1)
      self.view.insert_column_with_attributes(-1, "Thumb",
                                              Gtk.CellRendererPixbuf(), \
                                              pixbuf = EDF_model.COL_THUMB)
      self.renderMean = Gtk.CellRendererToggle()
      self.view.insert_column_with_data_func(-1, "Mean", self.renderMean,
                                               self.displayMean)
      self.renderMean.connect("toggled", self.onUseForMean)
      self.view.insert_column_with_attributes(-1, "Date",
                                              Gtk.CellRendererText(), \
                                              text = EDF_model.COL_DATE)
      
      self.view.get_selection().set_mode(Gtk.SelectionMode.SINGLE)
    return (self.view, self.renderMean)

  def displayEDFData(self, col, render, model, iter, closure):
    (date, iFrame, iGroup, filename, iDir, data, norm, avg, corr) = model.get(iter,
                                                        EDF_model.COL_DATE,
                                                        EDF_model.COL_EDF_FRAME,
                                                        EDF_model.COL_EDF_GROUP,
                                                        EDF_model.COL_FILE,
                                                        EDF_model.COL_DIR_TYPE,
                                                        EDF_model.COL_EDF_RAW,
                                                        EDF_model.COL_EDF_NORM,
                                                        EDF_model.COL_EDF_AVG,
                                                        EDF_model.COL_EDF_CORR)
    lbl = ""
    if not(iDir == EDF_model.DIR_NONE):
      # Directory entry case.
      (mean, kind, mMNorm, mMCorr) = model.get(iter, EDF_model.COL_MEANVAL,
                                               EDF_model.COL_DIR_TYPE,
                                               EDF_model.COL_SPAN_NORM,
                                               EDF_model.COL_SPAN_CORR)
      if iGroup > 0:
        lbl = "<i>%s/scan%04d</i>" % (filename, iGroup)
      else:
        lbl = "<i>%s</i>" % filename
      if kind == self.DIR_DARK:
        lbl += " - dark series"
      elif kind == self.DIR_FLAT:
        lbl += " - flat series"
      if mean is not None and not(mean[1] == 0.):
        lbl += "\n<span size=\"smaller\" font-family=\"mono\">"
        lbl += "  %s = %g mA" % (EDF_model.COUNTER_NAMES.get(self.normKey, self.normKey), mean[1])
        lbl += "</span>"
      if mMNorm is not None:
        lbl += "\n<span size=\"smaller\" font-family=\"mono\">"
        lbl += "  intensity span (norm.)      = [%d; %d] a.u." % tuple(mMNorm)
        lbl += "</span>"
      if mMCorr is not None:
        lbl += "\n<span size=\"smaller\" font-family=\"mono\">"
        lbl += "  intensity span (dark corr.) = [%d; %d] a.u." % tuple(mMCorr)
        lbl += "</span>"
    elif data is not None or norm is not None or corr is not None:
      # File entry case.
      if iFrame > 0:
        filename = "frame %d" % iFrame
      if data is not None and norm is not None:
        lbl = "<b>%s</b>\n" % filename
        if self.normKey in data.counter:
          dev = " %+.3g %%" % ((float(norm.counter[self.normKey]) -
                                float(data.counter[self.normKey])) /
                               float(data.counter[self.normKey]) * 100.)
      else:
        lbl = "%s\n" %filename
        dev = ""
      if corr is not None:
        data_ = corr
      if norm is not None:
        data_ = norm
      if data is not None:
        data_ = data
      data = data_
      lbl += "<span size=\"smaller\" font-family=\"mono\">"
      lbl += "  %s = " % EDF_model.COUNTER_NAMES.get(self.normKey, self.normKey)
      if self.normKey in data.counter:
        lbl += "%g%s mA" % (float(data.counter[self.normKey]), dev)
      else:
        lbl += "<i>not available</i>"
      if len(data.average) > 0:
        lbl += "\n  average of %s" % str(data.average)
      if corr is not None:
        lbl += "\n  corrected"
      lbl += "</span>"
    render.set_property("markup", lbl)

  def displayMean(self, col, render, model, iter):
    (kind, mean) = model.get(iter, EDF_model.COL_DIR_TYPE, EDF_model.COL_MEAN)
    render.set_property("visible", (self.anyData(iter) and kind == EDF_model.DIR_NONE))
    render.set_property("active", mean)

  def displayKind(self, col, render, model, iter, closure):
    (data, date, kind) = model.get(iter, EDF_model.COL_EDF_RAW,
                                   EDF_model.COL_DATE, EDF_model.COL_DIR_TYPE)
    render.set_property("stock-id", "")
    render.set_property("cell-background", None)
    if data is not None and (not self.normKey in data.counter or
                             data.counter[self.normKey] == 0.):
      render.set_property("stock-id", Gtk.STOCK_DIALOG_WARNING)
      render.set_property("stock-size", Gtk.IconSize.MENU)
    if kind == EDF_model.DIR_IMG or \
       kind == EDF_model.DIR_DARK or \
       kind == EDF_model.DIR_FLAT:
      render.set_property("stock-id", Gtk.STOCK_DIRECTORY)
    if date is not None:
      parent = model.iter_parent(iter)
      if parent is not None:
        (kind, ) = model.get(parent, EDF_model.COL_DIR_TYPE)
    if kind == EDF_model.DIR_DARK:
      render.set_property("cell-background", "Dim Gray")
    elif kind == EDF_model.DIR_FLAT:
      render.set_property("cell-background", "Slate Blue")

  def onUseForMean(self, cell, path, force = None):
    iterFilter = self.filter.get_iter(path)
    it = self.filter.convert_iter_to_child_iter(iterFilter)
    if force is None:
      self.set(it, EDF_model.COL_MEAN, not(cell.get_active()))
    else:
      self.set(it, EDF_model.COL_MEAN, force)

    # Update the mean value.
    mean = self.calcMean(self.normKey, iterFilter)

  def anyData(self, iterFilter):
    (raw, norm, corr) = self.filter.get(iterFilter, EDF_model.COL_EDF_RAW,
                                        EDF_model.COL_EDF_NORM,
                                        EDF_model.COL_EDF_CORR)
    return raw is not None or norm is not None or corr is not None

  def selectAll(self, iterFilter = None, modulo = 1):
    iterFilter = self.getIterFirst(iterFilter)
    i = 0
    it = iterFilter.copy()
    while (it):
      if self.anyData(it):
        if i % modulo == 0:
          self.set(self.filter.convert_iter_to_child_iter(it),
                   EDF_model.COL_MEAN, True)
        i += 1
      it = self.filter.iter_next(it)
    # Update the mean value.
    mean = self.calcMean(self.normKey, iterFilter)

  def getSelectedFile(self):
    (model, iter) = self.view.get_selection().get_selected()
    if iter is None:
      return None
    return model.get(iter, EDF_model.COL_FILE)[0]

  def getSelected(self, kind):
    (model, iter) = self.view.get_selection().get_selected()
    if iter is None:
      return None
    if kind == EDF_model.KIND_RAW:
      (data, ) = model.get(iter, EDF_model.COL_EDF_RAW)
    elif kind == EDF_model.KIND_NORM:
      (data, ) = model.get(iter, EDF_model.COL_EDF_NORM)
    elif kind == EDF_model.KIND_DARK:
      (data, ) = model.get(iter, EDF_model.COL_EDF_CORR)
    elif kind == EDF_model.KIND_FLAT:
      (data, ) = model.get(iter, EDF_model.COL_EDF_RESC)
    else:
      raise AttributeError
    return data

  def getSelectedRC(self, kind, iterFilter = None):
    if iterFilter is None:
      (model, iterFilter) = self.view.get_selection().get_selected()
    if iterFilter is not None:
      (rc, ) = self.filter.get(iterFilter, EDF_model.KIND_DICT_COL_RCV[kind])
      if rc is not None:
        return rc
    parent = self.getIterParent(iterFilter)
    if parent is None:
      parent = self.filter.iter_children(None)
    (rc, ) = self.filter.get(parent, EDF_model.KIND_DICT_COL_RCV[kind])
    return rc

  #######################
  ## The ComboBox part ##
  #######################
  def displayDir(self, model, iter, ref):
    (kind, ) = model.get(iter, self.COL_DIR_TYPE)
    return kind == ref or kind == self.DIR_OTHER
  
  def getCbDark(self):
    if self.cbDark is None:
      filter = self.filter_new()
      self.cbDark = Gtk.ComboBox.new_with_model(filter)
      filter.set_visible_func(self.displayDir, self.DIR_DARK)
      render = Gtk.CellRendererText()
      self.cbDark.pack_start(render, False)
      self.cbDark.add_attribute(render, "text", EDF_model.COL_FILE)

    return self.cbDark

  def getCbFlat(self):
    if self.cbFlat is None:
      filter = self.filter_new()
      self.cbFlat = Gtk.ComboBox.new_with_model(filter)
      filter.set_visible_func(self.displayDir, self.DIR_FLAT)
      render = Gtk.CellRendererText()
      self.cbFlat.pack_start(render, False)
      self.cbFlat.add_attribute(render, "text", EDF_model.COL_FILE)

    return self.cbFlat

