#!/usr/bin/env python3

import numpy
from optparse import OptionParser, OptionGroup

latt = {}
latt["diamond"] = {"latt": "C", "a0": 3.5671}
latt["silicon"] = {"latt": "C", "a0": 5.431}
latt["germanium"] = {"latt": "C", "a0": 5.657}
latt["SiGe"] = {"latt": "C", "a0": 5.544}
latt["quartz"] = {"latt": "H", "a0": (4.9138, 5.4052)}
latt["CdTe"] = {"latt": "C", "a0": 6.48}
latt["PbWO4"] = {"latt": "H", "a0": (5.4620, 12.0486)}
latt["W"] = {"latt": "C", "a0": 3.1648}
latt["Bi4Ge3O12"] = {"latt": "C", "a0": 10.518}
latt["BaF2"] = {"latt": "C", "a0": 6.196}
latt["CsI"] = {"latt": "C", "a0": 4.503}
latt["Rh"] = {"latt": "C", "a0": 3.8032}
latt["Y"] = {"latt": "H", "a0": (3.6474, 5.7306)}
latt["KBr"] = {"latt": "C", "a0": 6.598}
latt["SiC"] = {"latt": "H", "a0": (3.073, 10.053)}
latt["GaN"] = {"latt": "H", "a0": (3.188, 5.185)}
latt["InP"] = {"latt": "C", "a0": 5.8687}
ene=1000.*numpy.array((12., 15.))

def bragg(energies, hkl, a0, latt = "C"):
  if latt == "C":
    idhkl = numpy.linalg.norm(numpy.array(hkl)) / a0
  elif latt == "H":
    h, k, l = hkl
    idhkl = numpy.sqrt((h**2+h*k+k**2) / (a0[0] ** 2) * 4. / 3. + (l ** 2) / (a0[1] ** 2))
  return numpy.arcsin(12398.425 / energies / 2. * idhkl) * 180. / numpy.pi

def doBragg(hkl):
  return bragg(ene, hkl, **a)

def toStr(hkls):
  ret = ""
  for hkl in hkls:
    ret += " angle(%d%d%d)(deg)" % hkl
  return ret

def parse():
  parser = OptionParser("usage: bragg.py [options]")
  parser.add_option("-m", "--material", dest="material", type="string",
                    help="select the material to get Bragg angles for.")
  return parser

if __name__ == "__main__":
  parser = parse()
  (options, args) = parser.parse_args()

  a = latt[options.material]
  hkls = ((4,0,0), (2,2,0), (1,1,1), (3,3,3))
  angles = numpy.vstack(list(map(doBragg, hkls)))

  print("# material: %s" % options.material)
  print("# lattice param: ", a, "A")
  print("# energy(keV)" + toStr(hkls))
  for (e, ang) in zip(ene, angles.T):
    print(e / 1000, ang)
