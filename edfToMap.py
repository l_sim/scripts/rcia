#!/usr/bin/env python3

import edf
import rcv
import os
import sys

edfs = rcv.EDFSet()

path = sys.argv[1] if len(sys.argv) > 1 else "."
for f in sorted(os.listdir(path)):
    if os.path.isfile(os.path.join(path, f)):
        if f.endswith(".edf") and not(f.endswith("map.edf")):
            print(f)
            data = edf.RawImage()
            data.read(os.path.join(path, f))
            edfs.add(data)

def info(message, fraction = 0.):
    print(message)

rcs = edfs.calcRC(infoFunc = info)

rcvs = rcv.RockingCurveSet()
for rc in rcs:
    rcvs.add(rc)

mpInt, mpFwhm, mpPeak = rcvs.calcMap(infoFunc = info)
prefix = rcs[0].prefix
mpInt.save(os.path.join(path, "%s_integral_map.edf" % prefix))
mpFwhm.save(os.path.join(path, "%s_FWHM_map.edf" % prefix))
mpPeak.save(os.path.join(path, "%s_peakpos_map.edf" % prefix))
