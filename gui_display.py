#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2012-2013 Damien Caliste <damien.caliste@cea.fr>,
#                         Thu Nhi Tran Thi <thu-nhi.tran-thi@esrf.fr>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA

import os
import numpy
import matplotlib
from matplotlib.backends.backend_gtk3agg import FigureCanvasGTK3Agg as FigureCanvas
from matplotlib.backends import backend_cairo
from gi.repository import Gtk, GdkPixbuf, GObject, Gdk, Pango, GLib
import edf, rcv

class EDF_display(Gtk.VBox):
  KIND_RAW  = 0
  KIND_DARK = 1
  KIND_FLAT = 2
  KIND_NORM = 3

  KIND_DICT_NAME = {KIND_RAW : "raw",
                    KIND_NORM : "norm",
                    KIND_DARK : "dark_corr",
                    KIND_FLAT : "flat_corr"}

  CMAP_LIST_NAME = [("gray", edf.grayScale),
                    ("xray", edf.xrayScale),
                    ("viridis", edf.viridisScale),
                    ("jet",  edf.jetScale),
                    ("stress", edf.stressScale)]

  ACTION_NONE               = 0
  ACTION_ROCKING_CURVES     = 1
  ACTION_INTENSITY_PROFILES = 2

  __gsignals__ = {'rectangle-selection' : (GObject.SIGNAL_RUN_LAST,
                                           GObject.TYPE_NONE,
                                           (GObject.TYPE_PYOBJECT, GObject.TYPE_PYOBJECT)),
                  }

  def __init__(self):
    super(EDF_display, self).__init__()

    self.model = None
    self.datas = {}
    self.rcvs = {}
    self.canvasRC  = None
    self.canvasIP  = None
    self.colorMap = edf.jetScale
    self.rawPix = None
    self.action = EDF_display.ACTION_NONE
    self.mouseSelectionDone = False
    self.highlight = False

    self.offset = 0.

    self.rcWithGuess = True
    self.fitFunc = rcv.GaussianFit()
    
    self.xRC = None
    self.yRC = None

    # Make a model to store color maps.
    self.cmodel = Gtk.ListStore(GdkPixbuf.Pixbuf)
    w = 45
    h = 10
    for (name, scale) in EDF_display.CMAP_LIST_NAME:
      pix = GdkPixbuf.Pixbuf.new_from_bytes(GLib.Bytes.new(scale(numpy.tile(numpy.arange(0., 255., 255. / float(w)), h))),
                                         GdkPixbuf.Colorspace.RGB,
                                         False, 8, w, h, w * 3)
      self.cmodel.set(self.cmodel.append(), 0, pix)

    hbox = Gtk.HBox()
    self.pack_start(hbox, True, True, 0)

    scroll = Gtk.ScrolledWindow()
    scroll.set_policy(Gtk.PolicyType.ALWAYS, Gtk.PolicyType.ALWAYS)
    scroll.set_shadow_type(Gtk.ShadowType.NONE)
    scroll.set_size_request(400, 400)
    hbox.pack_start(scroll, True, True, 0)

    vbox = Gtk.VBox()
    hbox.pack_start(vbox, False, False, 0)

    # Colourisation options.
    self.cbCmap = Gtk.ComboBox()
    self.cbCmap.set_model(self.cmodel)
    render = Gtk.CellRendererPixbuf()
    self.cbCmap.pack_start(render, False)
    self.cbCmap.add_attribute(render, "pixbuf", 0)
    self.cbCmap.set_active(3)
    self.cbCmap.connect("changed", self.onCMap)
    vbox.pack_start(self.cbCmap, False, False, 0)
    self.checkScale = Gtk.CheckButton("log. scale")
    self.checkScale.set_active(True)
    self.checkScale.set_tooltip_text("Use logarithmic coloured scale.")
    self.checkScale.connect("toggled", self.onCheckScale)
    vbox.pack_start(self.checkScale, False, False, 0)
    self.checkAuto = Gtk.CheckButton("adjust scale")
    self.checkAuto.set_active(True)
    self.checkAuto.set_tooltip_text("Adjust scale on every file load to best fit image range.")
    vbox.pack_start(self.checkAuto, False, False, 0)
    # Colour scale.
    vbox.pack_start(Gtk.Label("Scaling factor:"), False, False, 0)
    align = Gtk.Alignment.new(1., 0.5, 0., 0.)
    vbox.pack_start(align, False, False, 0)
    self.factor = Gtk.Entry()
    self.factor.set_text("1.")
    self.factor.set_width_chars(6)
    self.factor.connect("activate", self.onFactor)
    align.add(self.factor)
    self.scaleMax = Gtk.Entry()
    self.scaleMax.set_width_chars(4)
    self.scaleMax.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, "go-top")
    self.scaleMax.set_icon_tooltip_text(Gtk.EntryIconPosition.SECONDARY,
                                        "Auto scale the maximum value")
    self.scaleMax.connect("icon-release", self.onScaleAuto)
    self.scaleMax.connect("activate", self.onScaleManual)
    self.scaleMax.set_tooltip_text("Enter a numerical value or 'auto'.")
    vbox.pack_start(self.scaleMax, False, False, 0)
    hbox2 = Gtk.HBox()
    vbox.pack_start(hbox2, True, True, 0)
    wd = Gtk.Frame()
    wd.set_shadow_type(Gtk.ShadowType.ETCHED_IN)
    hbox2.pack_start(wd, False, False, 2)
    self.imgScale = Gtk.Image()
    self.imgScale.set_size_request(15, -1)
    self.imgScale.connect("size-allocate", self.onScaleSizeChanged)
    wd.add(self.imgScale)
    vbox2 = Gtk.VBox()
    hbox2.pack_end(vbox2, True, True, 0)
    self.lblScale = []
    wd = Gtk.Label("")
    wd.set_alignment(0., 0.5)
    self.lblScale.append(wd)
    vbox2.pack_start(wd, False, False, 0)
    wd = Gtk.Label("")
    wd.set_alignment(0., 0.5)
    self.lblScale.append(wd)
    vbox2.pack_start(wd, True, True, 0)
    wd = Gtk.Label("")
    wd.set_alignment(0., 0.5)
    self.lblScale.append(wd)
    vbox2.pack_start(wd, False, False, 0)
    wd = Gtk.Label("")
    wd.set_alignment(0., 0.5)
    self.lblScale.append(wd)
    vbox2.pack_start(wd, True, True, 0)
    wd = Gtk.Label("")
    wd.set_alignment(0., 0.5)
    self.lblScale.append(wd)
    vbox2.pack_start(wd, False, False, 0)
    self.lblOffset = Gtk.Label("")
    self.lblOffset.set_no_show_all(True)
    vbox.pack_start(self.lblOffset, False, False, 0)
    self.scaleMin = Gtk.Entry()
    self.scaleMin.set_width_chars(4)
    self.scaleMin.set_icon_from_stock(Gtk.EntryIconPosition.SECONDARY, Gtk.STOCK_GOTO_BOTTOM)
    self.scaleMin.set_icon_tooltip_text(Gtk.EntryIconPosition.SECONDARY,
                                        "Auto scale the minimum value")
    self.scaleMin.connect("icon-release", self.onScaleAuto)
    self.scaleMin.connect("activate", self.onScaleManual)
    self.scaleMin.set_tooltip_text("Enter a numerical value or 'auto'.")
    vbox.pack_start(self.scaleMin, False, False, 0)
    # Image info.
    self.lblIntensity = Gtk.Label("<span size=\"small\" font-family=\"monospace\">I = not def.</span>")
    self.lblIntensity.set_use_markup(True)
    self.lblIntensity.set_size_request(60, -1)
    vbox.pack_end(self.lblIntensity, False, False, 0)
    self.lblPoxY = Gtk.Label("<span size=\"small\" font-family=\"monospace\">y = not def.</span>")
    self.lblPoxY.set_use_markup(True)
    vbox.pack_end(self.lblPoxY, False, False, 0)
    self.lblPoxX = Gtk.Label("<span size=\"small\" font-family=\"monospace\">x = not def.</span>")
    self.lblPoxX.set_use_markup(True)
    self.lblPoxX.set_property("width-chars", 10)
    vbox.pack_end(self.lblPoxX, False, False, 0)
    vbox.pack_end(Gtk.Label("Image info:"), False, False, 0)

    self.img = Gtk.Image()
    self.event = Gtk.EventBox()
    self.event.set_can_focus(True)
    self.event.add(self.img)
    self.event.add_events(Gdk.EventMask.POINTER_MOTION_MASK |
                            Gdk.EventMask.BUTTON_PRESS_MASK |
                            Gdk.EventMask.KEY_PRESS_MASK |
                            Gdk.EventMask.BUTTON1_MOTION_MASK |
                            Gdk.EventMask.BUTTON_RELEASE_MASK)
    self.event.connect("motion-notify-event", self.onCoord)
    self.event.connect("button-press-event", self.onMenu)
    self.event.connect("button-release-event", self.onMenu)
    self.event.connect("key-press-event", self.onKey)
    scroll.add_with_viewport(self.event)
    self.viewport = scroll.get_child()
    self.viewport.get_hadjustment().page_size = 400
    self.viewport.get_vadjustment().page_size = 400

    self.message = Gtk.InfoBar()
    self.message.set_no_show_all(True)
    self.message.add_button(Gtk.STOCK_OK, Gtk.ResponseType.OK)
    self.message.set_message_type(Gtk.MessageType.WARNING)
    self.message.connect("response", self.onMessage)
    self.messageLbl = Gtk.Label()
    self.messageLbl.show()
    self.message.get_content_area().add(self.messageLbl)
    self.pack_start(self.message, False, False, 0)

    self.select = Gtk.InfoBar()
    self.select.set_no_show_all(True)
    self.select.add_button(Gtk.STOCK_APPLY, Gtk.ResponseType.APPLY)
    self.select.add_button(Gtk.STOCK_REMOVE, Gtk.ResponseType.CANCEL)
    self.select.set_message_type(Gtk.MessageType.OTHER)
    self.select.connect("response", self.onRectangle)
    self.showRectangle = False
    hbox = Gtk.HBox()
    hbox.pack_start(Gtk.Label("Rectangular\n selection"), False, False, 0)
    self.spinRectRotate = Gtk.SpinButton.new(Gtk.Adjustment.new(0, 0, 180,
                                                                  0.1, 2., 2.),
                                              1., 2)
    self.spinRectRotate.set_wrap(True)
    hbox.pack_end(self.spinRectRotate, False, False, 0)
    hbox.pack_end(Gtk.Label(" rot.:"), False, False, 0)
    table = Gtk.Table(rows = 2, columns = 4)
    hbox.pack_end(table, False, False, 0)
    self.spinRectY = Gtk.SpinButton.new_with_range(0, 2047, 1)
    table.attach(self.spinRectY, 3, 4, 0, 1)
    table.attach(Gtk.Label("×"), 2, 3, 0, 1)
    self.spinRectX = Gtk.SpinButton.new_with_range(0, 2047, 1)
    table.attach(self.spinRectX, 1, 2, 0, 1)
    table.attach(Gtk.Label("orig.:"), 0, 1, 0, 1)
    self.spinRectH = Gtk.SpinButton.new_with_range(1, 2048, 1)
    self.spinRectH.set_value(2048)
    table.attach(self.spinRectH, 3, 4, 1, 2)
    table.attach(Gtk.Label("×"), 2, 3, 1, 2)
    self.spinRectW = Gtk.SpinButton.new_with_range(1, 2048, 1)
    self.spinRectW.set_value(2048)
    table.attach(self.spinRectW, 1, 2, 1, 2)
    table.attach(Gtk.Label("size:"), 0, 1, 1, 2)
    hbox.show_all()
    self.select.get_content_area().add(hbox)
    self.pack_start(self.select, False, False, 0)

    wd = Gtk.Toolbar()
    wd.set_orientation(Gtk.Orientation.HORIZONTAL)
    wd.set_style(Gtk.ToolbarStyle.ICONS)
    wd.set_icon_size(Gtk.IconSize.SMALL_TOOLBAR)
    self.pack_end(wd, False, False, 0)

    self.zoomFit = Gtk.ToggleToolButton(stock_id = Gtk.STOCK_ZOOM_FIT)
    self.zoomFit.set_active(True)
    self.zoomFit.connect("toggled", self.onZoomToggled)
    self.zoomFit.set_homogeneous(False)
    wd.insert(self.zoomFit, -1)
    self.inputDatas = {}
    item = Gtk.RadioToolButton()
    item.set_label("Raw")
    item.set_active(True)
    item.connect("toggled", self.onSourceToggled)
    wd.insert(item, -1)
    item.set_no_show_all(True)
    self.inputDatas[EDF_display.KIND_RAW] = item
    item = Gtk.RadioToolButton(group = item)
    item.set_label("Dark c.")
    item.connect("toggled", self.onSourceToggled)
    wd.insert(item, -1)
    item.set_no_show_all(True)
    self.inputDatas[EDF_display.KIND_DARK] = item
    item = Gtk.RadioToolButton(group = item)
    item.set_label("Flat c.")
    item.connect("toggled", self.onSourceToggled)
    wd.insert(item, -1)
    item.set_no_show_all(True)
    self.inputDatas[EDF_display.KIND_FLAT] = item
    item = Gtk.RadioToolButton(group = item)
    item.set_label("Norm.")
    item.connect("toggled", self.onSourceToggled)
    wd.insert(item, -1)
    item.set_no_show_all(True)
    self.inputDatas[EDF_display.KIND_NORM] = item
    self.group = item.get_group()
    item = Gtk.SeparatorToolItem()
    wd.insert(item, -1)

    hbox = Gtk.HBox()
    self.fileIcon = Gtk.Image()
    hbox.pack_start(self.fileIcon, False, False, 0)
    vbox = Gtk.VBox()
    hbox.pack_start(vbox, True, False, 0)
    self.fileLabel = Gtk.Label()
    self.fileLabel.set_ellipsize(Pango.EllipsizeMode.START)
    vbox.pack_start(self.fileLabel, True, False, 0)
    self.filePropLabel = Gtk.Label()
    self.filePropLabel.set_use_markup(True)
    vbox.pack_start(self.filePropLabel, False, False, 0)
    item = Gtk.ToolButton()
    item.set_icon_widget(hbox)
    #item.set_label_widget(vbox)
    item.set_homogeneous(False)
    #item.set_sensitive(False)
    item.set_expand(True)
    wd.insert(item, -1)
    
    item = Gtk.SeparatorToolItem()
    wd.insert(item, -1)
    item = Gtk.ToolButton(stock_id = Gtk.STOCK_SAVE_AS)
    item.set_tooltip_text("Save image as PNG file")
    item.connect("clicked", self.onSaveAs)
    item.set_homogeneous(False)
    wd.insert(item, -1)
    item = Gtk.ToolButton(stock_id = Gtk.STOCK_SAVE)
    item.set_tooltip_text("Save data as EDF file")
    item.connect("clicked", self.onSave)
    item.set_homogeneous(False)
    wd.insert(item, -1)
    item = Gtk.ToolButton(stock_id = Gtk.STOCK_PRINT)
    item.set_tooltip_text("Export into a PNG and a PDF file")
    item.connect("clicked", self.onExport)
    item.set_homogeneous(False)
    wd.insert(item, -1)
    item = Gtk.ToggleToolButton(stock_id = Gtk.STOCK_FIND)
    item.set_tooltip_text("Select a rectangular region")
    item.connect("clicked", self.onRect)
    item.set_homogeneous(False)
    wd.insert(item, -1)

  def setAction(self, kind):
    self.action = kind

  def setLabel(self, lbl):
    if lbl is not None:
      self.fileLabel.set_text(lbl)
      data = self.getShownData()
      if data is not None:
        self.filePropLabel.set_markup("<span size=\"small\"><i>image size: %d x %d px</i></span>" % (data.width, data.height))
    else:
      self.fileLabel.set_text("")
      self.filePropLabel.set_markup("")
    if lbl is not None and len(lbl) > 0:
      self.fileIcon.set_from_stock(Gtk.STOCK_FILE, Gtk.IconSize.SMALL_TOOLBAR)
    else:
      self.fileIcon.clear()

  def set(self, kind, data):
    if data is not None:
      data.setStored(True)
    if kind in self.datas and self.datas[kind] is not None:
      self.datas[kind].setStored(False)
    self.datas[kind] = data

    # Toggle visibility
    if data is None:
      self.inputDatas[kind].hide()
    else:
      self.inputDatas[kind].show()

    self.rawPix = None

    if self.checkAuto.get_active():
      self.scaleMin.set_text("auto")
      self.scaleMax.set_text("auto")

  def setRC(self, kind, rcSet):
    self.rcvs[kind] = rcSet

  def setFromModel(self, model):
    self.model = model
    
    data = model.getSelected(EDF_display.KIND_RAW)
    self.set(EDF_display.KIND_RAW, data)
    data = model.getSelected(EDF_display.KIND_NORM)
    self.set(EDF_display.KIND_NORM, data)
    data = model.getSelected(EDF_display.KIND_DARK)
    self.set(EDF_display.KIND_DARK, data)
    data = model.getSelected(EDF_display.KIND_FLAT)
    self.set(EDF_display.KIND_FLAT, data)
    # Select one if none available.
    if self.getShownData() is None:
      # List of available EDF data.
      lst = [kind for (kind, val) in self.datas.items() if val is not None]
      if len(lst) > 0:
        self.inputDatas[lst[0]].set_active(True)
    # Same for RC data
    rc = model.getSelectedRC(EDF_display.KIND_RAW)
    self.setRC(EDF_display.KIND_RAW, rc)
    rc = model.getSelectedRC(EDF_display.KIND_NORM)
    self.setRC(EDF_display.KIND_NORM, rc)
    rc = model.getSelectedRC(EDF_display.KIND_DARK)
    self.setRC(EDF_display.KIND_DARK, rc)
    rc = model.getSelectedRC(EDF_display.KIND_FLAT)
    self.setRC(EDF_display.KIND_FLAT, rc)

    self.setLabel(model.getSelectedFile())

  def setLogscale(self, status):
    self.checkScale.set_active(status)

  def setFactor(self, factor = 1.):
    try:
      self.factor.set_text("%g" % factor)
    except TypeError:
      self.factor.set_text("1.")

  def setCMap(self, id):
    for (i, (name, s)) in enumerate(EDF_display.CMAP_LIST_NAME):
      if id == name:
        self.cbCmap.set_active(i)

  def buildScaleShade(self, alloc):
    h = alloc.height
    w = alloc.width
    a = numpy.arange(0., 255.0000001, 255. / float(h - 1))[::-1].astype(numpy.dtype('u1'))
    a = GLib.Bytes.new(self.colorMap(a.repeat(w)))
    pix = GdkPixbuf.Pixbuf.new_from_bytes(a, GdkPixbuf.Colorspace.RGB,
                                          False, 8, w, h, w * 3)
    self.imgScale.set_from_pixbuf(pix)

  def buildScale(self, scaleValues):
    m, M, offset = scaleValues
    if hasattr(self.imgScale, "get_realized"):
      if not self.imgScale.get_realized():
        return
    else:
      if not self.imgScale.flags() & Gtk.REALIZED:
        return
    #self.buildScaleShade(self.imgScale.get_allocation())
    fmt = "%.3f"
    if M < 0.5:
      fmt = "%.3e"
    if self.checkScale.get_active():
      lm = numpy.log(m)
      lM = numpy.log(M)
      self.lblScale[0].set_text(fmt % M)
      self.lblScale[1].set_text(fmt % numpy.exp(lm + (lM - lm) * 0.75))
      self.lblScale[2].set_text(fmt % numpy.exp(lm + (lM - lm) * 0.5))
      self.lblScale[3].set_text(fmt % numpy.exp(lm + (lM - lm) * 0.25))
      self.lblScale[4].set_text(fmt % m)
    else:
      lm = m
      lM = M
      self.lblScale[0].set_text(fmt % lM)
      self.lblScale[1].set_text(fmt % (lm + (lM - lm) * 0.75))
      self.lblScale[2].set_text(fmt % (lm + (lM - lm) * 0.5))
      self.lblScale[3].set_text(fmt % (lm + (lM - lm) * 0.25))
      self.lblScale[4].set_text(fmt % lm)
    if offset is not None and offset > 0.:
      self.lblOffset.set_text("+%.8g" % offset)
      self.lblOffset.show()
    else:
      self.lblOffset.hide()

  def onScaleSizeChanged(self, img, alloc):
      pix = self.imgScale.get_pixbuf()
      if pix is not None and pix.get_width() == alloc.width and  pix.get_height() == alloc.height:
        return
      self.buildScaleShade(alloc)
      self.imgScale.set_size_request(-1, 0)

  def onFactor(self, entry):
    try:
      float(entry.get_text())
      entry.set_icon_from_stock(Gtk.EntryIconPosition.SECONDARY, None)
    except:
      entry.set_icon_from_stock(Gtk.EntryIconPosition.SECONDARY, Gtk.STOCK_DIALOG_WARNING)
      entry.set_icon_tooltip_text(Gtk.EntryIconPosition.SECONDARY,
                                    "Wrong scaling value.")
      entry.set_text("1.")
      return
    self.rawPix = None
    self.scaleMin.set_text("")
    self.scaleMax.set_text("")
    self.show()

  def show(self):
    self.img.clear()
    if self.rawPix is None:
      data = self.getShownData()
      if data is None:
        return
      # We change the rectangular selection boundaries.
      self.spinRectW.set_range(1, data.width)
      self.spinRectH.set_range(1, data.height)
      # We get the range, manual or auto.
      try:
        m = float(self.scaleMin.get_text())
      except ValueError:
        m = None
      try:
        M = float(self.scaleMax.get_text())
      except ValueError:
        M = None
      if self.offset is None:
        try:
          self.offset = float(data.dict["mean_angle"]) * float(self.factor.get_text())
        except:
          pass
      scale = [m, M, self.offset]
      self.rawPix = data.getPixbuf(scale = scale,
                                   logScale = self.checkScale.get_active(),
                                   highlight = self.highlight,
                                   colorMap = self.colorMap,
                                   factor = float(self.factor.get_text()))
      self.scaleMin.set_text(str(scale[0]))
      self.scaleMax.set_text(str(scale[1]))
      self.buildScale(scale)
      self.offset = scale[2]
    pix = self.rawPix.copy()
    # Scaling to the rendering window.
    scale = 1.
    dimx = pix.get_width()
    dimy = pix.get_height()
    if self.zoomFit.get_active():
      scalex = float(max(self.viewport.get_allocated_width(), 24)) / float(pix.get_width())
      scaley = float(max(self.viewport.get_allocated_height(), 24)) / float(pix.get_height())
      scale = min(scalex, scaley)
      dimx = int(float(pix.get_width()) * scale)
      dimy = int(float(pix.get_height()) * scale)
      pix = pix.scale_simple(dimx, dimy, GdkPixbuf.InterpType.BILINEAR)
    # Add a rectangular overlay.
    if self.showRectangle:
      rect = GdkPixbuf.Pixbuf.new(GdkPixbuf.Colorspace.RGB, False, 8,
                              max(int(self.spinRectW.get_value() * scale), 1),
                              max(int(self.spinRectH.get_value() * scale), 1))
      rect.fill(0x4169E1FF)
      rect.composite(pix,
                     int(self.spinRectX.get_value() * scale),
                     int(self.spinRectY.get_value() * scale),
                     max(int(self.spinRectW.get_value() * scale), 1),
                     max(int(self.spinRectH.get_value() * scale), 1),
                     0, 0, 1., 1., GdkPixbuf.InterpType.NEAREST, 128)
    # Pixel overlay and zoom box
    if self.xRC is not None and self.yRC is not None:
      (x, y) = (int((self.xRC - 1) * scale), int((self.yRC - 1) * scale))
      x = min(dimx - 1, x)
      y = min(dimy - 1, y)
      if x - 1 >= 0:
        pix.new_subpixbuf(0, y, x - 1, 1).fill(0x4169E1FF)
      if x + 2 < dimx:
        pix.new_subpixbuf(x + 2, y, dimx - x - 2, 1).fill(0x4169E1FF)
      if y - 1 >= 0:
        pix.new_subpixbuf(x, 0, 1, y - 1).fill(0x4169E1FF)
      if y + 2 < dimy:
        pix.new_subpixbuf(x, y + 2, 1, dimy - y - 2).fill(0x4169E1FF)
        # Zoom box
      z = 5
      n = 5
      if x < dimx - (z * n + 20):
        dx = 10
      else:
        dx = -10 - z * n
      if y < dimy - (z * n + 20):
        dy = 10
      else:
        dy = -10 - z * n
      px = dx + int(z * (n + 1) / 2)
      py = dy + int(z * (n + 1) / 2)
      pix.new_subpixbuf(x + dx - 1, y + dy - 1, z * n + 2, z * n + 2).fill(0x4169E1FF)
      self.rawPix.composite(pix,
                            x + dx, y + dy,
                            z * n, z * n,
                            -z * self.xRC + x + px, -z * self.yRC + y + py,
                            z, z, GdkPixbuf.InterpType.NEAREST, 196)
    self.img.set_from_pixbuf(pix)
    return False

  def onScaleAuto(self, entry, pos, event):
    self.rawPix = None
    entry.set_text("auto")
    self.show()

  def onScaleManual(self, entry):
    self.rawPix = None
    self.show()

  def getShownData(self):
    kind = [key for (key, val) in self.inputDatas.items() if val.get_active()][0]
    if not(kind in self.datas):
      return None
    else:
      return self.datas[kind]

  def getShownRC(self):
    kind = [key for (key, val) in self.inputDatas.items() if val.get_active()][0]
    if not(kind in self.rcvs):
      return None
    else:
      return self.rcvs[kind]

  def getRectangle(self):
    return (int(self.spinRectX.get_value()),
            int(self.spinRectY.get_value()),
            int(self.spinRectW.get_value()),
            int(self.spinRectH.get_value()))

  def getPlot(self):
    if self.canvasRC is None:
      vbox = Gtk.VBox()
      dpi = 72
      f = matplotlib.figure.Figure(figsize=(500 / dpi, 400 / dpi), dpi = dpi)
      self.plot = f.add_subplot(111)
      self.plot.scalingFactor = 1.
      self.canvasRC = FigureCanvas(f)
      self.canvasRC.mpl_connect('button_press_event',self.onPlot)
      vbox.pack_start(self.canvasRC, True, True, 0)
      hbox = Gtk.HBox()
      vbox.pack_end(hbox, False, False, 0)
      wd = Gtk.Toolbar()
      wd.set_orientation(Gtk.Orientation.HORIZONTAL)
      wd.set_style(Gtk.ToolbarStyle.ICONS)
      wd.set_icon_size(Gtk.IconSize.SMALL_TOOLBAR)
      hbox.pack_start(wd, True, True, 0)
      item = Gtk.RadioToolButton()
      item.set_label("Degrees")
      item.set_active(True)
      item.connect("toggled", self.onRCDegrees)
      wd.insert(item, -1)
      item = Gtk.RadioToolButton(group = item)
      item.set_label("Arc seconds")
      item.connect("toggled", self.onRCASeconds)
      wd.insert(item, -1)
      item = Gtk.RadioToolButton()
      item.set_label("Gaussian")
      item.connect("toggled", self.onRCAGaussians)
      wd.insert(item, -1)
      item = Gtk.RadioToolButton(group = item)
      item.set_label("Log normal")
      item.connect("toggled", self.onRCALogNormal)
      wd.insert(item, -1)
      item = Gtk.ToolButton(stock_id = Gtk.STOCK_SAVE)
      item.set_tooltip_text("Export rocking curve data")
      item.connect("clicked", self.onExportRC)
      wd.insert(item, -1)
      exportProf = Gtk.ToolButton(stock_id = Gtk.STOCK_PRINT)
      exportProf.set_tooltip_text("Export image as PNG or PDF file")
      exportProf.connect("clicked", self.onSaveImage, self.plot)
      wd.insert(exportProf, -1)
    return vbox

  def getPlotProfile(self):
    if self.canvasIP is None:
      vbox = Gtk.VBox()
      dpi = 72
      self.fIP = matplotlib.figure.Figure(figsize=(500 / dpi, 400 / dpi), dpi = dpi)
      self.plotIP = self.fIP.add_subplot(111)
      self.canvasIP = FigureCanvas(self.fIP)
      vbox.pack_start(self.canvasIP, True, True, 0)
      hbox = Gtk.HBox()
      vbox.pack_end(hbox, False, False, 0)
      vbox2 = Gtk.VBox()
      hbox.pack_end(vbox2, False, False, 0)
      self.lblIPY = Gtk.Label("<span size=\"small\" font-family=\"monospace\">y = not def.</span>")
      self.lblIPY.set_use_markup(True)
      vbox2.pack_end(self.lblIPY, False, False, 0)
      self.lblIPX = Gtk.Label("<span size=\"small\" font-family=\"monospace\">x = not def.</span>")
      self.lblIPX.set_use_markup(True)
      vbox2.pack_end(self.lblIPX, False, False, 0)
      vbox2.pack_end(Gtk.Label("Plot info:"), False, False, 0)
      wd = Gtk.Toolbar()
      wd.set_orientation(Gtk.Orientation.HORIZONTAL)
      wd.set_style(Gtk.ToolbarStyle.ICONS)
      wd.set_icon_size(Gtk.IconSize.SMALL_TOOLBAR)
      hbox.pack_start(wd, True, True, 0)
      getProf = Gtk.ToolButton(stock_id = Gtk.STOCK_ADD)
      getProf.connect("clicked", self.onIntensityProfile)
      wd.insert(getProf, -1)
      getProf = Gtk.ToolButton(stock_id = Gtk.STOCK_REMOVE)
      getProf.set_tooltip_text("Remove last added curve")
      getProf.connect("clicked", self.onRemoveProfile)
      wd.insert(getProf, -1)
      clearProf = Gtk.ToolButton(stock_id = Gtk.STOCK_CLEAR)
      clearProf.set_tooltip_text("Remove all curves and reset the area selection")
      clearProf.connect("clicked", self.onClearProfile)
      wd.insert(clearProf, -1)
      saveProf = Gtk.ToolButton(stock_id = Gtk.STOCK_SAVE)
      saveProf.set_tooltip_text("Export data in a column text file")
      saveProf.connect("clicked", self.onExportProfile)
      wd.insert(saveProf, -1)
      exportProf = Gtk.ToolButton(stock_id = Gtk.STOCK_PRINT)
      exportProf.set_tooltip_text("Export image as PNG or PDF file")
      exportProf.connect("clicked", self.onSaveImage, self.plotIP)
      wd.insert(exportProf, -1)
      self.setZero = Gtk.ToggleToolButton(stock_id = Gtk.STOCK_COLOR_PICKER)
      self.setZero.set_tooltip_text("Choose x position as zero for thickness measurement")
      self.setZero.connect("clicked", self.onThickness)
      wd.insert(self.setZero, -1)
      self.canvasIP.mpl_connect('motion_notify_event', self.onPlotIP)
      self.canvasIP.mpl_connect('button_press_event',self.onClickIP)
    return vbox

  def getPixels(self, event):
    data = self.getShownData()
    if data is None:
      return (0,0)
    w = float(self.viewport.get_allocated_width())
    h = float(self.viewport.get_allocated_height())
    if self.zoomFit.get_active():
      pix = self.img.get_pixbuf()
      pw = float(pix.get_width())
      ph = float(pix.get_height())
      zoom = max(float(data.width) / float(pw),
                 float(data.height) / float(ph))
      x = max(1, min(data.width, zoom * (event.x - (w - data.width / zoom) / 2.)))
      y = max(1, min(data.height, zoom * (event.y - (h - data.height / zoom) / 2.)))
    else:
      x = max(1, min(data.width,  event.x + 1 - max(0., (w - data.width)  / 2.)))
      y = max(1, min(data.height, event.y + 1 - max(0., (w - data.height) / 2.)))
    return (int(x), int(y))

  def onCoord(self, e, event):
    (x, y) = self.getPixels(event)
    self.lblPoxX.set_markup("<span size=\"small\" font-family=\"monospace\">x = %4.u px</span>" % x)
    self.lblPoxY.set_markup("<span size=\"small\" font-family=\"monospace\">y = %4.u px</span>" % y)
    data = self.getShownData()
    if data is not None:
      vals = data.getData()
      self.lblIntensity.set_markup("<span size=\"small\" font-family=\"monospace\">I = %8g a.u.</span>" % (vals[(y -1) * data.width + x - 1] * float(self.factor.get_text())))
    else:
      self.lblIntensity.set_markup("<span size=\"small\" font-family=\"monospace\">I = not def.</span>")
    if (self.action == EDF_display.ACTION_INTENSITY_PROFILES and
        event.state & Gdk.EventMask.BUTTON1_MOTION_MASK and not(self.mouseSelectionDone)):
      if x - self.origRectX > 0:
        self.spinRectX.set_value(self.origRectX)
        self.spinRectW.set_value(x - self.origRectX)
      else:
        self.spinRectX.set_value(x)
        self.spinRectW.set_value(self.origRectX - x)
      self.onRectangle(self.select, Gtk.ResponseType.APPLY)
    elif self.select.get_visible() and (event.state & Gdk.EventMask.BUTTON1_MOTION_MASK):
      if x <= self.spinRectX.get_value():
        self.spinRectW.set_value(self.spinRectW.get_value() + self.spinRectX.get_value() - x)
        self.spinRectX.set_value(x)
      else:
        self.spinRectW.set_value(x - self.spinRectX.get_value())
      if y <= self.spinRectY.get_value():
        self.spinRectH.set_value(self.spinRectH.get_value() + self.spinRectY.get_value() - y)
        self.spinRectY.set_value(y)
      else:
        self.spinRectH.set_value(y - self.spinRectY.get_value())
    return False

  def setRCPix(self, x, y):
    self.xRC = x
    self.yRC = y

  def showRC(self, verbose = False):
    if self.xRC is None or self.yRC is None:
      return
    self.show()
    rc = self.getShownRC()
    try:
      pix = rc.getAt(self.xRC - 1, self.yRC - 1)
      pix.fit(func = self.fitFunc)
      pix.plot(self.plot, withGuess = self.rcWithGuess)
      self.canvasRC.draw()
    except rcv.PixelIndexError:
      if verbose:
        self.messageLbl.set_text("No stored rocking curve at pixel %d×%d." % (self.xRC, self.yRC))
        self.message.show()
    except AttributeError:
      if verbose:
        self.messageLbl.set_text("No stored rocking curve for selected data.")
        self.message.show() 
    except Exception as e:
      raise(e)   

  def onShowRC(self, item, menu, coords):
    x, y = coords
    menu.popdown()
    menu.destroy()
    self.setRCPix(x, y)
    self.showRC(verbose = True)

  def onHideRC(self, item, menu):
    menu.popdown()
    menu.destroy()
    self.setRCPix(None, None)
    self.show()

  def onRCDegrees(self, wd):
    self.plot.scalingFactor = 1.
    self.showRC(verbose = True)

  def onRCASeconds(self, wd):
    self.plot.scalingFactor = 3600.
    self.showRC(verbose = True)

  def onRCAGaussians(self, wd):
    self.fitFunc = rcv.GaussianFit()
    self.showRC(verbose = True)

  def onRCALogNormal(self, wd):
    self.fitFunc = rcv.LogNormalFit()
    self.showRC(verbose = True)

  def onMenu(self, e, event):
    pos = self.getPixels(event)
    if event.button == 1:
      if (self.action == EDF_display.ACTION_ROCKING_CURVES and
          event.type == Gdk.EventType.BUTTON_RELEASE):
        self.event.grab_focus()
        self.setRCPix(pos[0], pos[1])
        self.showRC(verbose = True)
      elif self.action == EDF_display.ACTION_INTENSITY_PROFILES:
        if event.type == Gdk.EventType.BUTTON_PRESS:
          if not(self.mouseSelectionDone):
            data = self.getShownData()
            self.origRectX = pos[0]
            self.origRectY = pos[1]
            self.spinRectX.set_value(pos[0])
            self.spinRectY.set_value(0)
            self.spinRectW.set_value(1)
            self.spinRectH.set_value(data.height)
        elif event.type == Gdk.EventType.BUTTON_RELEASE:
          if not(self.mouseSelectionDone):
            if pos[0] - self.origRectX > 0:
              self.spinRectX.set_value(self.origRectX)
              self.spinRectW.set_value(pos[0] - self.origRectX)
            else:
              self.spinRectX.set_value(pos[0])
              self.spinRectW.set_value(self.origRectX - pos[0])
            self.mouseSelectionDone = True
          else:
            self.spinRectY.set_value(pos[1])
            self.spinRectH.set_value(1)
          self.onRectangle(self.select, Gtk.ResponseType.APPLY)
      elif self.select.get_visible():
        if event.type == Gdk.EventType.BUTTON_PRESS:
          self.spinRectX.set_value(pos[0])
          self.spinRectY.set_value(pos[1])
          self.spinRectW.set_value(1)
          self.spinRectH.set_value(1)
        elif event.type == Gdk.EventType.BUTTON_RELEASE:
          ## self.spinRectW.set_value(pos[0] - self.spinRectX.get_value())
          ## self.spinRectH.set_value(pos[1] - self.spinRectY.get_value())
          self.onRectangle(self.select, Gtk.ResponseType.APPLY)
    elif event.button == 3 and event.type == Gdk.EventType.BUTTON_RELEASE:
      menu = Gtk.Menu()
      item = Gtk.MenuItem("Remove pixel highlight")
      item.connect("activate", self.onHideRC, menu)
      menu.append(item)
      item = Gtk.MenuItem("Show rocking curve of pixel %d×%d" % tuple(pos))
      item.connect("activate", self.onShowRC, menu, pos)
      menu.append(item)
      menu.show_all()
      menu.popup(None, None, None, event.button, event.time)
    return False

  def onKey(self, e, event):
    key = Gdk.keyval_name(event.keyval)
    data = self.getShownData()
    if key == "Right" and self.xRC < data.width:
      self.xRC += 1
    if key == "Left" and self.xRC > 1:
      self.xRC -= 1
    if key == "Down" and self.yRC < data.height:
      self.yRC += 1
    if key == "Up" and self.yRC > 1:
      self.yRC -= 1
    self.showRC(verbose = True)
    return True

  def onPlot(self, event):
    rc = self.getShownRC().lst[0]
    iData = int(round((event.xdata - rc.origAng) / rc.stepAng))
    self.model.select(iData)

  def onPlotIP(self, event):
    if event.xdata is not None:
      self.lblIPX.set_markup("<span size=\"small\" font-family=\"monospace\">x = %4.2f µm</span>" % event.xdata)
    if event.ydata is not None:
      self.lblIPY.set_markup("<span size=\"small\" font-family=\"monospace\">y = %4.2f a.u.</span>" % event.ydata)

  def onClickIP(self, event):
    if self.setZero.get_active():
      # We unset the trnsformation.
      shift = None
    else:
      # We shift the data.
      shift = matplotlib.transforms.Affine2D()
      shift.translate(-event.xdata, 0.)
    for line in self.plotIP.get_lines():
      #print line.get_xdata(orig = True)[:5]
      line.set_transform(shift)
      #print line.get_xdata(orig = False)[:5]
    self.canvasIP.draw()

  def onThickness(self, tg):
    if tg.get_active():
      return
    if len(self.fIP.axes) > 1:
      self.fIP.delaxes(self.fIP.axes[1])
      self.plotIP.set_ylim(auto = True)
      self.canvasIP.draw()

  def scrollToRC(self):
    # Scroll to the pixel.
    data = self.getShownData()
    adj = self.viewport.get_hadjustment()
    adj.set_value(max(min(self.xRC - adj.page_size / 2, data.width - adj.page_size), 0))
    adj = self.viewport.get_vadjustment()
    adj.set_value(max(min(self.yRC - adj.page_size / 2, data.height - adj.page_size), 0))
    return False

  def onZoomToggled(self, bt):
    self.show()
    if self.xRC is not None and self.yRC is not None and not(bt.get_active()):
      GLib.idle_add(self.scrollToRC)

  def onCheckScale(self, tg):
    self.rawPix = None
    self.scaleMax.set_text("Auto")
    self.scaleMin.set_text("Auto")
    self.offset = 0.
    GLib.idle_add(self.show)

  def onCMap(self, cb):
    if cb.get_active() == 0:
      self.colorMap = edf.grayScale
    if cb.get_active() == 1:
      self.colorMap = edf.xrayScale
    elif cb.get_active() == 2:
      self.colorMap = edf.viridisScale
    elif cb.get_active() == 3:
      self.colorMap = edf.jetScale
    elif cb.get_active() == 4:
      self.colorMap = edf.stressScale
    self.rawPix = None
    self.buildScaleShade(self.imgScale.get_allocation())
    GLib.idle_add(self.show)

  def onSourceToggled(self, item):
    if not(item.get_active()):
      return
    self.message.hide()
    self.rawPix = None
    self.scaleMin.set_text("auto")
    self.scaleMax.set_text("auto")
    GLib.idle_add(self.show)
    self.showRC()

  def onRect(self, bt):
    if bt.get_active():
      self.select.show()
    else:
      self.select.hide()

  def onIntensityProfile(self, bt):
    (x, y, w, h) = self.getRectangle()
    data = self.getShownData()
    if data is not None:
      vals = data.getData(flat = False)[y:y + h, x:x + w].reshape(h, w)
      xr = thickness(numpy.arange(0, float(w * 0.75), 0.75))
      if h > 1:
        lbl = 'run %d' % data.run
        # We arbitrarily skip values within scaling range.
        m = float(self.scaleMin.get_text())
        M = float(self.scaleMax.get_text())
        out = (vals < m) | (vals > M)
        vals = vals.copy()
        vals[out] = 0.
        yr = vals.sum(0) / (float(h) - out.sum(0))
        sr = vals - yr
        sr[out] = 0.
        sr = numpy.sqrt((sr ** 2).sum(0) / (float(h) - out.sum(0)))
      else:
        lbl = 'run %d (y = %d)' % (data.run, y)
        yr = vals.reshape(-1)
        sr = None
      self.plotIP.errorbar(xr, yr[::-1], yerr = sr, fmt = '.-', label = lbl, elinewidth = 0.3, capsize = 1.5, ecolor = "black")
      self.plotIP.legend()
      if hasattr(self.plotIP, "set_xlabel"):
        self.plotIP.set_xlabel(r'Thickness ($\mu$m)')
        self.plotIP.set_ylabel('Intensity (a.u.)')
      self.canvasIP.draw()

  def onRemoveProfile(self, bt):
    lines = self.plotIP.get_lines()
    if len(lines) > 0:
      if len(lines) > 1:
        lines.pop(-1).remove()
        self.plotIP.legend()
        self.plotIP.relim()
        self.plotIP.autoscale_view(scalex = False)
      else:
        self.plotIP.cla()
      self.canvasIP.draw()

  def onClearProfile(self, bt):
    self.mouseSelectionDone = False
    self.spinRectX.set_value(0)
    self.spinRectY.set_value(0)
    self.spinRectW.set_value(2048)
    self.spinRectH.set_value(2048)
    self.onRectangle(self.select, Gtk.ResponseType.CANCEL)
    self.plotIP.cla()
    self.canvasIP.draw()

  def onSaveImage(self, bt, plot):
    dialog = Gtk.FileChooserDialog("Save intensity profile", None,
                                   Gtk.FileChooserAction.SAVE,
                                   (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                    Gtk.STOCK_SAVE, Gtk.ResponseType.OK))
    filter = Gtk.FileFilter()
    filter.set_name("Portable Document Format (*.pdf)")
    filter.add_mime_type("application/pdf")
    dialog.add_filter(filter)
    filter = Gtk.FileFilter()
    filter.set_name("Portable Network Graphics (*.png)")
    filter.add_mime_type("image/png")
    dialog.add_filter(filter)
    filter = Gtk.FileFilter()
    filter.set_name("All files")
    filter.add_pattern("*")
    dialog.add_filter(filter)
    response = dialog.run()
    if response == Gtk.ResponseType.OK:
      pp = backend_cairo.FigureCanvasCairo(plot.get_figure())
      pp.print_figure(dialog.get_filename())
      manager = Gtk.RecentManager.get_default()
      manager.add_item(dialog.get_uri())
    dialog.destroy()

  def onExportRC(self, bt):
    if self.xRC is None or self.yRC is None:
      return
    rc = self.getShownRC()
    data = rc.getDataAt(self.xRC - 1, self.yRC - 1)
    if data is None:
      return
    dialog = Gtk.FileChooserDialog("Export rocking curve data", None,
                                   Gtk.FileChooserAction.SAVE,
                                   (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                    Gtk.STOCK_SAVE, Gtk.ResponseType.OK))
    dialog.set_current_name("rc_%03dx%03d.dat" % (self.xRC, self.yRC))
    response = dialog.run()
    if response == Gtk.ResponseType.OK:
      with open(dialog.get_filename(), "w") as f:
        x = rc.getAnglesAt(self.xRC - 1, self.yRC - 1)
        for vals in zip(x, data):
          f.write("%g %g\n" % vals)
      manager = Gtk.RecentManager.get_default()
      manager.add_item(dialog.get_uri())
    dialog.destroy()

  def onExportProfile(self, bt):
    dialog = Gtk.FileChooserDialog("Export intensity profile as data", None,
                                   Gtk.FileChooserAction.SAVE,
                                   (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                    Gtk.STOCK_SAVE, Gtk.ResponseType.OK))
    response = dialog.run()
    if response == Gtk.ResponseType.OK:
      x0 = None
      lst = []
      for l in self.plotIP.get_lines():
        (xr, yr) = l.get_data()
        if x0 is None:
          x0 = xr
          lst.append(x0)
        if not numpy.array_equal(x0, xr):
          raise ValueError
        lst.append(yr)
      vals = numpy.hstack(lst).reshape(len(lst), -1).T
      legends = [it.get_text() for it in self.plotIP.get_legend().get_texts()]
      f = open(dialog.get_filename(), "w")
      f.write("# position")
      for legend in legends:
        f.write("   " + legend)
      f.write("\n")
      for val in vals:
        f.write(str(val)[1:-1] + "\n")
      f.close()
      manager = Gtk.RecentManager.get_default()
      manager.add_item(dialog.get_uri())
    dialog.destroy()

  def onMessage(self, info, id):
    info.hide()

  def onRectangle(self, info, id):
    self.showRectangle = (id == Gtk.ResponseType.APPLY)
    self.show()
    self.emit("rectangle-selection",
              (int(self.spinRectX.get_value()), int(self.spinRectY.get_value())),
              (int(self.spinRectW.get_value()), int(self.spinRectH.get_value())))

  def onSaveAs(self, item):
    kind = 3 - list((Gtk.RadioButton.get_active(g) for g in self.group)).index(True)
    lbl = EDF_display.KIND_DICT_NAME[kind]
    data = self.getShownData()
    if data is None:
      return
    if self.rawPix is None:
      self.messageLbl.set_text("No '%s' image to save, select a valid image." % lbl)
      self.message.show()
      return
    # Create a dialog to export.
    dialog = Gtk.FileChooserDialog("Save data as image", None,
                                   Gtk.FileChooserAction.SAVE,
                                   (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                    Gtk.STOCK_SAVE_AS, Gtk.ResponseType.OK))
    filter = Gtk.FileFilter()
    filter.set_name("Portable Network Graphic (*.png)")
    filter.add_mime_type("image/png")
    dialog.add_filter(filter)
    filter = Gtk.FileFilter()
    filter.set_name("All files")
    filter.add_pattern("*")
    dialog.add_filter(filter)
    dialog.set_current_folder(data.path)
    if data.run == -1:
      filename = "%s.png" % data.prefix
    else:
      filename = "%s%04d_%s.png" % (data.prefix, data.run, lbl)
    if filename.startswith('.'):
      dialog.set_current_name(filename[1:])
    else:
      dialog.set_current_name(filename)
    response = dialog.run()
    if response == Gtk.ResponseType.OK:
      # Add a rectangular overlay.
      pix = self.rawPix.copy()
      if self.showRectangle:
        rect = GdkPixbuf.Pixbuf.new(GdkPixbuf.Colorspace.RGB, False, 8,
                                max(int(self.spinRectW.get_value()), 1),
                                max(int(self.spinRectH.get_value()), 1))
        rect.fill(0x4169E1FF)
        rect.composite(pix,
                       int(self.spinRectX.get_value()),
                       int(self.spinRectY.get_value()),
                       max(int(self.spinRectW.get_value()), 1),
                       max(int(self.spinRectH.get_value()), 1),
                       0, 0, 1., 1., GdkPixbuf.InterpType.NEAREST, 128)
      pix.savev(dialog.get_filename(), "png", [], [])
      manager = Gtk.RecentManager.get_default()
      manager.add_item(dialog.get_uri())
    dialog.destroy()
      
  def onSave(self, item):
    kind = 3 - list((Gtk.RadioButton.get_active(g) for g in self.group)).index(True)
    lbl = EDF_display.KIND_DICT_NAME[kind]
    data = self.getShownData()
    #data.store()
    if data is None:
      self.messageLbl.set_text("No '%s' data to save, select a valid data." % lbl)
      self.message.show()
      return
    if data.prefix.startswith('.'):
      prefix = data.prefix[1:]
    else:
      prefix = data.prefix
    if data.run == -1:
      data.save(os.path.join(data.path, "%s.edf" % prefix))
    else:
      data.save(os.path.join(data.path, "%s%04d_%s.edf" % (prefix, data.run, lbl)))

  def onExport(self, item):
    kind = 3 - list((Gtk.RadioButton.get_active(g) for g in self.group)).index(True)
    lbl = EDF_display.KIND_DICT_NAME[kind]
    data = self.getShownData()
    if data is None:
      self.messageLbl.set_text("No '%s' data to export, select a valid data." % lbl)
      self.message.show()
      return
    if self.rawPix is None:
      self.messageLbl.set_text("No '%s' image to export, select a valid data." % lbl)
      self.message.show()
      return

    # Create a dialog to export.
    dialog = Gtk.FileChooserDialog("Export data as PDF", None,
                                   Gtk.FileChooserAction.SAVE,
                                   (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                    Gtk.STOCK_PRINT, Gtk.ResponseType.OK))
    filter = Gtk.FileFilter()
    filter.set_name("Portable Document Format (*.pdf)")
    filter.add_mime_type("application/pdf")
    dialog.add_filter(filter)
    filter = Gtk.FileFilter()
    filter.set_name("All files")
    filter.add_pattern("*")
    dialog.add_filter(filter)
    dialog.set_current_folder(data.path)
    if data.run == -1:
      filename = "%s.pdf" % data.prefix
    else:
      filename = "%s%04d_%s.pdf" % (data.prefix, data.run, lbl)
    if filename.startswith('.'):
      dialog.set_current_name(filename[1:])
    else:
      dialog.set_current_name(filename)
    response = dialog.run()
    if response == Gtk.ResponseType.OK:
      if data.prefix is None or len(data.prefix) < 6:
        if data.dataFile is not None:
          title = os.path.basename(data.dataFile)
        else:
          title = None
      else:
        title = data.prefix
      data.exportPDF(dialog.get_filename(),
                     scale = (float(self.scaleMin.get_text()),
                              float(self.scaleMax.get_text())),
                     logScale = self.checkScale.get_active(),
                     colorMap = self.colorMap,
                     rawPix = self.rawPix, offset = self.offset,
                     title = title, factor = float(self.factor.get_text()))
      manager = Gtk.RecentManager.get_default()
      manager.add_item(dialog.get_uri())
    dialog.destroy()

def thickness(h):
  w = 10.
  theta = 9.144 * numpy.pi / 180.
  return (h * numpy.cos(2. * theta) - w) / (2. * numpy.sin(theta))

