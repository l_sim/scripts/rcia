#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2012 Damien Caliste <damien.caliste@cea.fr>,
#                    Thu Nhi Tran Thi <autumn_nhi@yahoo.com>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA

import sys, os, re
import numpy
from scipy.optimize import leastsq
from scipy.special import wofz
import matplotlib
from matplotlib.backends.backend_gtkagg import FigureCanvasGTKAgg as FigureCanvas
from matplotlib.backends import backend_pdf
from matplotlib.backends import backend_agg
import pygtk
import gtk, gobject

def gauss_eval(x, ampl, sig, x0):
  return ampl / (sig * numpy.sqrt(2. * numpy.pi)) * \
         numpy.exp((x - x0) ** 2 / (-2. * (sig ** 2)))

def voigt_eval(x, ampl, sig, gamma, x0):
  z = (x - x0 + gamma * 1j) / (sig * numpy.sqrt(2.))
  return ampl * wofz(z).real / (sig * numpy.sqrt(2. * numpy.pi))

def linear_eval(x, x1, x2, y1, y2):
  return y1 + (y2 - y1) / (x2 - x1) * (x - x1)

fitFunc = voigt_eval
constraintIntensityRatio = True

def hkl_sort(((n1, s1), hkl1), ((n2, s2), hkl2)):
  if n1 < n2:
    return -1
  if n1 > n2:
    return +1
  return 0

hkls = {}
for k in range(6):
  for j in range(6):
    for i in range(6):
      n = i**2 + j**2 + k**2
      s = i + j + k
      if not (n, s) in hkls:
        hkls[(n, s)] = (i, j, k)
hkls = hkls.items()
hkls.sort(hkl_sort)
hkls = numpy.array([hkl for (k, hkl) in hkls])
hkls_keys = numpy.sqrt((hkls * hkls).sum(1))

class peak_data():
  I_BG_Y1  =  0
  I_BG_Y2  =  1
  I_THETA  =  2
  I_AMPL_1 =  3
  I_AMPL_2 =  4
  I_SIG_1  =  5
  I_SIG_2  =  6
  I_GAMMA_1=  7
  I_GAMMA_2=  8
  I_FIT_X1 = 11
  I_FIT_X2 = 12
  N_VALUES = 13

  kalpha1 = 1.54439
  kalpha2 = 1.54056

  aSi = 5.43102
  aAl = 4.05
  aCr = 2.88494
  aCu = 3.61
  aFe = 2.86700
  aAs = 4.130

  def __init__(self, filename, fitFunc = gauss_eval, constraintIntensityRatio = True):
    data = []
    f = open(filename, "r")
    for line in f.xreadlines():
      if not line.strip() == "":
        data.append(map(float, line.split()))
    f.close()
    self.filename = filename
    self.data = numpy.array(data)

    rg = 25
    self.sdata = self.data.copy()
    for i in range(rg, len(data) - rg):
      self.sdata[i, 1] = self.data[i - rg:i + rg, 1].sum() / float(2. * rg)
      #print self.sdata[i, 0], self.sdata[i, 1]

    self.fit = fitFunc
    self.constrainRatio = constraintIntensityRatio
    self.dTheta = 0.

  def __iter__(self):
    return self.data.__iter__()

  def __next__(self):
    return self.data.__next__()

  def calc_theta_2(self, theta_1):
    return (2. * numpy.arcsin(peak_data.kalpha1 / peak_data.kalpha2 *
                             numpy.sin(theta_1 / 2. * numpy.pi / 180.)) *
            180. / numpy.pi)

  def calc_hkl_key(self, theta, a):
    print theta, a, 2 * a * numpy.sin(theta * numpy.pi / 180.) / peak_data.kalpha1
    return 2 * a * numpy.sin(theta * numpy.pi / 180.) / peak_data.kalpha1

  def get_hkl_si_al(self, theta):
    key_si = self.calc_hkl_key(theta, a = peak_data.aSi)
    match = numpy.abs(hkls_keys - key_si)
    hkl_si = (match.min(), hkls[match.argmin()])
    key_al = self.calc_hkl_key(theta, a = peak_data.aAl)
    match = numpy.abs(hkls_keys - key_al)
    hkl_al = (match.min(), hkls[match.argmin()])
    return (hkl_si, hkl_al)

  def get_hkl_other(self, theta, a0):
    key = self.calc_hkl_key(theta, a = a0)
    match = numpy.abs(hkls_keys - key)
    hkl = (match.min(), hkls[match.argmin()])
    return hkl

  def double_peak_eval(self, x, p, withBackground = True):
    f0 = p[peak_data.I_BG_Y1]
    f1 = p[peak_data.I_BG_Y2]
    x0 = p[peak_data.I_THETA]
    ampl1 = p[peak_data.I_AMPL_1]
    ampl2 = p[peak_data.I_AMPL_2]
    sig1 = p[peak_data.I_SIG_1]
    sig2 = p[peak_data.I_SIG_2]
    x1 = x0
    x2 = self.calc_theta_2(x0)
    if self.constrainRatio:
      ampl2 = ampl1 / 2.
    if self.fit == gauss_eval:
      ret = gauss_eval(x, ampl1, sig1, x1) + gauss_eval(x, ampl2, sig2, x2)
    elif self.fit == voigt_eval:
      gamma1 = p[peak_data.I_GAMMA_1]
      gamma2 = p[peak_data.I_GAMMA_2]
      ret = (voigt_eval(x, ampl1, sig1, gamma1, x1) +
             voigt_eval(x, ampl2, sig2, gamma2, x2))
    if withBackground:
      ret += linear_eval(x, x[0], x[-1], f0, f1)
    return ret

  def residuals(self, p, x, y):
    param = numpy.zeros(peak_data.N_VALUES)
    param[peak_data.I_BG_Y1] = y[0]
    param[peak_data.I_BG_Y2] = y[-1]
    if self.fit == gauss_eval:
      param[peak_data.I_THETA:peak_data.I_SIG_2 + 1] = p
    elif self.fit == voigt_eval:
      param[peak_data.I_THETA:peak_data.I_GAMMA_2 + 1] = p
    if constraintIntensityRatio:
      p[2] = p[1] / 2.
    return y - self.double_peak_eval(x, param)

  def fit_peak(self, peak, angBefore = None, angAfter = None):
    dTheta = 2.
    if angBefore is None:
      local = self.data[(self.data[:,0] > (peak - dTheta / 2.)) &
                        (self.data[:,0] < peak)]
      angBefore = local[local[:,1].argmin(), 0]
    if angAfter is None:
      local = self.data[(self.data[:,0] > peak) &
                        (self.data[:,0] < (peak + dTheta / 2.))]
      angAfter = local[local[:,1].argmin(), 0]
    # Try to find expansion of the pick.
    local = self.data[(self.data[:,0] > angBefore) &
                      (self.data[:,0] < angAfter)]
    peak = local[local[:,1].argmax(), 0]
    angBefore = max(angBefore, peak - dTheta / 2.)
    angAfter  = min(angAfter,  peak + dTheta / 2.)
    local = self.data[(self.data[:,0] > angBefore) &
                      (self.data[:,0] < angAfter)]

    # Fit part
    mu = 0.05
    ptp = local[:,1].ptp()
    ampl = mu * numpy.sqrt(2. * numpy.pi) * ptp
    x = local[:, 0]
    param = numpy.zeros(peak_data.N_VALUES)
    param[peak_data.I_BG_Y1] = local[0,1]
    param[peak_data.I_BG_Y2] = local[-1,1]
    if self.fit == gauss_eval:
      p0 = (local[local[:,1].argmax(), 0],
            ampl, ampl / 2., mu, mu)
    elif self.fit == voigt_eval:
      p0 = (local[local[:,1].argmax(), 0],
            ampl, ampl / 2., mu, mu, mu, mu)
    p = leastsq(self.residuals, p0,
                args = (x, local[:,1]))
    if self.fit == gauss_eval:
      param[peak_data.I_THETA:peak_data.I_SIG_2 + 1] = p[0]
    elif self.fit == voigt_eval:
      param[peak_data.I_THETA:peak_data.I_GAMMA_2 + 1] = p[0]
    param[peak_data.I_FIT_X1] = angBefore
    param[peak_data.I_FIT_X2] = angAfter
    return param

  def plot_zoom(self, plot, iPeak, dTheta = 3.0):
    peak = self.peaks[iPeak, peak_data.I_THETA]
    local = self.data[(self.data[:,0] > (peak - dTheta / 2.)) &
                      (self.data[:,0] < (peak + dTheta / 2.))]
    
    x = local[:, 0]
    xprec = numpy.arange(local[0, 0], local[-1, 0],
                         (local[-1, 0] - local[0, 0]) / 200.)
    y_gauss = self.double_peak_eval(xprec, self.peaks[iPeak])
    plot.cla()
    plot.set_xlabel(r"$2\theta$ angle (degree)")
    plot.set_ylabel("Intensity (a.u.)")
    plot.set_yscale("log")
    plot.plot(x + 2. * self.dTheta, local[:,1], '.-', xprec, y_gauss)
    plot.axvline(self.get_theta_1(iPeak) * 2.,
                 linestyle = "--", color = "black")
    plot.axvline(self.get_theta_2(iPeak) * 2.,
                 linestyle = "--", color = "gray")
    plot.axvspan(self.peaks[iPeak][peak_data.I_FIT_X1],
                 self.peaks[iPeak][peak_data.I_FIT_X2],
                 color = 'red', zorder = 0, alpha = 0.075, hatch = '/')

  def fit_all(self, minIntensity = None, renormalize = True):
    peaks = []
    n = 25
    for i in range(0, len(self.sdata), n):
      peaks.append((self.sdata[min(i + n / 2, len(self.sdata) - 1), 0], self.sdata[i:i+n, 1].max()))
    peaks.append((90, 999))
    top = None
    fpeaks = []
    if minIntensity is None:
      # First dry run to set-up minIntensity
      maxInt = 0.
      for i in range(1, len(peaks) - 1):
        if peaks[i - 1][1] > peaks[i][1] and peaks[i + 1][1] >= peaks[i][1]:
          if top is not None:
            maxInt = max(maxInt, max(top[1] - bottom[1], top[1] - peaks[i][1]))
            top = None
          bottom = peaks[i]
        if peaks[i - 1][1] < peaks[i][1] and peaks[i + 1][1] <= peaks[i][1]:
          top = peaks[i]
      minIntensity = maxInt / 35.
    for i in range(1, len(peaks) - 1):
      if peaks[i - 1][1] > peaks[i][1] and peaks[i + 1][1] >= peaks[i][1]:
        if top is not None:
          ## print top[0], top[1], max(top[1] - bottom[1], top[1] - peaks[i][1])
          if max(top[1] - bottom[1], top[1] - peaks[i][1]) > minIntensity and top[0] > 25.:
            fpeaks.append(self.fit_peak(top[0], angBefore = bottom[0],
                                        angAfter = peaks[i][0]))
          top = None
        bottom = peaks[i]
      if peaks[i - 1][1] < peaks[i][1] and peaks[i + 1][1] <= peaks[i][1]:
        top = peaks[i]
    self.peakThreshold = minIntensity
    self.peaks = numpy.array(fpeaks)
    # Label the peaks.
    self.labels = {}
    for iPeak in range(self.get_n_peaks()):
      ((i_si, hkl_si), (i_al, hkl_al)) = self.get_hkl_si_al(self.get_theta_1(iPeak))
      p_si = max(0., 100. - abs(i_si) * 100. / 0.1)
      p_al = max(0., 100. - abs(i_al) * 100. / 0.1)
      if p_si > p_al:
        self.labels[iPeak] = (("Si", hkl_si, peak_data.aSi, p_si),
                              ("Al", hkl_al, peak_data.aAl, p_al))
      else:
        self.labels[iPeak] = (("Al", hkl_al, peak_data.aAl, p_al),
                              ("Si", hkl_si, peak_data.aSi, p_si))
      ## (i_cr, hkl_cr) = self.get_hkl_other(self.get_theta_1(iPeak), peak_data.aCr)
      ## print i_si, i_al, (i_cr, hkl_cr)    
    # Renormalise all angles by using the first peak.
    if renormalize:
      ## for iPeak in range(self.get_n_peaks()):
      ##   if self.labels[iPeak][0][0] == "Si":
      ##     hkl = self.labels[iPeak][0][1]
      ##     i_hkl = numpy.sqrt((hkl * hkl).sum())
      ##     theta = numpy.arcsin(peak_data.kalpha1 * i_hkl / 2. / peak_data.aSi) * 180. / numpy.pi
      ##     print i_hkl, self.get_theta_1(iPeak), theta
      theta111 = numpy.arcsin(peak_data.kalpha1 * numpy.sqrt(3.) / 2. / peak_data.aSi) * 180. / numpy.pi
      self.dTheta = theta111 - self.get_theta_1(0)
      ## for iPeak in range(self.get_n_peaks()):
      ##   dTheta = 0.0033211 * self.get_theta_1(iPeak) -0.0203762
      ##   self.peaks[iPeak, peak_data.I_THETA] += 2. * dTheta
      self.peaks[:, peak_data.I_THETA] += 2. * self.dTheta
      
  def plot_all(self, plot, dTheta = 4.0):
    plot.cla()
    plot.set_xlabel(r"$2\theta$ angle (degree)")
    plot.set_ylabel("Intensity (a.u.)")
    plot.set_yscale("log")
    plot.plot(self.data[:, 0] + 2. * self.dTheta, self.data[:, 1])
    for iPeak in range(self.get_n_peaks()):
      plot.axvline(self.get_theta_avg(iPeak) * 2.,
                   linestyle = "--", color = "black", zorder = 0)
      peak = self.peaks[iPeak, peak_data.I_THETA]
      local = self.data[(self.data[:,0] > (peak - dTheta / 2.)) &
                        (self.data[:,0] < (peak + dTheta / 2.))]
      plot.plot(local[:, 0], self.double_peak_eval(local[:, 0], self.peaks[iPeak, :peak_data.I_FIT_X1]),
                color = 'green')
      span = len(local) / 2.
      hkl = self.get_hkl(iPeak)
      plot.annotate('%s %s' % (hkl[0][0], str(hkl[0][1])),
                    xy = (self.get_theta_avg(iPeak) * 2.,
                          local[span - 50: span + 50].max() * 1.25),
                    xycoords = 'data', xytext = (-6, +40), textcoords = "offset points",
                    bbox=dict(boxstyle="round", fc="0.8"), size = 8, rotation = 'vertical',
                    arrowprops={})
    plot.set_ylim(max(1, plot.get_ylim()[0]), plot.get_ylim()[1])

  def save_all(self, filename, dTheta = 2.0):
    f = open(filename, "w")
    f.write("# fitted peaks from '%s'\n" % self.filename)
    f.write("# 2.theta     intensity\n")
    for iPeak in range(self.get_n_peaks()):
      peak = self.peaks[iPeak, peak_data.I_THETA]
      points = ((self.data[:,0] > (peak - dTheta / 2.)) &
                (self.data[:,0] < (peak + dTheta / 2.)))
      x = self.data[points][:, 0] + 2. * self.dTheta
      y = self.double_peak_eval(x, self.peaks[iPeak, :peak_data.I_FIT_X1])
      for vals in zip(x, y):
        f.write("%12.3f %12.3f\n" % vals)
      fi = open(re.sub(r"(.*)[.]([^.]*)", r"\1.peak_%02d.\2" % (iPeak + 1), filename), "w")
      peak = self.get_theta_avg(iPeak) * 2.
      #xprec = numpy.arange(peak - 0.5, peak + 0.5, 1. / 500.)
      y = self.double_peak_eval(x, self.peaks[iPeak, :peak_data.I_FIT_X1],
                                withBackground = False) / self.get_int_intensity(iPeak)
      fi.write("# fitted peak %d from '%s'\n" % (iPeak + 1, self.filename))
      fi.write("# 2.theta     intensity\n")
      for vals in zip(x, y, self.data[points][:, 1]  / self.get_int_intensity(iPeak)):
        fi.write("%16.8f %16.8f %16.8f\n" % vals)
      fi.close()
    f.close()
      
  def get_n_peaks(self):
    return self.peaks.shape[0]

  def get_i_peak(self, theta):
    dist = numpy.abs(self.peaks[:, peak_data.I_THETA] - theta)
    return dist.argmin()

  def get_theta_1(self, iPeak):
    return self.peaks[iPeak][peak_data.I_THETA] / 2.

  def get_theta_2(self, iPeak):
    return self.calc_theta_2(self.peaks[iPeak][peak_data.I_THETA]) / 2.

  def get_theta_avg(self, iPeak):
    return (self.get_theta_1(iPeak) * 2. + self.get_theta_2(iPeak)) / 3.

  def get_int_intensity(self, iPeak):
    return self.peaks[iPeak][peak_data.I_AMPL_1] + \
           self.peaks[iPeak][peak_data.I_AMPL_2]

  def get_intensity_ratio(self, iPeak):
    return self.peaks[iPeak][peak_data.I_AMPL_1] / \
           self.peaks[iPeak][peak_data.I_AMPL_2]

  def get_fwhm_1(self, iPeak):
    fg = self.peaks[iPeak][peak_data.I_SIG_1] * 2.35482 / 2.
    if self.fit == voigt_eval:
      fl = self.peaks[iPeak][peak_data.I_GAMMA_1]
    else:
      fl = 0.
    return 0.5346 * fl + numpy.sqrt(0.2166 * (fl ** 2) + fg ** 2)

  def get_fwhm_2(self, iPeak):
    fg = self.peaks[iPeak][peak_data.I_SIG_2] * 2.35482 / 2.
    if self.fit == voigt_eval:
      fl = self.peaks[iPeak][peak_data.I_GAMMA_2]
    else:
      fl = 0.
    return 0.5346 * fl + numpy.sqrt(0.2166 * (fl ** 2) + fg ** 2)

  def get_hkl(self, iPeak):
    return self.labels[iPeak]

  def get_a0(self, iPeak):
    (sp, hkl, aRef, p) = self.get_hkl(iPeak)[0]
    a0 = (peak_data.kalpha1 * numpy.sqrt((hkl * hkl).sum()) /
            2. / numpy.sin(self.get_theta_1(iPeak) * numpy.pi / 180.))
    return (a0, (a0 - aRef) / aRef * 100.)

  def get_peak_threshold(self):
    return self.peakThreshold

class peak_gui(gtk.HPaned):
  COL_LABEL = 0
  COL_ID    = 1
  
  def __init__(self):
    super(peak_gui, self).__init__()

    dpi = 72

    pane = gtk.VPaned()
    self.pack1(pane, True, True)
    
    fAll = matplotlib.figure.Figure(figsize=(533 / dpi, 300 / dpi), dpi = dpi)
    self.pAll = fAll.add_subplot(111)
    self.cAll = FigureCanvas(fAll)
    self.cAll.set_size_request(533, 300)
    self.cAll.mpl_connect('button_press_event', self.onZoom)
    pane.pack1(self.cAll, resize = True)

    hbox = gtk.HBox()
    pane.pack2(hbox)

    fZoom = matplotlib.figure.Figure(figsize=(400 / dpi, 300 / dpi), dpi = dpi)
    self.pZoom = fZoom.add_subplot(111)
    self.cZoom = FigureCanvas(fZoom)
    self.cZoom.set_size_request(400, 300)
    hbox.pack_start(self.cZoom, True, True)

    self.lbl = gtk.Label("")
    self.lbl.set_use_markup(True)
    self.lbl.set_alignment(0., 0.)
    self.lbl.set_selectable(True)
    self.lbl.set_size_request(133, 300)
    hbox.pack_end(self.lbl, False, False, 5)

    self.store = gtk.ListStore(gobject.TYPE_STRING,
                               gobject.TYPE_UINT)

    vbox = gtk.VBox()
    self.pack2(vbox, True, True)

    scroll = gtk.ScrolledWindow()
    scroll.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
    scroll.set_shadow_type(gtk.SHADOW_ETCHED_IN)
    scroll.set_size_request(350, -1)
    vbox.pack_start(scroll, True, True)
    
    self.view = gtk.TreeView(model = self.store)
    scroll.add(self.view)
    self.view.set_rules_hint(True)
    
    self.view.insert_column_with_attributes(0, "Id",
                                       gtk.CellRendererText(),
                                       text = peak_gui.COL_LABEL)
    render = gtk.CellRendererText()
    col = gtk.TreeViewColumn("θ1", render)
    col.set_cell_data_func(render, self.displayPeakInfo, (peak_data.get_theta_1, "%8.3f°"))
    self.view.insert_column(col, -1)
    render = gtk.CellRendererText()
    col = gtk.TreeViewColumn("θ2", render)
    col.set_cell_data_func(render, self.displayPeakInfo, (peak_data.get_theta_2, "%8.3f°"))
    self.view.insert_column(col, -1)
    render = gtk.CellRendererText()
    col = gtk.TreeViewColumn("θavg", render)
    col.set_cell_data_func(render, self.displayPeakInfo, (peak_data.get_theta_avg, "%8.3f°"))
    self.view.insert_column(col, -1)

    render = gtk.CellRendererText()
    col = gtk.TreeViewColumn("∫intensity", render)
    col.set_cell_data_func(render, self.displayPeakInfo, (peak_data.get_int_intensity, "%8.1f a.u."))
    self.view.insert_column(col, -1)

    render = gtk.CellRendererText()
    col = gtk.TreeViewColumn("FWHM1", render)
    col.set_cell_data_func(render, self.displayPeakInfo, (peak_data.get_fwhm_1, "%8.3f°"))
    self.view.insert_column(col, -1)
    render = gtk.CellRendererText()
    col = gtk.TreeViewColumn("FWHM2", render)
    col.set_cell_data_func(render, self.displayPeakInfo, (peak_data.get_fwhm_2, "%8.3f°"))
    self.view.insert_column(col, -1)
    render = gtk.CellRendererText()

    render = gtk.CellRendererText()
    col = gtk.TreeViewColumn("Miller index", render)
    col.set_cell_data_func(render, self.displayMiller)
    self.view.insert_column(col, -1)

    render = gtk.CellRendererText()
    col = gtk.TreeViewColumn("Lattice parameter", render)
    col.set_cell_data_func(render, self.displayPeakInfo, (peak_data.get_a0, "%8.4fÅ (%+2.2f%%)"))
    self.view.insert_column(col, -1)

    self.view.get_selection().set_mode(gtk.SELECTION_SINGLE)
    self.view.get_selection().connect("changed", self.onSelectPeak)

    hbox = gtk.HBox()
    vbox.pack_end(hbox, False, False)

    wd = gtk.Toolbar()
    wd.set_orientation(gtk.ORIENTATION_HORIZONTAL)
    wd.set_style(gtk.TOOLBAR_ICONS)
    wd.set_icon_size(gtk.ICON_SIZE_SMALL_TOOLBAR)
    hbox.pack_start(wd, True, True)

    self.peakThreshold = gtk.Entry()
    self.peakThreshold.set_width_chars(8)
    self.peakThreshold.connect("activate", self.onPeakThreshold)
    hbox.pack_end(self.peakThreshold, False, False)

    self.addBt(wd, "Save data as a column text file", gtk.STOCK_SAVE, self.onSave)
    self.addBt(wd, "Save fitted values as a column text file", gtk.STOCK_INDEX, self.onSaveFit)
    self.addBt(wd, "Save full curve as a PNG file", gtk.STOCK_SAVE_AS, self.onSaveAs)
    self.addBt(wd, "Save full curve as a PDF file", gtk.STOCK_PRINT, self.onExport)

  def addBt(self, toolbar, lbl, kind, callback):
    item = gtk.ToolButton(stock_id = kind)
    item.set_tooltip_text(lbl)
    item.connect("clicked", callback, lbl, kind)
    toolbar.insert(item, -1)

  def displayPeakInfo(self, col, render, model, iter, (getVal, fmt)):
    (iPeak, ) = model.get(iter, peak_gui.COL_ID)
    render.set_property("text", fmt % getVal(self.data, iPeak))

  def displayMiller(self, col, render, model, iter):
    (iPeak, ) = model.get(iter, peak_gui.COL_ID)
    hkls = self.data.get_hkl(iPeak)
    render.set_property("text", "%s %s" % (hkls[0][0], hkls[0][1]))

  def update_data(self, minIntensity = None):
    # We fit peaks.
    self.data.fit_all(minIntensity)
    # We plot it.
    self.data.plot_all(self.pAll)
    # We change the storage.
    self.store.clear()
    for iPeak in range(self.data.get_n_peaks()):
      self.store.set(self.store.append(), peak_gui.COL_LABEL, "Peak %d" % (iPeak + 1),
                     peak_gui.COL_ID, iPeak)

  def set_data(self, filename):
    self.filename = filename
    self.data = peak_data(filename, fitFunc)
    self.update_data()
    # Update interface.
    self.peakThreshold.set_text("%g" % self.data.get_peak_threshold())

  def onSelectPeak(self, selection):
    (model, iter) = selection.get_selected()
    if iter is None:
      return
    (iPeak, ) = model.get(iter, peak_gui.COL_ID)
    self.selectPeak(iPeak)

  def onZoom(self, event):
    iPeak = self.data.get_i_peak(event.xdata)
    it = self.store.iter_nth_child(None, iPeak)
    self.view.grab_focus()
    self.view.get_selection().select_iter(it)

  def onPeakThreshold(self, entry):
    try:
      threshold = float(self.peakThreshold.get_text())
    except:
      threshold = None
    self.update_data(threshold)
    self.cAll.draw()

  def selectPeak(self, iPeak):
    self.data.plot_zoom(self.pZoom, iPeak)
    self.cZoom.draw()
    # Update the label with info.
    info = ""
    if not self.data.dTheta == 0.:
      info += "<b>δθ</b> = <span color=\"red\">%8.5f°</span>\n\n" % self.data.dTheta
    info += "<b>θ<sub>1</sub></b> = %8.5f°\n\n" % self.data.get_theta_1(iPeak)
    info += "<b>θ<sub>2</sub></b> = %8.5f°\n\n" % self.data.get_theta_2(iPeak)
    info += "<b>θ<sub>avg</sub></b> = %8.5f°\n\n" % self.data.get_theta_avg(iPeak)
    info += "<b>∫intensity</b> =\n        %8.2f u.a.\n\n" % self.data.get_int_intensity(iPeak)
    info += "<b>FWHM<sub>1</sub></b> = %8.5f°\n\n" % self.data.get_fwhm_1(iPeak)
    info += "<b>FWHM<sub>2</sub></b> = %8.5f°\n\n" % self.data.get_fwhm_2(iPeak)
    info += "<b>Miller index</b> =\n"
    hkls = self.data.get_hkl(iPeak)
    if hkls[0][3] >= 90.:
      info += "        %s %s (%2.0f%%)\n\n" % (hkls[0][0], hkls[0][1], hkls[0][3])
    else:
      info += "        %s %s (%2.0f%%)\n"   % (hkls[0][0], hkls[0][1], hkls[0][3])
      info += "        %s %s (%2.0f%%)\n\n" % (hkls[1][0], hkls[1][1], hkls[1][3])
    info += "<b>a<sub>0</sub></b> = %8.5fÅ\n        (%+2.2f%%)" % self.data.get_a0(iPeak)
    self.lbl.set_markup(info)
    (a0, da) = self.data.get_a0(iPeak)
    ## print numpy.arcsin(peak_data.kalpha1 * numpy.sqrt((hkls[0][1] * hkls[0][1]).sum()) / 2. / (a0 * (1. - da/100.))) * 180. / numpy.pi * 2.

  def save(self, filename):
    f = open(filename, "w")
    f.write("# id  theta_1  theta_2  theta_avg  intensity  FWHM1    FWHM2      a0     stress\n")
    for iPeak in range(self.data.get_n_peaks()):
      f.write("  %2d" % (iPeak + 1))
      f.write("  %8.5f %8.5f %8.5f" % (self.data.get_theta_1(iPeak),
                                       self.data.get_theta_2(iPeak),
                                       self.data.get_theta_avg(iPeak)))
      f.write("  %8.2f " % self.data.get_int_intensity(iPeak))
      f.write("  %8.6f %8.6f" % (self.data.get_fwhm_1(iPeak),
                                 self.data.get_fwhm_2(iPeak)))
      f.write("  %8.5f %+2.2f" % self.data.get_a0(iPeak))
      hkls = self.data.get_hkl(iPeak)
      f.write("  # %s %s\n" % (hkls[0][0], hkls[0][1]))
    f.close()
    return False

  def saveFit(self, filename):
    self.data.save_all(filename)
    return False

  def exportPNG(self, filename):
    pp = backend_agg.FigureCanvasAgg(self.pAll.get_figure())
    pp.print_figure(filename, dpi = 200)
    return False

  def exportPDF(self, filename):
    pp = backend_pdf.PdfPages(filename)
    pp.savefig(self.pAll.get_figure())
    pp.close()
    return False

  def fileDialog(self, title, bt, name, mime, ext, func):
    export = None
    # Create a dialog to export.
    dialog = gtk.FileChooserDialog(title, None,
                                   gtk.FILE_CHOOSER_ACTION_SAVE,
                                   (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                                    bt, gtk.RESPONSE_OK))
    filter = gtk.FileFilter()
    filter.set_name(name)
    filter.add_mime_type(mime)
    dialog.add_filter(filter)
    filter = gtk.FileFilter()
    filter.set_name("All files")
    filter.add_pattern("*")
    dialog.add_filter(filter)
    dialog.set_current_folder(os.path.dirname(self.filename))
    dialog.set_current_name(os.path.basename(self.filename).replace(".xy", ext))
    response = dialog.run()
    if response == gtk.RESPONSE_OK:
      export = dialog.get_filename()
      manager = gtk.recent_manager_get_default()
      manager.add_item(dialog.get_uri())
    dialog.destroy()
    if export is not None:
      gobject.idle_add(func, export)

  def onSave(self, bt, title, stock):
    self.fileDialog(title, stock,
                    "Text file (*.txt, *.dat, ...)", "text/plain", ".dat", self.save)

  def onSaveFit(self, bt, title, stock):
    self.fileDialog(title, stock,
                    "Text file (*.txt, *.dat, ...)", "text/plain", ".dat", self.saveFit)

  def onSaveAs(self, bt, title, stock):
    self.fileDialog(title, stock,
                    "Portable network graphic (*.png)", "image/png", ".png", self.exportPNG)

  def onExport(self, bt, title, stock):
    self.fileDialog(title, stock,
                    "Portable document format (*.pdf)", "appliaction/pdf", ".pdf", self.exportPDF)

def build_window(pane):
  window = gtk.Window(gtk.WINDOW_TOPLEVEL)
  window.connect("delete_event", gtk.main_quit)
  window.connect("destroy", gtk.main_quit)

  window.add(pane)

  window.show_all()
  return window

if __name__ == "__main__":
  gui = peak_gui()
  gui.set_data(sys.argv[1])
  build_window(gui)

  gtk.main()
