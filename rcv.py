#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2012-2018 Damien Caliste <damien.caliste@cea.fr>,
#                         Thu Nhi Tran Thi <autumn_nhi@yahoo.com>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA

import re, os
import numpy
from scipy.optimize import leastsq
import edf
import multiprocessing
import queue
import time
from optparse import OptionParser, OptionGroup

class GaussianFit:
  def init(self, avg, mu):
    return (mu * numpy.sqrt(2. * numpy.pi)) * avg.ptp(), mu, avg.argmax(), avg.min()

  def evaluate(self, x, p):
    return p[3] + p[0] / (p[1] * numpy.sqrt(2. * numpy.pi)) * numpy.exp((x - p[2]) ** 2 / (-2. * (p[1] ** 2)))

  def fwhm(self, p):
    return p[1] * 2.35482

  def peakpos(self, p):
    return p[2] - 1.

  def amplitude(self, p):
    return p[0] / (p[1] * numpy.sqrt(2. * numpy.pi))

class DoubleGaussianFit(GaussianFit):
  def init(self, avg, mu):
    A = (mu * numpy.sqrt(2. * numpy.pi)) * avg.ptp()
    x0 = avg.argmax()
    return A, mu, x0, avg.min(), A, 5 * mu, x0

  def evaluate(self, x, p):
    return GaussianFit.evaluate(self, x, p) + GaussianFit.evaluate(self, x, list(p[4:]) + [0., ])

class LogNormalFit:
  def init(self, avg, mu):
    return (mu * numpy.sqrt(2. * numpy.pi)) * avg.ptp(), 0.1, numpy.log(avg.argmax()), avg.min()

  def evaluate(self, x, p):
    return p[3] + p[0] / (p[1] * x * numpy.sqrt(2. * numpy.pi)) * numpy.exp((numpy.log(x) - p[2]) ** 2 / (-2. * (p[1] ** 2)))

  def fwhm(self, p):
    f = numpy.sqrt(2. * numpy.log(2))
    return self.peakpos(p) * (numpy.exp(p[1] * f) - numpy.exp(- p[1] * f))

  def peakpos(self, p):
    return numpy.exp(p[2] - p[1] ** 2)

def calc_residuals(p, fit, x, y):
  return y - fit.evaluate(x, p)

class PixelIndexError(IndexError):
  pass

class TextData():
  def read(self, rcv, filename, store = False):
    f = open(filename, "rb")
    rcv.path = os.path.dirname(filename)
    rcv.dataOnDisk = filename

    # Read header
    # ===========
    reader = re.compile(r"(?P<key>[a-zA-Z_0-9]+)[^=]*=(?P<value>[^;]+);")
    line = f.readline()
    while not(line.endswith(b"}\n")):
      parsed = reader.match(line.decode('utf-8'))
      # angle stuffs
      if line.startswith(b"nAngles"):
        rcv.nAng = int(parsed.group("value").strip())
      if line.startswith(b"originAngles"):
        rcv.origAng = float(parsed.group("value").strip())
      if line.startswith(b"stepAngles"):
        rcv.stepAng = float(parsed.group("value").strip())
      # origin
      if line.startswith(b"origin "):
        (rcv.xStart, rcv.yStart) = map(int, parsed.group("value").split())
      # range
      if line.startswith(b"range"):
        rcv.shape = tuple(map(int, parsed.group("value").split()))
      # Misc
      if line.startswith(b"prefix"):
        rcv.prefix = parsed.group("value").strip()
      # Iterator
      line = f.readline()

    # Read image
    # ==========
    if store:
      rcv.data = numpy.fromfile(f, dtype = numpy.dtype('u2')).reshape(rcv.shape + (rcv.nAng, ))

    f.close()

class HDF5Data():
  def read(self, rcv, filename, grp = 1, detector = "lima_frelon2", motor = "dty", store = False):
    import h5py
    f = h5py.File(filename, "r")
    rcv.dataOnDisk = filename
    rcv.detector = detector
    rcv.motor = motor
    rcv.group = grp
    rcv.prefix = '.'.join(os.path.basename(filename).split('.')[:-1]) + "_scan%04d_" % grp
    groupId = "%d.1" % grp
    if groupId in f.keys() and "measurement" in f[groupId].keys():
      # Multi-scan file case.
      measure = f[groupId]["measurement"]
      rcv.h5dtset = measure[detector]
      rcv.xStart = 0
      rcv.yStart = 0
      if len(rcv.h5dtset.shape) > 2:
        rcv.shape = rcv.h5dtset.shape[1:]
        rcv.nAng = rcv.h5dtset.shape[0]
      else:
        rcv.shape = rcv.h5dtset.shape
        rcv.nAng = 1
      if motor in measure and len(measure[motor].shape) == 1:
        retry = True
        while (retry):
          try:
            rcv.rgAng = numpy.array(measure[motor])
            retry = False
          except OSError:
            print("retry", flush=True)
            time.sleep(1)
        if len(rcv.rgAng.shape) == 1 and len(rcv.rgAng) > 1:
          rcv.origAng = rcv.rgAng[0]
          rcv.stepAng = rcv.rgAng[1] - rcv.rgAng[0]
    elif "entry_0000" in f.keys() and "measurement" in f["entry_0000"].keys():
      # Single file case.
      rcv.h5dtset = f["entry_0000"]["measurement"]["data"]
      rcv.xStart = 0
      rcv.yStart = 0
      if len(rcv.h5dtset.shape) > 2:
        rcv.shape = rcv.h5dtset.shape[1:]
        rcv.nAng = rcv.h5dtset.shape[0]
      else:
        rcv.shape = rcv.h5dtset.shape
        rcv.nAng = 1
    else:
      raise KeyError
    if store:
      rcv.data = numpy.array(f.h5dtset)
      f.close()
    else:
      rcv.h5file = f

class PixelData():
  def __init__(self, orig, step, data, xpix, ypix):
    self.orig = orig
    self.step = step
    self.data = data

    self.xpix = xpix
    self.ypix = ypix

    self.integral = self.data.sum()
    self.maxint = self.data.max()
    self.fwhm = None
    self.peakpos = None

    self.func = None
    self.p0 = None
    self.p = None
    self.origFit = self.orig
    self.nFit = len(data)

  def fit(self, ptpMax = 7, func = GaussianFit(), start = 0, end = -1, **kwargs):
    self.func = func
    ptp = self.data[start:end].ptp()
    if ptp > ptpMax:
      fit = self.data[start:end].copy()
      # Remove peaks at 2 sigma
      #der = numpy.zeros(self.data.shape, dtype = numpy.float)
      #der[:-1] += self.data[1:] - self.data[:-1]
      #der[1:]  -= self.data[1:] - self.data[:-1]
      #sig = numpy.sqrt((der ** 2).sum() / der.size)
      #fit[numpy.abs(der) > 2. * sig] = 0

      mu = max(min(5., len(fit) / 10.), 0.5)
      # Look for the highest zone by doing a moving average
      n = max(min(int(3 * mu), int(len(fit) / 10)), 1)
      avg = numpy.cumsum(fit, dtype = numpy.float)
      avg[n:] = avg[n:] - avg[:-n]
      avg /= n
      avg = avg[n:-n]

      self.nFit = len(fit)
      self.origFit = self.orig + self.step * start
      x = numpy.arange(self.nFit) + 1
      self.p0 = func.init(avg, mu)
      self.p, info = leastsq(calc_residuals, self.p0, args = (func, x, fit), xtol = 5e-3, maxfev = 200)
      winStart = max(0, int(func.peakpos(self.p) - len(self.data) * 0.05))
      winStop  = min(len(fit), int(func.peakpos(self.p) + len(self.data) * 0.05))
      if (func.fwhm(self.p) / len(self.data) < 0.01 and winStop - winStart > 10):
        fit = fit[winStart:winStop]
        self.nFit = len(fit)
        self.origFit += self.step * (func.peakpos(self.p) - self.nFit / 2)
        x = numpy.arange(self.nFit) + 1
        self.p0 = (self.p[0], self.p[1], self.nFit / 2, self.p[3])
        self.p, info = leastsq(calc_residuals, self.p0, args = (func, x, fit), xtol = 5e-3, maxfev = 200)
      if numpy.isnan(self.p[1]):
        self.p[1] = 1.
      if numpy.isnan(self.p[2]):
        self.p[2] = 0.
    else:
      self.p0 = [0., 1., 0., 0.]
      self.p = self.p0
    if start != 0 or end != -1:
      self.integral = self.data[start:end].sum()
      self.maxint = self.data[start:end].max()
    self.fwhm = min(max(func.fwhm(self.p), 0), self.nFit) * abs(self.step)
    self.peakpos = self.origFit + self.step * min(max(func.peakpos(self.p), - 0.5 * self.nFit), 1.5 * self.nFit)

  def evaluate(self, angs = None):
    if angs is not None:
      x = (angs - self.origFit) / self.step + 1
    else:
      x = numpy.arange(self.nFit) + 1
    return self.func.evaluate(x, self.p)

  def plot(self, plt, withGuess = True):
    plt.cla()
    xprec = numpy.arange(0, self.nFit, self.nFit / 200.) + 1
    y2_gauss = None
    if self.p[1] == 0.:
      y_gauss = numpy.zeros(xprec.size)
    else:
      y_gauss = self.func.evaluate(xprec, self.p)
      if isinstance(self.func, DoubleGaussianFit):
        y2_gauss = GaussianFit.evaluate(self.func, xprec, self.p)
        y2_lbl = 'Output'
    if withGuess:
      if self.p0[1] == 0.:
        y0_gauss = numpy.zeros(xprec.size)
      else:
        y0_gauss = self.func.evaluate(xprec, self.p0)
    x = (numpy.arange(len(self.data)) * self.step + self.orig - self.peakpos) * plt.scalingFactor
    xprec = ((xprec - 1) * self.step + self.origFit - self.peakpos) * plt.scalingFactor
    if withGuess:
      plt.plot(x, self.data, '.-', xprec, y_gauss, xprec, y0_gauss, "--")
      plt.legend(['Data', 'Fit', 'Guess'])
    elif y2_gauss is not None:
      plt.plot(x, self.data, '.-', xprec, y_gauss, xprec, y2_gauss)
      plt.legend(['Data', 'Fit', y2_lbl])
    else:
      plt.plot(x, self.data, '.-', xprec, y_gauss)
      plt.legend(['Data', 'Fit'])
    if hasattr(plt, "set_title"):
      plt.set_title("Fit on rocking curve at %ux%u\n" % (self.xpix + 1, self.ypix + 1) +
                    "FWHM = %g'' ; ampl. = %g a.u." % (self.fwhm * 3600, self.func.amplitude(self.p)))
    if hasattr(plt, "set_xlabel"):
      unit = "a.u."
      if plt.scalingFactor == 1.:
        unit = "degress"
      elif plt.scalingFactor == 3600.:
        unit = "arcseconds"
      plt.set_xlabel(r'Angle $\omega$ (%s), ref. at %g degrees' % (unit, self.peakpos))
      plt.set_ylabel('Intensity (a.u.)')

class RockingCurve():
  def __init__(self):
    self.path       = "."
    self.prefix     = ""
    self.nAng       = 0
    self.stepAng    = 1.
    self.origAng    = 0.
    self.rgAng      = None
    self.xStart     = 0
    self.yStart     = 0
    self.shape      = None
    self.imgs       = []
    self.data       = None
    self.dataOnDisk = None
    self.cachedY    = None
    self.cachedData = None

  def setFromList(self, edfList, xStart = 0, yStart = 0, width = None, height = None, infoFunc = None):
    if self.data is not None:
      raise ValueError
    prefix = []
    path   = "."
    for data in edfList:
      path   = data.path
      prefix.append(data.prefix)
      if infoFunc is not None:
        infoFunc("gathering pixels from run %04d for rocking curve (ystart = %d)" % (data.run, yStart))
      w = data.width
      if width is not None:
        w = width
      h = data.height
      if height is not None:
        h = height
      self.add(data.getData(flat = False).T[xStart:xStart + w, yStart:yStart + h])
    self.finalise(xStart, yStart, path, os.path.commonprefix(prefix))

  def add(self, intensity):
    if (self.shape is not None and not(intensity.shape == self.shape)):
      raise TypeError
    elif (self.shape is None):
      self.shape = intensity.shape
    self.nAng += 1
    self.imgs.append(numpy.array(intensity))

  def finalise(self, xStart = 0, yStart = 0, path = ".", prefix = ""):
    self.path   = path
    self.prefix = prefix
    self.xStart = xStart
    self.yStart = yStart
    n = len(self.imgs)
    self.data = numpy.vstack(self.imgs).reshape((n, ) + self.shape).transpose((1,2,0))
    self.imgs = []

  def export(self, filename, empty = True):
    self.dataOnDisk = filename
    f = open(filename, "wb")
    out = ""
    out += "Rocking Curve:\n"
    out += "{\n"
    out += "nAngles      = %d;\n" % self.nAng
    out += "originAngles = %g;\n" % self.origAng
    out += "stepAngles   = %g;\n" % self.stepAng
    out += "origin       = %d %d;\n" % (self.xStart, self.yStart)
    out += "range        = %d %d;\n" % self.shape
    out += "prefix       = %s ;\n" % self.prefix
    out += "dir          = %s ;\n" % os.path.abspath(os.path.join(os.curdir, self.path))
    out += "}\n"
    f.write(out.encode())
    f.write(self.data.astype(numpy.dtype('u2')).tobytes())
    f.close()
    if empty:
      self.data = None

  def read(self, filename, grp = 1, store = False, **kwargs):
    if filename.endswith(".rcv"):
      reader = TextData()
      reader.read(self, filename, store)
    elif filename.endswith(".h5"):
      reader = HDF5Data()
      reader.read(self, filename, grp = grp, store = store, **kwargs)

  def close(self):
    if hasattr(self, "h5file"):
      self.h5file.close()

  def load(self, filename):
    print("# loading RCV", self.dataOnDisk)
    f = open(filename, "rb")

    # Skip header
    # ===========
    line = f.readline()
    while not(line.endswith(b"}\n")):
      # Iterator
      line = f.readline()

    # Read image
    # ==========
    data = numpy.fromfile(f, dtype = numpy.dtype('u2')).reshape(self.shape + (self.nAng, ))
    f.close()

    return data

  def getData(self):
    data = self.data
    if self.dataOnDisk is not None and data is None:
      # Read the data on the fly
      data = self.load(self.dataOnDisk)
    if data is None:
      raise ValueError
    return data

  def getDataAt(self, xpix, ypix):
    if xpix - self.xStart < 0 or ypix - self.yStart < 0:
      raise PixelIndexError
    if xpix - self.xStart >= self.shape[0] or ypix - self.yStart >= self.shape[1]:
      raise PixelIndexError
    if hasattr(self, "h5dtset"):
      dy = 25
      if self.cachedY is not None and self.cachedY <= ypix and self.cachedY + dy > ypix:
        return self.cachedData[:, ypix - self.cachedY, xpix]
      retry = True
      while (retry):
        try:
          self.cachedData = self.h5dtset[:, ypix:min(ypix + dy, self.shape[1]), :]
          self.cachedY = ypix
          return self.cachedData[:, 0, xpix]
        except OSError as e:
          print(e)
          print("try harder for %dx%d" % (xpix, ypix), flush=True)
          time.sleep(1)
    else:
      return self.getData()[xpix - self.xStart, ypix - self.yStart, :]

  def getAt(self, xpix, ypix):
    return PixelData(self.origAng, self.stepAng,
                     self.getDataAt(xpix, ypix).astype(numpy.int), xpix, ypix)

  def getMap(self, xr = None, yr = None, infoFunc = None, startMap = None, endMap = None, **kwargs):
    if yr is None:
      yr = numpy.arange(self.yStart, self.yStart + self.shape[1])
    else:
      yr = numpy.array(yr)
      if yr.min() < self.yStart or yr.max() >= (self.yStart + self.shape[1]):
        raise PixelIndexError
    if xr is None:
      xr = numpy.arange(self.xStart, self.xStart + self.shape[0])
    else:
      xr = numpy.array(xr)
      if xr.min() < self.xStart or xr.max() >= (self.xStart + self.shape[0]):
        raise PixelIndexError
    mp = numpy.zeros((len(xr), len(yr), 4), dtype = numpy.double)
    # We store the data for the run of this rc file.
    if not(hasattr(self, "h5dtset")):
      self.data = self.getData()
    for j, y in enumerate(yr):
      start = time.time()
      for i, x in enumerate(xr):
        #if infoFunc is not None:
        #  infoFunc("getData(%d,%d)" % (x, y))
        pix = self.getAt(x, y)
        #if infoFunc is not None:
        #  infoFunc("getData(%d,%d) done" % (x, y))
        if startMap is not None:
          kwargs["start"] = startMap[j, i]
        if endMap is not None:
          kwargs["end"] = endMap[j, i]
        pix.fit(**kwargs)
        mp[x - xr[0], y - yr[0]] = (pix.integral, pix.fwhm, pix.peakpos, pix.maxint)
      if infoFunc is not None:
        infoFunc("Calculating maps for line %04d (%6.2f pixels per second)" % (y, float(len(xr)) / (time.time() - start)))
    # We empty the rc data.
    self.data = None
    return mp.transpose(1,0,2)

class EDFSet():
  def __init__(self):
    self.lst = []

  def add(self, raw):
    if not(type(raw) == type(edf.RawImage())):
      raise TypeError
    self.lst.append(raw)

  def calcRC(self, xStart = 0, yStart = 0, width = None, height = None, step = 64, nproc = 1, infoFunc = None, kind = "raw"):
    if len(self.lst) == 0:
      return []
    
    if width is None:
      width = self.lst[0].width
    if height is None:
      height = self.lst[0].height
    scan = self.lst[0].dict.get("scan", "").split()
    if len(scan) > 4:
      angOrig = float(scan[2])
      angStep = (float(scan[3]) - float(scan[2])) / float(scan[4])
    else:
      raise ValueError("No scan definition.")
    if nproc is None or nproc > 1:
      q = multiprocessing.Queue()
      pool = multiprocessing.Pool(processes = nproc,
                                  initializer = rcSlabInit, initargs = (q, ))
      rcvs = pool.map_async(calcRcSlab, [(self.lst, xStart, i, width,
                                          min(yStart + height - i, step),
                                          angOrig, angStep, kind)
                                         for i in range(yStart, yStart + height, step)])
      nData = 0
      while not(rcvs.ready()):
        try:
          val = q.get(block = True, timeout = 1)
          frac = None
          if val.startswith("gathering pixels from run"):
            nData += 1
            frac = float(nData) / (len(self.lst) * len(range(yStart, yStart + height, step)))
            val += " (%6.2f%% done)" % (frac * 100.)
          if infoFunc is not None:
            infoFunc(val, fraction = frac)
          else:
            print(val)
        except queue.Empty:
          continue
      pool.close()
      rcvs = rcvs.get()
    else:
      rcvs = []
      calcRcSlab.q = None
      for i in range(yStart, yStart + height, step):
        rocks = calcRcSlab((self.lst, xStart, i, width, min(yStart + height - i, step),
                            angOrig, angStep, kind))
        rcvs.append(rocks)
    return rcvs

def rcSlabInit(q):
  calcRcSlab.q = q

class RockingCurveSet():
  def __init__(self):
    self.lst = []

  def add(self, rc):
    if not(type(rc) == type(RockingCurve())):
      raise TypeError
    self.lst.append(rc)

  def getAt(self, xpix, ypix):
    for rc in self.lst:
      try:
        return rc.getAt(xpix, ypix)
      except PixelIndexError:
        pass
    raise PixelIndexError

  def getMap(self, xr = None, yr = None, **kwargs):
    for rc in self.lst:
      try:
        return rc.getMap(xr, yr, **kwargs)
      except PixelIndexError:
        pass
    raise PixelIndexError

  def calcMap(self, infoFunc = None, np = None, **kwargs):
    # Calculate map size.
    w = 0
    h = 0
    for rc in self.lst:
      w = max(w, rc.xStart + rc.shape[0])
      h = max(h, rc.yStart + rc.shape[1])
    # We calculate
    mp = numpy.zeros(w * h * 4, dtype = numpy.double).reshape((h, w, 4))
    start = time.time()
    if (np is None or np > 1):
      q = multiprocessing.Queue()
      if len(self.lst) > 1:
        pool = multiprocessing.Pool(processes = np,
                                    initializer = rcInit,
                                    initargs = (q, kwargs))
        a = pool.map_async(rcGetMap, self.lst)
      elif hasattr(self.lst[0], "h5dtset"):
        pool = multiprocessing.Pool(processes = np,
                                    initializer = newrcInit,
                                    initargs = (q, self.lst[0].dataOnDisk, self.lst[0].group, self.lst[0].detector, self.lst[0].motor, kwargs))
        a = pool.map_async(newrcGetMap, range(self.lst[0].shape[1]))
      line = 0
      while not(a.ready()):
        try:
          val = q.get(block = True, timeout = 1)
          frac = None
          if val.startswith("Calculating maps for line"):
            line += 1
            frac = float(line) / float(h)
            timeToCompletion = int((time.time() - start) * (1. - frac) / frac / 60.)
            val += " (%6.2f%% done, %d min. remain)" % (frac * 100.,
                                                        timeToCompletion)
          if infoFunc is not None:
            infoFunc(val, fraction = frac)
          elif line % (h / 20) == 0:
            print(val, flush=True)
        except queue.Empty:
          continue
      pool.close()
      if len(self.lst) > 1:
        for (rc, mploc) in zip(self.lst, a.get()):
          mp[rc.yStart:rc.yStart + rc.shape[1],
             rc.xStart:rc.xStart + rc.shape[0]] = mploc
      elif hasattr(self.lst[0], "h5dtset"):
        for (y, mploc) in zip(range(self.lst[0].shape[1]), a.get()):
          if numpy.max(mploc) == 0.:
            if infoFunc is not None:
              infoFunc("Retrying line %d" % y)
            else:
              print("Retrying line %d" % y, flush=True)
            try:
              rcv = RockingCurve()
              rcv.read(self.lst[0].dataOnDisk, self.lst[0].group,
              detector = self.lst[0].detector, motor = self.lst[0].motor)
              mploc = rcv.getMap(yr = (y, ), ptpMax = ptpMax, fitFunc = fitFunc)
              rcv.close()
            except:
              if infoFunc is not None:
                infoFunc("Failure line %d" % y)
              else:
                print("Failure line %d" % y, flush=True)
              mploc = numpy.zeros((1, rc.shape[0], 4), dtype = numpy.double)
          if infoFunc is not None:
            infoFunc("Assembling line %d (%s)" % (y, str(numpy.max(mploc, (0, 1)))))
          mp[y, rc.xStart:rc.xStart + rc.shape[0]] = mploc
    else:
      for rc in self.lst:
        mploc = rc.getMap(infoFunc = infoFunc)
        mp[rc.yStart:rc.yStart + rc.shape[1],
           rc.xStart:rc.xStart + rc.shape[0], :] = mploc
    if infoFunc is not None:
      infoFunc("mapping speed: %6.2f pixels per minute" % (float(60 * h * w) / (time.time() - start)))
    rg = ""
    rgDict = {}
    if "start" in kwargs or "end" in kwargs:
      start = int(kwargs.get("start", "0"))
      end = int(kwargs.get("end", "-1"))
      if start != 0 or end != -1:
        rg = "_%04dx%04d" % (start, end)
      if start != 0:
        rgDict["startIndex"] = start
      if end != 0:
        rgDict["endIndex"] = end
    if "startMapFile" in kwargs or "endMapFile" in kwargs:
      start = kwargs["startMapFile"] if kwargs["startMapFile"] is not None else ""
      if "." in start:
        start = ".".join(start.split(".")[:-1])
      end = kwargs["endMapFile"] if kwargs["endMapFile"] is not None else ""
      if "." in end:
        end = ".".join(end.split(".")[:-1])
      if start != "" and end != "":
        rg = "_%s_%s" % (start, end)
      elif start != "":
        rg = "_from_%s" % start
      elif end != "":
        rg = "_to_%s" % end
      if start != "":
        rgDict["startMap"] = start
      if end != "":
        rgDict["endMap"] = end
    integral = edf.RawImage()
    integral.set(mp[:, :, 0])
    integral.path   = self.lst[0].path
    integral.prefix = self.lst[0].prefix + "integral" + rg
    integral.dict.update(rgDict)
    integral.store(kind = "map")
    fwhm = edf.RawImage()
    fwhm.set(mp[:, :, 1])
    fwhm.path   = self.lst[0].path
    fwhm.prefix = self.lst[0].prefix + "FWHM" + rg
    fwhm.dict.update(rgDict)
    fwhm.store(kind = "map")
    peakpos = edf.RawImage()
    peakpos.set(mp[:, :, 2])
    peakpos.path   = self.lst[0].path
    peakpos.prefix = self.lst[0].prefix + "peakpos" + rg
    peakpos.dict.update(rgDict)
    peakpos.store(kind = "map")
    maxint = edf.RawImage()
    maxint.set(mp[:, :, 3])
    maxint.path   = self.lst[0].path
    maxint.prefix = self.lst[0].prefix + "maxint" + rg
    maxint.dict.update(rgDict)
    maxint.store(kind = "map")
    return (integral, fwhm, peakpos, maxint)

  def dump(self):
    # Calculate map size.
    w = 0
    h = 0
    for rc in self.lst:
      w = max(w, rc.xStart + rc.shape[0])
      h = max(h, rc.yStart + rc.shape[1])
    rc = self.lst[0]
    omegas = numpy.linspace(rc.origAng, rc.origAng + (rc.nAng - 1) * rc.stepAng, num = rc.nAng)
    for j in range(h):
      for i in range(w):
        data = self.getDataAt(i, j)
        with open("pix_%04dx%04d.dat" % (i, j), "w") as f:
          for vals in zip(omegas, data):
            f.write("%g %g\n" % vals)

def prof(r, n = 100):
  for i in range(n):
    r.get(i % 4, 1)

def rcInit(q, kwargs):
  rcGetMap.q = q
  rcGetMap.kwargs = kwargs

def rcGetMap(rc):
  #print "Get map for rc", rc
  return rc.getMap(infoFunc = rcGetMap.q.put, **rcGetMap.kwargs)

def newrcInit(q, filename, group, detector, motor, kwargs):
  newrcGetMap.q = q
  newrcGetMap.filename = filename
  newrcGetMap.group = group
  newrcGetMap.detector = detector
  newrcGetMap.motor = motor
  newrcGetMap.kwargs = kwargs

def newrcGetMap(y):
  startMap = newrcGetMap.kwargs.get("startMapFile", "")
  if startMap is not None and startMap != "":
    newrcGetMap.kwargs["startMap"] = edf.RawImage().read(startMap).getData(flat = False)[y:y+1, :]
    
  endMap = newrcGetMap.kwargs.get("endMapFile", "")
  if endMap is not None and endMap != "":
    newrcGetMap.kwargs["endMap"] = edf.RawImage().read(endMap).getData(flat = False)[y:y+1, :]
  import traceback
  rcv = RockingCurve()
  iRetry = 0
  while (iRetry < 60):
    try:
      rcv.read(newrcGetMap.filename, newrcGetMap.group,
               detector = newrcGetMap.detector, motor = newrcGetMap.motor)
      if numpy.isnan(rcv.stepAng) or numpy.isnan(rcv.origAng):
        raise ValueError("reading dty failed")
      out = rcv.getMap(yr = (y, ), infoFunc = newrcGetMap.q.put, **newrcGetMap.kwargs)
      rcv.close()
      return out
    except Exception as e:
      newrcGetMap.q.put(traceback.format_exc())
      rcv.close()
      newrcGetMap.q.put("re-reading for %d" % y)
      iRetry += 1
      time.sleep(1)
  newrcGetMap.q.put("Error reading line %d." % y)
  return numpy.zeros((1, 2048, 4), dtype = numpy.double)

def parse():
  parser = OptionParser("usage: rcv.py [options] [files]")
  parser.add_option("-g", "--group", dest="group", type="int", default = "1",
                      help="select the i-th group in the file.")
  parser.add_option("-c", "--curves", dest = "curves", action="store_true",
                      help="compute the rocking curves")
  parser.add_option("-o", "--angle-origin", dest = "angOrig", type="float",
                      default = 0.,
                      help="angle starting value")
  parser.add_option("-s", "--angle-step", dest = "angStep", type="float",
                      default = 1e-4,
                      help="angle stepping value")
  parser.add_option("-m", "--maps", dest = "maps", action="store_true",
                      help="compute maps from rocking curves")
  parser.add_option("-n", "--n-threads", dest = "np", type="int", default = None,
                      help="number of threads to compute maps")
  parser.add_option("-d", "--dump", dest = "dump", action="store_true",
                      help="dump rocking curve data per pixels")
  parser.add_option("-r", "--rocking-curve", dest = "rc", type = "string",
                      help="dump rocking curve data for the given pixel (x:y)")
  parser.add_option("", "--detector", dest="detector", type="string",
                    help="name of the detector")
  parser.add_option("", "--motor", dest="motor", type="string",
                    help="name of the rocking angle motor")
  parser.add_option("", "--start", dest="start", type="int", default = "0",
                    help="restrict fit range for angles after the start-th one")
  parser.add_option("", "--end", dest="end", type="int", default = "-1",
                    help="restrict fit range for angles before the end-th one")
  parser.add_option("", "--start-map", dest="startMap", type="string",
                    help="restrict fit range for angles per pixel")
  parser.add_option("", "--end-map", dest="endMap", type="string",
                    help="restrict fit range for angles per pixel")
  
  return parser

def calcRcSlab(args):
  (datas, xStart, yStart, width, height, angOrig, angStep, lbl) = args
  rocks = RockingCurve()
  if calcRcSlab.q is None:
    rocks.setFromList(datas, xStart, yStart, width, height)
  else:
    rocks.setFromList(datas, xStart, yStart, width, height, calcRcSlab.q.put)
  rocks.stepAng = angStep
  rocks.origAng = angOrig
  rocks.export(os.path.join(rocks.path,
                            "%s%04d_%s.rcv" % (rocks.prefix, yStart, lbl)),
               empty = True)
  return rocks

def message(mess, fraction = None):
  print(mess, flush=True)
  
if __name__ == "__main__":
  import sys

  parser = parse()
  (options, args) = parser.parse_args()

  if options.curves is not None:
    edfList = []
    for entry in args:
      data = edf.RawImage()
      data.read(entry)
      edfList.append(data)

    step = 64
    height = edfList[0].height
    pool = multiprocessing.Pool(processes = None)
    pool.map(calcRcSlab, [(edfList, i, min(height - i, step),
                             options.angOrig, options.angStep, "raw")
                            for i in range(0, height, step)])
    pool.close()
    sys.exit(0)

  if options.maps:
    r = RockingCurveSet()
    for entry in args:
      rcv = RockingCurve()
      rcv.read(entry, grp = options.group, detector = options.detector, motor = options.motor)
      r.add(rcv)
    (mpInt, mpFwhm, mpPeak, mpMax) = r.calcMap(infoFunc = message,
                                               np = options.np,
                                               start = options.start,
                                               end = options.end,
                                               startMapFile = options.startMap,
                                               endMapFile = options.endMap)
    mpInt.save(os.path.join(mpInt.path, "%s_map.edf" % mpInt.prefix))
    mpFwhm.save(os.path.join(mpFwhm.path, "%s_map.edf" % mpFwhm.prefix))
    mpPeak.save(os.path.join(mpPeak.path, "%s_map.edf" % mpPeak.prefix))
    mpMax.save(os.path.join(mpMax.path, "%s_map.edf" % mpMax.prefix))
    sys.exit(0)

  if options.dump:
    r = RockingCurveSet()
    for entry in args:
      rcv = RockingCurve()
      rcv.read(entry, grp = options.group,
               detector = options.detector, motor = options.motor)
      r.add(rcv)
    r.dump()
    sys.exit(0)

  if options.rc is not None:
    x, y = map(int, options.rc.split(":"))
    r = RockingCurveSet()
    for entry in args:
      rcv = RockingCurve()
      rcv.read(entry, grp = options.group,
               detector = options.detector, motor = options.motor)
      r.add(rcv)
    pix = r.getAt(x, y)
    angs = pix.orig + numpy.arange(len(pix.data)) * pix.step
    if options.start != 0 or options.end != -1:
      pix.fit(start = options.start, end = options.end)
    elif options.startMap is not None or options.endMap is not None:
      start = 0
      if options.startMap is not None:
        start = edf.RawImage().read(options.startMap).getData(flat = False)[y, x]
      end = -1
      if options.endMap is not None:
        end = edf.RawImage().read(options.endMap).getData(flat = False)[y, x]
      pix.fit(start = start, end = end)
    for vals in zip(angs, pix.data, pix.evaluate(angs)):
      print("%g %g %g" % vals)
    sys.exit(0)

  r = RockingCurve()
  r.read(sys.argv[1], grp = options.group,
         detector = options.detector, motor = options.motor)
  #for v in r.getDataAt(1219, 1152):
  #  print v
  sys.exit(0)

  data = RockingCurve()
  p0 = (50, 8.45, 80, 100)
  ref = gauss_eval(numpy.arange(200), p0)
  
  for i in range(200):
    vals = (ref[i] + numpy.arange(16).reshape((4,4))) + 20. * (numpy.random.rand(4,4) - 0.5)
    data.add(vals)
      
  data.finalise()
  data.export("out_test.rcv")

  r = RockingCurve()
  r.read("out_test.rcv")
  #print r.nAng
  #print r.xStart, r.yStart
  #print r.shape

  ## import cProfile
  ## cProfile.run("prof(r, 2048)")

  rcs = RockingCurveSet()
  for i in range(16):
    r = RockingCurve()
    r.read("out_test.rcv")
    r.xStart = 4 * i
    r.yStart = 4 * i
    rcs.add(r)
  mp = rcs.calcMap(np = 4)
  mp.save("map.edf")

  ## import matplotlib.pyplot as plt
  ## fwhm = r.get(2, 1, plot = plt)
  ## print fwhm
  ## plt.show()

  

