#!/usr/bin/env python

import sys, os
import xrayutilities as xu

filename = sys.argv[1]
h5file = filename.replace(".spec", ".h5")
if not os.path.isfile(h5file):
  s = xu.io.SPECFile(os.path.basename(filename), path = os.path.dirname(filename))
  s.Update()
  s.Save2HDF5(h5file)
