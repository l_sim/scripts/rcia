#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013 Marie-Ingrid Richard <marie-ingrid.richard@im2np.fr>,
#                    Gilbert Chahine <chahine@esrf.fr>
#
#       Contributors Damien Caliste <damien.caliste@cea.fr>
#                    Thu Nhi Tran Thi <thu-nhi.tran-thi@esrf.fr>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA

import sys, os, re
import edf
import matplotlib
import xrutils as xu #linux
import spec
#import xrayutilities as xu #windows
import matplotlib as mpl
import matplotlib.pyplot as plt
#from mayavi import mlab #if MayaVi is installed
import scipy, numpy, pylab
from numpy import *
from pylab import *
from scipy import *
from scipy.optimize import leastsq
import matplotlib.pyplot as plt
import scipy.signal
import time
from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib import rc, font_manager
from numpy import arange, cos, pi
from matplotlib.pyplot import figure, axes, plot, xlabel, ylabel, title, grid, savefig, show

#==============================================================================
# gaussian fit
#==============================================================================
gauss_fit = lambda p, x: p[0]*(1/sqrt(2*pi*(p[2]**2)))*exp(-(x-p[1])**2/(2*p[2]**2)) #1d Gaussian func
e_gauss_fit = lambda p, x, y: (gauss_fit(p,x) - y) #1d Gaussian fit

#==============================================================================
# linear fit
#==============================================================================
linear_fit = lambda p, x: p[0]*x + p[1]
e_linear_fit = lambda p, x, y: (linear_fit(p,x) - y)

class PScan:
    motors = {"pix": "adcX", "piy": "adcY", "piz": "adcZ"}
    def __init__(self, vals):
        self.x_min = float(vals[3])
        self.x_max = float(vals[4])
        self.nb_points_1 = int(vals[5])
        self.motor2 = vals[6]
        self.y_min = float(vals[7])
        self.y_max = float(vals[8])
        self.nb_points_2 = int(vals[9])
        self.step_time = double(vals[10])

        self.XX = d[PScan.motors[vals[2]]]
        self.YY = d[PScan.motors[vals[6]]]

        pix1 = zeros((nb_points_1+20,nb_points_2+20))
        piy1 = zeros((nb_points_1+20,nb_points_2+20))

class XAScan:
    def __init__(self, h, d):
        vars = {var.lower(): val for (var, val) in zip(h['O'], h['P'])}
        
        vals = h['S'].split()
        if not(vals[2] in d):
            raise ValueError("motor '%s' is not evolving." % vals[2])

        if "imgnr" in d:
            self.number = "imgnr"
        elif "mpx4inr" in d:
            self.number = "mpx4inr"
        self.motor = vals[2]
        self.length = len(d[self.motor])
        self.varying = d
        self.constants = vars

def scan_spec(spec_file, scanId, queue = None):
    h, d = spec.ReadSpec(spec_file, scanId)
    vars = {var.lower(): val for (var, val) in zip(h['O'], h['P'])}
    vals = h['S'].split()
    if vals[1] == "pscando":
        scan = PScan(vals)
    elif vals[1] == "xascan":
        scan = XAScan(h, d)
    if queue is not None:
        queue.put(scanId, block = False)
    return scan

def padArray(arr, length):
    return [float(arr[i % len(arr)]) for i in range(length)]

def mat_from_specs(spec_file, scanIds, queue = None):
    mat = {}
    if queue is not None:
        queue.put(0, block = False)
    scans = [scan_spec(spec_file, scanId, queue) for scanId in scanIds]
    ref = scans[0]
    if isinstance(ref, XAScan):
        mat["rcLength"] = numpy.array([s.length for s in scans], dtype = numpy.int).reshape(len(scanIds),1)
        maxLength = mat["rcLength"].max()
        for key in ref.constants.keys():
            mat[key] = numpy.array([float(s.constants[key]) for s in scans]).reshape(len(scanIds),1,1)
        for key in ref.varying.keys():
            mat[key] = numpy.array([padArray(s.varying[key], maxLength) for s in scans]).reshape(len(scanIds),1,maxLength)
        # Do an average on motor values.
        mat[ref.motor + "-avg"] = sum(mat[ref.motor], 0).reshape(maxLength) / len(scanIds)
        # Reconstruct square from mu, nu.
        mat['X'] = mat['mu']
        mat['Y'] = mat['nu']
        mat['number'] = mat[ref.number]
        mat['type'] = "XAScan"

    return mat

#==============================================================================
# k_mapping (for one scan) in the case of 2D scans: pscando pix 1 50 100 piy 1 50 100 .02
# remove retracing
#==============================================================================
def k_mapping(spec_file,scan,index_left = None):
    h,d=spec.ReadSpec(spec_file,scan)
    headers = {}
    headers['M']=['delta','eta','chi','phi','mu','nu','thx','thy','thz']
    vars = {var.lower(): val for (var, val) in zip(h['O'], h['P'])}
    headers['V']=[vars.get('delta', vars['del']),
                  vars.get('eta', None),
                  vars.get('chi', None),
                  vars.get('phi', None),
                  vars.get('mu', None),
                  vars.get('nu', None),
                  vars['thx'], vars['thy'], vars['thz']] #return values of header
    vals = h['S'].split()
    motor_1 = vals[2]
    x_min = float(vals[3])
    x_max = float(vals[4])
    nb_points_1 = int(vals[5]) 

    motor_2 = vals[6]
    y_min = float(vals[7])
    y_max = float(vals[8])			    
    nb_points_2 = int(vals[9])
    step_time = double(vals[10])
    
    if (motor_1=='pix'):
        XX = d['adcX']
    if (motor_1=='piy'):
        XX = d['adcY']
    if (motor_1=='piz'):
        XX = d['adcZ']
    if (motor_2=='pix'):
        YY = d['adcX']
    if (motor_2=='piy'):
        YY = d['adcY']
    if (motor_2=='piz'):
        YY = d['adcZ']
        
    pix1 = zeros((nb_points_1+20,nb_points_2+20))
    piy1 = zeros((nb_points_1+20,nb_points_2+20))
    pix1[0,0] = XX[0]
    piy1[0,0] = YY[0]  

    arr = {}
    for k in ('mpx4int', 'ccdint2', 'roi1', 'roi2', 'roi3',
              'roi4', 'roi5', 'imgnr'):
        if k in d:
            v = d[k]
            if k != 'imgnr':
                v = d[k] / step_time
            a = zeros((nb_points_1+20,nb_points_2+20))
            a[0, 0] = v[0]
            arr[k] = (a, v)

    #remove retracing
    if (XX.ptp() / nb_points_1) > 0.7:
        y = 0
        forward = True
        i0 = 1
        for i in range(1, len(XX) - 1):
            f = XX[i] < XX[i + 1] and XX[i - 1] < XX[i]
            if forward and not(f):
                pix1[:i - i0, y] = XX[i0:i]
                piy1[:i - i0, y] = YY[i0:i]
                for (k, (a, v)) in arr.items():
                    a[:i - i0, y] = v[i0:i]
            if not(forward) and f:
                i0 = i
                y += 1
            forward = f
    else:
        y = 0
        forward = True
        i0 = 3
        for i in range(3, len(XX) - 3):
            f = XX[i] < XX[i + 3] and XX[i] < XX[i + 2] and XX[i] < XX[i + 1] and XX[i - 1] < XX[i] and XX[i - 2] < XX[i] and XX[i - 3] < XX[i]
            if forward and not(f):
                pix1[:i - i0, y] = XX[i0:i]
                piy1[:i - i0, y] = YY[i0:i]
                for (k, (a, v)) in arr.items():
                    a[:i - i0, y] = v[i0:i]
            if not(forward) and f:
                i0 = i
                y += 1
            forward = f
        
    matrix = {}
    if index_left is None: 
        index_left = nb_points_1#where(ccd1[0:100,1:100]==0)[0].min()
    matrix['size']=[[x_min,x_max],[y_min,y_max]]      
    matrix['X']=pix1[0:index_left,0:nb_points_2]
    matrix['Y']=piy1[0:index_left,0:nb_points_2]
    for (k, (a, v)) in arr.items():
        matrix[k] = a[0:nb_points_1, 0:nb_points_2]
	
    #print "eta = {0}".format (eta)
    ## for (X, Y, I) in zip(matrix['X'], matrix['Y'], matrix['ccdint2']):
    ##     for (x, y, i) in zip(X, Y, I):
    ##         print x, y, i
    ##     print
    ## gui
    if "F" in h:
        path = os.path.basename(os.path.normpath(re.match("^dir\[([\w/]+)\]$", h["F"][0]).group(1)))
        f = re.match("^prefix\[(\w+)\]$", h["F"][1]).group(1) + \
            "%04d" + \
            re.match("^suffix\[([\w.]+)\]$", h["F"][3]).group(1)
        headers["F"] = os.path.join(path, f)
    return headers,matrix,index_left



#==============================================================================
# create 3D matrices   
#==============================================================================    
def k_mapping_3D(spec_file,scan,outfile = None, queue = None):
    delta = numpy.zeros((len(scan)))
    eta = numpy.zeros((len(scan)))
    chi = numpy.zeros((len(scan)))
    phi = numpy.zeros((len(scan)))
    mu = numpy.zeros((len(scan)))
    nu = numpy.zeros((len(scan)))
    thx = numpy.zeros((len(scan)))
    thy = numpy.zeros((len(scan)))
    thz = numpy.zeros((len(scan)))
    
    if queue is not None:
        queue.put(0, block = False)
    h,m,index_left = k_mapping(spec_file,scan[0])
    
    len1 = shape(m['X'])[0]
    len2 = shape(m['X'])[1]
    X = zeros((len1,len2,len(scan)))    
    Y = zeros((len1,len2,len(scan)))
    ccdint1 = zeros((len1,len2,len(scan)))    
    ccdint2 = zeros((len1,len2,len(scan)))       
    roi1 = zeros((len1,len2,len(scan)))    
    roi2 = zeros((len1,len2,len(scan)))
    roi3 = zeros((len1,len2,len(scan)))    
    roi4 = zeros((len1,len2,len(scan))) 
    roi5 = zeros((len1,len2,len(scan))) 
    number = -ones((len1,len2,len(scan)))
    paths = [h["F"], ]
    
    delta[0] = h['V'][0]
    eta[0] = h['V'][1]
    chi[0] = h['V'][2]
    phi[0] = h['V'][3]
    mu[0] = h['V'][4]
    nu[0] = h['V'][5]
    thx[0] = h['V'][6]
    thy[0] = h['V'][7]
    thz[0] = h['V'][8]
    
    X[:,:,0] = m['X']
    Y[:,:,0] = m['Y']
    for ii in range(len(m.keys())):
        if (m.keys()[ii]=='mpx4int'):
            ccdint1[:,:,0] = m['mpx4int'] 
        if (m.keys()[ii]=='ccdint2'):
            ccdint2[:,:,0] = m['ccdint2']
        if (m.keys()[ii]=='roi1'):
            roi1[:,:,0] = m['roi1'] 
        if (m.keys()[ii]=='roi2'):
            roi2[:,:,0] = m['roi2'] 
        if (m.keys()[ii]=='roi3'):
            roi3[:,:,0] = m['roi3'] 
        if (m.keys()[ii]=='roi4'):
            roi4[:,:,0] = m['roi4']
        if (m.keys()[ii]=='roi5'):
            roi5[:,:,0] = m['roi5'] 
        if (m.keys()[ii]=='imgnr'):
            number[:,:,0] = m['imgnr']
  
    for ii in xrange(1,len(scan)):
        if queue is not None:
            queue.put(ii, block = False)
        h,m,r = k_mapping(spec_file,scan[ii],index_left)
        delta[ii] = h['V'][0]
        eta[ii] = h['V'][1]
        chi[ii] = h['V'][2]
        phi[ii] = h['V'][3]
        mu[ii] = h['V'][4]
        nu[ii] = h['V'][5]
        thx[ii] = h['V'][6]
        thy[ii] = h['V'][7]
        thz[ii] = h['V'][8]
        paths.append(h["F"])
        
        X[:,:,ii] = m['X']
        Y[:,:,ii] = m['Y']
        for jj in range(len(m.keys())):
            if (m.keys()[jj]=='mpx4int'):
                ccdint1[:,:,ii] = m['mpx4int'] 
            if (m.keys()[jj]=='ccdint2'):
                ccdint2[:,:,ii] = m['ccdint2']
            if (m.keys()[jj]=='roi1'):
                roi1[:,:,ii] = m['roi1'] 
            if (m.keys()[jj]=='roi2'):
                roi2[:,:,ii] = m['roi2'] 
            if (m.keys()[jj]=='roi3'):
                roi3[:,:,ii] = m['roi3'] 
            if (m.keys()[jj]=='roi4'):
                roi4[:,:,ii] = m['roi4']
            if (m.keys()[jj]=='roi5'):
                roi5[:,:,ii] = m['roi5'] 
            if (m.keys()[jj]=='imgnr'):
                number[:,:,ii] = m['imgnr']    
    
    mat = {}    
    mat['delta'] = delta
    mat['eta'] = eta
    mat['chi'] = chi
    mat['phi'] = phi
    mat['mu'] = mu
    mat['nu'] = nu
    mat['thx'] = thx
    mat['thy'] = thy
    mat['scan'] = scan
    mat['thz'] = thz
    mat['size']= m['size'] 
    mat['paths'] = paths
        	
    mat['X'] = X
    mat['Y'] = Y
    for ii in range(len(m.keys())):
        if (m.keys()[ii]=='mpx4int'):
            mat['mpx4int'] = ccdint1 
        if (m.keys()[ii]=='ccdint2'):
            mat['ccdint2'] = ccdint2
        if (m.keys()[ii]=='roi1'):
            mat['roi1'] = roi1 
        if (m.keys()[ii]=='roi2'):
            mat['roi2'] = roi2 
        if (m.keys()[ii]=='roi3'):
            mat['roi3'] = roi3 
        if (m.keys()[ii]=='roi4'):
            mat['roi4'] = roi4
        if (m.keys()[ii]=='roi5'):
            mat['roi5'] = roi5
        if (m.keys()[ii]=='imgnr'):
            mat['number'] = number

    if outfile is not None:
        import cPickle as pickle
        with open(outfile, 'wb') as fp:
            pickle.dump(mat,fp)
       
    return mat


#==============================================================================
# create 3D matrices - multiprocessing  
#==============================================================================    

def k_mapping_3D_multiprocessing(spec_file,scan,nb_of_cpu,pathResults = None,name_save_matrix = None):
    delta = numpy.zeros((len(scan)))
    eta = numpy.zeros((len(scan)))
    chi = numpy.zeros((len(scan)))
    phi = numpy.zeros((len(scan)))
    mu = numpy.zeros((len(scan)))
    nu = numpy.zeros((len(scan)))
    thx = numpy.zeros((len(scan)))
    thy = numpy.zeros((len(scan)))
    thz = numpy.zeros((len(scan)))
    
    h,m,index_left = k_mapping(spec_file,scan[0])
    
    len1 = shape(m['X'])[0]
    len2 = shape(m['X'])[1]
    X = zeros((len1,len2,len(scan)))    
    Y = zeros((len1,len2,len(scan)))
    ccdint1 = zeros((len1,len2,len(scan)))    
    ccdint2 = zeros((len1,len2,len(scan)))       
    roi1 = zeros((len1,len2,len(scan)))    
    roi2 = zeros((len1,len2,len(scan)))
    roi3 = zeros((len1,len2,len(scan)))    
    roi4 = zeros((len1,len2,len(scan))) 
    roi5 = zeros((len1,len2,len(scan))) 
    number = -ones((len1,len2,len(scan)))
    
    delta[0] = h['V'][0]
    eta[0] = h['V'][1]
    chi[0] = h['V'][2]
    phi[0] = h['V'][3]
    mu[0] = h['V'][4]
    nu[0] = h['V'][5]
    thx[0] = h['V'][6]
    thy[0] = h['V'][7]
    thz[0] = h['V'][8]
    
    X[:,:,0] = m['X']
    Y[:,:,0] = m['Y']
    for ii in range(len(m.keys())):
        if (m.keys()[ii]=='ccdint1'):
            ccdint1[:,:,0] = m['ccdint1'] 
        if (m.keys()[ii]=='ccdint2'):
            ccdint2[:,:,0] = m['ccdint2']
        if (m.keys()[ii]=='roi1'):
            roi1[:,:,0] = m['roi1'] 
        if (m.keys()[ii]=='roi2'):
            roi2[:,:,0] = m['roi2'] 
        if (m.keys()[ii]=='roi3'):
            roi3[:,:,0] = m['roi3'] 
        if (m.keys()[ii]=='roi4'):
            roi4[:,:,0] = m['roi4']
        if (m.keys()[ii]=='roi5'):
            roi5[:,:,0] = m['roi5'] 
        if (m.keys()[ii]=='number'):
            number[:,:,0] = m['number']
 
    import multiprocessing  
    try:
        index_start, index_final = indexrange
    except:
        raise ValueError, "Need 2 file indices integers in indexrange=(indexstart, indexfinal)"

    indexdivision = getlist_indexrange_multiprocessing(index_start, index_final, nb_of_cpu)    
    import time
    t0 = time.time()
    jobs = []
    for ii in range(nb_of_cpu):
        proc = multiprocessing.Process(target=getPeakPosition,
                                    args=(indexdivision[ii],matrix,datadir,prefix_image,prefix_im,nx,ny,nz,nav,roi,cch,chpdeg,en,fit,pathResults))

        jobs.append(proc)
        proc.start()
    t_mp = time.time() - t0
    print "Execution time : %.2f" % t_mp

 
  
    for ii in xrange(1,len(scan)):
        h,m,r = k_mapping(spec_file,scan[ii],index_left)
        delta[ii] = h['V'][0]
        eta[ii] = h['V'][1]
        chi[ii] = h['V'][2]
        phi[ii] = h['V'][3]
        mu[ii] = h['V'][4]
        nu[ii] = h['V'][5]
        thx[ii] = h['V'][6]
        thy[ii] = h['V'][7]
        thz[ii] = h['V'][8]
        
        X[:,:,ii] = m['X']
        Y[:,:,ii] = m['Y']
        for jj in range(len(m.keys())):
            if (m.keys()[jj]=='ccdint1'):
                ccdint1[:,:,ii] = m['ccdint1'] 
            if (m.keys()[jj]=='ccdint2'):
                ccdint2[:,:,ii] = m['ccdint2']
            if (m.keys()[jj]=='roi1'):
                roi1[:,:,ii] = m['roi1'] 
            if (m.keys()[jj]=='roi2'):
                roi2[:,:,ii] = m['roi2'] 
            if (m.keys()[jj]=='roi3'):
                roi3[:,:,ii] = m['roi3'] 
            if (m.keys()[jj]=='roi4'):
                roi4[:,:,ii] = m['roi4']
            if (m.keys()[jj]=='roi5'):
                roi5[:,:,ii] = m['roi5'] 
            if (m.keys()[jj]=='number'):
                number[:,:,ii] = m['number']    
    
    mat = {}    
    mat['delta'] = delta
    mat['eta'] = eta
    mat['chi'] = chi
    mat['phi'] = phi
    mat['mu'] = mu
    mat['nu'] = nu
    mat['thx'] = thx
    mat['thy'] = thy
    mat['thz'] = thz
    mat['size'] = m['size']
    mat['X'] = X
    mat['Y'] = Y
    for ii in range(len(m.keys())):
        if (m.keys()[ii]=='ccdint1'):
            mat['ccdint1'] = ccdint1 
        if (m.keys()[ii]=='ccdint2'):
            mat['ccdint2'] = ccdint2
        if (m.keys()[ii]=='roi1'):
            mat['roi1'] = roi1 
        if (m.keys()[ii]=='roi2'):
            mat['roi2'] = roi2 
        if (m.keys()[ii]=='roi3'):
            mat['roi3'] = roi3 
        if (m.keys()[ii]=='roi4'):
            mat['roi4'] = roi4
        if (m.keys()[ii]=='roi5'):
            mat['roi5'] = roi5
        if (m.keys()[ii]=='number'):
            mat['number'] = number
    
    if pathResults is not None: 
        if name_save_matrix is not None:
            import cPickle as pickle
            with open(os.path.join(pathResults, name_save_matrix), 'wb') as fp:
                pickle.dump(mat,fp)
       
    return mat



#==============================================================================
# parameters for plotting   
#==============================================================================
params = {'backend': 'ps',
          'axes.labelsize': 15,
           'text.fontsize': 15,
           'legend.fontsize': 15,
           'title.fontsize':30,
           'xtick.labelsize': 15,
           'ytick.labelsize': 15,
           'text.usetex': False,
           'figure.figsize': (10,6)}
           
           
pylab.rcParams.update(params)


##here, new colormap
cdict = {'red':  ((0.0, 1.0, 1.0),
                  (0.11, 0.0, 0.0),
                  (0.36, 0.0, 0.0),
                  (0.62, 1.0, 1.0),
                  (0.87, 1.0, 1.0),
                  (1.0, 0.0, 0.0)),
          'green': ((0.0, 1.0, 1.0),
                  (0.11, 0.0, 0.0),
                  (0.36, 1.0, 1.0),
                  (0.62, 1.0, 1.0),
                  (0.87, 0.0, 0.0),
                  (1.0, 0.0, 0.0)),
          'blue': ((0.0, 1.0, 1.0),
                  (0.11, 1.0, 1.0),
                  (0.36, 1.0, 1.0),
                  (0.62, 0.0, 0.0),
                  (0.87, 0.0, 0.0),
                  (1.0, 0.0, 0.0))}
my_cmap = matplotlib.colors.LinearSegmentedColormap('my_colormap',cdict,256)


#==============================================================================
# plot two-dimensional images of integrated intensities (ccdint1, ccdint2, roi1, ...)
# as a function of X and Y and rocking angle (here, eta)  
# list_of_ROIs = ['ccdint1','ccdint2','roi1','roi2','roi3','roi4','roi5']
# plotROI2D(spec_file,2,['ccdint1','ccdint2','roi1','roi2','roi3','roi4','roi5'])
#==============================================================================
def plotOneIntegrated2D(jj):
    scanId = plotOneIntegrated2D.m['scan'][jj]
    print ("do it for scan = %04d " % scanId)
    length = len(plotOneIntegrated2D.list_of_ROIs)
    fig = mpl.figure.Figure(figsize=(20*length,10), dpi=80, facecolor='w', edgecolor='k')
    X = plotOneIntegrated2D.m['X'][:,:,jj] 
    Y = plotOneIntegrated2D.m['Y'][:,:,jj]
    for (ii, ROI) in enumerate(plotOneIntegrated2D.list_of_ROIs):
        plt = fig.add_subplot(1,length,ii+1) 
        ax = plt.contourf(X,Y,log10(plotOneIntegrated2D.m[ROI][:,:,jj]),75,cmap=my_cmap)
	plt.axis([float(plotOneIntegrated2D.m['size'][0][0]),float(plotOneIntegrated2D.m['size'][0][1]),float(plotOneIntegrated2D.m['size'][1][0]),float(plotOneIntegrated2D.m['size'][1][1])])#, vmin = 4.4, vmax = 7.25); possible to use jet()    
        plt.set_xlabel('X (micrometer)')
        plt.set_ylabel('Y (micrometer)')
        #plt.tick_params(axis='both', which='major', labelsize=22*length)
        plt.set_title('log('+str(ROI)+') omega = %g' % plotOneIntegrated2D.m['eta'][jj])
        cbar = fig.colorbar(ax)    
    if plotOneIntegrated2D.outFile is not None:
        canvas = FigureCanvasAgg(fig)
        fig.savefig(plotOneIntegrated2D.outFile + '_scan=%04d' % scanId + '.png')
    if plotOneIntegrated2D.queue is not None:
        plotOneIntegrated2D.queue.put(jj)

def plotROI2D(m,list_of_ROIs,outfile = None, queue = None):
    import multiprocessing
    
    plotOneIntegrated2D.outFile = outfile
    plotOneIntegrated2D.m = m
    plotOneIntegrated2D.list_of_ROIs = list_of_ROIs
    plotOneIntegrated2D.queue = queue
    pool = multiprocessing.Pool()
    pool.map(plotOneIntegrated2D, range(len(m['scan'])))
    ## for jj in range(len(m['scan'])):
    ##     plotOneIntegrated2D(jj)
    #show()

#==============================================================================
# plot rocking-curve for the different integrated intensities (ccdint1, ccdint2, roi1, ...)
# list_of_ROIs = ['ccdint1','ccdint2','roi1','roi2','roi3','roi4','roi5']
# when "result = k_mapping_3D(spec_file,scan,pathResults = None,name_save_matrix = None)" have been calculated
# ploteta(result,50,50,['ccdint1','ccdint2','roi1','roi2','roi3','roi4','roi5'])
#==============================================================================
def num2str(num, precision): 
    return "%0.*f" % (precision, num) 

def ploteta(result,pixel_X,pixel_Y,list_of_ROIs,pathResults = None):
    fig = mpl.figure.Figure(figsize=(30*len(list_of_ROIs),20), dpi=80, facecolor='w', edgecolor='k')
    for (ii, ROI) in enumerate(list_of_ROIs):
        plt = fig.add_subplot(1,len(list_of_ROIs),ii+1) 
        plt.plot(result['eta'],result[ROI][pixel_X,pixel_Y,:],'o-',linewidth=5, markersize = 8)  
        #fig.subplots_adjust(hspace = 1,wspace = 0.5)
        plt.set_xlabel('omega'+' (degree)')#, fontsize=16*len(list_of_ROIs))
        plt.set_ylabel(str(ROI))#, fontsize=16*len(list_of_ROIs))
	#yticks(fontsize=16*len(list_of_ROIs))
	#xticks(fontsize=16*len(list_of_ROIs))
        #plt.tick_params(axis='both', which='major', labelsize=16*len(list_of_ROIs))
        plt.set_title('X ='+num2str(result['X'][pixel_X,pixel_Y,0],3)+' - Y ='+num2str(result['Y'][pixel_X,pixel_Y,0],3))#, fontsize=16*len(list_of_ROIs))
    if pathResults is not None: 
        canvas = FigureCanvasAgg(fig)
        fig.savefig(pathResults+'.png')
    #show()    


    
#==============================================================================
# create VTK file   
#==============================================================================
def vtkfile(intensity, vtk_name, pathResults, axis1 = None, axis2 = None, axis3 = None):
    image_vtk = log10(intensity+1) 
    #create a VTK file
    output_filename = vtk_name
    f = open((pathResults+vtk_name+".vtk"),'w') #ouvrir un fichier vtk
    s = shape(image_vtk) #taille de límage vtk
    taille = s[0]*s[1]*s[2] #nombre total de points de la matrice
    if axis1 is not None: 
        f.write("# vtk DataFile Version 2.0"+"\n"+output_filename+"\n"+"ASCII"+"\n"+"\n"+"DATASET STRUCTURED_POINTS"+"\n"+"dimensions"+" "+str(s[0])+" "+str(s[1])+" "+str(s[2])+"\n"+"ORIGIN "+str(axis1[0])+" "+str(axis2[0])+" "+str(axis3[0])+"\n"+"SPACING "+str(axis1[1]-axis1[0])+" "+str(axis2[1]-axis2[0])+" "+str(axis3[1]-axis3[0])+"\n"+"\n"+"POINT_DATA"+" "+str(taille)+"\n"+"SCALARS scalars float"+"\n"+"LOOKUP_TABLE default"+"\n")  
    else:
        f.write("# vtk DataFile Version 2.0"+"\n"+output_filename+"\n"+"ASCII"+"\n"+"\n"+"DATASET STRUCTURED_POINTS"+"\n"+"dimensions"+" "+str(s[0])+" "+str(s[1])+" "+str(s[2])+"\n"+"ORIGIN "+str(0)+" "+str(0)+" "+str(0)+"\n"+"SPACING "+str(1)+" "+str(1)+" "+str(1)+"\n"+"\n"+"POINT_DATA"+" "+str(taille)+"\n"+"SCALARS scalars float"+"\n"+"LOOKUP_TABLE default"+"\n")
    for kk in range(s[2]):
        for jj in range(s[1]):
            for ii in range(s[0]):
	         f.write(str(image_vtk[ii,jj,kk])+"\n")
    	
    f.close()
                  


#==============================================================================
# Get detector parameter
# cch : center channel of the Maxipix detector: vertical(delta)/horizontal(nu)
# chpdeg : channel per degree for the detector
#==============================================================================
def GetDetectorParameter(file1,file2,file3):
    chpdeg = [0,0]
    cch = [0,0]
    x = arange(0,516,1)
    y = arange(0,516,1)
    e1 = xu.io.EDFFile(file1)
    for ii in range(len(e1.header['motor_mne'].split())):
        if (e1.header['motor_mne'].split()[ii]=='del'):		
            delta1 = float(e1.header['motor_pos'].split()[ii])
        if (e1.header['motor_mne'].split()[ii]=='nu'):		
            nu1 = float(e1.header['motor_pos'].split()[ii])
    d1 = scipy.signal.medfilt(e1.data,[3,3])
    x1 = leastsq(e_gauss_fit, [10000,argmax(d1.sum(axis=0)==max(d1.sum(axis=0))),5], args=(x, d1.sum(axis=0)), maxfev=100000, full_output=1)[0][1]
    y1 = leastsq(e_gauss_fit, [10000,argmax(d1.sum(axis=1)==max(d1.sum(axis=1))),5], args=(y, d1.sum(axis=1)), maxfev=100000, full_output=1)[0][1]
    e2 = xu.io.EDFFile(file2)
    for ii in range(len(e2.header['motor_mne'].split())):
        if (e2.header['motor_mne'].split()[ii]=='del'):		
            delta2 = float(e2.header['motor_pos'].split()[ii])
        if (e2.header['motor_mne'].split()[ii]=='nu'):		
            nu2 = float(e2.header['motor_pos'].split()[ii])
    d2 = scipy.signal.medfilt(e2.data,[3,3])
    x2 = leastsq(e_gauss_fit, [10000,argmax(d2.sum(axis=0)==max(d2.sum(axis=0))),5], args=(x, d2.sum(axis=0)), maxfev=100000, full_output=1)[0][1]
    y2 = leastsq(e_gauss_fit, [10000,argmax(d2.sum(axis=1)==max(d2.sum(axis=1))),5], args=(y, d2.sum(axis=1)), maxfev=100000, full_output=1)[0][1]    
    e3 = xu.io.EDFFile(file3)
    for ii in range(len(e3.header['motor_mne'].split())):
        if (e3.header['motor_mne'].split()[ii]=='del'):		
            delta3 = float(e3.header['motor_pos'].split()[ii])
        if (e3.header['motor_mne'].split()[ii]=='nu'):		
            nu3 = float(e3.header['motor_pos'].split()[ii])
    d3 = scipy.signal.medfilt(e3.data,[3,3])
    x3 = leastsq(e_gauss_fit, [10000,argmax(d3.sum(axis=0)==max(d3.sum(axis=0))),5], args=(x, d3.sum(axis=0)), maxfev=100000, full_output=1)[0][1]
    y3 = leastsq(e_gauss_fit, [10000,argmax(d3.sum(axis=1)==max(d3.sum(axis=1))),5], args=(y, d3.sum(axis=1)), maxfev=100000, full_output=1)[0][1]
    deltafit = [delta1,delta2,delta3]
    nufit = [nu1,nu2,nu3]		
    xfit = [x1,x2,x3]
    yfit = [y1,y2,y3]
    for ii in xrange(1,3):
        if ((yfit[ii]<(yfit[0]-2)) or (yfit[ii]>(yfit[0]+2))): 
	    chpdeg[0] = (yfit[ii]-yfit[0])/(deltafit[ii]-deltafit[0])
	    cch[0] = yfit[ii] - chpdeg[0]*deltafit[ii]
	    chpdeg[0] = abs(chpdeg[0])
    for ii in xrange(1,3):
        if ((xfit[ii]<(xfit[0]-2)) or (xfit[ii]>(xfit[0]+2))): 
	    chpdeg[1] = (xfit[ii]-xfit[0])/(nufit[ii]-nufit[0])
	    cch[1] = xfit[ii] - chpdeg[1]*nufit[ii]
	    chpdeg[1] = abs(chpdeg[1])
    return cch,chpdeg

#==============================================================================
# Remove hot pixels
#==============================================================================
def hotpixelkill(ccd):
    #function to remove hot pixels from CCD frames
    ccd[44,160] = 0
    ccd[45,160] = 0
    ccd[43,160] = 0
    ccd[46,160] = 0
    ccd[44,159] = 0
    ccd[45,159] = 0
    ccd[43,159] = 0
    ccd[46,159] = 0
    ccd[44,159] = 0
    ccd[45,159] = 0
    ccd[43,159] = 0
    ccd[46,159] = 0
    ccd[304,95] = 0  
    ccd[304,96] = 0    
    ccd[304,97] = 0           
    ccd[305,95] = 0  
    ccd[305,96] = 0    
    ccd[305,97] = 0     
    ccd[303,95] = 0  
    ccd[303,96] = 0    
    ccd[303,97] = 0  
    return ccd



#==============================================================================
# get Peak from Reciprocal Space
#==============================================================================
def getPeakFromRecipSpace(ccdn,phi,nu,mu,delta,eta,datadir,prefix_image,prefix_im,nx,ny,nz,nav,roi,cch,chpdeg,en):
    for (idx, i) in enumerate(ccdn):
        # read ccd image from EDF file
        if "%" in prefix_image:
            prefix_image_ = prefix_image % round(eta[idx] * 10000.)
        else:
            prefix_image_ = prefix_image
        if "%" in prefix_im:
            prefix_im_ = prefix_im % round(eta[idx] * 10000.)
        else:
            prefix_im_ = prefix_im
        path_image = os.path.join(datadir,prefix_image_,prefix_im_+'_%04d.edf.gz'%0)
        if not(os.path.isfile(path_image)):
            print idx, path_image
            raise IOError(idx, path_image)
        e = edf.RawImage()
        e.read(path_image, int(i))
        #e = xu.io.EDFFile(path_image)
        #ccdraw = e.data
        ccdraw = e.getData().reshape(e.height, e.width)
        ccd = hotpixelkill(ccdraw)    
        # reduce data size
        CCD = xu.blockAverage2D(ccd, nav[0],nav[1], roi=roi)         
        if idx==0: 
            # now the size of the data is known -> create data array
            intensity = numpy.zeros( (len(ccdn),) + CCD.shape )
        intensity[idx,:,:] = CCD
        # scipy.signal.medfilt(CCD,[3,3])   
    # transform scan angles to reciprocal space coordinates for all detector pixels
    #==============================================================================
    # detector and sample's geometries
    #==============================================================================
    #qconv = xu.experiment.QConversion(['z+','y-'],['z+','y-'],[1,0,0]) # 2S+2D goniometer (sample: mu+phi, eta; detector: nu,del)
    qconv = xu.experiment.QConversion(['y-'],['z+','y-'],[1,0,0]) # 2S+2D goniometer (sample: mu+phi, eta; detector: nu,del)    
    # convention for coordinate system: x downstream; z upwards; y to the "outside" (righthanded)
    hxrd = xu.HXRD([1,0,0],[0,0,1],en=en,qconv=qconv)
    hxrd.Ang2Q.init_area('z-','y+',cch1=cch[0],cch2=cch[1],Nch1=516,Nch2=516,chpdeg1=chpdeg[0],chpdeg2=chpdeg[1],Nav=nav,roi=roi)
    #qx,qy,qz = hxrd.Ang2Q.area(mu+phi,eta,nu,delta)
    qx,qy,qz = hxrd.Ang2Q.area(eta,nu,delta)    
    # convert data to rectangular grid in reciprocal space
    gridder = xu.Gridder3D(nx,ny,nz)
    gridder(qx,qy,qz,intensity)    
    return gridder.xaxis,gridder.yaxis,gridder.zaxis,gridder.gdata,gridder  
    

#==============================================================================
# get peak position
#==============================================================================
def getPeakPosition(matrix,datadir,prefix_image,prefix_im,nx,ny,nz,nav,roi,cch,chpdeg,en,fit = True,pathResults = None):
    t0 = time.time() 
    result = {}
    result['X'] = zeros((shape(matrix['X'])[0],shape(matrix['X'])[1]))
    result['Y'] = zeros((shape(matrix['X'])[0],shape(matrix['X'])[1]))
    result['qx_peak'] = zeros((shape(matrix['X'])[0],shape(matrix['X'])[1]))
    result['qy_peak'] = zeros((shape(matrix['X'])[0],shape(matrix['X'])[1]))
    result['qz_peak'] = zeros((shape(matrix['X'])[0],shape(matrix['X'])[1]))
    result['I_peak'] = zeros((shape(matrix['X'])[0],shape(matrix['X'])[1]))
    result['X'] = matrix['X'][:,:,0]
    result['Y'] = matrix['Y'][:,:,0]
    if fit: 
        print('fit of peak')
        for ii in range(shape(matrix['X'])[0]):
            print 'X =',ii,'/',shape(matrix['X'])[0]
            for jj in range(shape(matrix['X'])[1]):
                ##print 'X =',ii,'/',shape(matrix['X'])[0],'-- Y =',jj,'/',shape(matrix['X'])[1]            
                ccdn = zeros((shape(matrix['X'])[2]))
                for tt in range((shape(matrix['X'])[2])):
                    ccdn[tt] = matrix['number'][ii,jj,tt]
                if (ccdn.min() == -1):
                    print('nothing')
                else:
                    qx,qy,qz,gint,gridder = getPeakFromRecipSpace(ccdn,matrix['phi'],matrix['nu'],matrix['mu'],matrix['delta'],matrix['eta'],datadir,prefixe_image,prefix_im,nx,ny,nz,nav,roi,cch,chpdeg,en)
                    v0= [1.0,mean(qz),1.0]                    
                    result['qz_peak'][ii,jj]=leastsq(e_gauss_fit, v0[:], args=(qz, (gint.sum(axis=0)).sum(axis=0)), maxfev=100000, full_output=1)[0][1]
                    v0= [1.0,mean(qy),1.0] 
                    result['qy_peak'][ii,jj]=leastsq(e_gauss_fit, v0[:], args=(qy, (gint.sum(axis=2)).sum(axis=0)), maxfev=100000, full_output=1)[0][1]
                    v0= [1.0,mean(qx),1.0] 
                    result['qx_peak'][ii,jj]=leastsq(e_gauss_fit, v0[:], args=(qx, (gint.sum(axis=2)).sum(axis=1)), maxfev=100000, full_output=1)[0][1] 
                    result['I_peak'][ii,jj]=leastsq(e_gauss_fit, v0[:], args=(qx, (gint.sum(axis=2)).sum(axis=1)), maxfev=100000, full_output=1)[0][0]
    else:   
        print('maximum of peak')
        for ii in range(shape(matrix['X'])[0]):
            print 'X =',ii,'/',shape(matrix['X'])[0]
            for jj in range(shape(matrix['X'])[1]):
                ##print 'X =',ii,'/',shape(matrix['X'])[0],'-- Y =',jj,'/',shape(matrix['X'])[1]
                ccdn = zeros((shape(matrix['X'])[2]))
                for tt in range((shape(matrix['X'])[2])):
                    ccdn[tt] = matrix['number'][ii,jj,tt]
                if (ccdn.min() == -1):
                    print('nothing')
                else:
                    qx,qy,qz,gint,gridder = getPeakFromRecipSpace(ccdn,matrix['phi'],matrix['nu'],matrix['mu'],matrix['delta'],matrix['eta'],datadir,prefix_image,prefix_im,nx,ny,nz,nav,roi,cch,chpdeg,en)
                    result['qz_peak'][ii,jj] = qz[argmax((gint.sum(axis=0)).sum(axis=0))]
                    result['qy_peak'][ii,jj] = qy[argmax((gint.sum(axis=2)).sum(axis=0))]
                    result['qx_peak'][ii,jj] = qx[argmax((gint.sum(axis=2)).sum(axis=1))]
                    result['I_peak'][ii,jj] = gint[find(qx==result['qx_peak'][ii,jj]),find(qy==result['qy_peak'][ii,jj]),find(qz==result['qz_peak'][ii,jj])]
		     
    if pathResults is not None: 
        if prefix_image is not None:
            import cPickle as pickle
            with open(pathResults+prefix_im, 'wb') as fp:
                pickle.dump(result,fp)  
           
            fichier = open(pathResults+prefix_im+'.dat', "w")  
            string=''
            string+= 'qx_peak qy_peak qz_peak X Y I_peak\n\n'
            for ii in xrange(shape(matrix['X'])[0]):
                for jj in xrange(shape(matrix['X'])[1]):
                    string+=str(result['qx_peak'][ii,jj])+" "+str(result['qy_peak'][ii,jj])+" "+str(result['qz_peak'][ii,jj])+" "+str(matrix['X'][ii,jj,0])+" "+str(matrix['Y'][ii,jj,0])+" "+str(result['I_peak'][ii,jj])+'\n'
            fichier.write(string)
            fichier.close()
    t_mp = time.time() - t0
    print "Execution time : %.2f" % t_mp       
    return result
 


#==============================================================================
# get peak position
#==============================================================================
def getPeakPosition_mult(indexrange,matrix,datadir,prefix_image,prefix_im,nx,ny,nz,nav,roi,cch,chpdeg,en,fit = True,outfile = None, q = None):
    t0 = time.time() 
    result = {}
    result['X'] = zeros((shape(matrix['X'])[0],shape(matrix['X'])[1]))
    result['Y'] = zeros((shape(matrix['X'])[0],shape(matrix['X'])[1]))
    result['qx_peak'] = zeros((shape(matrix['X'])[0],shape(matrix['X'])[1]))
    result['qy_peak'] = zeros((shape(matrix['X'])[0],shape(matrix['X'])[1]))
    result['qz_peak'] = zeros((shape(matrix['X'])[0],shape(matrix['X'])[1]))
    result['I_peak'] = zeros((shape(matrix['X'])[0],shape(matrix['X'])[1]))
    result['X'] = matrix['X'][:,:,0]
    result['Y'] = matrix['Y'][:,:,0]
    fullsize = result['X'].size
    if fit:
        print('fit of peak')
        for ii in range(indexrange[0], indexrange[1] + 1):
            print 'X =',ii,'/',shape(matrix['X'])[0]
            for jj in range(shape(matrix['X'])[1]):
                ##print 'X =',ii,'/',shape(matrix['X'])[0],'-- Y =',jj,'/',shape(matrix['X'])[1]            
                ccdn = zeros((shape(matrix['X'])[2]))
                for tt in range((shape(matrix['X'])[2])):
                    ccdn[tt] = matrix['number'][ii,jj,tt]
                if (ccdn.min() == -1):
                    print('nothing')
                else:
                    try:
                        qx,qy,qz,gint,gridder = getPeakFromRecipSpace(ccdn,matrix['phi'],matrix['nu'],matrix['mu'],matrix['delta'],matrix['eta'],datadir,prefix_image,prefix_im,nx,ny,nz,nav,roi,cch,chpdeg,en)
                    except IOError as inst:
                        tt, filename = inst.args
                        raise IOError("missing file '%s' at %gx%g." % (filename, matrix['X'][ii, jj, tt], matrix['Y'][ii, jj, tt]))
                    v0= [1.0,mean(qz),1.0]                    
                    result['qz_peak'][ii,jj]=leastsq(e_gauss_fit, v0[:], args=(qz, (gint.sum(axis=0)).sum(axis=0)), maxfev=100000, full_output=1)[0][1]
                    v0= [1.0,mean(qy),1.0] 
                    result['qy_peak'][ii,jj]=leastsq(e_gauss_fit, v0[:], args=(qy, (gint.sum(axis=2)).sum(axis=0)), maxfev=100000, full_output=1)[0][1]
                    v0= [1.0,mean(qx),1.0] 
                    result['qx_peak'][ii,jj]=leastsq(e_gauss_fit, v0[:], args=(qx, (gint.sum(axis=2)).sum(axis=1)), maxfev=100000, full_output=1)[0][1] 
                    result['I_peak'][ii,jj]=leastsq(e_gauss_fit, v0[:], args=(qx, (gint.sum(axis=2)).sum(axis=1)), maxfev=100000, full_output=1)[0][0]
                if q is not None:
                    q.put((ii, jj, 1. / fullsize))
    else:   
        print('maximum of peak')
        for ii in range(indexrange[0], indexrange[1] + 1):
            print 'X =',ii,'/',shape(matrix['X'])[0]
            for jj in range(shape(matrix['X'])[1]):
                ##print 'X =',ii,'/',shape(matrix['X'])[0],'-- Y =',jj,'/',shape(matrix['X'])[1]
                ccdn = zeros((shape(matrix['X'])[2]))
                for tt in range((shape(matrix['X'])[2])):
                    ccdn[tt] = matrix['number'][ii,jj,tt]
                if (ccdn.min() == -1):
                    print('nothing')
                else:
                    qx,qy,qz,gint,gridder = getPeakFromRecipSpace(ccdn,matrix['phi'],matrix['nu'],matrix['mu'],matrix['delta'],matrix['eta'],datadir,prefix_image,prefix_im,nx,ny,nz,nav,roi,cch,chpdeg,en)
                    result['qz_peak'][ii,jj] = qz[argmax((gint.sum(axis=0)).sum(axis=0))]
                    result['qy_peak'][ii,jj] = qy[argmax((gint.sum(axis=2)).sum(axis=0))]
                    result['qx_peak'][ii,jj] = qx[argmax((gint.sum(axis=2)).sum(axis=1))]
                    result['I_peak'][ii,jj] = gint[find(qx==result['qx_peak'][ii,jj]),find(qy==result['qy_peak'][ii,jj]),find(qz==result['qz_peak'][ii,jj])]
                if q is not None:
                    q.put((ii, jj, 1. / fullsize))
		     
    import cPickle as pickle
    if outfile is None:
        with open('from'+str(indexrange[0])+'to'+str(indexrange[1]), 'wb') as fp:
            pickle.dump(result,fp)
    else:
        with open(outfile+'from'+str(indexrange[0])+'to'+str(indexrange[1]), 'wb') as fp:
            pickle.dump(result,fp)
        """            
        fichier = open(pathResults+prefix_image+'.dat', "w")  
        string=''
        string+= 'qx_peak qy_peak qz_peak X Y I_peak\n\n'
        for ii in xrange(shape(matrix['X'])[0]):
            for jj in xrange(shape(matrix['X'])[1]):
                string+=str(result['qx_peak'][ii,jj])+" "+str(result['qy_peak'][ii,jj])+" "+str(result['qz_peak'][ii,jj])+" "+str(matrix['X'][ii,jj,0])+" "+str(matrix['Y'][ii,jj,0])+" "+str(result['I_peak'][ii,jj])+'\n'
        fichier.write(string)
        fichier.close()
        """        
    return result


def getlist_indexrange_multiprocessing(index_start, index_final, nb_of_cpu):
    """
    returns list of 2 elements (index_start, index_final) for each cpu
    """
    nb_files = index_final - index_start + 1
    step = nb_files / nb_of_cpu
    indexdivision = []
    st_ind = index_start
    fi_ind = index_start + step - 1
    for k in range(nb_of_cpu):
        indexdivision.append([st_ind, fi_ind])
        st_ind += step
        fi_ind += step
    #last cpu will to the remaining file
    indexdivision[-1][-1] = index_final
    return indexdivision


def getPeakPosition_multiprocessing(indexrange,matrix,datadir,prefix_image,prefix_im,nx,ny,nz,nav,roi,cch,chpdeg,en,nb_of_cpu = None,fit = True,outfile = None, q = None):
    import multiprocessing
    try:
        index_start, index_final = indexrange
    except:
        raise ValueError, "Need 2 file indices integers in indexrange=(indexstart, indexfinal)"
    if nb_of_cpu is None:
        nb_of_cpu = multiprocessing.cpu_count()

    indexdivision = getlist_indexrange_multiprocessing(index_start, index_final, nb_of_cpu)
    t0 = time.time()
    jobs = []
    for ii in range(nb_of_cpu):
        proc = multiprocessing.Process(target=getPeakPosition_mult,
                                    args=(indexdivision[ii],matrix,datadir,prefix_image,prefix_im,nx,ny,nz,nav,roi,cch,chpdeg,en,fit,outfile,q))
        jobs.append(proc)
        proc.start()
    
    for j in jobs:
        j.join()
        print '%s.exitcode = %s' % (j.name, j.exitcode)
    t_mp = time.time() - t0
    print "Execution time : %.2f" % t_mp
    print "creating data"
    data = {} 
    data['qx_peak'] = zeros((shape(matrix['X'])[0],shape(matrix['X'])[1]))
    data['qy_peak'] = zeros((shape(matrix['X'])[0],shape(matrix['X'])[1]))
    data['qz_peak'] = zeros((shape(matrix['X'])[0],shape(matrix['X'])[1]))
    data['I_peak'] = zeros((shape(matrix['X'])[0],shape(matrix['X'])[1])) 
    data['X'] = zeros((shape(matrix['X'])[0],shape(matrix['X'])[1]))
    data['Y'] = zeros((shape(matrix['X'])[0],shape(matrix['X'])[1]))
    data['X'] = matrix['X'][:,:,0]
    data['Y'] = matrix['Y'][:,:,0]
    for ii in range(nb_of_cpu):
        indexrange = indexdivision[ii]
	import cPickle as pickle
        if outfile is None:
            with open('from'+str(indexrange[0])+'to'+str(indexrange[1]),'rb') as fp:
                result=pickle.load(fp)
        else:
            with open(outfile + 'from'+str(indexrange[0])+'to'+str(indexrange[1]),'rb') as fp:
                result=pickle.load(fp)
        data['qx_peak'][indexrange[0]:indexrange[1]+1,:]=result['qx_peak'][indexrange[0]:indexrange[1]+1,:]            
        data['qy_peak'][indexrange[0]:indexrange[1]+1,:]=result['qy_peak'][indexrange[0]:indexrange[1]+1,:] 
        data['qz_peak'][indexrange[0]:indexrange[1]+1,:]=result['qz_peak'][indexrange[0]:indexrange[1]+1,:] 
        data['I_peak'][indexrange[0]:indexrange[1]+1,:]=result['I_peak'][indexrange[0]:indexrange[1]+1,:] 
    if outfile is not None: 
        import cPickle as pickle
        with open(outfile, "w") as fp:
            pickle.dump(data,fp)  

        fichier = open(outfile + prefix_im + ".dat", "w")  
	q = 0
        string=''
        string+= 'X Y qx_peak qy_peak qz_peak q I_peak\n\n'
        for ii in xrange(shape(matrix['X'])[0]):
            for jj in xrange(shape(matrix['X'])[1]):
		q = sqrt(data['qx_peak'][ii,jj]**2 + data['qy_peak'][ii,jj]**2 + data['qz_peak'][ii,jj]**2)
                string+=str(matrix['X'][ii,jj,0])+" "+str(matrix['Y'][ii,jj,0])+" "+str(data['qx_peak'][ii,jj])+" "+str(data['qy_peak'][ii,jj])+" "+str(data['qz_peak'][ii,jj])+" "+str(q)+" "+str(data['I_peak'][ii,jj])+'\n'
        fichier.write(string)
        fichier.close()
     
    return data



#==============================================================================
# plot 2D maps of qx_peak, qy_peak, qz_peak and I_peak
#==============================================================================
def plotQmesh(fig, mat, plt, X, Y, data, title = None):
     #for ii in range(shape(data)[0]):
     #   for jj in range(shape(data)[1]):
     #       if(X[ii,jj]==0):
     #           X[ii,jj]=NaN    
     #           Y[ii,jj]=NaN 			    
    ax = plt.contourf(X[0:min(where(X==0)[0]),:], Y[0:min(where(X==0)[0]),:], data[0:min(where(X==0)[0]),:], 150, cmap=my_cmap)
    plt.axis([float(mat['size'][0][0]),float(mat['size'][0][1]),float(mat['size'][1][0]),float(mat['size'][1][1])])
    plt.set_xlabel('X (micrometer)')#, fontsize=20)
    plt.set_ylabel('Y (micrometer)')#, fontsize=20)
    #plt.tick_params(axis='both', which='major', labelsize=20)
    if title is not None:
        plt.set_title(title)#, fontsize=20)
    cbar = fig.colorbar(ax)

def plotQI(result,mat, outfile=None):
    fig = mpl.figure.Figure(figsize=(10,13), dpi=80, facecolor='w', edgecolor='k')
    plt = fig.add_subplot(3,2,1)
    plotQmesh(fig, mat, plt, result['X'],result['Y'],result['qx_peak'], "Qx")
    plt = fig.add_subplot(3,2,2)
    plotQmesh(fig, mat, plt, result['X'],result['Y'],result['qy_peak'], "Qy")
    plt = fig.add_subplot(3,2,3)
    plotQmesh(fig, mat, plt, result['X'],result['Y'],result['qz_peak'], "Qz")
    plt = fig.add_subplot(3,2,4)
    plotQmesh(fig, mat, plt, result['X'],result['Y'],sqrt(result['qx_peak']*result['qx_peak']+result['qy_peak']*result['qy_peak']+result['qz_peak']*result['qz_peak']), "Qtotal")
    plt = fig.add_subplot(3,2,5)
    plotQmesh(fig, mat, plt, result['X'],result['Y'],result['I_peak'], "Imax")    
    if outfile is not None: 
        FigureCanvasAgg(fig)
        fig.savefig(outfile)
    return fig


#==============================================================================
# plot 2D cuts of reciprocal space at a certain (X,Y) position
#==============================================================================
def plot2Dcuts(X_pixel,Y_pixel,matrix,datadir,prefix_image,prefix_im,nx,ny,nz,nav,roi,cch,chpdeg,en,pathResults=None,name_fig=None):
    ccdn = zeros((shape(matrix['X'])[2]))
    for tt in range((shape(matrix['X'])[2])):
        ccdn[tt] = matrix['number'][X_pixel,Y_pixel,tt]
        if (ccdn.min() == -1):
            print('nothing')
        else:
            qx,qy,qz,gint,gridder = getPeakFromRecipSpace(ccdn,matrix['phi'],matrix['nu'],matrix['mu'],matrix['delta'],matrix['eta'],datadir,prefix_image,prefix_im,nx,ny,nz,nav,roi,cch,chpdeg,en)
    plt.figure(1,figsize=(9,20))
    plt.clf()
    plt.subplot(3,1,1)
    plt.contourf(qx,qy,xu.maplog(gint.sum(axis=2),5,0).T,150,cmap=my_cmap)
    plt.xlabel("Qx")
    plt.ylabel("Qy")
    plt.colorbar()
    plt.subplot(3,1,2)
    plt.contourf(qx,qz,xu.maplog(gint.sum(axis=1),5,0).T,150,cmap=my_cmap)
    plt.xlabel("Qx")
    plt.ylabel("Qz")
    plt.colorbar()
    plt.subplot(3,1,3)
    plt.contourf(qy,qz,xu.maplog(gint.sum(axis=0),5,0).T,150,cmap=my_cmap)
    plt.xlabel("Qy")
    plt.ylabel("Qz")
    plt.colorbar()
    if pathResults is not None: 
        if name_fig is not None:
            savefig(pathResults+name_fig+num2str(X_pixel, 0)+'_'+num2str(Y_pixel, 0)+'.png')    
    show()  




