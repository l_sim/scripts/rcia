#!/usr/bin/env python
# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright (C) 2012-2013 Damien Caliste <damien.caliste@cea.fr>,
#                         Thu Nhi Tran Thi <thu-nhi.tran-thi@esrf.fr>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA


import os, threading, sys, re
import gtk, gobject
import numpy
import matplotlib
from matplotlib.backends.backend_gtkagg import FigureCanvasGTKAgg as FigureCanvas
from matplotlib.backends import backend_pdf
import chi, edf
import ID1
from gui_display import EDF_display
#from mayavi import mlab
import xrayutilities as xu
from optparse import OptionParser

import ID1_gui_model
import ID1_gui_options

class ID1_gui(gtk.VBox):
  def __init__(self, options, args):
    super(ID1_gui, self).__init__()

    self.specs = None

    # Horrible stuffs
    self.scanRange = range(0, 2748)

    self.model = ID1_gui_model.ScansModel()
    self.options = ID1_gui_options.GuiOptions()

    self.paned = gtk.HPaned()
    self.pack_start(self.paned, True, True)

    #############
    # Left pane #
    #############
    vbox = gtk.VBox()
    vbox.set_size_request(375, -1)
    vbox.set_border_width(5)
    self.paned.pack1(vbox)

    vbox.pack_start(self.options.getResultOptions(outDir = options.out_dir), False)

    wd = gtk.Label("<b>Spec file</b>")
    wd.set_use_markup(True)
    self.frameSpec = gtk.Frame()
    self.frameSpec.set_label_widget(wd)
    self.frameSpec.set_label_align(0.5, 0.5)
    align = gtk.Alignment(0.5, 0.5, 1., 0.)
    align.set_padding(10, 0, 0, 0)
    align.add(self.frameSpec)
    vbox.pack_start(align, False)

    vbox2 = gtk.VBox()
    vbox2.set_border_width(5)
    self.frameSpec.add(vbox2)
    vbox2.pack_start(self.options.getSpecChooser(filename = options.spec, ids = options.scan_numbers), False, False)
    ## self.options.connect("spec-selected", self.chooseSpec)

    vbox2.pack_start(self.options.getSpecOptions(), False, False)
    vbox2.pack_start(self.options.getIntOptions(), False, False)
    self.options.connect("matrices-loaded", self.onMatrices)

    wd = gtk.Label("<b>Diffraction analysis</b>")
    wd.set_use_markup(True)
    self.frameDif = gtk.Frame()
    self.frameDif.set_label_widget(wd)
    self.frameDif.set_label_align(0.5, 0.5)
    align = gtk.Alignment(0.5, 0.5, 1., 0.)
    align.set_padding(10, 0, 0, 0)
    align.add(self.frameDif)
    vbox.pack_start(align, False)
    
    vbox2 = gtk.VBox()
    vbox2.set_border_width(5)
    self.frameDif.add(vbox2)
    ## vbox.pack_start(self.options.getMethodChooser(), False, False)
    ## self.options.connect("method-selected", self.chooseMethod)
    vbox2.pack_start(self.options.getBeamOptions(self.model, options.energy), False, False)
    vbox2.pack_start(self.options.getDetectorOptions(self.model, options.alignments), False, False)
    vbox2.pack_start(self.options.getImageOptions(), False, False)
    vbox2.pack_start(self.options.getComputeQ(), False, False)

    wd = gtk.Label("<b>Strain calculation</b>")
    wd.set_use_markup(True)
    self.frameStrain = gtk.Frame()
    self.frameStrain.set_label_widget(wd)
    self.frameStrain.set_label_align(0.5, 0.5)
    align = gtk.Alignment(0.5, 0.5, 1., 0.)
    align.set_padding(10, 0, 0, 0)
    align.add(self.frameStrain)
    vbox.pack_start(align, False)
    
    vbox2 = gtk.VBox()
    vbox2.set_border_width(5)
    self.frameStrain.add(vbox2)

    vbox2.pack_start(self.options.getLatticeOptions(), False, False)
    vbox2.pack_start(self.options.getComputeStrain(), False, False)
    ## vbox.pack_start(self.options.getOptionIntegrate(), False, False)
    ## self.options.checkInt.connect("toggled", self.chooseSaveInt)

    ##############
    # Right pane #
    ##############
    hbox = gtk.HBox()

    vbox = gtk.VBox()
    vbox.set_border_width(5)
    hbox.pack_start(vbox, False)

    vbox.pack_end(self.options.getDatadirChooser(options.root_data,
                                                 options.sub_directory,
                                                 options.prefix_image), False, False)
    self.options.connect("datadir-selected", self.chooseDir)

    # Sample view
    display = ID1_gui_model.ScansDisplay(self.model)
    self.view = display.view
    self.view.connect("scan-selected", self.onScanSelected)
    self.view.connect("pix-selected", self.onPixSelected)
    self.view.connect("piy-selected", self.onPiySelected)
    self.options.connect("rc-angle-selected", self.onRcAngle)
    vbox.pack_start(display, True, True)

    book = gtk.Notebook()
    book.set_border_width(5)
    hbox.pack_start(book, True, True)

    self.edf = EDF_display()
    self.edf.connect("rectangle-selection", self.options.onRoiSelection)
    book.append_page(self.edf, gtk.Label("EDF file viewer"))

    self.rc_fig = matplotlib.figure.Figure(figsize=(10,10), dpi=80,
                                           facecolor='w', edgecolor='k')
    self.rc_plot = self.rc_fig.add_subplot(1,1,1)
    self.rcview = ID1_gui_options.ScansMapViewer(title = "rocking curve",
                                                 default = "Please select a (piy, pix) couple")
    book.append_page(self.rcview, gtk.Label("rocking curve viewer"))

    book.append_page(self.options.getMapViewer(), gtk.Label("Q maps viewer"))
    book.append_page(self.options.getStrainViewer(), gtk.Label("strain map viewer"))

    ## self.proj = gtk.VBox()
    ## self.proj.set_size_request(-1, 400)
    ## self.proj.set_no_show_all(True)
    ## vbox.pack_start(self.proj)

    ## hbox = gtk.HBox()
    ## self.proj.pack_start(hbox)

    ## dpi = 60
    ## f = matplotlib.figure.Figure(figsize=(400 / dpi, 400 / dpi), dpi = dpi)
    ## self.plotQX = f.add_subplot(111)
    ## self.canvasQX = FigureCanvas(f)
    ## self.canvasQX.set_size_request(200, 400)
    ## hbox.pack_start(self.canvasQX)

    ## f = matplotlib.figure.Figure(figsize=(400 / dpi, 400 / dpi), dpi = dpi)
    ## self.plotQY = f.add_subplot(111)
    ## self.canvasQY = FigureCanvas(f)
    ## self.canvasQY.set_size_request(200, 400)
    ## hbox.pack_start(self.canvasQY)

    ## f = matplotlib.figure.Figure(figsize=(400 / dpi, 400 / dpi), dpi = dpi)
    ## self.plotQZ = f.add_subplot(111)
    ## self.canvasQZ = FigureCanvas(f)
    ## self.canvasQZ.set_size_request(200, 400)
    ## hbox.pack_start(self.canvasQZ)
    ## hbox.show_all()

    ## wd = gtk.Toolbar()
    ## wd.set_orientation(gtk.ORIENTATION_HORIZONTAL)
    ## wd.set_style(gtk.TOOLBAR_ICONS)
    ## wd.set_icon_size(gtk.ICON_SIZE_SMALL_TOOLBAR)
    ## self.proj.pack_end(wd, False, False)
    ## bt = gtk.ToolButton(stock_id = gtk.STOCK_SAVE)
    ## bt.connect("clicked", self.onExportProj, self.view.get_selection())
    ## wd.insert(bt, -1)
    ## wd.show_all()

    ## hbox = gtk.HBox()
    ## vbox.pack_start(hbox)

    ## f = matplotlib.figure.Figure(figsize=(400 / dpi, 400 / dpi), dpi = dpi)
    ## self.plotX = f.add_subplot(111)
    ## self.canvasX = FigureCanvas(f)
    ## self.canvasX.set_size_request(200, 250)
    ## hbox.pack_start(self.canvasX)

    ## f = matplotlib.figure.Figure(figsize=(400 / dpi, 400 / dpi), dpi = dpi)
    ## self.plotY = f.add_subplot(111)
    ## self.canvasY = FigureCanvas(f)
    ## self.canvasY.set_size_request(200, 250)
    ## hbox.pack_start(self.canvasY)

    ## f = matplotlib.figure.Figure(figsize=(400 / dpi, 400 / dpi), dpi = dpi)
    ## self.plotZ = f.add_subplot(111)
    ## self.canvasZ = FigureCanvas(f)
    ## self.canvasZ.set_size_request(200, 250)
    ## hbox.pack_start(self.canvasZ)

    ## wd = gtk.Toolbar()
    ## wd.set_orientation(gtk.ORIENTATION_HORIZONTAL)
    ## wd.set_style(gtk.TOOLBAR_ICONS)
    ## wd.set_icon_size(gtk.ICON_SIZE_SMALL_TOOLBAR)
    ## vbox.pack_end(wd, False, False)
    ## bt = gtk.ToolButton(stock_id = gtk.STOCK_SAVE)
    ## bt.connect("clicked", self.onExportGraph, self.view.get_selection())
    ## wd.insert(bt, -1)

    self.paned.pack2(hbox)

    self.bar = gtk.ProgressBar()
    self.bar.set_no_show_all(True)
    self.pack_end(self.bar, False, False)

  def lock(self):
    self.paned.set_sensitive(False)
    self.bar.show()
    self.bar.set_fraction(0)
    self.bar.set_text("Reading spec file.")
    while gtk.events_pending():
      gtk.main_iteration()

  def unlock(self):
    self.paned.set_sensitive(True)
    self.bar.hide()
    while gtk.events_pending():
      gtk.main_iteration()

  def setProgress(self, text, fraction = None):
    if fraction is None:
      self.bar.pulse()
    else:
      self.bar.set_fraction(fraction)
    self.bar.set_text(text)
    while gtk.events_pending():
      gtk.main_iteration()

  def onMatrices(self, options, mat):
    gobject.idle_add(self.model.populateFromMatrices, mat, options)

  def onRcAngle(self, options, ang):
    self.model.rcAngle = ang

  def setRcAngle(self, rcAngle):
    self.options.rcAngle.set_text(rcAngle)

  def setSpecFromFile(self, path):
    self.specs = {}
    if path.endswith(".new.csv"):
      print "Parsing CSV format of new experimental setup."
      self.specs["c2_004"] = chi.dataFromCSVNew(path)
    elif path.endswith(".csv"):
      self.specs["unknown sample"] = chi.dataFromCSV(path)
    elif path.endswith(".spec") or path.endswith(".h5"):
      exp = ID1.Experiment(path, rcAngle = self.options.rcAngle.get_text(),
                           pixelMotors = map(gtk.Entry.get_text,
                                             self.options.pMotor))
      exp.getSampleScans(override = {"TIV48A_75_E1_": range(146, 605),
                                     "si_004_40_2_351800_": range(4,55)})
      self.setProgress("Found %d experiments." % len(exp.sample_scans), 0.)
      if len(sys.argv) > 4:
        exp.buildSamples(scanId = sys.argv[4])
      else:
        exp.buildSamples()
      self.specs = {}
      for (k, v) in exp.samples.items():
        self.specs[k] = v.getFullScan()
    else:
      raise ValueError
      # self.specs = chi.dataFromSpec(sys.argv[2], self.scanRange)

  def populate(self):
    # Run the populate.
    if self.specs is not None and self.options.datadir is not None:
      self.view.set_model()
      self.model.clear()
      for (k, v) in self.specs.items():
        self.model.populate(v, self.options.datadir, k, infoFunc = self.setProgress)
      self.view.set_model(self.model)

  def populateFromCache(self):
    return self.model.populateFromCache(infoFunc = self.setProgress)

  def chooseDir(self, object, dirname):
    self.lock()
    self.populate()
    self.unlock()

  def chooseSpec(self, object, specFile):
    self.lock()
    self.setSpecFromFile(specFile)
    self.populate()
    self.unlock()

  def chooseMethod(self, object, method):
    self.model.method = method

  def chooseSaveInt(self, widget):
    self.model.saveIntegral = widget.get_active()

  def getRecipFactors(self):
    #hkls = []
    #for i in range(3):
    #  hkls.append(self.validateMillerIndices(self.entryHKL[i]))
    #hkls = numpy.array(hkls)
    hkl = self.options.getPeakIndices()
    hkls = numpy.array((hkl, hkl, hkl))
    return 2. * numpy.pi * numpy.sqrt((hkls * hkls).sum(1))

  def plot(self, canvas, plot, x, y, p, title = None):
    plot.cla()
    yg = chi.gauss_eval(x, p[1], p[2], p[0])
    if title is not None:
      plot.set_title(title)
    plot.plot(x, y, '.-', x, yg, 'r--')
    plot.set_yscale("log")
    plot.set_ylim((1, plot.get_ylim()[1]))
    plot.set_xlim((plot.get_xlim()[0], x[-1]))
    canvas.draw()

  def onPixSelected(self, widget, piy, pix, rc):
    # Plot the rocking curve.
    self.rc_plot.cla()
    angs = numpy.array([ang for (ang, path) in rc])
    i = []
    for (ang, path) in rc:
      data = edf.RawImage()
      data.read(path)
      data = data.getData().reshape(data.height, data.width)
      # Should do the integral here on ROI only.
      ((x, y), (w, h)) = self.options.getRoi()
      i.append(data[y:y+h, x:x+w].sum())
    y = numpy.array(i)
    self.rc_plot.plot(angs, y, '.-', color = "#FF0000")
    self.rc_plot.set_title("Rocking curve at piy = %g $\mu$m, pix = %g $\mu$m\n" % (piy, pix))
    # self.rc_plot.legend(['Data', 'Fit', 'Guess'])
    if self.model.rcAngle == "theta" or self.model.rcAngle == "phi" or self.model.rcAngle == "omega" or self.model.rcAngle == "mu" or self.model.rcAngle == "nu" or self.model.rcAngle == "eta":
      self.rc_plot.set_xlabel('Angle $\%s$ (degrees)' % self.model.rcAngle)
    else:
      self.rc_plot.set_xlabel('Angle %s (degrees)' % self.model.rcAngle)
    self.rc_plot.set_ylabel('Intensity (a.u.)')
    if self.rcview.canvas is None:
      self.rcview.setFigure(self.rc_fig)
    else:
      self.rcview.canvas.draw()

  def display3D(self, qx, qy, qz):
    mlab.figure()
    #mlab.contour3d(QX,QY,QZ,INT,contours=5,opacity=0.5)
    #mlab.colorbar(title="log(int)",orientation="vertical")
    #mlab.axes(nb_labels=5,xlabel='Qx',ylabel='Qy',zlabel='Qz')
    mlab.show()

  def show3D(self, (model, iter)):
    (x, y, z) = \
      model.get(iter, ID1_gui_model.ScansModel.COL_X_X, ID1_gui_model.ScansModel.COL_Y_X, ID1_gui_model.ScansModel.COL_Z_X)
    th = threading.Thread(group = None, target = self.display3D, kwargs = {"qx": x, "qy": y, "qz": z})
    th.start()
    th.join()
    return False

  def fromPixelsToStrain(self, pos):
    if self.options.method == 1:
      sx, sy = model.computeStressFromPixel(pos[:, 2], pos[:, 3], self.options.spinLattice.get_value(),
                                            numpy.array(self.options.getPeakIndices()))
      sx *= 100.
      sy *= 100.
      sz = None
      fwhmx = pos[:, 5] * 2.35482 / model.detector["chpdeg"][0]
      fwhmy = pos[:, 6] * 2.35482 / model.detector["chpdeg"][1]
      fwhmz = None
    else:
      (sx, sy, sz) = model.computeStressFromRecip(pos[:, 2], pos[:, 3], pos[:, 4], self.options.spinLattice.get_value(),
                                                  numpy.array(self.options.getRecipIndices()))
      sx *= 100.
      sy *= 100.
      sz *= 100.
      fwhmx = pos[:, 5] * 2.35482
      fwhmy = pos[:, 6] * 2.35482
      fwhmz = pos[:, 7] * 2.35482
    return (pos[:, 1], sx, sy, sz, fwhmx, fwhmy, fwhmz, \
            pos[:, 2], pos[:, 3], pos[:, 4], \
            pos[:, 5], pos[:, 6], pos[:, 7])

  def onPiySelected(self, widget, piy, rcs):
    # Restrict to ~10 curves.
    mod = int(round(float(len(rcs)) / 10.))
    rcs = [dt for (i, dt) in enumerate(rcs) if i % mod == 0]
    # Plot rocking curve for all pix.
    self.rc_plot.cla()
    colors = edf.jetScale(numpy.arange(0., 255.01, 255. / (len(rcs) - 1))).reshape(len(rcs), 3)
    for (color, (pix, rc)) in zip(colors, rcs):
      angs = numpy.array([ang for (ang, path) in rc])
      i = []
      for (ang, path) in rc:
        data = edf.RawImage()
        data.read(path)
        data = data.getData().reshape(data.height, data.width)
        # Should do the integral here on ROI only.
        ((x, y), (w, h)) = self.options.getRoi()
        i.append(data[y:y+h, x:x+w].sum())
      y = numpy.array(i)
      
      self.rc_plot.plot(angs, y, '.-', color = "#%02x%02x%02x" % tuple(color))
    self.rc_plot.set_title("Rocking curves at piy = %g $\mu$m\n (every %d pix value)" % (piy, mod))
    # self.rc_plot.legend(['Data', 'Fit', 'Guess'])
    self.rc_plot.set_xlabel('Angle $\%s$ (degrees)' % self.model.rcAngle)
    self.rc_plot.set_ylabel('Intensity (a.u.)')
    if self.rcview.canvas is None:
      self.rcview.setFigure(self.rc_fig)
    else:
      self.rcview.canvas.draw()

  def updateGraphMap(self, data):
    if data is None and os.path.isfile("map.dat"):
      data = []
      for line in open("map.dat", "r").xreadlines():
        if len(line.strip()) > 0:
          data.append(map(float, line.split()))
      data = numpy.array(data)
      print data.shape
    # We switch flat data to 3D arrays.
    data3D = []
    dataX = []
    prev = None
    for vals in data:
      print vals
      if prev == vals[0]:
        dataX.append(vals)
      elif len(dataX) > 2:
        print "ok", len(dataX)
        data3D.append(numpy.array(dataX[0:18]))
        dataX = []
      prev = vals[0]
    data3D.append(numpy.array(dataX[0:18]))
    data = numpy.array(data3D)
    print data.shape
    # We plot if
    self.canvasX.hide()
    self.canvasY.hide()
    self.canvasZ.hide()
    self.canvasQX.hide()
    self.canvasQY.hide()
    self.canvasQZ.show()
    self.plotQZ.cla()
    cs = self.plotQZ.contourf(data[:, :, 0],data[:, :, 1],(2*numpy.pi/data[:, :, 4]*numpy.sqrt(8)),150)
    self.plotQZ.set_xlabel(r"pix (micrometer)")
    self.plotQZ.set_ylabel(r"piy (micrometer)")
    self.plotQZ.get_figure().colorbar(cs)
    self.canvasQZ.draw()

  def onExportGraph(self, bt, selection):
    (model, iter) = selection.get_selected()
    if iter is None:
      return
    obj = model.exportPiy(iter)
    if obj is None:
      return
    (piy, pos) = obj

    dialog = gtk.FileChooserDialog("Export graphs profile as data", None,
                                   gtk.FILE_CHOOSER_ACTION_SAVE,
                                   (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                                    gtk.STOCK_SAVE, gtk.RESPONSE_OK))
    dialog.set_current_name("epsilon data piy = %g.dat" % piy)
    response = dialog.run()
    if response == gtk.RESPONSE_OK:
      f = open(dialog.get_filename(), "w")
      (x, sx, sy, sz, fwhmx, fwhmy, fwhmz, \
       px, py, pz, fpx, fpy, fpz) = self.fromPixelsToStrain(pos)
      if sz is None:
        if fwhmx is None:
          lst = zip(x, sx, sy, px, py)
          f.write("# (2theta x nu) files\n")
          f.write("# piy Xdeformation (%) Ydeformation (%) xPeakPos (pixel) yPeakPos (pixel)\n")
        else:
          lst = zip(x, sx, sy, fwhmx, fwhmy, px, py, fpx, fpy)
          f.write("# (2theta x chi) files\n")
          f.write("# piy Xdeformation (%) Ydeformation (%) XFWHM (degree) YFWHM (degree) xPeakPos (pixel) yPeakPos (pixel)\n")
      else:
        lst = zip(x, sx, sy, sz)
        f.write("# (Qx, Qy, Qz) files\n")
        f.write("# piy Xdeformation (%) Ydeformation (%) Zdeformation (%)\n")
      for (vals, ) in zip(lst):
        for v in vals:
          f.write("  %g" % v)
        f.write("\n")
      f.close()
      manager = gtk.recent_manager_get_default()
      manager.add_item(dialog.get_uri())
    dialog.destroy()

  def onExportProj(self, bt, selection):
    (model, iter) = selection.get_selected()
    if iter is None:
      return
    (pix, ) = model.get(iter, ID1_gui_model.ScansModel.COL_PIX)
    if pix == 0.:
      return
    
    dialog = gtk.FileChooserDialog("Export projections as PDF", None,
                                   gtk.FILE_CHOOSER_ACTION_SAVE,
                                   (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                                    gtk.STOCK_SAVE, gtk.RESPONSE_OK))
    dialog.set_current_name("recip proj pix = %g.pdf" % pix)
    response = dialog.run()
    if response == gtk.RESPONSE_OK:
      pp = backend_pdf.PdfPages(dialog.get_filename())
      pp.savefig(self.plotQX.get_figure())
      pp.savefig(self.plotQY.get_figure())
      pp.savefig(self.plotQZ.get_figure())
      pp.close()
      manager = gtk.recent_manager_get_default()
      manager.add_item(dialog.get_uri())
    dialog.destroy()

  def onScanSelected(self, widget, data):
    ## self.proj.hide()
    self.edf.set(EDF_display.KIND_RAW, data)
    self.edf.show()

def parse():
  parser = OptionParser("usage: chi_gui.py [options]")
  parser.add_option("-o", "--out-dir", type="string", default=".", metavar="OUTDIR",
                    help="specify the path to put results in.")

  parser.add_option("-s", "--spec", type="string", default=None, metavar="SPECFILE",
                    help="specify the spec file to read.")

  parser.add_option("-i", "--prefix-image", type="string", default="", metavar="IMG",
                    help="give the naming scheme for EDF files.")

  parser.add_option("-u", "--sub-directory", type="string", default=".", metavar="DIR",
                    help="give the naming scheme for sub directories for EDF files.")

  parser.add_option("-r", "--root-data", type="string", default=os.getcwd(), metavar="DIR",
                    help="give the root path to find EDF files.")

  parser.add_option("-a", "--alignments", type="string", default=None,
                    metavar="FILE1 FILE2 FILE3", nargs = 3,
                    help="give the three files used to calibrate the detector.")

  parser.add_option("-e", "--energy", type="float", default=9000., metavar="ENERGY",
                    help="give the beam energy in eV.")

  parser.add_option("-n", "--scan-numbers", type="string", default=None, metavar="SCANFILE",
                    help="specify a file with the scan ids.")

  return parser

if __name__ == "__main__":
  parser = parse()
  (options, args) = parser.parse_args()

  window = gtk.Window(gtk.WINDOW_TOPLEVEL)
  window.connect("delete_event", gtk.main_quit)
  window.connect("destroy", gtk.main_quit)

  paned = ID1_gui(options, args)
  window.add(paned)
  window.show_all()

  if len(args) > 2:
    paned.lock()
    if len(args) > 3:
      paned.setRcAngle(args[3])
    if not paned.populateFromCache():
      paned.setSpecFromFile(sys.argv[2])
      paned.populate()
    paned.unlock()

  gtk.main()
