Interface to analyse peak diffraction in reciprocal space
=========================================================

1. Introduction
   ------------

This interface has a three panels layout.
- the left panel is used to set-up options and the read spec.
- the central panel is a tree view of the (X,Y) spots where the
  a rocking angle measurement is done.
- the right panel is used to visualise data, either input image
  from the detector or computed maps of peak position or strain.


2. The different panels
   --------------------

2.1 The option panel

On top of all options, there are two buttons, one to choose the
output directory where generated files will be written. By default
this is the current working directory (directory from where the
script is run from command-line or the home directory otherwise).
We call this directory RESULT_PATH in the following manual.
Next to this button, there is an entry to give a naming pattern
for the generated files. If this entry is left empty, it will be
automatically filled with the name of the spec file (minus the
.spec extension). This naming scheme is called OUT_NAMING in the
following.

2.1.1 The "spec file" frame

This frame handles options dealing with the spec file. The top button
is used to select this file. Below it, one can select the range
of scans to be taken into account for later analysis. There are
three spin buttons A, B, S meaning that scans from A to B with a
step S will be taken into account.
Below, there is a "read file" button to read the selected spec
file in the given range. This is a time consuming operation. It
is a blocking operation and is currently not parallelized.
When finished it creates a file RESULT_PATH/OUT_NAMING.matrices
containing a dump of the relevant information.
If a file named RESULT_PATH/OUT_NAMING.matrices already exists,
a small label is printed out next to the "read file" button.
This means that this file will be read instead of scaning the
whole spec file. It is much faster.
TODO: check that this file is compatible with the given scan
range. Currently if this file exists, the provided scan range
is ignored.

When the spec file has been read (or the .matrices file instead)
the program knows the (X,Y) grid of spots where a rocking angle
measurement has been done. It then populates the treeview of
the central panel. See later in the manual about the central
panel. This operation can be time consuming if the disk has a
slow access. But it is non-blocking. One can continue to setup
options or even begin to analyse results in the mean time.

After reading the spec file, the program tries to guess which
angle is varying during the rocking measurement. If not set
properly, one can change it with the combobox named "rc angle".

After the spec file has been read, some analysis can be done
based on the data contained in the spec file. Check the desired
quantities to analyse ("roi1-5" or "ccdint1-2") and click the
"generate PNGs" button. This operation is very time consuming
also, depending on the number of checked boxes. It is very
memory demanding also (it generates huge maps for each scan).
It is parallelised and will run parallel on the number of cores
of the host machine. This operation is non-blocking.

2.1.2 The "Diffraction analysis" frame

This frame deals with additional information required to compute
the reciprocal space associated to the detector images. One has
to select the beam energy in eV. Then provide some callibration
information for the detector. Either enter manually the position
of the pristine beam and the number of pixel per angle degree, or
click the right folder button to open a file chooser dialog to
select three files and compute these values automatically. Use
the "control" key to select several files at once.

The following option "detector orientation" is not currently used.

Then choose the region of interest which is the region of the
detector where there is the diffraction spot. Enter it manually
or open a detector file with the central panel and use the right
panel to select a region with the mouse, see the third panel part
of this manual for further details.

Choose a pixel averaging (by default a 2x2 value) with the
following spin buttons.

Choose the method used to find the peak position in reciprocal
space (either gaussian fit, slow but accurate or the maximum
value position, faster but sensitive to noise and less accurate).

Choose the number of points to discretise the reciprocal space
with the three spin buttons below.

Then click the "calculate Q" button. This operation is time
consuming and blocking. It is parallelised and will use all the
cores of the hosting machine. When finished it generates a 
RESULT_PATH/OUT_NAMING.qmesh file.
If this file already exists before clicking the "calculate Q"
button, it is printed out in a label next to the button. Then,
when clicking on the button, this file will be read instead of
performing the calculation. It is much faster.

When finished, the resulting maps of peak position in reciprocal
space are available in the tab "Q maps viewer" of the right panel.

2.1.3 The "strain calculation" panel

This operation is simple and not time consuming. It requires to
have the Q mesh calculated. It requires also to specify the
Miller indices of the diffracting peak and the lattice parameter
of the sample.

the resulting maps of strain is available in the tab "strain map
viewer" of the right panel.

2.2 The central panel

The central panel displays the EDF file data base related to the
read spec file. After clicking the "read file" of the spec frame,
this panel is refreshed.

Data are sorted as a tree of the (x,y) scan, showing the y nodes
first, then for each y, the x nodes and then for each x node, the
different scan at different value of the rocking angle.

By clicking on one EDF file, it is displayed on the right panel.

By cliking on a given x node, the right panel displays the
corresponding rocking curve. (TO BE DONE)

The location of the EDF file data base is setup with the text entries
at the bottom of this panel. One needs to give the path where to find
the EDF files. One needs also to give the naming scheme of the
subdirectories and of the file naming scheme. For instance, if files
look like that: fib4_Si_004_429800/fib4_Si_map1_429800_9991.edf.gz, one
needs to enter in the first text entry "fib4_Si_004_%d" and
"fib4_Si_map1_%d" in the second. The "%d" value will by replaced by
a value of the rocking angle.

The scan of the EDF file data base can be long, but it's not blocking
anything of the interface. It is anyhow recommended to wait for the
complete scan before running a Q calculation since this scan will
check if all necessary EDF files are present. If some files are missing
a warning dialog will show up in this central panel. Check the path and
the naming scheme and press the "update" button.

2.3 The right panel

This panel is used to display EDF files. Currently, there is a bug
and EDF are represented with x and y inverted (it displays the
transposition of the EDF data).

There is a toolbar at the bottom with several icons:
- zooming icon, used to fit the EDF file to the screen or to see it
  1:1 pixel.
- Raw, Norm., Dark c. Flat c. icons, not used here. (TO BE REMOVED)
- Save as icon, export image as a PNG file (just the pixels).
- Save icon, export the image as a new EDF file (not relevant here).
- Print icon, save the image plus a title and the legend into a PDF
  file and a PNG file.
- Magnifier icon, used to select a rectangular area of the image. One
  can specify it with the spin buttons or with the mouse by clicking
  on the image and dragging. The apply button can be used to transfer
  the selected region to the left panel for the region of interest
  selection.

This right panel also holds some tab (Q maps viewer and strain map
viewer) where result of calculations are shown. These tabs have also
a toolbar:
- a save icon, used to export the result to a PNG file.
- a fullscreen icon, used to show the maps fullscreen.


3. Command line options
   --------------------

One can obtain the list of available options by ruinning:
 python chi_gui.py -h  or
 python chi_gui.py --help

Most options are used to setup values in the interface.

3.1 Results and spec file

  -o OUTDIR, --out-dir=OUTDIR
                        specify the path to put results in.
  -s SPECFILE, --spec=SPECFILE
                        specify the spec file to read.

3.2 EDF file data base

  -i IMG, --prefix-image=IMG
                        give the naming scheme for EDF files.
  -u DIR, --sub-directory=DIR
                        give the naming scheme for sub directories for EDF
                        files.
  -r DIR, --root-data=DIR
                        give the root path to find EDF files.

3.3 Beam calibration

  -a FILE1 FILE2 FILE3, --alignments=FILE1 FILE2 FILE3
                        give the three files used to calibrate the detector.
  -e ENERGY, --energy=ENERGY
                        give the beam energy in eV.
