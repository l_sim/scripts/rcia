#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2012-2013 Damien Caliste <damien.caliste@cea.fr>,
#                         Thu Nhi Tran Thi <thu-nhi.tran-thi@esrf.fr>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA

import gtk, gobject
import os
import numpy
import edf, chi

class ScansModel(gtk.TreeStore):
  COL_PATH      =  0
  COL_PATH0     = 24
  COL_IMGNR     = 23
  COL_PIX_INFO  =  1
  COL_PIX       =  2
  COL_PIY       =  3
  COL_RC_ANG       = 18
  COL_ADD_ANG   =  4
  COL_VALID     =  5
  COL_X_X       =  6
  COL_X_Y       =  7
  COL_X_FIT     =  8
  COL_Y_X       =  9
  COL_Y_Y       = 10
  COL_Y_FIT     = 11
  COL_Z_X       = 12
  COL_Z_Y       = 13
  COL_Z_FIT     = 14
  COL_EDF       = 15
  COL_PEAK_POS  = 16
  COL_ROCKING   = 17
  COL_X_YZ      = 19
  COL_Y_ZX      = 20
  COL_Z_XY      = 21
  COL_RC_PATHS  = 22
  COL_PIY_LBL   = 25

  __gsignals__ = {'start-populating' : (gobject.SIGNAL_RUN_LAST,
                                        gobject.TYPE_NONE,
                                        (gobject.TYPE_UINT,)),
                  'stop-populating' : (gobject.SIGNAL_RUN_LAST,
                                       gobject.TYPE_NONE,
                                       (gobject.TYPE_UINT,)),
                  'populating' : (gobject.SIGNAL_RUN_LAST,
                                  gobject.TYPE_NONE,
                                  (gobject.TYPE_FLOAT,)),
                  }
  
  def __init__(self):
    super(ScansModel, self).__init__(gobject.TYPE_STRING,
                                     gtk.gdk.Pixbuf,
                                     gobject.TYPE_DOUBLE,
                                     gobject.TYPE_DOUBLE,
                                     gobject.TYPE_PYOBJECT,
                                     gobject.TYPE_BOOLEAN,
                                     gobject.TYPE_PYOBJECT,
                                     gobject.TYPE_PYOBJECT,
                                     gobject.TYPE_PYOBJECT,
                                     gobject.TYPE_PYOBJECT,
                                     gobject.TYPE_PYOBJECT,
                                     gobject.TYPE_PYOBJECT,
                                     gobject.TYPE_PYOBJECT,
                                     gobject.TYPE_PYOBJECT,
                                     gobject.TYPE_PYOBJECT,
                                     gobject.TYPE_PYOBJECT,
                                     gobject.TYPE_PYOBJECT,
                                     gobject.TYPE_PYOBJECT,
                                     gobject.TYPE_DOUBLE,
                                     gobject.TYPE_PYOBJECT,
                                     gobject.TYPE_PYOBJECT,
                                     gobject.TYPE_PYOBJECT,
                                     gobject.TYPE_PYOBJECT,
                                     gobject.TYPE_UINT,
                                     gobject.TYPE_STRING,
                                     gobject.TYPE_STRING)
    self.detector = {"en": 9000., "cch": [398.416, 189.208], "chpdeg": [241, 241],
                     "orient": ('z-', 'y+')}
    self.dirname = None
    self.pathDict = None
    self.method = 1
    self.saveIntegral = False
    self.rcAngle = None
    self.edfFileMap = {}

  def getChild(self, itParent, iCol, value):
    it = self.iter_children(itParent)
    if it is not None:
      while (it):
        (value_, ) = self.get(it, iCol)
        if not iCol == ScansModel.COL_PATH and abs(value_ - value) < 0.1:
          return it
        it = self.iter_next(it)
    it = self.append(itParent)
    self.set(it, iCol, value, ScansModel.COL_VALID, True)
    return it

  def getIter(self, sample, thy, piy):
    if sample is None:
      sample = "Unknown sample"
    return self.getChild(self.getChild(self.getChild(None, ScansModel.COL_PATH, sample),
                                       ScansModel.COL_PIY, thy),
                         ScansModel.COL_PIX, piy)

  def makeEDFList(self, dirname, storeId = True, infoFunc = None):
    entries = os.listdir(dirname)
    lst = {}
    if (storeId):
      pat = re.compile(r".*_(?P<id>[0-9]+).edf(.gz)?$")
    N = len(entries)
    for (i, entry) in enumerate(entries):
      if infoFunc is not None and i % 250 == 0:
        infoFunc("Lookup for EDF files in '%s'" % dirname, fraction = float(i) / float(N))
      filename = os.path.join(dirname, entry)
      if os.path.isfile(filename) and (entry.endswith(".edf") or
                                       entry.endswith(".edf.gz")):
        if (storeId):
          lst[pat.match(entry).group('id')] = filename
        else:
          lst[entry.replace(".gz", "")] = filename
      elif os.path.isdir(filename) and not(entry.startswith(".")):
        lst.update(self.makeEDFList(filename, storeId, infoFunc))
    return lst

  def getRawImageAt(self, path, nr):
    if not(path in self.edfFileMap):
      if not(os.path.isfile(path)):
        return None
      self.edfFileMap[path] = edf.multiRawImages(path)
    return self.edfFileMap[path][nr]

  def populateFromXAScan(self, mat, options):
    missingFiles = 0
    N = int(mat["rcLength"].sum())
    self.emit("start-populating", N)
    frac = 0.
    # Trick here
    if len(mat[self.rcAngle].shape) == 1:
      matAngles = mat['number']
    else:
      matAngles = mat[self.rcAngle]
    i = 0
    for scans in mat['scheme']:
      itY = self.append(None)
      lbl = scans.keys()[0]
      self.set(itY, ScansModel.COL_VALID, True,
               ScansModel.COL_PIY_LBL, lbl)
      for s in scans[lbl]:
        itX = self.append(itY)
        self.set(itX, ScansModel.COL_VALID, True,
                 ScansModel.COL_PIX, mat['X'][i, 0, 0])
        lg = mat["rcLength"][i, 0]
        paths = mat.get("paths", (options.getDataScheme(), ) * lg)
        nr0 = mat["number"][i, 0, 0]
        for (nr, ang, path) in zip(mat["number"][i, 0, :lg], mat[self.rcAngle][i, 0, :lg], paths):
          it = self.append(itX)
          if "%" in path:
            filename = path % nr #self.getDataFile(nr, round(ang*10000.))
            filename0 = path % nr0 #self.getDataFile(nr, round(ang*10000.))
          else:
            filename = path
            filename0 = path
          if os.path.isfile(filename) or os.path.isfile(filename0):
            self.set(it, ScansModel.COL_PATH, filename,
                     ScansModel.COL_PATH0, filename0,
                     ScansModel.COL_VALID, True,
                     ScansModel.COL_IMGNR, int(nr - nr0),
                     ScansModel.COL_RC_ANG, ang)
          else:
            self.set(it, ScansModel.COL_PATH, "no file #%d" % nr,
                     ScansModel.COL_VALID, False,
                     ScansModel.COL_RC_ANG, ang)
            missingFiles += 1
        frac += 1. / lg
        self.emit("populating", frac)
        i += 1
    self.emit("stop-populating", missingFiles)

  ## def findEDF(self, lst, nr, dirname, samplename):
  ##   tag = "_%04d.edf" % nr
  ##   for entry in lst:
  ##     if entry.endswith(tag) or entry.endswith(tag + ".gz"):
  ##       return entry
  ##   return None
  def populateFromPScan(self, mat, options):
    missingFiles = 0
    Xavg = mat['X'].sum(2) / mat['X'].shape[2]
    Yavg = mat['Y'].sum(2) / mat['Y'].shape[2]
    self.emit("start-populating", mat['X'].size)
    frac = 0.
    for (lineX, lineY, lineNr) in \
          zip(Xavg.T, Yavg.T, mat['number'].transpose((1, 0, 2))):
      itY = self.append(None)
      self.set(itY, ScansModel.COL_VALID, True,
               ScansModel.COL_PIY, lineY.sum() / lineY.size)
      for (x, imgnr) in zip(lineX, lineNr):
        itX = self.append(itY)
        self.set(itX, ScansModel.COL_VALID, True,
                 ScansModel.COL_PIX, x)
        paths = mat.get("paths", (options.getDataScheme(), ) * len(imgnr))
        for (nr, ang, path) in zip(imgnr, mat[self.rcAngle], paths):
          it = self.append(itX)
          if "%" in path:
            filename = path % nr #self.getDataFile(nr, round(ang*10000.))
            filename0 = path % 0 #self.getDataFile(nr, round(ang*10000.))
          else:
            filename = path
            filename0 = path
          if os.path.isfile(filename) or os.path.isfile(filename0):
            self.set(it, ScansModel.COL_PATH, filename,
                     ScansModel.COL_PATH0, filename0,
                     ScansModel.COL_VALID, True,
                     ScansModel.COL_IMGNR, int(nr),
                     ScansModel.COL_RC_ANG, ang)
          else:
            self.set(it, ScansModel.COL_PATH, "no file #%d" % nr,
                     ScansModel.COL_VALID, False,
                     ScansModel.COL_IMGNR, int(nr),
                     ScansModel.COL_RC_ANG, ang)
            missingFiles += 1
      frac += 1. / float(Yavg.shape[1])
      self.emit("populating", frac)
    self.emit("stop-populating", missingFiles)

  ## def findEDFNew(self, lst, nr, dirname, samplename):
  ##   return os.path.join(dirname, samplename + "_%04d.edf.gz" % nr)
  def populateFromMatrices(self, mat, options):
    self.getDataFile = options.getDataFile
    self.clear()
    if mat['type'] == "PScan":
      self.populateFromPScan(mat, options)
    elif mat['type'] == "XAScan":
      self.populateFromXAScan(mat, options)
    return

  def populateFromCache(self, infoFunc = None):
    entries = os.listdir(os.getcwd())
    cache = []
    desc = {'names': ("Phi", ), 'formats': ("f8", )}
    for entry in entries:
      if entry.startswith("sample_") and entry.endswith(".tree"):
        cache.append(entry)
    for (i, entry) in enumerate(cache):
      if infoFunc is not None:
        infoFunc("Read %d/%d cache file." % (i + 1, len(cache)), fraction = float(i) / float(len(cache)))
      samplename = entry[7:-5]
      itSample = self.getChild(None, ScansModel.COL_PATH, samplename)
      x = None
      y = None
      itX = None
      for line in open(entry, "r"):
        vals = line[:-1].split(None, 3)
        if not(y == vals[0]):
          y = vals[0]
          itY = self.getChild(itSample, ScansModel.COL_PIY, float(y))
        if not(x == vals[1]):
          x = vals[1]
          if itX is not None:
            self.set(itX, ScansModel.COL_VALID, pvalid,
                     ScansModel.COL_ADD_ANG, numpy.array(scan, dtype = desc),
                     ScansModel.COL_RC_PATHS, edfFiles)
          itX = self.getChild(itY, ScansModel.COL_PIX, float(x))
          pvalid = True
          edfFiles = []
          scan = []
        valid = not(vals[3].startswith("<i>"))
        it = self.append(itX)
        self.set(it, ScansModel.COL_PATH, vals[3],
                 ScansModel.COL_VALID, valid,
                 ScansModel.COL_RC_ANG, float(vals[2]))
        scan.append((float(vals[2]), ))
        if valid:
          edfFiles.append(vals[3])
        else:
          edfFiles.append(None)
        pvalid = pvalid and valid
      self.set(itX, ScansModel.COL_VALID, pvalid,
               ScansModel.COL_ADD_ANG, numpy.array(scan, dtype = desc),
               ScansModel.COL_RC_PATHS, edfFiles)
      print "treeview populated"
    return len(cache) > 0

  def populate(self, spec, dirname, samplename, infoFunc = None):
    if not self.dirname == dirname or self.pathDict is None:
      if infoFunc is not None:
        infoFunc("Lookup for EDF files in '%s'" % dirname)
      self.dirname = dirname
      self.pathDict = self.makeEDFList(dirname, storeId = False, infoFunc = infoFunc)
      print "list done (%d)" % len(self.pathDict)
    if infoFunc is not None:
      infoFunc("Start populating treeview (%d)" % len(spec), fraction = 0.)
##     for v in spec:
##       (thy, piy, scan) = v
##       parent = self.getIter(samplename, thy, piy)
##       edfFiles = zip(scan["expnr"], scan["chi"])
##       pvalid = True
##       print "entry %d (%d)" % (scan["expnr"][0], len(edfFiles))
##       for (i, chi) in edfFiles:
##         it = self.append(parent)
##         if i not in lst:
##           path = "<i>Scan %s not found</i>" % i
##           valid = False
##         else:
##           path = lst[i]
##           valid = True
##         self.set(it, ScansModel.COL_PATH, path,
##                  ScansModel.COL_VALID, valid,
##                  ScansModel.COL_RC_ANG, chi)
##         pvalid = pvalid and valid
##       self.set(parent, ScansModel.COL_ADD_ANG, scan, ScansModel.COL_VALID, pvalid)
    itSample = self.getChild(None, ScansModel.COL_PATH, samplename)
    y_prev = 0.
    N = len(spec)
    for (i, (x, y, scan)) in enumerate(spec):
      if infoFunc is not None and i % 10 == 0:
        infoFunc("Pscan for sample '%s'" % samplename, fraction = float(i) / float(N))
      if abs(y_prev - y) > 0.5:
        itY = self.getChild(itSample, ScansModel.COL_PIY, y)
        y_prev = y
      itX = self.getChild(itY, ScansModel.COL_PIX, x)
      pvalid = True
      edfFiles = []
      for (entry, phi, n) in zip(scan["entry"], scan["Phi"], scan["imgnr"]):
        entry = entry.strip()
        it = self.append(itX)
        if entry not in self.pathDict:
          path = "<i>Scan %d not found</i>" % n
          edfFiles.append(None)
          valid = False
        else:
          path = self.pathDict[entry]
          edfFiles.append(path)
          valid = True
        self.set(it, ScansModel.COL_PATH, path,
                 ScansModel.COL_VALID, valid,
                 ScansModel.COL_RC_ANG, phi)
        pvalid = pvalid and valid
      self.set(itX, ScansModel.COL_ADD_ANG, scan, ScansModel.COL_VALID, pvalid,
               ScansModel.COL_RC_PATHS, edfFiles)
    print "treeview populated"
    f = open("sample_%s.tree" % samplename, "w")
    ity = self.iter_children(itSample)
    while ity is not None:
      (piy, ) = self.get(ity, ScansModel.COL_PIY)
      itx = self.iter_children(ity)
      while itx is not None:
        (pix, ) = self.get(itx, ScansModel.COL_PIX)
        its = self.iter_children(itx)
        while its is not None:
          (path, chi) = self.get(its, ScansModel.COL_PATH, ScansModel.COL_RC_ANG)
          f.write("%g\t%g\t%g\t\t%s\n" % (piy, pix, chi, path))
          its = self.iter_next(its)
        itx = self.iter_next(itx)
      ity = self.iter_next(ity)
    f.close()

  def getValidChi(self, parent):
    it = self.iter_children(parent)
    valid = []
    while (it):
      (v, ) = self.get(it, ScansModel.COL_VALID)
      valid.append(v)
      it = self.iter_next(it)
    return numpy.array(valid)

  def computePix(self, it):
    (v, pix, edfFiles) = self.get(it, ScansModel.COL_ADD_ANG,
                                  ScansModel.COL_PIX,
                                  ScansModel.COL_RC_PATHS)
    if pix == 0.:
      raise ValueError

    if self.saveIntegral:
      outputEDF = os.path.join(os.path.dirname(edfFiles[0]), "integral piy = %g.edf" % piy)
    else:
      outputEDF = None

    if self.method == 1:
      ((x0, y0), chiIntegral, chiEDF, intX, px, intY, py) = \
            chi.getPeakAtMaxChiCurve(listEDF = edfFiles, extended = True,
                                     outputEDF = outputEDF)
      if outputEDF is not None:
        chiEDF = edf.RawImage()
        chiEDF.read(outputEDF)
        data = chiEDF.getData().reshape(chiEDF.height, chiEDF.width)
        x = range(data.shape[1])
        intX = data.sum(0)
        px = chi.fitData(x, intX)
        x = range(data.shape[0])
        intY = data.sum(1)
        py = chi.fitData(x, intY)
      x = None
      y = None
      z = None
      intZ = None
      pz = None
      x_yz = None
      y_zx = None
      z_xy = None
    else:
      ((qx, qy, qz), data, x, intX, px, y, intY, py, z, intZ, pz) = \
        chi.getPeakFromRecipSpace(v["mu"], v["phi"], v["nu"], v["delta"], v["chi"], v["eta"],
                                  listEDF = edfFiles, extended = True,
                                  detector = self.detector, outputEDF = outputEDF)
      chiEDF = None
      x_yz = xu.maplog(data.sum(axis=0),5,0).T
      y_zx = xu.maplog(data.sum(axis=1),5,0).T
      z_xy = xu.maplog(data.sum(axis=2),5,0).T
    return (chiEDF, chiIntegral, x, intX, px, x_yz, y, intY, py, y_zx, z, intZ, pz, z_xy)

  def updatePix(self, it, data):
    (chiEDF, chiIntegral, \
     x, intX, px, x_yz, \
     y, intY, py, y_zx, \
     z, intZ, pz, z_xy) = data
    #self.computePix(edfFiles, method, outputEDF)

    if self.method == 1:
      self.set(it, ScansModel.COL_X_X, None, ScansModel.COL_X_Y, intX,
               ScansModel.COL_X_FIT, px)
      self.set(it, ScansModel.COL_Y_X, None, ScansModel.COL_Y_Y, intY,
               ScansModel.COL_Y_FIT, py)
      self.set(it, ScansModel.COL_ROCKING, chiIntegral)
      self.set(it, ScansModel.COL_EDF, chiEDF)
    else:
      self.set(it, ScansModel.COL_X_X, x, ScansModel.COL_X_Y, intX,
               ScansModel.COL_X_FIT, px, ScansModel.COL_X_YZ,
               xu.maplog(data.sum(axis=0),5,0).T)
      self.set(it, ScansModel.COL_Y_X, y, ScansModel.COL_Y_Y, intY,
               ScansModel.COL_Y_FIT, py, ScansModel.COL_Y_ZX,
               xu.maplog(data.sum(axis=1),5,0).T)
      self.set(it, ScansModel.COL_Z_X, z, ScansModel.COL_Z_Y, intZ,
               ScansModel.COL_Z_FIT, pz, ScansModel.COL_Z_XY,
               xu.maplog(data.sum(axis=2),5,0).T)
      self.set(it, ScansModel.COL_EDF, None)

  def computePiy(self, it):
    data = []
    (piy, ) = self.get(it, ScansModel.COL_PIY)
    # Create a list of pix iterators to calculate.
    jobs = []
    ch = self.iter_children(it)
    while (ch):
      (pix, px, py, pz) = self.get(ch, ScansModel.COL_PIX, ScansModel.COL_X_FIT,
                                   ScansModel.COL_Y_FIT, ScansModel.COL_Z_FIT)
      if px is None or py is None:
        jobs.append(ch)
      else:
        # Save already calculated values.
        if pz is None:
          pz = (0., 0., 0.)
        data.append((piy, pix, px[0], py[0], pz[0], px[2], py[2], pz[2]))
      ch = self.iter_next(ch)
    # Do the different pix.
    for ch in jobs:
      while gtk.events_pending():
        gtk.main_iteration()
      self.updatePix(ch, self.computePix(ch))
    # Gather results.
    for ch in jobs:
      (pix, px, py, pz) = self.get(ch, ScansModel.COL_PIX, ScansModel.COL_X_FIT,
                                   ScansModel.COL_Y_FIT, ScansModel.COL_Z_FIT)
      if pz is None:
        pz = (0., 0., 0.)
      data.append((piy, pix, px[0], py[0], pz[0], px[2], py[2], pz[2]))
    data = numpy.array(data)
    self.set(it, ScansModel.COL_PEAK_POS, data)
    return data

  def computeMap(self, it):
    data = []
    ch = self.iter_children(it)
    while (ch):
      while gtk.events_pending():
        gtk.main_iteration()
      (ppos, ) = self.get(ch, ScansModel.COL_PEAK_POS)
      if ppos is None:
        ppos = self.computePiy(ch)
      (thy, ) = self.get(ch, ScansModel.COL_PIY)
      data.append(ppos)
      ch = self.iter_next(ch)
    # We flatten the data for disk exportation.
    data = numpy.vstack(data)
    self.set(it, ScansModel.COL_PEAK_POS, data)
    f = open("map.dat", "w")
    prev = None
    for vals in data:
      if not prev == vals[0]:
        prev = vals[0]
        f.write("\n")
      f.write("%g %g  %g %g %g  %g %g %g\n" % tuple(vals))
    f.close()
    return data

  def computeStressFromPixel(self, px, py, a0, hkl):
    dx = px - self.detector["cch"][1]
    ddx = -dx / self.detector["chpdeg"][0] * numpy.pi / 180. / 2.
    dy = py - self.detector["cch"][0]
    ddy = -dy / self.detector["chpdeg"][1] * numpy.pi / 180. / 2.
    # http://physics.nist.gov/cgi-bin/cuu/Value?minvev|search_for=electron+volt
    theta = numpy.arcsin(12398.41930 / self.detector["en"] / 2. / a0 * numpy.sqrt((hkl * hkl).sum()))
    sx = numpy.sin(theta) / numpy.sin(theta + ddx) - 1.
    sy = numpy.sin(theta) / numpy.sin(theta + ddy) - 1.
    #sx = - ddx / (ddx + numpy.tan(theta))
    #sy = - ddy / (ddy + numpy.tan(theta))
    return (sx, sy)

  def computeStressFromRecip(self, qx, qy, qz, a0, hkl):
    qs = numpy.array((qx, qy, qz))
    factor = (2. * numpy.pi * numpy.sqrt((hkl * hkl).sum(1)) / a0).repeat(qs.shape[1]).reshape(3, -1)
    return factor / qs - 1.
    #return (factor / qx -1., factor / qy - 1., factor / qz - 1.)

  def getPiyAtIter(self, iter):
    (piy, pos) = model.get(iter, ID1_gui_model.ScansModel.COL_PIY,
                        ID1_gui_model.ScansModel.COL_PEAK_POS)
    if piy == 0.:
      return None
    else:
      return (piy, pos)

class ScansTreeView(gtk.TreeView):
  __gsignals__ = {'scan-selected' : (gobject.SIGNAL_RUN_LAST,
                                     gobject.TYPE_NONE,
                                     (gobject.TYPE_PYOBJECT, )),
                  'pix-selected' : (gobject.SIGNAL_RUN_LAST,
                                    gobject.TYPE_NONE,
                                    (gobject.TYPE_PYOBJECT, gobject.TYPE_PYOBJECT, gobject.TYPE_PYOBJECT)),
                  'piy-selected' : (gobject.SIGNAL_RUN_LAST,
                                    gobject.TYPE_NONE,
                                    (gobject.TYPE_PYOBJECT, gobject.TYPE_PYOBJECT)),
                  }

  def __init__(self, model):
    super(ScansTreeView, self).__init__(model)
    
    render = gtk.CellRendererText()
    col = gtk.TreeViewColumn("Scan mesh", render)
    col.set_cell_data_func(render, self.display)
    render = gtk.CellRendererPixbuf()
    render.set_property("width", 16)
    col.pack_end(render, expand = False)
    col.set_expand(True)
    self.insert_column(col, -1)

    self.get_selection().set_mode(gtk.SELECTION_SINGLE)
    self.set_rules_hint(True)

    self.get_selection().connect("changed", self.select)

  def display(self, col, render, model, iter):
    render.set_property("foreground", "black")
    (piy, piy_lbl, pix, chi, path, valid, intX, pos) = \
          model.get(iter, ScansModel.COL_PIY,
                    ScansModel.COL_PIY_LBL,
                    ScansModel.COL_PIX,
                    ScansModel.COL_RC_ANG,
                    ScansModel.COL_PATH,
                    ScansModel.COL_VALID,
                    ScansModel.COL_X_Y,
                    ScansModel.COL_PEAK_POS)
    lbl = "no label"
    if path is not None and not path == "":
      if valid:
        lbl = os.path.basename(path)
      else:
        lbl = path
      if chi != 0.:
        lbl += " <span size=\"small\">(%s = %g°)</span>" % (model.rcAngle, chi)
    elif not piy == 0.:
      lbl = "piy = %g µm" % piy
      if pos is not None:
        lbl = "<b>" + lbl + "</b>"
      n = model.iter_n_children(iter)
      lbl += " <span size=\"small\">(%d pix values)</span>" % n
    elif piy_lbl is not None:
      lbl = "<b>" + piy_lbl + "</b>"
      n = model.iter_n_children(iter)
      lbl += " <span size=\"small\">(%d pix values)</span>" % n
    elif not pix == 0.:
      n = model.iter_n_children(iter)
      lbl = "pix = %g µm" % pix
      if intX is not None:
        lbl = "<b>" + lbl + "</b>"
      lbl += " <span size=\"small\">(%d %s scans)</span>" % (n, model.rcAngle)
    render.set_property("markup", lbl)
    if valid:
      render.set_property("foreground", "black")
    else:
      render.set_property("foreground", "gray")

  def select(self, selection):
    (model, iter) = selection.get_selected()
    if iter is None:
      return

    (piy, pix, path, valid) = model.get(iter, ScansModel.COL_PIY,
                                        ScansModel.COL_PIX,
                                        ScansModel.COL_PATH,
                                        ScansModel.COL_VALID)
    if path is not None and not path == "":
      data = edf.RawImage()
      try:
        data.read(path)
      except:
        try:
          (nr, path0) = model.get(iter, ScansModel.COL_IMGNR, ScansModel.COL_PATH0)
          print path0, nr
          data.read(path0, nr)
        except:
          data = None
        #(ang, nr, path0) = model.get(iter, ScansModel.COL_RC_ANG, ScansModel.COL_IMGNR, ScansModel.COL_PATH0)
        #data = model.getRawImageAt(path0, nr)
        #filename = model.getDataFile(nr, round(ang*10000.))
        #if filename is not None:
        #  data = edf.RawImage()
        #  data = data.read(filename)
        #else:
        #  data = None
      model.set(iter, ScansModel.COL_VALID, data is not None)
      self.emit("scan-selected", data)
    elif not pix == 0.:
      # Generate the list of files and corresponding rocking angles.
      it = model.iter_children(iter)
      rc = []
      while (it):
        vals = model.get(it, ScansModel.COL_RC_ANG, ScansModel.COL_PATH)
        rc.append(vals)
        it = model.iter_next(it)
      (piy, ) = model.get(model.iter_parent(iter), ScansModel.COL_PIY)
      self.emit("pix-selected", piy, pix, rc)
    elif not piy == 0.:
      rcs = []
      itx = model.iter_children(iter)
      while (itx):
        # Generate the list of files and corresponding rocking angles.
        it = model.iter_children(itx)
        (pix, ) = model.get(itx, ScansModel.COL_PIX)
        rc = []
        while (it):
          vals = model.get(it, ScansModel.COL_RC_ANG, ScansModel.COL_PATH)
          rc.append(vals)
          it = model.iter_next(it)
        rcs.append((pix, rc))
        itx = model.iter_next(itx)
      self.emit("piy-selected", piy, rcs)

class ScansDisplay(gtk.VBox):
  def __init__(self, model):
    super(ScansDisplay, self).__init__()
    
    scroll = gtk.ScrolledWindow()
    scroll.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
    scroll.set_shadow_type(gtk.SHADOW_ETCHED_IN)
    scroll.set_size_request(300, -1)
    self.pack_start(scroll, True, True)
    self.view = ScansTreeView(model)
    scroll.add(self.view)

    info = gtk.InfoBar()
    info.set_no_show_all(True)
    info.add_button(gtk.STOCK_REFRESH, gtk.RESPONSE_APPLY)
    info.set_message_type(gtk.MESSAGE_INFO)
    info.connect("response", self.onPathUpdate, model)
    vbox = gtk.VBox()
    self.message = gtk.Label()
    vbox.pack_start(self.message, True, True)
    self.progress = gtk.ProgressBar()
    self.progress.set_no_show_all(True)
    vbox.pack_end(self.progress, False)
    vbox.show_all()
    info.get_content_area().add(vbox)
    self.pack_start(info, False)
    model.connect("start-populating", self.onStartPop, info)
    model.connect("stop-populating", self.onStopPop, info)
    model.connect("populating", self.onPop)

  def onStartPop(self, model, nfiles, info):
    self.message.set_text("Scanning %d files." % nfiles)
    info.set_response_sensitive(gtk.RESPONSE_APPLY, False)
    self.progress.show()
    info.show()
    self.view.set_sensitive(False)
    self.view.set_model()
    while gtk.events_pending():
      gtk.main_iteration()

  def onPop(self, model, frac):
    self.progress.set_fraction(frac)
    while gtk.events_pending():
      gtk.main_iteration()

  def onStopPop(self, model, nfiles, info):
    self.view.set_model(model)
    self.view.set_sensitive(True)
    self.progress.hide()
    info.set_response_sensitive(gtk.RESPONSE_APPLY, True)
    if nfiles > 0:
      self.message.set_text("There are %d missing files." % nfiles)
      info.show()
    else:
      info.hide()

  def onPathUpdate(self, info, id, model):
    if id == gtk.RESPONSE_CLOSE:
      info.hide()
    print "redo path"
