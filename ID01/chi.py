#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2012-2013 Damien Caliste <damien.caliste@cea.fr>,
#                         Thu Nhi Tran Thi <thu-nhi.tran-thi@esrf.fr>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA

import edf
import os, tables, sys
import numpy
from scipy import ndimage
from scipy.optimize import leastsq
from scipy.special import wofz
import xrayutilities as xu

def gauss_eval(x, ampl, sig, x0):
  return ampl / (sig * numpy.sqrt(2. * numpy.pi)) * \
         numpy.exp((x - x0) ** 2 / (-2. * (sig ** 2)))

def voigt_eval(x, ampl, sig, gamma, x0):
  z = (x - x0 + gamma * 1j) / (sig * numpy.sqrt(2.))
  return ampl * wofz(z).real / (sig * numpy.sqrt(2. * numpy.pi))

def residual_gauss_peak(p, x, y):
  return y - gauss_eval(x, p[1], p[2], p[0])

def residual_voigt_peak(p, x, y):
  return y - voigt_eval(x, p[1], p[2], p[3], p[0])

def disk_structure(n):
  struct = numpy.zeros((2 * n + 1, 2 * n + 1))
  x, y = numpy.indices((2 * n + 1, 2 * n + 1))
  mask = (x - n)**2 + (y - n)**2 <= n**2
  struct[mask] = 1
  return struct.astype(numpy.bool)

def oneDifraction(implicitEDF = None, listEDF = None, outputEDF = None):
  if implicitEDF is not None:
    prefix, chi0, chiN = implicitEDF
    listEDF = []
    for i in range(chi0, chiN + 1):
      listEDF.append(prefix % i)
  elif listEDF is None:
    raise AttributeError
  listEDF = [f for f in listEDF if f is not None]
  ln = len(listEDF)

  edfInt = []
  chiIntegral = []
  for path in listEDF:
    f = edf.RawImage()
    f.read(path)
    edfInt.append(f.getData().astype(numpy.dtype('f4')).reshape(f.height, f.width))
    chiIntegral.append(f.getData().sum())
  chiIntegral = numpy.array(chiIntegral)

  if outputEDF is not None:
    img = edf.RawImage()
    img.set(edfInt.sum(1))
    img.save(outputEDF)

  chiMax = chiIntegral.argmax()
  dataMax = edfInt[chiMax]
  # Remove hot pixels by making an opening on the integral over the rc.
  if ln > 1:
    avg = numpy.zeros(edfInt[0].shape, dtype = numpy.dtype('f4'))
    for arr in edfInt:
      ## arr0 = arr.copy()
      ## arr0[arr > 1] = 0.
      ## avg0 = arr0.sum() / (~(arr > 1)).sum()
      ## arr -= avg0
      ## arr[arr < 0] = 0.
      avg += arr
    ## edfInt = numpy.array(edfInt)
    ## print "array done"
    ## avg = edfInt.sum(0)
    img = edf.RawImage()
    img.set(avg / float(ln))
    img.save("pouet.edf")
    opened = ndimage.binary_opening(avg >= 1., structure = disk_structure(2))
    mask = ~(ndimage.binary_dilation(opened, structure = disk_structure(5)))
    img = edf.RawImage()
    img.set(mask.astype(numpy.int))
    img.save("mask.edf")
    # Experimental
    dataMax = avg / float(ln)
  else:
    mask = None

  img = edf.RawImage()
  img.read(listEDF[chiMax])
  return (chiIntegral, img, dataMax, mask)

def fitData(x, y, sigma = 50., output = None):
  i0 = y.argmax()
  p0 = (x[i0], y[i0] * (sigma * numpy.sqrt(2. * numpy.pi)), sigma)
  p = leastsq(residual_gauss_peak, p0, args = (x, y))

  if output is not None:
    fChi = open(output, "w")
    for (i, val) in zip(range(len(y)), y):
      fChi.write("%d %g %g\n" % (i, val, gauss_eval(i, p[0][1], p[0][2], p[0][0])))
    fChi.close()

  return p[0]

def getPeakAtMaxChiCurve(implicitEDF = None, listEDF = None,
                         extended = False, outputEDF = None):
  (chiIntegral, chiEDF, data, mask) = oneDifraction(implicitEDF, listEDF,
                                                    outputEDF = outputEDF)
  if mask is not None:
    data[mask] = 0.
  x = range(data.shape[1])
  dx = data.sum(0)
  dx[dx <= 0.] = 0.01
  px = fitData(x, dx) #, output = "chiMaxX" + suffix)
  x = range(data.shape[0])
  dy = data.sum(1)
  dy[dy <= 0.] = 0.01
  py = fitData(x, dy) #, output = "chiMaxY" + suffix)

  if extended:
    return ((px[0], py[0]), chiIntegral, chiEDF, dx, px, dy, py)
  else:
    return (px[0], py[0])

def getPeakFromRecipSpace(mu, phi, nu, delta, chi, eta,
                          implicitEDF = None, listEDF = None, extended = False,
                          detector = {}, outputEDF = None):
  if implicitEDF is not None:
    prefix, chi0, chiN = implicitEDF
    listEDF = []
    for i in range(chi0, chiN + 1):
      listEDF.append(prefix % i)
  elif listEDF is None:
    raise AttributeError
  ln = len(listEDF)

  # Read first EDFFile to get detector size.
  s = None
  for path in listEDF:
    if path is not None and s is None:
      e = xu.io.EDFFile(path)
      s = e.data.shape
      break
  en = detector["en"] #x-ray energy in eV 
  cch = detector["cch"] #center channel of the Maxipix detector: vertical(del)/horizontal(nu) z-y+
  chpdeg = detector["chpdeg"] # channel per degree for the detector
  nav = [2,2] # reduce data: number of pixels to average in each detector direction
  roi = [0,s[0],0,s[1]] # region of interest
  nx, ny, nz = 100,100,100
  dw = detector["orient"][0]
  dh = detector["orient"][1]

  qconv = xu.experiment.QConversion(['z+','y-','x-'],['z+','y-'],[1,0,0])
  hxrd = xu.HXRD([1,0,0],[0,0,1],en=en,qconv=qconv)
  hxrd.Ang2Q.init_area(dw,dh,cch1=cch[0],cch2=cch[1],Nch1=s[0],Nch2=s[1], chpdeg1=chpdeg[0],chpdeg2=chpdeg[1],Nav=nav,roi=roi)

  #mu    = numpy.zeros(ln) + mu
  #phi   = numpy.zeros(ln) + phi
  #nu    = numpy.zeros(ln) + nu
  #delta = numpy.zeros(ln) + delta

  qx,qy,qz = hxrd.Ang2Q.area(mu + phi,eta,chi,nu,delta)
  intensity = numpy.zeros( (ln, s[0] / nav[0], s[1] / nav[1]) )
  for (i, path) in zip(range(ln), listEDF):
    # read ccd image from EDF file
    if path is not None:
      e = xu.io.EDFFile(path)
      ccdraw = e.data
      #ccd = hotpixelkill(ccdraw)
      ccd = ccdraw
      ####
      # here a darkfield correction would be done
      ####
      # reduce data size
      CCD = xu.blockAverage2D(ccd, nav[0],nav[1], roi=roi)
    else:
      shape = ((roi[1] - roi[0]) / nav[0], (roi[3] - roi[2]) / nav[1])
      CCD = numpy.zeros(shape[0] * shape[1]).reshape(shape)

    intensity[i,:,:] = CCD

  # convert data to rectangular grid in reciprocal space
  gridder = xu.Gridder3D(nx,ny,nz)
  gridder(qx,qy,qz,intensity)

  dx = gridder.gdata.sum(2).sum(1)
  px = fitData(gridder.xaxis, dx, sigma = 0.01)
  dy = gridder.gdata.sum(0).sum(1)
  py = fitData(gridder.yaxis, dy, sigma = 0.01)
  dz = gridder.gdata.sum(0).sum(0)
  pz = fitData(gridder.zaxis, dz, sigma = 0.01)

  if extended:
    return ((px[0], py[0], pz[0]), gridder.gdata, gridder.xaxis, dx, px, gridder.yaxis, dy, py, gridder.zaxis, dz, pz)
  else:
    return (px[0], py[0], pz[0])


def dataFromCSV(filename):
  data = []
  for line in open(filename, "r").xreadlines():
    vals = line.split(",")
    if vals[1] == "chi":
      expnr = int(vals[0])
      thy = float(vals[3])
      piy = float(vals[4])
      chi0 = int(vals[7])
      chiN = int(vals[8])
      data.append((expnr, thy, piy, chi0, chiN))
  desc = {'names': ("expnr", "thy", "piy", "chi0", "chiN"),
          'formats': ("i4", "f8", "f8", "i4", "i4")}
  return numpy.array(data, dtype = desc)

def dataFromCSVNew(filename):
  data = []
  for line in open(filename, "rb").xreadlines():
    vals = line.split(",")
    try:
      angs = map(float, vals[0:8])
      f = vals[10].strip()
      if angs[6] > 0. and angs[7] > 0. and not f == "":
        v = tuple(angs) + (int(f[f.rfind("_") + 1:-7]), )
        data.append(v)
    except ValueError:
      pass
  desc = {'names': ("chi", "mu", "delta", "nu", "eta", "phi", "pix", "piy", "expnr"),
          'formats': ("f8", "f8", "f8", "f8", "f8", "f8", "f8", "f8", "i4")}
  data = numpy.array(data, dtype = desc)

  dpiy = {}
  piy = data["piy"]
  validy = numpy.zeros(len(piy), dtype = numpy.bool)
  for iy in range(len(piy)):
    if not validy[iy]:
      dy = (piy - piy[iy]) ** 2
      validy = validy | (dy < 0.25)
      xdata = data[dy < 0.25]
  
      dpix = {}
      pix    = xdata["pix"]
      validx = numpy.zeros(len(pix), dtype = numpy.bool)
      for ix in range(len(pix)):
        if not validx[ix]:
          d      = (pix - pix[ix]) ** 2
          validx = validx | (d < 0.25)
          spot = xdata[d < 0.25]
          kpix = float(round(spot["pix"].sum() / float(len(spot)) * 100.) / 100.)
          kpiy = float(round(spot["piy"].sum() / float(len(spot)) * 100.) / 100.)
          dpix[kpix] = spot
      dpiy[kpiy] = dpix
  
  # We flatten everything
  data = []
  ky = dpiy.keys()
  ky.sort()
  f = open("grid.dat", "w")
  for k in ky:
    pix = dpiy[k]
    kx = pix.keys()
    kx.sort()
    for k2 in kx:
      for (x, y) in zip(pix[k2]["pix"], pix[k2]["piy"]):
        f.write("%g %g\n" % (x, y))
      data.append((k, k2, pix[k2]))
  f.close()
  return data

def dataFromSpec(filename, scanRange = range(1, 401)):
  h5file = filename.replace(".spec", ".h5")
  if not os.path.isfile(h5file):
    s = xu.io.SPECFile(os.path.basename(filename), path = os.path.dirname(filename))
    s.Update()
    s.Save2HDF5(h5file)

  data = []
  h5 = tables.openFile(h5file, mode='r')
  h5root = h5.listNodes(h5.root)[0]

  desc = {'names': ("expnr", "thy", "piy", "chi", "mu", "phi", "nu", "delta", "eta"),
          'formats': ("i4", "f8", "f8", "f8", "f8", "f8", "f8", "f8", "f8")}

  for i in scanRange:
    try:
      h5scan = h5.getNode(h5root,"scan_%d" % i)
      sdata = h5scan.data.read()
      #ccdn = sdata['ccd_n']
      #chi  = sdata['Chi']
      #if not ccdn[0] == ccdn[-1]:
      chi = h5.getNodeAttr(h5scan,"INIT_MOPO_Chi")
      thy = h5.getNodeAttr(h5scan,"INIT_MOPO_thy")
      piy = h5.getNodeAttr(h5scan,"INIT_MOPO_piy")
      mu  = h5.getNodeAttr(h5scan,"INIT_MOPO_Mu")
      phi = h5.getNodeAttr(h5scan,"INIT_MOPO_Phi")
      nu  = h5.getNodeAttr(h5scan,"INIT_MOPO_Nu")
      delta = h5.getNodeAttr(h5scan,"INIT_MOPO_Delta")
      eta = h5.getNodeAttr(h5scan,"INIT_MOPO_Eta")
      adcX = sdata['adcX']
      adcZ = sdata['adcZ']
      imgnr = sdata['imgnr']
      dt = []
      for (x, y, n) in zip(adcZ, adcX, imgnr):
        dt.append((n, x, y, chi, mu, phi, nu, delta, eta))
      data.append((x, y, numpy.array(dt, dtype = desc)))
    except tables.exceptions.NoSuchNodeError:
      continue
    except ValueError:
      continue
  h5.close()

  return data

if __name__ == "__main__":
  ((nuMax, deltaMax), chiIntegral, chiEDF, dx, px, dy, py, mask) = getPeakAtMaxChiCurve(listEDF = (sys.argv[1], ), extended = True)
  print nuMax, deltaMax
  biuhio
  
  exp = dataFromCSVNew("c2_004_2.csv")
  gyu

  edfFiles = "/media/MINH/images TIV48A_73_C1/thy = 0.4 chi scan/TIV48A_73_C1_12_%04d.edf"

  exp = dataFromSpec("id01_27_07_2012.spec")
  v = exp[exp["expnr"] == 235]
  suffix = "_thy=%3.2f_piy=%7.5f" % (v["thy"], v["piy"])
  (qx0, qy0, qz0) = getPeakFromRecipSpace(edfFiles, v["chi0"], v["chiN"], suffix)
  print v["piy"], qx0, qy0, qz0
  hui

  exp = dataFromCSV("ID01_data_files_July2012.csv")

  for v in exp[exp["thy"] == 0.4]:
    suffix = "_thy=%3.2f_piy=%7.5f" % (v["thy"], v["piy"])
    (x0, y0) = getPeakAtMaxChiCurve(edfFiles, v["chi0"], v["chiN"], suffix)
    print v["piy"], x0, y0
