#!/usr/bin/env python
#
# Copyright (C) 2012-2013 Damien Caliste <damien.caliste@cea.fr>,
#                         Thu Nhi Tran Thi <thu-nhi.tran-thi@esrf.fr>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA

import os
import tables, numpy

def nrm2((x0, y0), (x1, y1)):
  return (x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0)

def jpos_new(h5, rg = None, rcAngle = "phi", pixelMotors = ("adcX", "adcZ")):
  jpos = []
  h5root = h5.listNodes(h5.root)[0]
  if rg is None:
    rg = range(len(h5.listNodes(h5root)))
  nTot = 0
  for i in rg:
    node = h5.getNode(h5root, "scan_%d" % i)
    sdata = node.data.read()
    if "imgnr" in sdata.dtype.names:
      phi = node._v_attrs["INIT_MOPO_%s" % rcAngle.capitalize()]
      index = 0
      for index in range(len(jpos)):
        if abs(sdata[pixelMotors[1]][0] - jpos[index][1]) < 0.5:
          break
      nTot += len(sdata["imgnr"])
      x_prev2 = -1.
      x_prev  =  0.
      y_prev  =  sdata[pixelMotors[1]][0]
      dy = 0.
      for (x, y, n) in zip(sdata[pixelMotors[0]], sdata[pixelMotors[1]], sdata["imgnr"]):
        ## if x_prev2 > x_prev and x > x_prev and jpos[index][3][-1][0] + 10 < n:
        ##   if abs(y_prev - y) < 0.5:
        ##     dy += 5.
        ##   y_prev = y
        if x >= x_prev:
          index = jpos_add(jpos, (x, y + dy), index, (n, phi))
        ## nTot += 1
        ## nTot_ = 0
        ## for (x_, y_, n_, add) in jpos:
        ##   nTot_ += n_
        ## print nTot, nTot_, x, y, n
        ## x_prev2 = x_prev
        x_prev = x
        if len(jpos) > 1 and index > 1 and (int(jpos[index - 1][0]) < int(jpos[index - 2][0]) and abs(jpos[index - 2][1] - jpos[index - 1][1]) < 0.5):
          #print jpos
          print x, y, n
          print len(jpos), index, node._v_name
          print jpos[index - 2][0:2]
          print jpos[index - 1][0:2]
          raise ValueError
  #for (x, y, n, add) in jpos:
  #  print x, y, n
  ## print nTot
  ## nTot = 0
  ## for (x, y, n, add) in jpos:
  ##   nTot += n
  ## print nTot
  return jpos

def jpos_add(jpos, A, index = 0, add = None):
  if index >= len(jpos):
    jpos.append(A + (1., [add, ]))
    return len(jpos) - 1
  if nrm2(A, jpos[index][0:2]) < 0.04:
    x = ((jpos[index][0] * jpos[index][2]) + A[0]) / (jpos[index][2] + 1)
    y = ((jpos[index][1] * jpos[index][2]) + A[1]) / (jpos[index][2] + 1)
    n = jpos[index][2] + 1
    jpos[index][3].append(add)
    jpos[index] = (x, y, n, jpos[index][3])
    return index
  else:
    if A[0] < jpos[index][0] and abs(A[1] - jpos[index][1]) < 0.5:
      jpos = [A + (1., [add, ]), ] + jpos
      return index
    return jpos_add(jpos, A, index + 1, add)

def jpos_get_rc(jpos, j, sample = "%d.edf"):
  out = []
  ln = len(sample % 1000000)
  desc = {'names': ("imgnr", "Phi", "entry"),
          'formats': ("i4", "f8", "a%d" % ln)}
  (x, y, n, add) = jpos[j]
  for (n, phi) in add:
    out.append((n, phi, sample.ljust(ln) % n))
  return numpy.array(out, dtype = desc)

def h5_list_exp(h5, rg = None):
  h5root = h5.listNodes(h5.root)[0]
  lst = {}
  for node in h5root:
    if "prefix" in node._v_attrs:
      scanid = int(node._v_name[5:])
      cur = node._v_attrs["prefix"]
      if cur in lst:
        lst[cur].append(scanid)
      else:
        lst[cur] = [scanid, ]
  for v in lst.values():
    v.sort()
  return lst

def h5_get_grid_scan(h5, iscan):
  h5root = h5.listNodes(h5.root)[0]
  node = h5.getNode(h5root, "scan_%d" % iscan)
  sdata = node.data.read()
  if not "imgnr" in sdata.dtype.names:
    raise IndexError
  return numpy.vstack((sdata["adcX"], sdata["adcZ"])).T    

class Sample():
  def __init__(self, title, rcAngle = "phi", pixelMotors = ("adcX", "adcZ")):
    self.title = title
    self.jpos = None
    self.rcAngle = rcAngle
    self.pixelMotors = pixelMotors
    print "# New sample '%s', rocking angle is '%s'" % (title, rcAngle.capitalize())
    
  def setFromH5(self, h5, rg):
    self.jpos = jpos_new(h5, rg, self.rcAngle, self.pixelMotors)
    print "#%s# jpos read OK (%d)" % (self.title, len(self.jpos))

  def getRC(self, j):
    return jpos_get_rc(self.jpos, j, sample = self.title + "%04d.edf")

  def getFullScan(self):
    lst = []
    for i in range(len(self.jpos)):
      lst.append((self.jpos[i][0], self.jpos[i][1], self.getRC(i)))
    return lst

def _buildSample((k, rg)):
  sample = Sample(k, rcAngle = _buildSample.rcAngle,
                  pixelMotors = _buildSample.pixelMotors)
  h5 = tables.openFile(_buildSample.h5File, mode='r')
  sample.setFromH5(h5, rg)
  h5.close()
  return sample

class Experiment():
  def __init__(self, specFile, rcAngle = "phi", pixelMotors = ("adcX", "adcZ")):
    h5file = specFile.replace(".spec", ".h5")
    if not os.path.isfile(h5file):
      import xrutils as xu

      s = xu.io.SPECFile(specFile)
      s.Update()
      s.Save2HDF5(h5file)
    self.h5File = h5file
    self.sample_scans = {}
    self.samples = {}
    self.rcAngle = rcAngle
    self.pixelMotors = pixelMotors

  def getSampleScans(self, override = None):
    h5 = tables.openFile(self.h5File, mode='r')
    self.sample_scans = h5_list_exp(h5)
    h5.close()
    if override is not None:
      for (k, v) in override.items():
        if k in self.sample_scans:
          self.sample_scans[k] = v
    return self.sample_scans

  def getGridScan(self, iscan):
    h5 = tables.openFile(self.h5File, mode='r')
    grid = h5_get_grid_scan(h5, iscan)
    h5.close()
    return grid

  def buildSamples(self, scanId = None):
    if len(self.sample_scans) == 0:
      self.getSampleScans()
    
    _buildSample.h5File = self.h5File
    _buildSample.rcAngle = self.rcAngle
    _buildSample.pixelMotors = self.pixelMotors
    withThreads = True
    if withThreads and scanId is None:
      from multiprocessing.pool import Pool

      p = Pool()
      for sample in p.map(_buildSample, self.sample_scans.items()):
        self.samples[sample.title] = sample
    else:
      self.samples = {}
      if scanId is None:
        for (title, rg) in self.sample_scans.items():
          self.samples[title] = _buildSample((title, rg))
      else:
        self.samples[scanId] = _buildSample((scanId, self.sample_scans[scanId]))

if __name__ == "__main__":
  import sys

  s = Sample("TIV48A_75_E1_", pixelMotors = ("adcZ", "adcX"))
  s.setFromH5(tables.openFile(sys.argv[1], mode='r'), range(146, 605))
  data = s.getFullScan()
  #print data[2]

  jpos_new(tables.openFile(sys.argv[1], mode='r'), range(146, 605))
  bhji

  _buildSample.h5File = sys.argv[1]
  s = _buildSample(("TIV48A_75_E1_", range(146, 605)))
  print s.getRC(-2), len(s.getRC(-2))
  print s.getRC(-1), len(s.getRC(-1))
  bjk

  exp = Experiment(sys.argv[1])
  exp.getSampleScans(override = {"TIV48A_75_E1_": range(146, 605)})
  exp.buildSamples()
    
  for sample in exp.samples.values():
    rc = sample.getRC(3)
    f = open(sample.title + "plots", "w")
    for (phi, s) in zip(rc["Phi"], rc["entry"]):
      f.write("%g\t# %s\n" % (phi, s))
    f.close()
    print "# output phi OK (%d)" % len(rc)
