#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2012-2013 Damien Caliste <damien.caliste@cea.fr>,
#                         Thu Nhi Tran Thi <thu-nhi.tran-thi@esrf.fr>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA

import gtk, gobject
import os, re
import chi, edf
import socs_linux
import numpy
import pickle
import Queue
import multiprocessing
import matplotlib

def queueGetLast(q):
  obj = None
  try:
    while True:
      obj = q.get(block = False)
  except Queue.Empty:
    pass
  return obj

class GuiOptions(gobject.GObject):
  __gsignals__ = {'datadir-selected' : (gobject.SIGNAL_RUN_LAST,
                                        gobject.TYPE_NONE,
                                        (gobject.TYPE_STRING,)),
                  'spec-selected' : (gobject.SIGNAL_RUN_LAST,
                                     gobject.TYPE_NONE,
                                     (gobject.TYPE_STRING,)),
                  'method-selected' : (gobject.SIGNAL_RUN_LAST,
                                       gobject.TYPE_NONE,
                                       (gobject.TYPE_INT,)),
                  'rc-angle-selected' : (gobject.SIGNAL_RUN_LAST,
                                         gobject.TYPE_NONE,
                                         (gobject.TYPE_STRING,)),
                  'locked' : (gobject.SIGNAL_RUN_LAST,
                              gobject.TYPE_NONE,
                              ()),
                  'unlocked' : (gobject.SIGNAL_RUN_LAST,
                                gobject.TYPE_NONE,
                                ()),
                  'matrices-loaded' : (gobject.SIGNAL_RUN_LAST,
                                       gobject.TYPE_NONE,
                                       (gobject.TYPE_PYOBJECT, )),
                  'q-loaded' : (gobject.SIGNAL_RUN_LAST,
                                gobject.TYPE_NONE,
                                (gobject.TYPE_PYOBJECT, )),
                  }
  millerIndices = re.compile(r"^(?P<x>-?[0-9])(?P<y>-?[0-9])(?P<z>-?[0-9])$")

  def __init__(self):
    gobject.GObject.__init__(self)
    self.method = 2
    self.peakMethod = 1
    self.spec = None
    self.scanIds = None
    self.scanRange = {"start": 0, "stop": 100, "step": 4}
    self.mat = None
    self.result = None
    self.rcAngle = None
    self.pMotor = (None, None)
    self.entryPeak = None
    self.spinLatticeA = None
    self.hboxMiller = None
    self.entryHKL = (None, None, None)
    self.checkInt = None
    return

  def getOutFile(self, postfix = ""):
    dirname = self.expDir.get_current_folder()
    if dirname is None:
      dirname = self.outDir
    if len(self.expName.get_text().strip()) == 0:
      raise ValueError
    return os.path.join(dirname, self.expName.get_text() + postfix)

  def getResultOptions(self, outDir = os.getcwd()):
    hbox = gtk.HBox()
    wd = gtk.Label("<i>Result path </i>")
    wd.set_use_markup(True)
    hbox.pack_start(wd, False)
    wd = gtk.FileChooserDialog(title = "Choose a directory to put results to", \
                               buttons = (gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT,
                                          gtk.STOCK_OPEN, gtk.RESPONSE_ACCEPT))
    self.expDir = gtk.FileChooserButton(wd)
    hbox.pack_start(self.expDir, True, True)
    self.expDir.set_action(gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER)
    self.expDir.set_current_folder(outDir)
    self.outDir = outDir
    self.expDir.set_tooltip_text("Choose a directory where results will be saved.")
    hbox.pack_start(gtk.Label("/"), False)
    self.expName = gtk.Entry()
    self.expName.set_tooltip_text("Choose a name for generated files.")
    hbox.pack_end(self.expName, False)
    return hbox
  
  def getDatadirChooser(self, dirname = os.getcwd(), prefixdir = ".", prefiximg = ""):
    vbox = gtk.VBox()

    hbox = gtk.HBox()
    vbox.pack_start(hbox, False)
    # EDFdata directory and sample names.
    wd = gtk.FileChooserDialog(title = "Choose a directory with EDF data", \
                               buttons = (gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT,
                                          gtk.STOCK_OPEN, gtk.RESPONSE_ACCEPT))
    self.datadirChooser = gtk.FileChooserButton(wd)
    hbox.pack_start(self.datadirChooser, True, True)
    self.datadirChooser.set_action(gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER)
    self.datadirChooser.set_current_folder(dirname)
    self.datadirChooser.set_tooltip_text("Choose a directory (with possible subdirs) containing the EDF or EDF.gz files.")

    hbox = gtk.HBox()
    vbox.pack_start(hbox, False)
    self.datadirPrefix = gtk.Entry()
    self.datadirPrefix.set_text(prefixdir)
    self.datadirPrefix.set_width_chars(10)
    
    hbox.pack_start(self.datadirPrefix, True, True)
    hbox.pack_start(gtk.Label("/"), False)
    self.imgPrefix = gtk.Entry()
    self.imgPrefix.set_text(prefiximg)
    self.imgPrefix.set_width_chars(10)
    hbox.pack_start(self.imgPrefix, True, True)
    self.imgPrefix.connect_object("changed", gtk.Entry.set_icon_from_stock, self.imgPrefix,
                                  gtk.ENTRY_ICON_SECONDARY, None)
    
    return vbox

  def getDataFile(self, imgnr, ang = None):
    path = self.datadirChooser.get_filename()
    prefixdir = self.datadirPrefix.get_text()
    if "%" in prefixdir:
      prefixdir = prefixdir % ang
    prefiximg = self.imgPrefix.get_text()
    if "%" in prefiximg:
      prefiximg = prefiximg % ang
    filename = os.path.join(path, prefixdir, prefiximg + "_%04d.edf" % imgnr)
    if not(os.path.isfile(filename)):
      filename += ".gz"
      if not(os.path.isfile(filename)):
        filename = None
    return filename

  def getDataScheme(self, ang = None):
    path = self.datadirChooser.get_filename()
    prefixdir = self.datadirPrefix.get_text()
    if "%" in prefixdir and ang is not None:
      prefixdir = prefixdir % ang
    prefiximg = self.imgPrefix.get_text()
    return os.path.join(path, prefixdir, prefiximg)
    
#    path_image = (datadir+prefix_image+str(int(round(float(str(eta[idx]))*10000)))+'/'+prefix_im+str(int(round(float(str(eta[idx]))*10000)))+'_'+'%04d.edf.gz')%i 

  def getSpecChooser(self, filename = None, ids = None):
    hbox = gtk.HBox()

    wd = gtk.FileChooserDialog(title = "Choose a spec or csv experiment file", \
                               buttons = (gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT,
                                          gtk.STOCK_OPEN, gtk.RESPONSE_ACCEPT))
    dirSel = gtk.FileChooserButton(wd)
    dirSel.set_action(gtk.FILE_CHOOSER_ACTION_OPEN)
    if filename is not None:
      dirSel.set_filename(filename)
      gobject.idle_add(self.setSpec, filename)
    dirSel.connect("file-set", self.chooseSpec)
    dirSel.set_tooltip_text("Choose a .spec or a .h5 file. When a .spec file is choosen, a corresponding .h5 file is automatically generated.")
    hbox.pack_start(dirSel, True)

    wd = gtk.FileChooserDialog(title = "Choose a YAML file with range setup", \
                               buttons = (gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT,
                                          gtk.STOCK_OPEN, gtk.RESPONSE_ACCEPT))
    dirSel = gtk.FileChooserButton(wd)
    dirSel.set_action(gtk.FILE_CHOOSER_ACTION_OPEN)
    if ids is not None:
      dirSel.set_filename(ids)
      gobject.idle_add(self.setSpecIdsFromFile, ids)
    dirSel.connect("file-set", self.chooseSpecIds)
    dirSel.set_tooltip_text("Choose a .yaml file with the range description to select a list of scans.")
    hbox.pack_start(dirSel, True)
    
    return hbox

  def setSpec(self, filename):
    if not(os.path.isfile(filename)):
      return
    self.spec = filename
    manager = gtk.recent_manager_get_default()
    manager.add_item("file://" + os.path.abspath(filename))
    if len(self.expName.get_text().strip()) == 0:
      self.expName.set_text(".".join(os.path.basename(self.spec).split(".")[:-1]))
      matfile = self.getOutFile(".matrices")
      if os.path.isfile(matfile):
        self.setSpecMessage("found cached matrix file '%s'" % os.path.basename(matfile))
      qfile = self.getOutFile(".qmesh")
      if os.path.isfile(qfile):
        self.setQMessage("found cached q mesh file '%s'" % os.path.basename(qfile))
    self.emit('spec-selected', filename)
    if self.scanIds is not None and self.spec in self.scanIds:
      self.updateScans(self.scanIds[self.spec])

  def setSpecMessage(self, message):
    self.specMessage.set_markup("<span size=\"smaller\">" + message + "</span>")

  def chooseSpec(self, widget):
    self.setSpec(widget.get_filename())

  def chooseSpecIds(self, widget):
    self.setSpecIdsFromFile(widget.get_filename())

  def setSpecIdsFromFile(self, ids):
    import yaml

    self.scanIds = [d for d in yaml.load_all(open(ids, "r"))][0]
    if self.spec is not None and self.spec in self.scanIds:
      self.updateScans(self.scanIds[self.spec])

  def updateScans(self, scheme):
    if (isinstance(scheme, list) or isinstance(scheme, tuple)) and isinstance(scheme[0], int):
      self.scanRangeBox.hide()
      self.specScan = numpy.array(scheme, dtype = numpy.int)
    elif (isinstance(scheme, list) or isinstance(scheme, tuple)):
      self.scanRangeBox.hide()
      self.specScan = numpy.array([j for i in scheme for j in i[i.keys()[0]]])
    elif isinstance(scheme, dict):
      if "start" in scheme and "stop" in scheme and "step" in scheme:
        self.scanRangeBox.show()
        self.specScan = numpy.arange(scheme["start"], scheme["stop"], scheme["step"],
                                     dtype = numpy.int)
    else:
      raise TypeError
    self.scanScheme = scheme
    self.specLen.set_markup("(%d scans)" % self.specScan.size)
  
  def updateScanFromRange(self, widget, id):
    self.scanRange[id] = int(widget.get_value())
    self.updateScans(self.scanRange)

  def getSpecOptions(self):
    vbox = gtk.VBox()
    hbox = gtk.HBox()
    vbox.pack_start(hbox, False)

    # Range for scans
    wd = gtk.Label("<i>Scan range</i>")
    wd.set_use_markup(True)
    wd.set_alignment(0., 0.5)
    hbox.pack_start(wd, True, True)

    self.scanRangeBox = gtk.HBox()
    hbox.pack_start(self.scanRangeBox, False)
    wd1 = gtk.SpinButton(gtk.Adjustment(value = self.scanRange["start"],
                                        lower = 0,
                                        upper = 9999,
                                        step_incr = 1,
                                        page_incr = 10))
    wd1.connect("value-changed", self.updateScanFromRange, "start")
    wd1.set_tooltip_text("The starting scan #id to take into account.")
    self.scanRangeBox.pack_start(wd1, False)
    self.scanRangeBox.pack_start(gtk.Label("/"), False)
    wd2 = gtk.SpinButton(gtk.Adjustment(value = self.scanRange["stop"],
                                        lower = 0,
                                        upper = 9999,
                                        step_incr = 1,
                                        page_incr = 10))
    wd2.connect("value-changed", self.updateScanFromRange, "stop")
    wd2.set_tooltip_text("The ending scan #id to take into account.")
    self.scanRangeBox.pack_start(wd2, False)
    self.scanRangeBox.pack_start(gtk.Label("#"), False)
    wd3 = gtk.SpinButton(gtk.Adjustment(value = self.scanRange["step"],
                                        lower = 0,
                                        upper = 100,
                                        step_incr = 1,
                                        page_incr = 5))
    wd3.connect("value-changed", self.updateScanFromRange, "step")
    wd3.set_tooltip_text("The step to read scan #ids.")
    self.scanRangeBox.pack_start(wd3, False)

    self.specLen = gtk.Label()
    self.specLen.set_use_markup(True)
    hbox.pack_end(self.specLen, False)
    self.updateScans(self.scanRange)
    ## wd = gtk.Label("<i>pixel motors</i>")
    ## wd.set_alignment(1., 0.5)
    ## wd.set_use_markup(True)
    ## hbox.pack_start(wd)
    ## wd1 = gtk.Entry()
    ## wd1.set_width_chars(4)
    ## wd1.set_text("adcZ")
    ## wd1.set_tooltip_text("The name of the motor changing during the scan for the X axis.")
    ## hbox.pack_start(wd1, False, False)
    ## wd2 = gtk.Entry()
    ## wd2.set_width_chars(4)
    ## wd2.set_text("adcX")
    ## wd2.set_tooltip_text("The name of the motor changing during the scan for the Y axis.")
    ## hbox.pack_start(wd2, False, False)
    ## self.pMotor = (wd1, wd2)

    # Scanning the spec file.
    hbox = gtk.HBox(False, 10)
    vbox.pack_start(hbox, False)
    wd = gtk.Button("read file")
    wd.connect("clicked", self.onReadSpec)
    hbox.pack_start(wd, False)
    vbox2 = gtk.VBox()
    hbox.pack_end(vbox2, True, True)
    self.specProgress = gtk.ProgressBar()
    vbox2.pack_end(self.specProgress, False)
    self.specMessage = gtk.Label()
    self.specMessage.set_use_markup(True)
    vbox2.pack_start(self.specMessage, False)

    return vbox

  def onReadSpec(self, widget):
    gobject.idle_add(self.readSpec, widget)

  def threadReadSpec(self, q):
    matfile = self.getOutFile(".matrices")
    if os.path.isfile(matfile):
      q.put(False)
      mat = pickle.load(open(matfile, 'rb'))
      if "scheme" in mat:
        self.updateScans(mat["scheme"])
    else:
      q.put(True)
      #mat = socs_linux.k_mapping_3D(self.spec, self.specScan, matfile, q)
      mat = socs_linux.mat_from_specs(self.spec, self.specScan, q)
      mat["scheme"] = self.scanScheme
      pickle.dump(mat, open(matfile, 'wb'))
    q.put(mat)

  def waitReadSpec(self, pr, q, widget = None):
    obj = queueGetLast(q)
    if type(obj) == type({}):
      if widget is not None:
        widget.set_sensitive(True)
      self.specProgress.set_fraction(1.)
      self.mat = obj
      self.updateInt()
      pr.join()
      if self.waitReadSpecFraction:
        self.setSpecMessage("saved to '%s'" % os.path.basename(self.getOutFile(".matrices")))
      gobject.idle_add(self.emit, "matrices-loaded", self.mat)
      return False
    elif type(obj) == type(0) and self.waitReadSpecFraction:
      self.setSpecMessage("read scan #%d (%d/%d)" % (self.specScan[obj], obj, len(self.specScan)))
      self.specProgress.set_fraction(float(obj) / len(self.specScan))
    elif not(self.waitReadSpecFraction):
      self.specProgress.pulse()
    return True
    
  def readSpec(self, widget = None):
    if widget is not None:
      widget.set_sensitive(False)
    q = multiprocessing.Queue()
    pr = multiprocessing.Process(target = self.threadReadSpec, args = (q, ))
    pr.start()
    self.waitReadSpecFraction = q.get()
    gobject.timeout_add(200, self.waitReadSpec, pr, q, widget)
    return False

  def getIntOptions(self):
    vbox = gtk.VBox()
    # ROIs
    hbox = gtk.HBox()
    vbox.pack_start(hbox, False)
    wd = gtk.Label("<i>rc angle</i>")
    wd.set_use_markup(True)
    wd.set_alignment(0., 0.5)
    hbox.pack_start(wd, True, True)
    self.rcAngle = gtk.combo_box_new_text()
    self.rcAngle.connect("changed", self.onRcAngle)
    self.rcAngle.set_tooltip_text("The name of the rocking angle in the spec file.")
    hbox.pack_start(self.rcAngle, False)
    hbox.pack_start(gtk.Label(" minus offset (°)"), False)
    self.rcOffset = gtk.SpinButton(gtk.Adjustment(value = 0.,
                                                  lower = -90.,
                                                  upper = 90.,
                                                  step_incr = 0.5,
                                                  page_incr = 5),
                                   digits = 2)
    hbox.pack_start(self.rcOffset, False)
    hbox = gtk.HBox()
    vbox.pack_start(hbox, False)
    wd = gtk.Label("<i>Integrations</i>")
    wd.set_use_markup(True)
    wd.set_alignment(0., 0.5)
    hbox.pack_start(wd, True, True)
    self.intChecks = {}
    for (lbl, active) in {"roi1":True, "roi2":True, "roi3":True, "roi4":True, "roi5":False}.items(): #, "ccdint1", "ccdint2"):
      wd = gtk.CheckButton(lbl)
      wd.set_active(active)
      wd.set_sensitive(False)
      hbox.pack_start(wd, False)
      self.intChecks[lbl] = wd
    hbox = gtk.HBox()
    vbox.pack_start(hbox, False)
    wd = gtk.Label("")
    hbox.pack_start(wd, True, True)
    for (lbl, active) in {"ccdint1":False, "ccdint2":False}.items():
      wd = gtk.CheckButton(lbl)
      wd.set_active(active)
      wd.set_sensitive(False)
      hbox.pack_start(wd, False)
      self.intChecks[lbl] = wd

    # Generating integral files.
    hbox = gtk.HBox(False, 10)
    vbox.pack_start(hbox, False)
    wd = gtk.Button("generate PNGs")
    wd.connect("clicked", self.onGenerateInt)
    hbox.pack_start(wd, False)
    vbox2 = gtk.VBox()
    hbox.pack_end(vbox2, True, True)
    self.intProgress = gtk.ProgressBar()
    vbox2.pack_end(self.intProgress, False)
    self.intMessage = gtk.Label()
    self.intMessage.set_use_markup(True)
    vbox2.pack_start(self.intMessage, False)
    return vbox

  def setIntMessage(self, message):
    self.intMessage.set_markup("<span size=\"smaller\">" + message + "</span>")

  def updateInt(self):
    self.rcAngle.get_model().clear()
    for (lbl, v) in self.mat.items():
      if len(numpy.shape(v)) == 1 and not(lbl == "scan") and not(lbl == "paths") and not(lbl == "scheme"):
        # We try to guess which angle is varying.
        dev = ((v - v[0])**2).sum()
        if dev > 1e-6:
          if lbl.endswith("-avg"):
            self.rcAngle.append_text(lbl[:-4])
          else:
            self.rcAngle.append_text(lbl)
    self.rcAngle.set_active(0)
    for (lbl, wd) in self.intChecks.items():
      wd.set_sensitive(lbl in self.mat)

  def onRcAngle(self, widget):
    self.emit("rc-angle-selected", widget.get_active_text())

  def onGenerateInt(self, widget):
    gobject.idle_add(self.generateInt, widget)

  def threadGenerateInt(self, q):
    pngfile = self.getOutFile()
    lbls = self.intChecks.keys()
    lbls.sort()
    rois = [lbl for lbl in lbls if self.intChecks[lbl].get_active()]
    socs_linux.plotROI2D(self.mat, rois, self.getOutFile(), q)
    q.put("Done")

  def waitGenerateInt(self, pr, q, widget = None):
    try:
      while True:
        obj = q.get(block = False)
        if type(obj) == type(0):
          self.setIntMessage("done for scan #%d (%d/%d)" % (self.mat['scan'][obj],
                                                            self.intDone,
                                                            len(self.mat['scan'])))
          self.intProgress.set_fraction(float(self.intDone) / len(self.mat['scan']))
          self.intDone += 1
        elif type(obj) == type(""):
          if widget is not None:
            widget.set_sensitive(True)
          self.intProgress.set_fraction(1.)
          pr.join()
          self.setIntMessage("")
          return False
      return True
    except Queue.Empty:
      return True
    
  def generateInt(self, widget = None):
    if self.mat is None:
      self.setIntMessage("<span color=\"red\">read spec file first</span>")
      return
    if widget is not None:
      widget.set_sensitive(False)
    q = multiprocessing.Queue()
    pr = multiprocessing.Process(target = self.threadGenerateInt, args = (q, ))
    pr.start()
    self.intDone = 0
    self.setIntMessage("generate images…")
    gobject.timeout_add(200, self.waitGenerateInt, pr, q, widget)
    return False
  
  def getMethodChooser(self, method = 1):
    self.method = method
    # Comptuting method
    hbox = gtk.HBox()
    wd = gtk.Label("<i>Method</i>")
    wd.set_alignment(0., 0.5)
    wd.set_use_markup(True)
    hbox.pack_start(wd)
    rd = gtk.RadioButton(label = "max chi curve")
    rd.connect("toggled", self.onMethod, 1)
    rd.set_tooltip_text("Integrate the images along the rocking curve and fit the position of the peak with gaussians after projections along X and Y.")
    hbox.pack_start(rd)
    rd2 = gtk.RadioButton(group = rd, label = "max in reciprocal space")
    if method == 2:
      rd2.set_active(True)
    rd2.connect("toggled", self.onMethod, 2)
    rd2.set_tooltip_text("Reconstruct the peak in the Fourier space and fit it with gaussians after projection along Qx, Qy and Qz.")
    hbox.pack_start(rd2)
    return hbox

  def onMethod(self, widget, m):
    if widget.get_active():
      self.method = m
      self.checkInt.set_sensitive(m == 1)
      self.hboxMiller.set_sensitive(not m == 1)
      self.emit("method-selected", m)

  def getBeamOptions(self, model, energy = None):
    # Beam energy
    hbox = gtk.HBox()
    wd = gtk.Label("<i>Beam energy</i>")
    wd.set_alignment(0., 0.5)
    wd.set_use_markup(True)
    hbox.pack_start(wd)
    hbox.pack_end(gtk.Label("eV"), False, False)
    self.beamEn = gtk.SpinButton(gtk.Adjustment(value = model.detector["en"],
                                       lower = 5000,
                                       upper = 9999,
                                       step_incr = 0.5,
                                       page_incr = 5),
                        digits = 2)
    self.beamEn.connect("value-changed", self.onDetector, model, "en")
    self.beamEn.set_tooltip_text("Energy of the beam, used to calculate the angle of the pristine diffraction.")
    if energy is not None:
      self.beamEn.set_value(energy)
    hbox.pack_end(self.beamEn, False, False)
    return hbox

  def getDetectorOptions(self, model, align = None):
    vbox = gtk.VBox()
    # Detector alignment.
    hbox = gtk.HBox()
    vbox.pack_start(hbox, False)
    wd = gtk.Button()
    wd.add(gtk.image_new_from_stock(gtk.STOCK_OPEN, gtk.ICON_SIZE_SMALL_TOOLBAR))
    wd.set_relief(gtk.RELIEF_NONE)
    wd.connect("clicked", self.onBeamSelector)
    wd.set_tooltip_text("Define the position of the pristine beam and the variations when the detector is tilted.")
    hbox.pack_end(wd, False)
    vbox2 = gtk.VBox()
    hbox.pack_start(vbox2, True, True)

    # Detector centre
    hbox = gtk.HBox()
    vbox2.pack_start(hbox, False, False)
    wd = gtk.Label("<i>Pristine beam</i>")
    wd.set_alignment(0., 0.5)
    wd.set_use_markup(True)
    hbox.pack_start(wd)
    hbox.pack_end(gtk.Label("px"), False, False)
    wd = gtk.SpinButton(gtk.Adjustment(value = model.detector["cch"][0],
                                       lower = 0,
                                       upper = 515,
                                       step_incr = 0.05,
                                       page_incr = 1),
                        digits = 4)
    wd.connect("value-changed", self.onDetector, model, "cch", 0)
    hbox.pack_end(wd, False, False)
    self.pristineY = wd
    hbox.pack_end(gtk.Label(" × v="), False, False)
    wd = gtk.SpinButton(gtk.Adjustment(value = model.detector["cch"][1],
                                       lower = 0,
                                       upper = 515,
                                       step_incr = 0.05,
                                       page_incr = 1),
                        digits = 4)
    wd.connect("value-changed", self.onDetector, model, "cch", 1)
    hbox.pack_end(wd, False, False)
    self.pristineX = wd
    hbox.pack_end(gtk.Label("h="), False, False)

    # Pixel per degree
    hbox = gtk.HBox()
    vbox2.pack_start(hbox, False, False)
    wd = gtk.Label("<i>Ch. per deg.</i>")
    wd.set_alignment(0., 0.5)
    wd.set_use_markup(True)
    hbox.pack_start(wd)
    hbox.pack_end(gtk.Label("px"), False, False)
    wd = gtk.SpinButton(gtk.Adjustment(value = model.detector["chpdeg"][1],
                                       lower = -515,
                                       upper = 515,
                                       step_incr = 0.05,
                                       page_incr = 1),
                        digits = 3)
    wd.connect("value-changed", self.onDetector, model, "chpdeg", 1)
    hbox.pack_end(wd, False, False)
    self.cchY = wd
    hbox.pack_end(gtk.Label(" × v="), False, False)
    wd = gtk.SpinButton(gtk.Adjustment(value = model.detector["chpdeg"][0],
                                       lower = -515,
                                       upper = 515,
                                       step_incr = 0.05,
                                       page_incr = 1),
                        digits = 3)
    wd.connect("value-changed", self.onDetector, model, "chpdeg", 0)
    hbox.pack_end(wd, False, False)
    self.cchX = wd
    hbox.pack_end(gtk.Label("h="), False, False)

    if align is not None:
      (cch, chpdeg) = socs_linux.GetDetectorParameter(align[0], align[1], align[2])
      self.cchX.set_value(chpdeg[1])
      self.cchY.set_value(chpdeg[0])
      self.pristineX.set_value(cch[1])
      self.pristineY.set_value(cch[0])

    # Detector orientation
    hbox = gtk.HBox()
    wd = gtk.Label("<i>Detector orientation</i>")
    wd.set_alignment(0., 0.5)
    wd.set_use_markup(True)
    hbox.pack_start(wd)
    rd = gtk.RadioButton(label = "width is μ")
    rd.connect("toggled", self.onDetectorOrientation, model, ('z-', 'y+'))
    hbox.pack_start(rd, False)
    rd2 = gtk.RadioButton(group = rd, label = "width is φ")
    rd2.connect("toggled", self.onDetectorOrientation, model, ('y+', 'z-'))
    hbox.pack_start(rd2, False)
    vbox.pack_start(hbox, False, False)

    return vbox

  def onPreview(self, widget, img):
    f = widget.get_preview_filename()
    dt = None
    try:
      if f is not None: 
        dt = edf.RawImage()
        dt.read(f)
    except IOError:
      dt = None
    if dt is not None:
      img.set_from_pixbuf(dt.getPixbuf(logScale = True, colorMap = edf.jetScale).scale_simple(200, 200, gtk.gdk.INTERP_BILINEAR))
      img.show()
    widget.set_preview_widget_active(dt is not None)

  def onValidateAlignment(self, widget):
    widget.set_response_sensitive(gtk.RESPONSE_ACCEPT, len(widget.get_filenames()) == 3)

  def onBeamSelector(self, widget):
    dialog = gtk.FileChooserDialog("Select three alignment EDF files", None,
                                   gtk.FILE_CHOOSER_ACTION_OPEN,
                                   (gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT,
                                    gtk.STOCK_OPEN, gtk.RESPONSE_ACCEPT))
    dialog.set_select_multiple(True)
    dialog.connect("selection-changed", self.onValidateAlignment)
    filter = gtk.FileFilter()
    filter.set_name("ESRF Data Format (*.edf, *.edf.gz)")
    filter.add_pattern("*.edf")
    filter.add_pattern("*.edf.gz")
    dialog.add_filter(filter)
    filter = gtk.FileFilter()
    filter.set_name("All files")
    filter.add_pattern("*")
    dialog.add_filter(filter)
    dialog.set_current_folder(os.getcwd())
    # Preview.
    vbox = gtk.VBox()
    wd = gtk.Image()
    wd.set_size_request(200, 200)
    vbox.pack_start(wd)
    dialog.set_preview_widget(vbox)
    dialog.connect("selection-changed", self.onPreview, wd)
    dialog.show_all()
    response = dialog.run()
    if response == gtk.RESPONSE_ACCEPT:
      files = dialog.get_filenames()
      (cch, chpdeg) = socs_linux.GetDetectorParameter(files[0], files[1], files[2])
      self.cchX.set_value(chpdeg[1])
      self.cchY.set_value(chpdeg[0])
      self.pristineX.set_value(cch[1])
      self.pristineY.set_value(cch[0])
    dialog.destroy()

  def onDetector(self, widget, model, k, i = None):
    if i is None:
      model.detector[k] = widget.get_value()
    else:
      model.detector[k][i] = widget.get_value()
  def onDetectorOrientation(self, toggle, model, value):
    if not toggle.get_active():
      return
    model.detector["orient"] = value

  def getImageOptions(self):
    vbox = gtk.VBox()
    
    hbox = gtk.HBox()
    vbox.pack_start(hbox, False)
    wd = gtk.Label("<i>Select region of interest</i>")
    wd.set_alignment(0., 0.5)
    wd.set_use_markup(True)
    hbox.pack_start(wd, True)

    table = gtk.Table(rows = 2, columns = 4)
    hbox.pack_end(table, False)
    self.spinRectX = gtk.SpinButton(gtk.Adjustment(lower = 0,
                                                   upper = 2047,
                                                   step_incr = 1,
                                                   page_incr = 10))
    table.attach(self.spinRectX, 1, 2, 0, 1)
    table.attach(gtk.Label("×"), 2, 3, 0, 1)
    self.spinRectY = gtk.SpinButton(gtk.Adjustment(lower = 0,
                                                   upper = 2047,
                                                   step_incr = 1,
                                                   page_incr = 10))
    table.attach(self.spinRectY, 3, 4, 0, 1)
    table.attach(gtk.Label("orig.:"), 0, 1, 0, 1)
    self.spinRectH = gtk.SpinButton(gtk.Adjustment(value = 516,
                                                   lower = 1,
                                                   upper = 2048,
                                                   step_incr = 1,
                                                   page_incr = 10))
    table.attach(self.spinRectH, 3, 4, 1, 2)
    table.attach(gtk.Label("×"), 2, 3, 1, 2)
    self.spinRectW = gtk.SpinButton(gtk.Adjustment(value = 516,
                                                   lower = 1,
                                                   upper = 2048,
                                                   step_incr = 1,
                                                   page_incr = 10))
    table.attach(self.spinRectW, 1, 2, 1, 2)
    table.attach(gtk.Label("size:"), 0, 1, 1, 2)
    self.roi = (self.spinRectX, self.spinRectY, self.spinRectW, self.spinRectH)

    hbox = gtk.HBox()
    vbox.pack_start(hbox, False)
    wd = gtk.Label("<i>Pixel averaging</i>")
    wd.set_alignment(0., 0.5)
    wd.set_use_markup(True)
    hbox.pack_start(wd, True)
    self.imgNav = []
    wd = gtk.SpinButton(gtk.Adjustment(value = 2,
                                       lower = 1,
                                       upper = 10,
                                       step_incr = 1,
                                       page_incr = 2))
    self.imgNav.append(wd)
    hbox.pack_start(wd, False)
    hbox.pack_start(gtk.Label("×"), False)
    wd = gtk.SpinButton(gtk.Adjustment(value = 2,
                                       lower = 1,
                                       upper = 10,
                                       step_incr = 1,
                                       page_incr = 2))
    self.imgNav.append(wd)
    hbox.pack_start(wd, False)

    hbox = gtk.HBox()
    vbox.pack_start(hbox, False)
    wd = gtk.Label("<i>Peak detection method</i>")
    wd.set_alignment(0., 0.5)
    wd.set_use_markup(True)
    hbox.pack_start(wd, True)
    rd = gtk.RadioButton(label = "gaussian fit")
    rd.connect("toggled", self.onPeakMethod, 1)
    rd.set_tooltip_text("Use a gaussian fit to find the peak position.")
    hbox.pack_start(rd, False)
    rd2 = gtk.RadioButton(group = rd, label = "max position")
    if self.peakMethod == 2:
      rd2.set_active(True)
    rd2.connect("toggled", self.onPeakMethod, 2)
    rd2.set_tooltip_text("Use the position of the maximum value.")
    hbox.pack_start(rd2, False)

    hbox = gtk.HBox()
    vbox.pack_start(hbox, False)
    wd = gtk.Label("<i>Output mesh</i>")
    wd.set_alignment(0., 0.5)
    wd.set_use_markup(True)
    hbox.pack_start(wd, True)
    self.qMesh = []
    hbox.pack_start(gtk.Label("qx="), False)
    wd = gtk.SpinButton(gtk.Adjustment(value = 50,
                                       lower = 1,
                                       upper = 100,
                                       step_incr = 1,
                                       page_incr = 5))
    self.qMesh.append(wd)
    hbox.pack_start(wd, False)
    hbox.pack_start(gtk.Label("qy="), False)
    wd = gtk.SpinButton(gtk.Adjustment(value = 50,
                                       lower = 1,
                                       upper = 100,
                                       step_incr = 1,
                                       page_incr = 5))
    self.qMesh.append(wd)
    hbox.pack_start(wd, False)
    hbox.pack_start(gtk.Label("qz="), False)
    wd = gtk.SpinButton(gtk.Adjustment(value = 30,
                                       lower = 1,
                                       upper = 100,
                                       step_incr = 1,
                                       page_incr = 5))
    self.qMesh.append(wd)
    hbox.pack_start(wd, False)

    return vbox

  def onPeakMethod(self, widget, method):
    if widget.get_active():
      self.peakMethod = method

  def onRoiSelection(self, widget, (x, y), (w, h)):
    self.spinRectX.set_value(x)
    self.spinRectY.set_value(y)
    self.spinRectW.set_value(w)
    self.spinRectH.set_value(h)

  def getRoi(self):
    return ((self.spinRectX.get_value(), self.spinRectY.get_value()),
            (self.spinRectW.get_value(), self.spinRectH.get_value()))

  def getComputeQ(self):
    # calculate qx, qy and qz of the COM of the 3D peak.
    hbox = gtk.HBox(False, 10)
    wd = gtk.Button("calculate Q")
    wd.connect("clicked", self.onCalculateQ)
    hbox.pack_start(wd, False)
    vbox2 = gtk.VBox()
    hbox.pack_end(vbox2, True, True)
    self.qProgress = gtk.ProgressBar()
    vbox2.pack_end(self.qProgress, False)
    self.qMessage = gtk.Label()
    self.qMessage.set_use_markup(True)
    vbox2.pack_start(self.qMessage, False)
    return hbox

  def onCalculateQ(self, widget):
    gobject.idle_add(self.calculateQ, widget)

  def setQMessage(self, message):
    self.qMessage.set_markup("<span size=\"smaller\">" + message + "</span>")

  def waitCalculateQ(self, pr, q, widget = None):
    try:
      while True:
        obj = q.get(block = False)
        if type(obj) == type(()):
          i, j, frac = obj
          self.setQMessage("spot %g×%g done." % (self.mat['X'][i, j, 0], self.mat['Y'][i, j, 0]))
          self.qProgress.set_fraction(self.qProgress.get_fraction() + frac)
        elif type(obj) == type({}):
          if widget is not None:
            widget.set_sensitive(True)
          self.qProgress.set_fraction(1.)
          self.result = obj
          self.result['X'] = self.mat['X'][:,:,0]
          self.result['Y'] = self.mat['Y'][:,:,0]
          pr.join()
          self.setQMessage("saved to '%s'" % os.path.basename(self.getOutFile(".qmesh")))
          gobject.idle_add(self.emit, "q-loaded", self.result)
          return False
      return True
    except Queue.Empty:
      return True

  def threadComputeQ(self, q):
    qfile = self.getOutFile(".qmesh")
    if os.path.isfile(qfile):
      with open(qfile,'rb') as fp:
        result = pickle.load(fp)
    else:
      # Remove the rc offset.
      rcAngle = self.rcAngle.get_active_text()
      self.mat[rcAngle] -= self.rcOffset.get_value()
      result = socs_linux.getPeakPosition_multiprocessing( \
        (0, self.mat['X'].shape[0]-1), self.mat,
        self.datadirChooser.get_current_folder(),
        self.datadirPrefix.get_text(), self.imgPrefix.get_text(),
        int(self.qMesh[0].get_value()), int(self.qMesh[1].get_value()),
        int(self.qMesh[2].get_value()),
        [int(w.get_value()) for w in self.imgNav],
        (int(self.roi[0].get_value()), int(self.roi[0].get_value() + self.roi[2].get_value()),
         int(self.roi[1].get_value()), int(self.roi[1].get_value() + self.roi[3].get_value())),
        (self.pristineY.get_value(), self.pristineX.get_value()),
        (self.cchY.get_value(), self.cchX.get_value()),
        self.beamEn.get_value(), None, (self.peakMethod == 1), self.getOutFile(".qmesh"), q)
    q.put(result)

  def calculateQ(self, widget = None):
    if self.mat is None:
      self.setQMessage("<span color=\"red\">read spec file first</span>")
      return
    if self.imgPrefix.get_text().strip() == "":
      self.setQMessage("<span color=\"red\">specify EDF naming scheme</span>")
      self.imgPrefix.set_icon_from_stock(gtk.ENTRY_ICON_SECONDARY, gtk.STOCK_DIALOG_WARNING)
      return
    if widget is not None:
      widget.set_sensitive(False)
      
    q = multiprocessing.Queue()
    pr = multiprocessing.Process(target = self.threadComputeQ, args = (q, ))
    pr.start()
    self.setQMessage("computing reciprocal space projections…")
    gobject.timeout_add(200, self.waitCalculateQ, pr, q, widget)
    return False

  def getLatticeOptions(self, a0 = 5.4309, peak = "220"):
    # Lattice parameter
    vbox = gtk.VBox()

    hbox = gtk.HBox()
    vbox.pack_start(hbox)
    wd = gtk.Label("<i>Lattice</i>")
    wd.set_alignment(0., 0.5)
    wd.set_use_markup(True)
    hbox.pack_start(wd)

    self.bravais = gtk.combo_box_new_text()
    self.bravais.append_text("cubic")
    self.bravais.append_text("tetragonal")
    self.bravais.append_text("orthorombic")
    self.bravais.append_text("hexagonal")
    self.bravais.append_text("rhombohedral")
    self.bravais.append_text("monoclinic")
    self.bravais.append_text("triclinic")
    self.bravais.connect("changed", self.onBravais)
    self.bravais.set_tooltip_text("Give the bravais lattice of the undeformed sample. It is used to calculate the reference diffraction angle.")
    hbox.pack_end(self.bravais, False)

    self.entryPeak = gtk.Entry()
    self.entryPeak.set_width_chars(6)
    self.entryPeak.set_text(peak)
    self.entryPeak.connect("changed", self.onMillerChanged)
    self.entryPeak.set_tooltip_text("Give the Miller indices of the diffraction peak. It is used to calculate the reference diffraction angle.")
    hbox.pack_end(self.entryPeak, False, False)
    hbox.pack_end(gtk.Label("peak="), False, False)

    self.hboxLattice = gtk.HBox()
    vbox.pack_start(self.hboxLattice)
    self.hboxLattice.pack_end(gtk.Label("Å"), False, False)
    self.spinLatticeC = gtk.SpinButton(gtk.Adjustment(value = a0,
                                                      lower = 0.5,
                                                      upper = 6,
                                                      step_incr = 0.0001,
                                                      page_incr = 0.01),
                                      digits = 4)
    self.spinLatticeC.connect("changed", self.onSpinLattice)
    self.hboxLattice.pack_end(self.spinLatticeC, False, False)
    self.hboxLattice.pack_end(gtk.Label(" c="), False, False)
    self.hboxLattice.pack_end(gtk.Label("Å"), False, False)
    self.spinLatticeB = gtk.SpinButton(gtk.Adjustment(value = a0,
                                                      lower = 0.5,
                                                      upper = 6,
                                                      step_incr = 0.0001,
                                                      page_incr = 0.01),
                                      digits = 4)
    self.spinLatticeB.connect("changed", self.onSpinLattice)
    self.hboxLattice.pack_end(self.spinLatticeB, False, False)
    self.hboxLattice.pack_end(gtk.Label(" b="), False, False)
    self.hboxLattice.pack_end(gtk.Label("Å"), False, False)
    self.spinLatticeA = gtk.SpinButton(gtk.Adjustment(value = a0,
                                                     lower = 0.5,
                                                     upper = 6,
                                                     step_incr = 0.0001,
                                                     page_incr = 0.01),
                                      digits = 4)
    self.spinLatticeA.connect("changed", self.onSpinLattice)
    self.hboxLattice.pack_end(self.spinLatticeA, False, False)
    self.hboxLattice.pack_end(gtk.Label(" a="), False, False)

    self.lblD = gtk.Label()
    self.lblD.set_alignment(1., 0.5)
    self.lblD.set_use_markup(True)
    vbox.pack_start(self.lblD)

    self.bravais.set_active(0)

    return vbox

  def getMillerChooser(self, defs = ("100", "010", "220")):
    # Reciprocal space orientation
    self.hboxMiller = gtk.HBox()
    self.hboxMiller.set_sensitive(self.method == 2)
    wd = gtk.Label("<i>Miller indices</i>")
    wd.set_alignment(0., 0.5)
    wd.set_use_markup(True)
    self.hboxMiller.pack_start(wd)
    self.entryHKL = [None, None, None]
    lbls = (" Qx=", " Qy=", " Qz=")
    for i in range(2, -1, -1):
      self.entryHKL[i] = gtk.Entry()
      self.entryHKL[i].set_width_chars(6)
      self.entryHKL[i].connect("changed", self.onMillerChanged)
      self.entryHKL[i].set_text(defs[i])
      self.hboxMiller.pack_end(self.entryHKL[i], False, False)
      self.hboxMiller.pack_end(gtk.Label(lbls[i]), False, False)
    return self.hboxMiller

  def onMillerChanged(self, entry):
    try:
      self.getMillerIndices(entry)
    except ValueError:
      pass
    self.setDLabel()

  def onBravais(self, combo):
    bravais = combo.get_active_text()
    if bravais == "cubic" or bravais == "rhombohedral":
      self.spinLatticeA.set_sensitive(True)
      self.spinLatticeB.set_sensitive(False)
      self.spinLatticeC.set_sensitive(False)
    elif bravais == "tetragonal" or bravais == "hexagonal":
      self.spinLatticeA.set_sensitive(True)
      self.spinLatticeB.set_sensitive(False)
      self.spinLatticeC.set_sensitive(True)
    elif bravais == "orthorombic" or bravais == "monoclinic" or bravais == "triclinic":
      self.spinLatticeA.set_sensitive(True)
      self.spinLatticeB.set_sensitive(True)
      self.spinLatticeC.set_sensitive(True)
    self.onSpinLattice(self.spinLatticeA)
    self.setDLabel()

  def onSpinLattice(self, spin):
    bravais = self.bravais.get_active_text()
    if not(spin.get_sensitive()):
      return
    val = self.spinLatticeA.get_value()
    if bravais == "cubic" or bravais == "rhombohedral":
      self.spinLatticeB.set_value(val)
      self.spinLatticeC.set_value(val)
    elif bravais == "tetragonal" or bravais == "hexagonal":
      self.spinLatticeB.set_value(val)
    self.setDLabel()

  def setDLabel(self):
    a = self.spinLatticeA.get_value()
    b = self.spinLatticeB.get_value()
    c = self.spinLatticeC.get_value()
    hkl = list(self.getMillerIndices(self.entryPeak))
    bravais = self.bravais.get_active_text()
    # Form on http://www.xtal.iqfr.csic.es/Cristalografia/parte_04-en.html
    if bravais == "cubic":
      d = a / numpy.sqrt((numpy.array(hkl)**2).sum())
      form = "a / sqrt(h<sup>2</sup> + k<sup>2</sup> + l<sup>2</sup>)"
    elif bravais == "tetragonal":
      hkl[2] *= a / c
      d = a / numpy.sqrt((numpy.array(hkl)**2).sum())
      form = "a / sqrt(h<sup>2</sup> + k<sup>2</sup> + (l*a/c)<sup>2</sup>)"
    elif bravais == "orthorombic":
      hkl[0] /= a
      hkl[1] /= b
      hkl[2] /= c
      d = 1 / numpy.sqrt((numpy.array(hkl)**2).sum())
      form = "1 / sqrt((h/a)<sup>2</sup> + (k/b)<sup>2</sup> + (l/c)<sup>2</sup>)"
    elif bravais == "hexagonal":
      hkl[2] *= a / c
      d = a / numpy.sqrt(4. / 3. * (hkl[0] * hkl[0] + hkl[1] * hkl[1] + hkl[0] * hkl[1]) + hkl[2] * hkl[2])
      form = "1 / sqrt(4/3 * (h<sup>2</sup> + k<sup>2</sup> + hk) + (l*a/c)<sup>2</sup>)"
    else:
      d = numpy.nan
      form = "not implemented"
    self.lblD.set_markup("d = <i>%s</i> = %gÅ" % (form, d))
    self.latticeD = d

  def setStrainMessage(self, message):
    self.strainMessage.set_markup("<span size=\"smaller\">" + message + "</span>")

  def computeStrain(self, widget = None):
    if self.result is None:
      self.setStrainMessage("<span color=\"red\">calculate Q first</span>")
      self.strainProgress.set_fraction(0.)
    else:
      dd = 2. * numpy.pi / numpy.sqrt(self.result['qx_peak']**2 +
                                      self.result['qy_peak']**2 +
                                      self.result['qz_peak']**2)
      d = self.latticeD
      s = (dd - d) / d
      fig = matplotlib.figure.Figure(figsize=(8,5), dpi=80, facecolor='w', edgecolor='k')
      plt = fig.add_subplot(1,1,1)
      socs_linux.plotQmesh(fig, self.mat,plt, self.result['X'], self.result['Y'],s, "strain")
      self.strain.setFigure(fig)
      self.setStrainMessage("")
      self.strainProgress.set_fraction(1.)
    if widget is not None:
      widget.set_sensitive(True)
    return False

  def onComputeStrain(self, widget):
    widget.set_sensitive(False)
    gobject.idle_add(self.computeStrain, widget)

  def getComputeStrain(self):
    # calculate qx, qy and qz of the COM of the 3D peak.
    hbox = gtk.HBox(False, 10)
    wd = gtk.Button("calculate strain")
    wd.connect("clicked", self.onComputeStrain)
    hbox.pack_start(wd, False)
    vbox2 = gtk.VBox()
    hbox.pack_end(vbox2, True, True)
    self.strainProgress = gtk.ProgressBar()
    vbox2.pack_end(self.strainProgress, False)
    self.strainMessage = gtk.Label()
    self.strainMessage.set_use_markup(True)
    vbox2.pack_start(self.strainMessage, False)
    return hbox

  def getStrainViewer(self):
    self.strain = ScansMapViewer(self.getOutFile, "_strain.png", "strain map",
                                 "Strain not calculated.")
    return self.strain

  def getOptionIntegrate(self):
    # Options
    hbox = gtk.HBox()
    wd = gtk.Label("<i>Integrate on nu at each piy</i>")
    wd.set_alignment(0., 0.5)
    wd.set_use_markup(True)
    hbox.pack_start(wd)
    self.checkInt = gtk.CheckButton("")
    hbox.pack_end(self.checkInt, False, False)
    return hbox

  def getMillerIndices(self, entry):
    parsed = GuiOptions.millerIndices.match(entry.get_text())
    if parsed is not None:
      entry.set_icon_from_stock(gtk.ENTRY_ICON_SECONDARY, None)
      return map(int, (parsed.group("x"), parsed.group("y"), parsed.group("z")))
    else:
      entry.set_icon_from_stock(gtk.ENTRY_ICON_SECONDARY,
                                gtk.STOCK_DIALOG_WARNING)
      raise ValueError

  def getPeakIndices(self):
    return self.getMillerIndices(self.entryPeak)

  def getRecipIndices(self):
    return map(self.getMillerIndices, self.entryHKL)

  def getMapViewer(self):
    self.maps = ScansMapViewer(self.getOutFile, "_qx_qy_qz.png", "Q maps",
                               "No Q matrice loaded.")
    self.connect("q-loaded", self.showMaps)
    return self.maps

  def showMaps(self, self_, qmat):
    fig = socs_linux.plotQI(qmat,self.mat,self.getOutFile("_qx_qy_qz.png"))
    self.maps.setFigure(fig)

from matplotlib.backends.backend_gtkagg import FigureCanvasGTKAgg
    
class ScansMapViewer(gtk.VBox):
  def __init__(self, getOutFile = None, file_suffix = "",
               title = "", default = "No data"):
    super(ScansMapViewer, self).__init__()

    self.title = title
    self.getOutFile = getOutFile
    self.suffix = file_suffix

    wd = gtk.Toolbar()
    wd.set_orientation(gtk.ORIENTATION_HORIZONTAL)
    wd.set_style(gtk.TOOLBAR_ICONS)
    wd.set_icon_size(gtk.ICON_SIZE_SMALL_TOOLBAR)
    self.pack_end(wd, False)

    item = gtk.ToolButton(stock_id = gtk.STOCK_SAVE)
    item.set_tooltip_text("Save " + self.title + " to PNG file.")
    item.connect("clicked", self.onSave)
    wd.insert(item, -1)

    item = gtk.ToolButton(stock_id = gtk.STOCK_FULLSCREEN)
    item.set_tooltip_text("View " + self.title + " full screen.")
    item.connect("clicked", self.onFullScreen)
    wd.insert(item, -1)

    self.lbl = gtk.Label(default)
    self.lbl.show()
    self.lbl.set_no_show_all(True)
    self.pack_start(self.lbl, True, True)
    
    self.progress = gtk.ProgressBar()
    self.progress.set_no_show_all(True)
    self.pack_start(self.progress, True, True)
    
    self.canvas = None
    self.win = gtk.Window()

  def setFigure(self, fig):
    self.lbl.hide()
    self.progress.hide()
    if self.canvas is not None:
      self.canvas.destroy()
    
    rect = self.get_allocation()
    (w, h) = fig.get_size_inches()
    fig.set_dpi(rect.width / w)
    
    self.canvas = FigureCanvasGTKAgg(fig)
    self.pack_start(self.canvas, True, True)
    self.canvas.draw()
    self.canvas.show()
    self.fig = fig

  def onFullScreen(self, item):
    parent = self.get_parent()
    if parent is self.win:
      self.reparent(self.usualParent)
      if type(self.usualParent) == gtk.Notebook:
        self.usualParent.set_tab_label_text(self, self.title + " viewer")
        self.usualParent.set_current_page(self.usualParent.page_num(self))
      self.win.unfullscreen()
      self.win.hide()
    else:
      self.usualParent = parent
      self.reparent(self.win)
      self.win.fullscreen()
      self.win.show_all()

  def onSave(self, item):
    if self.canvas is None:
      return

    from matplotlib.backends import backend_pdf
    from matplotlib.backends import backend_agg
    
    dialog = gtk.FileChooserDialog("Export " + self.title + " as PNG / PDF", None,
                                   gtk.FILE_CHOOSER_ACTION_SAVE,
                                   (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                                    gtk.STOCK_SAVE, gtk.RESPONSE_OK))
    if (self.getOutFile is not None):
      dialog.set_current_name(self.getOutFile(self.suffix))
    response = dialog.run()
    if response == gtk.RESPONSE_OK:
      if dialog.get_filename().endswith(".pdf"):
        pp = backend_pdf.PdfPages(dialog.get_filename())
        pp.savefig(self.fig)
        pp.close()
      else:
        pp = backend_agg.FigureCanvasAgg(self.fig)
        self.fig.savefig(dialog.get_filename())
      manager = gtk.recent_manager_get_default()
      manager.add_item(dialog.get_uri())
    dialog.destroy()
