#!/usr/bin/env python3
#
# Copyright (C) 2012-2020 Damien Caliste <damien.caliste@cea.fr>,
#                         Thu Nhi Tran Thi <thu-nhi.tran-thi@esrf.fr>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA

import sys, os, re, time, locale
import numpy
from optparse import OptionParser, OptionGroup
import inspect
from math import pi

def stressScale(img):
  img = img * 2.
  red = img.clip(min = 0, max = 255).astype(numpy.dtype('u1'))
  green1 = img
  green2 = 511 - img
  green = numpy.minimum(green1, green2).clip(min = 0, max = 255).astype(numpy.dtype('u1'))
  blue = (511 - img).clip(min = 0, max = 255).astype(numpy.dtype('u1'))
  return numpy.vstack((red, green, blue)).T.reshape(-1)

def grayScale(img):
  return img.astype(numpy.dtype('u1')).repeat(3)

def xrayScale(img):
  return 255-img.astype(numpy.dtype('u1')).repeat(3)

def jetScale(img):
  img = img * 4.
  red1 = img - 382
  red2 = -img + 1147
  red = numpy.minimum(red1, red2).clip(min = 0, max = 255).astype(numpy.dtype('u1'))
  green1 = img - 127
  green2 = -img + 892
  green = numpy.minimum(green1, green2).clip(min = 0, max = 255).astype(numpy.dtype('u1'))
  blue1 = img + 127
  blue2 = -img + 637
  blue = numpy.minimum(blue1, blue2).clip(min = 0, max = 255).astype(numpy.dtype('u1'))
  return numpy.vstack((red, green, blue)).T.reshape(-1)

def viridisScale(img):
  span = numpy.linspace(0., 255., 9)
  reds = numpy.array((0x44, 0x47, 0x3b, 0x2c, 0x21, 0x27, 0x5c, 0xaa, 0xfd), dtype = numpy.dtype('u1'))
  greens = numpy.array((0x01, 0x2c, 0x51, 0x71, 0x90, 0xad, 0xc8, 0xdc, 0xe7), dtype = numpy.dtype('u1'))
  blues = numpy.array((0x54, 0x7a, 0x8b, 0x8e, 0x8d, 0x81, 0x63, 0x32, 0x25), dtype = numpy.dtype('u1'))
  return numpy.vstack((numpy.interp(img, span, reds),
                       numpy.interp(img, span, greens),
                       numpy.interp(img, span, blues))).T.reshape(-1)  

class TextData():
  reader = re.compile(r"(?P<key>[a-zA-Z_0-9]+)[^=]*=(?P<value>[^;]+);")
  
  def __init__(self, filename):
    self.f = open(filename, "rb")

  def close(self):
    self.f.close()

  def readHeader(self, raw, grp, detector = None):
    # Read header
    # ===========
    line = self.f.readline(2)
    if not line.startswith(b"{"):
      line = self.f.readline(2)
    if not line.startswith(b"{"):
      raise ValueError("No header found")
    line = self.f.readline()
    dvalues = None
    dkeys = None
    while not(line.endswith(b"}\n")):
      parsed = TextData.reader.match(line.decode('utf-8'))
      # Endianness
      if line.startswith(b"ByteOrder") and \
           parsed.group("value").strip() == "LowByteFirst":
        raw.endian = '<'
      ## if line.startswith("ByteOrder") and \
      ##      parsed.group("value").strip() == "HighByteFirst":
      ##   raw.endian = '>'
      # Data type
      elif line.startswith(b"DataType"):
        if parsed.group("value").strip() == "UnsignedShort":
          raw.dtype = 'u2'
        elif parsed.group("value").strip().startswith("Double"):
          raw.dtype = 'f8'
        elif parsed.group("value").strip().startswith("Float"):
          raw.dtype = 'f4'
        elif parsed.group("value").strip() == "SignedInteger":
          raw.dtype = 'i4'
        elif parsed.group("value").strip() == "SignedLong":
          raw.dtype = 'i8'
      # Image size
      elif line.startswith(b"Dim_1"):
        raw.width = int(parsed.group("value"))
      elif line.startswith(b"Dim_2"):
        raw.height = int(parsed.group("value"))
      # Dictionnary
      elif line.startswith(b"counter_pos"):
        dvalues = parsed.group("value").split()
        if dkeys is not None:
          raw.counter = dict(zip(dkeys, dvalues))
          dvalues = None
          dkeys = None
      elif line.startswith(b"counter_mne"):
        dkeys = parsed.group("value").split()
        if dvalues is not None:
          raw.counter = dict(zip(dkeys, dvalues))
          dvalues = None
          dkeys = None
      # Dictionnary
      elif line.startswith(b"motor_pos"):
        dvalues = parsed.group("value").split()
        if dkeys is not None:
          raw.motor = dict(zip(dkeys, dvalues))
          dvalues = None
          dkeys = None
      elif line.startswith(b"motor_mne"):
        dkeys = parsed.group("value").split()
        if dvalues is not None:
          raw.motor = dict(zip(dkeys, dvalues))
          dvalues = None
          dkeys = None
      # Misc
      elif line.startswith(b"prefix"):
        raw.prefix = parsed.group("value").strip().replace("/", "")
      elif line.startswith(b"run") or line.startswith(b"Image"):
        raw.run = int(parsed.group("value"))
      elif line.startswith(b"average"):
        raw.average = map(int, parsed.group("value").split())
      elif line.startswith(b"Size"):
        pass
      else:
        raw.dict[parsed.group("key")] = parsed.group("value")
      # Iterator
      line = self.f.readline()
    raw.dataDiskOffset = self.f.tell()
    raw.nFrames = 1

  def moveTo(self, raw, nr):
    self.f.seek((raw.width * raw.height * int(raw.dtype[1:])) * max(0, nr), os.SEEK_CUR)

  def moveAt(self, raw, nr):
    self.f.seek(raw.dataDiskOffset + (raw.width * raw.height * int(raw.dtype[1:])) * max(0, nr), os.SEEK_SET)

  def currentData(self, raw, win = None):
    return numpy.frombuffer(self.f.read(raw.width * raw.height * int(raw.dtype[1:])), dtype = numpy.dtype(raw.endian + raw.dtype), count = raw.width * raw.height)


class GzData(TextData):
  def __init__(self, filename):
    import gzip
    self.f = gzip.open(filename, "rb")

  def close(self):
    self.f.close()

class H5Data():
  def __init__(self, filename):
    import h5py
    self.filename = filename
    self.f = h5py.File(filename, "r")
    self.nr = 0
    self.dtset = None

  def close(self):
    pass

  def readHeader(self, raw, grp, detector = "maxipix"):
    raw.prefix = '.'.join(os.path.basename(self.filename).split('.')[:-1]) + "_scan%04d" % grp
    raw.run = grp
    groups = []
    for k in self.f.keys():
      if k.endswith(".1"):
        groups.append(int(k.split('.')[0]))
    if len(groups) > 0:
      raw.groups = groups
      if grp == 0:
        for grp in groups:
          groupId = "%d.1" % grp
          if groupId in self.f.keys() and "measurement" in self.f[groupId].keys() and detector in self.f[groupId]["measurement"]:
            try:
              self.f[groupId]["measurement"][detector]
              break
            except KeyError:
              pass
    groupId = "%d.1" % grp
    if groupId in self.f.keys() and "measurement" in self.f[groupId].keys() and detector in self.f[groupId]["measurement"]:
      # Multi-scan file case.
      measure = self.f[groupId]["measurement"]
      raw.h5dtset = measure[detector]
      if "srcur" in measure and len(measure["srcur"].shape) == 1:
        raw.srcur = numpy.array(measure["srcur"])
      if "dty" in measure:
        if len(measure["dty"].shape) == 1 and len(measure["dty"]) > 1:
          raw.counter["dtyOrigin"] = measure["dty"][0]
          raw.counter["dtyStep"] = measure["dty"][1] - measure["dty"][0]
    elif len(self.f.keys()) == 1 and "measurement" in self.f[list(self.f.keys())[0]].keys() and "data" in self.f[list(self.f.keys())[0]]["measurement"].keys():
      # Single file case.
      raw.h5dtset = self.f[list(self.f.keys())[0]]["measurement"]["data"]
    else:
      raise ValueError("No %s found for group %s" % (detector, groupId))
    if raw.h5dtset.dtype == "uint8":
      raw.dtype = 'u1'
    elif raw.h5dtset.dtype == "uint16":
      raw.dtype = 'u2'
    elif raw.h5dtset.dtype == "float64":
      raw.dtype = 'f8'
    elif raw.h5dtset.dtype == "Float":
      raw.dtype = 'f4'
    elif raw.h5dtset.dtype == "SignedInteger":
      raw.dtype = 'i4'
    elif raw.h5dtset.dtype == "SignedLong":
      raw.dtype = 'i4'
    else:
      raise TypeError(raw.h5dtset.dtype)
    if len(raw.h5dtset.shape) == 3:
      raw.width = raw.h5dtset.shape[2]
      raw.height = raw.h5dtset.shape[1]
      raw.nFrames = raw.h5dtset.shape[0]
    else:
      raw.width = raw.h5dtset.shape[1]
      raw.height = raw.h5dtset.shape[0]

  def moveTo(self, raw, nr):
    self.nr += nr
    if hasattr(raw, "srcur") and len(raw.srcur) < self.nr:
      raw.counter["srcur"] = raw.srcur[self.nr]
    raw.run = self.nr + 1
  def moveAt(self, raw, nr):
    self.nr = nr
    if hasattr(raw, "srcur"):
      raw.counter["srcur"] = raw.srcur[self.nr]
    raw.run = self.nr + 1

  def currentData(self, raw, win = None):
    if win is None:
      x1, y1, x2, y2 = (0, 0, raw.width, raw.height)
    else:
      x1 = win.get("x", 0)
      x2 = x1 + win.get("width", raw.width - x1)
      y1 = win.get("y", 0)
      y2 = y1 + win.get("height", raw.height - y1)
      raw.width = x2 - x1
      raw.height = y2 - y1
    if len(raw.h5dtset.shape) == 2:
      return numpy.array(raw.h5dtset[y1:y2, x1:x2].reshape(raw.width * raw.height))
    else:
      return numpy.array(raw.h5dtset[self.nr, y1:y2, x1:x2].reshape(raw.width * raw.height))

class TiffData:
  def __init__(self, filename):
    from PIL import Image
    self.filename = filename
    self.f = Image.open(filename)
    self.nr = 0
    self.dtset = None

  def close(self):
    self.f.close()

  def readHeader(self, raw, grp, detector = None):
    if grp != 0:
      raise ValueError("No for group %d with Tiff format" % grp)
    raw.prefix = '.'.join(os.path.basename(self.filename).split('.')[:-1])
    raw.run = grp
    groups = []
    raw.width = self.f.width
    raw.height = self.f.height

  def moveTo(self, raw, nr):
    self.nr += nr
    if hasattr(raw, "srcur") and len(raw.srcur) < self.nr:
      raw.counter["srcur"] = raw.srcur[self.nr]
    raw.run = self.nr + 1
  def moveAt(self, raw, nr):
    self.nr = nr
    if hasattr(raw, "srcur"):
      raw.counter["srcur"] = raw.srcur[self.nr]
    raw.run = self.nr + 1

  def currentData(self, raw, win = None):
    if win is None:
      x1, y1, x2, y2 = (0, 0, raw.width, raw.height)
    else:
      x1 = win.get("x", 0)
      x2 = x1 + win.get("width", raw.width - x1)
      y1 = win.get("y", 0)
      y2 = y1 + win.get("height", raw.height - y1)
      raw.width = x2 - x1
      raw.height = y2 - y1
    if self.f.n_frames == 1:
      return numpy.array(self.f).reshape(-1)
    else:
      return numpy.array(self.f)[:,:,self.nr].reshape(-1)

def fromFilename(f):
  if f.endswith(".gz"):
    return GzData(f)
  elif f.endswith(".h5"):
    return H5Data(f)
  elif f.endswith(".tiff") or f.endswith(".tif") or f.endswith(".TIF"):
    return TiffData(f)
  else:
    try:
      return TextData(f)
    except IOError:
      return GzData(f + ".gz")

class RawImage():
  def __init__(self, store = False, transpose = False):
    self.wrap    = False
    self.endian  = '<'
    self.dtype   = 'u2'
    self.width   = 0
    self.height  = 0
    self.dict    = {}
    self.counter = {}
    self.motor   = {}
    self.data    = None
    self.prefix  = ""
    self.id      = 0
    self.run     = 0
    self.pixbuf  = None
    self.average = []
    self.path    = "."
    self.transpose = transpose
    # When self.data is accessed, it is stored
    # reading from dataOnDisk if storing is True
    self.storing = store
    self.dataFile       = None
    self.dataDiskOffset = None
    self.nFrames = 1
    self.groups  = (0, )
    self.nr      = 0

  def read(self, f, nr = 0, grp = 0, detector = None, win = None):
    needClose = False
    self.dataFile = f
    if isinstance(f, str):
      needClose = True
      self.path = os.path.dirname(f)
      f = fromFilename(f)
    elif isinstance(f, TextData) or isinstance(f, GzData):
      f.seek(0, os.SEEK_SET)
    elif not(isinstance(f, H5Data)):
      raise TypeError("f argument is of no known type (edf, gz or h5).")

    if detector is not None:
      f.readHeader(self, grp, detector = detector)
    else:
      f.readHeader(self, grp)
    if self.transpose:
      w = self.width
      self.width = self.height
      self.height = w

    f.moveTo(self, nr)
    # Read image
    # ==========
    if self.storing:
      if self.transpose:
        self.data = f.currentData(self, win).reshape((self.width, self.height)).T.reshape(-1)
      else:
        self.data = f.currentData(self, win)
    else:
      self.nr = nr

    if needClose:
      f.close()

    # Raz some attributes
    self.pixbuf = None
    return self

  def load(self, f, nr = 0):
    #print "loading data from " + f
    f = fromFilename(f)
    f.moveAt(self, nr)
    data = f.currentData(self)
    # Close file
    # ==========
    f.close()

    return data.reshape((self.width, self.height)).T.reshape(-1) if self.transpose else data

  def setStored(self, status):
    self.storing = status
    if not self.storing:
      self.data = None

  def getData(self, flat = True, dtype = None):
    data = self.data
    if data is None:
      # Read the data on the fly
      data = self.load(self.dataFile, self.nr)
    if data is None:
      raise ValueError
    if self.storing:
      self.data = data
    if not flat:
      data = data.reshape(self.height, self.width)
    if dtype is not None:
      return data.astype(dtype)
    else:
      return data

  def getNormData(self, scale = [None, None, None], logScale = False, factor = None):
    data = self.getData()
    if data is None:
      return None
    if logScale:
      tmp = numpy.array(data, dtype = numpy.float32)
      if factor is not None:
        tmp *= factor    
      if tmp.min() > 0 and tmp.max() / tmp.min() < 10 and scale[2] is None:
        scale[2] = tmp.min()
      if scale[2] is not None:
        tmp -= scale[2]
      else:
        scale[2] = 0.
      corrvals = (tmp <= 0)
      n = corrvals.sum()
      if n > 0:
        # pmin is the lowest non null positive value.
        tmp[corrvals] = numpy.nan
        pmin = numpy.nanmin(tmp)
        tmp[corrvals] = pmin
      else:
        pmin = tmp.min()
        corrvals = None
      pmax = tmp.max()
      # Search the second minimum in case of an offset.
      if (pmax - pmin) / max(pmin, 1e-6) < 10 and False:
        if corrvals is None:
          corrvals = (tmp == pmin)
        if scale[2] is not None:
          offset = scale[2]
        else:
          offset = pmin
        tmp[corrvals] = numpy.nan
        pmin = numpy.nanmin(tmp)
        tmp[corrvals] = pmin
        tmp -= offset
        scale[2] = offset
      img = numpy.log(tmp)
      if scale[0] is None:
        m = img.min()
        scale[0] = numpy.exp(m)
      else:
        m = numpy.log(scale[0])
      if scale[1] is None:
        M = img.max()
        scale[1] = numpy.exp(M)
      else:
        M = numpy.log(scale[1])
    else:
      img = numpy.array(data, copy = True, dtype = numpy.float32)
      if factor is not None:
        img *= factor
      m = img.min()
      M = img.max()
      if (M - m) / max(m, 1e-6) < 10 and scale[2] is None:
        scale[2] = img.sum() / img.size
      if scale[2] is not None:
        img -= scale[2]
        M -= scale[2]
        m -= scale[2]
      else:
        scale[2] = 0.
      if scale[0] is None:
        scale[0] = m
      else:
        m = scale[0]
      if scale[1] is None:
        scale[1] = M
      else:
        M = scale[1]
    return (img, (m, M))

  def getPixbuf(self, scale = [None, None, None], logScale = False,
                highlight = False, colorMap = grayScale, mask = None,
                factor = None, rect = None, rectColor = 0x4169E1FF):
    import gi
    gi.require_version('GdkPixbuf', '2.0')
    from gi.repository import GdkPixbuf, GLib

    (img, (m, M)) = self.getNormData(scale, logScale, factor)
    img = (img - m) * (255. / (M - m))
    img = colorMap(img.clip(min = 0, max = 255))
    if highlight:
      img = img.reshape((self.width * self.height, 3))
      img[data == 0] = [160, 82, 45] #[210, 105, 30]
    if mask is not None:
      img = img.reshape((self.width * self.height, 3))
      mask_ = mask.reshape(self.width * self.height)
      img_ = img.astype(numpy.dtype('f4'))
      img_[mask_] += [128, 128, 128]
      img = img_.clip(min = 0, max = 255).astype(numpy.dtype('u1'))
    img = GLib.Bytes.new(img)
    pixbuf = GdkPixbuf.Pixbuf.new_from_bytes(img, GdkPixbuf.Colorspace.RGB,
                                          False, 8, self.width, self.height,
                                          self.width * 3)
    if rect is not None:
      highlight = GdkPixbuf.Pixbuf.new(GdkPixbuf.Colorspace.RGB, False, 8,
                                rect["width"], rect["height"])
      highlight.fill(rectColor)
      highlight.composite(pixbuf,
                          rect["x"], rect["y"], rect["width"], rect["height"],
                          0, 0, 1., 1., GdkPixbuf.InterpType.NEAREST, 128)
    return pixbuf

  def getMinMax(self):
    data = self.getData()
    return (data.min(), data.max())

  def store(self, kind = "edf", wrap = False):
    if self.data is None:
      raise ValueError
    if wrap:
      self.wrap = True
      self.data[self.data < 0] += 65536
    if kind == "edf":
      self.data = self.data.clip(min = 0, max = 65535).astype(numpy.dtype("<u2"))
      self.endian = "<"
      self.dtype = "u2"
    else:
      self.endian = self.data.dtype.str[0]
      self.dtype = self.data.dtype.str[1:]
    if kind == "flatminusdark":
      self.data = self.data.clip(min = 1, max = 65535)

  def rescaleFromCounterId(self, name, value):
    out = RawImage()
    out.endian  = self.endian  
    out.dtype   = self.dtype   
    out.width   = self.width   
    out.height  = self.height  
    out.counter = self.counter.copy()
    out.counter[name] = value
    out.motor   = self.motor.copy()
    fact = value / float(self.counter[name])
    out.data    = (self.getData() * fact).astype(numpy.dtype(self.dtype))
    out.prefix  = self.prefix  
    out.run     = self.run
    out.path    = self.path
    return out

  def set(self, arr):
    self.pixbuf  = None
    self.endian  = arr.dtype.byteorder
    self.dtype   = ('f' if arr.dtype.char == 'd' else arr.dtype.char) + "%d" % arr.dtype.itemsize
    self.width   = arr.shape[1]   
    self.height  = arr.shape[0] 
    self.data    = arr.reshape(-1)
    self.run     = -1 

  def add(self, data):
    self.pixbuf  = None
    if self.data is None:
      self.endian  = data.endian  
      self.dtype   = data.dtype   
      self.width   = data.width   
      self.height  = data.height  
      self.data    = data.getData() * 1.
      self.prefix  = data.prefix  
      self.run     = -1 
      self.average = [data.run]
      self.path    = data.path
    else:
      if type(data) == type(RawImage()):
        if not(self.width == data.width) or \
           not(self.height == data.height):
          raise ValueError
        self.data += data.getData()
        self.average.append(data.run)
      elif type(data) == type(0.):
        self.data += data
      elif type(data) == type(0):
        self.data += data
      else:
        raise TypeError

  def multiply(self, val):
    self.pixbuf  = None
    if self.data is None:
      raise ValueError
    if type(val) == type(0.):
      self.data *= val
    elif type(val) == type(RawImage()):
      self.data *= val.getData()
    else:
      raise TypeError

  def divide(self, val):
    self.pixbuf  = None
    if self.data is None:
      raise ValueError
    if type(val) == type(0.):
      self.data /= val
    elif type(val) == type(RawImage()):
      self.data /= val.getData()
    else:
      raise TypeError

  def mean(self):
    return self.getData().mean()

  def mask(self, other, integralRange = (0.20, 0.25), threshold = None):
    dother = other.getData()
    if integralRange is not None:
      tmin, tmax = dother.sum() * numpy.array(integralRange)
      maxother = dother.max()
      alpha = 1.
      partial = dother.sum()
      while (partial > tmax or partial < tmin):
        if (partial > tmax):
          alpha *= 1.1
        if (partial < tmin):
          alpha *= 0.9
        partial = dother[dother > alpha * maxother].sum()
      print(alpha, alpha * maxother, partial)
      threshold = alpha * maxother
    self.data = numpy.where(dother > threshold, self.data, 0.)

  def save(self, filename):
    # The header.
    header = "{\n"
    if "HeaderID" in self.dict:
      header += "HeaderID       = %s ;\n" % self.dict["HeaderID"]
    else:
      header += "HeaderID       = EH:000001:000000:000000 ;\n"
    header += "Image          = 1 ;\n"
    header += "ByteOrder      = "
    if self.endian == "<":
      header += "LowByteFirst ;\n"
    else:
      header += "HighByteFirst ;\n"
    header += "DataType       = "
    if self.dtype == "u2":
      s = 2
      header += "UnsignedShort ;\n"
    elif self.dtype == "f8" or self.dtype == "d8":
      s = 8
      header += "DoubleValue ;\n"
    elif self.dtype == "f4":
      s = 4
      header += "FloatValue ;\n"
    else:
      s = 1
      header += "unknown ;\n"
    header += "Dim_1          = %d ;\n" % self.width
    header += "Dim_2          = %d ;\n" % self.height
    header += "Size           = %d ;\n" % (self.width * self.height * s)
    if "date" in self.dict:
      header += "date           = %s ;\n" % self.dict["date"]
    else:
      loc = locale.getlocale()
      locale.setlocale(locale.LC_ALL, "C")
      header += "date           = %s ;\n" % time.strftime("%a %b %d %H:%M:%S %Y")
      locale.setlocale(locale.LC_ALL, loc)
    header += "offset         = %d ;\n" % int(self.dict.get("offset", "0"))
    header += "count_time     = %d ;\n" % int(self.dict.get("count_time", "0"))
    header += "point_no       = %d ;\n" % int(self.dict.get("point_no", "0"))
    header += "scan_no        = %d ;\n" % int(self.dict.get("scan_no", "0"))
    header += "preset         = %g ;\n" % float(self.dict.get("preset", "0."))
    header += "col_end        = %d ;\n" % int(self.dict.get("col_end", str(self.width - 1)))
    header += "col_beg        = %d ;\n" % int(self.dict.get("col_beg", "0"))
    header += "row_end        = %d ;\n" % int(self.dict.get("row_end", str(self.height - 1)))
    header += "row_beg        = %d ;\n" % int(self.dict.get("row_beg", "0"))
    header += "counter_pos    ="
    for val in self.counter.values():
      header += " %s" % val
    header += " ;\n"
    header += "counter_mne    ="
    for val in self.counter.keys():
      header += " %s" % val
    header += " ;\n"
    header += "motor_pos      ="
    for val in self.motor.values():
      header += " %s" % val
    header += " ;\n"
    header += "motor_mne      ="
    for val in self.motor.keys():
      header += " %s" % val
    header += " ;\n"
    header += "suffix         = .edf ;\n"
    header += "prefix         = %s ;\n" % self.prefix
    header += "dir            = %s ;\n" % os.path.abspath(os.path.join(os.curdir, self.path))
    header += "run            = %d ;\n" % self.run
    header += "title          = %s ;\n" % self.dict.get("title", "EDF export")
    if "mean_angle" in self.dict:
      header += "mean_angle     = %12.8f ;\n" % self.dict["mean_angle"]
    if len(self.average) > 0:
      header += "average        ="
      for a in self.average:
        header += " %d" % a
      header += " ;\n"
    for kv in self.dict.items():
      header += "%-15s= %s ;\n" % kv
    f = open(filename, "wb")
    f.write(header.encode())
    for i in range(2046 - f.tell()):
      f.write(b' ')
    f.write(b"}\n")
    self.dataDiskOffset = f.tell()
    # The data
    if self.wrap:
      data = self.data.copy()
      data[data > (65536 / 2)] = 0
      f.write(data.tobytes())
    else:
      f.write(self.data.tobytes())
    f.write(b"\n")
    f.close

    # We put a flag saying that self is on disk now.
    self.dataFile = filename
    if not self.storing:
      self.data = None

  def toCairo(self, cr, wi, hi, res, buf,
              scale = (None, None), logScale = True, title = None,
              highlight = False, colorMap = jetScale, rawPix = None, offset = None,
              cross = None, legend = None, format = "%.2e", factor = None):
    import cairo
    import gi
    gi.require_version('GdkPixbuf', '2.0')
    gi.require_version('Gdk', '3.0')
    from gi.repository import GLib, GdkPixbuf, Gdk

    scale_ = [scale[0], scale[1], offset]
    if rawPix is None:
      rawPix = self.getPixbuf(scale = scale_,
                              logScale = logScale,
                              highlight = highlight,
                              colorMap = colorMap,
                              factor = factor)

    # Get the label values
    (m, M, offset) = scale_
    if logScale:
      lm = numpy.log(m)
      lM = numpy.log(M)
      lbls = [M,
              numpy.exp(lm + (lM - lm) * 0.75),
              numpy.exp(lm + (lM - lm) * 0.5),
              numpy.exp(lm + (lM - lm) * 0.25), m]
    else:
      lm = m
      lM = M
      lbls = [lM,
              lm + (lM - lm) * 0.75,
              lm + (lM - lm) * 0.5,
              lm + (lM - lm) * 0.25, lm]

    # Get the real scale width
    cr.select_font_face("Serif", cairo.FONT_SLANT_NORMAL,
                            cairo.FONT_WEIGHT_NORMAL)
    cr.set_font_size(max(self.width, self.height) / 32.)

    cr.set_source_rgb(1., 1., 1.)
    cr.rectangle(0., 0., wi, hi)
    cr.fill()
    
    if title is not None:
      cr.translate(0, res * 1.5)
    
    # Make the image.
    Gdk.cairo_set_source_pixbuf(cr, rawPix, buf, buf)
    cr.paint()
    if cross is not None:
      cr.set_source_rgba(1., 1., 1., 0.5)
      cr.move_to(0, cross[1])
      cr.line_to(self.width, cross[1])
      cr.move_to(cross[0], 0)
      cr.line_to(cross[0], self.height)
      cr.set_line_width(2 * buf)
      cr.stroke()

    # Add a scale.
    w = int(res / 1.6)
    h = self.height
    if offset is not None:
      vals = cr.text_extents(format % offset)
      dx_scale = vals[2]
      dy_scale = vals[3]
      h -= int(2. * dy_scale)
    pix = GdkPixbuf.Pixbuf.new_from_bytes(GLib.Bytes.new(colorMap(numpy.arange(0., 255., 255. / float(h))[::-1].repeat(w))),
                                            GdkPixbuf.Colorspace.RGB,
                                      False, 8, w, h, w * 3)
    Gdk.cairo_set_source_pixbuf(cr, pix, self.width + buf + res + buf, buf)
    cr.paint()

    # The frames.
    cr.set_line_width(buf)
    cr.set_source_rgb(0., 0., 0.)

    cr.rectangle(buf / 2, buf / 2, self.width, self.height)
    cr.stroke()

    x = self.width + res + 1.5 * buf
    cr.rectangle(x, buf / 2, w, h)
    cr.stroke()

    y = buf / 2 + h * 0.25
    cr.move_to(x, y)
    cr.line_to(x + w * 0.5, y)
    cr.stroke()

    y = buf / 2 + h * 0.5
    cr.move_to(x, y)
    cr.line_to(x + w * 0.5, y)
    cr.stroke()

    y = buf / 2 + h * 0.75
    cr.move_to(x, y)
    cr.line_to(x + w * 0.5, y)
    cr.stroke()

    # The labels
    vals = cr.text_extents("-0000.00")
    dy_scale = vals[3]

    x = self.width + res + 2 * buf + w + res * 0.25
    cr.move_to(x, buf + dy_scale + 5.)
    cr.show_text(format % lbls[0])
    cr.move_to(x, buf + h * 0.25 + dy_scale / 2)
    cr.show_text(format % lbls[1])
    cr.move_to(x, buf + h * 0.5 + dy_scale / 2)
    cr.show_text(format % lbls[2])
    cr.move_to(x, buf + h * 0.75 + dy_scale / 2)
    cr.show_text(format % lbls[3])
    cr.move_to(x, buf + h - 5.)
    cr.show_text(format % lbls[4])
    if offset is not None:
      cr.move_to(self.width + buf + res / 2. + buf, buf + h + 1.75 * dy_scale - 5.)
      if offset > 0.:
        cr.show_text("+%.8g" % offset)
      else:
        cr.show_text("%.8g" % offset)

    # Add a legend.
    if legend is not None:
      vals = cr.text_extents(legend)
      dx_scale = res * 5.
      dy_scale = vals[2]
      x = self.width + 2 * buf + res + 2 * buf + res / 1.6 + dx_scale + buf
      cr.move_to(x, buf + (h + dy_scale) / 2)
      cr.save()
      cr.rotate(-pi / 2.)
      cr.show_text(legend)
      cr.restore()
    
    # The Title
    if title is not None:
      cr.select_font_face("Serif", cairo.FONT_SLANT_NORMAL,
                              cairo.FONT_WEIGHT_NORMAL)
      cr.set_font_size(max(self.width, self.height) / 24.)

      vals = cr.text_extents(title)
      dx_scale = vals[2]
      dy_scale = vals[3]

      x = (wi - dx_scale) / 2.
      if x < 0:
        cr.set_font_size(max(self.width, self.height) / 24. / dx_scale * (wi - 8))
        x = 1
      cr.move_to(x, -res * 0.5)
      cr.show_text(title)

  def exportPDF(self, filename, **kwargs):
    import cairo 
    # Generate a cairo context from the pixbuf.
    res = max(self.width, self.height) / 28.5
    buf = res / 35.
    dx_scale = res * 5
    dx_legend = buf / 2. + res * 1.
    if "legend" not in kwargs or kwargs["legend"] is None:
      dx_legend = 0
    wi = int(round((self.width + 2 * buf + res + 2 * buf + res / 1.6 + dx_scale + dx_legend + 2.) / 2.)) * 2
    hi = int(round((self.height + 2 * buf + 2.) / 2.)) * 2
    if "title" in kwargs and kwargs["title"] is not None:
      hi += res * 1.5
    surf = cairo.PDFSurface(filename, wi, hi)
    cr = cairo.Context(surf)

    self.toCairo(cr, wi, hi, res, buf, **kwargs)

    surf.write_to_png(filename.replace(".pdf", ".png"))
    surf.finish()

  def show(self, logScale = True, lbound = None, ubound = None):
    import gi
    gi.require_version('Gtk', '3.0')
    from gi.repository import Gtk, GLib
    from gui_display import EDF_display

    window = Gtk.ApplicationWindow()
    window.connect("delete_event", Gtk.main_quit)
    window.connect("destroy", Gtk.main_quit)

    display = EDF_display()
    window.add(display)

    display.set(EDF_display.KIND_RAW, self)
    display.setLabel(self.dataFile)
    display.setLogscale(logScale)
    if lbound is not None:
      display.scaleMin.set_text("%g" % lbound)
    if ubound is not None:
      display.scaleMax.set_text("%g" % ubound)

    GLib.idle_add(display.show)

    window.set_title("%s - %04d" % (self.prefix, self.run))
    window.show_all()

    Gtk.main()


def multiRawImages(file):
  if file.endswith(".gz"):
    f = gzip.open(file, "rb")
  else:
    try:
      f = open(file, "rb")
    except IOError:
      f = gzip.open(file + ".gz", "rb")

  out = []
  try:
    while True:
      img = RawImage()
      img.dataFile = file
      img.read(f)
      out.append(img)
      #if len(out) > 1:
      #  print len(out), img.dataDiskOffset, img.dataDiskOffset - out[-2].dataDiskOffset
  except:
    pass

  f.close()
  
  return out

def parse():
  parser = OptionParser("usage: edf.py [options] [files]")
  parser.add_option("-g", "--group", dest="group", type="int", default = "0",
                    help="select the i-th group in the file.")
  parser.add_option("-i", "--index", dest="index", type="int", default = "0",
                    help="select the i-th image in the file.")
  parser.add_option("-e", "--export", dest="export", action="store_true",
                    help="export the input EDF file as a PNG with a scale, a title...")
  parser.add_option("-d", "--dump", dest="dump", action="store_true",
                    help="export the input EDF data as a PNG")
  parser.add_option("-s", "--scale", dest="scale", type="string",
                    help="use [log] scale or [linear] scale")
  parser.add_option("-u", "--upper-bound", dest="ubound", type="float",
                    help="set up the upper bound for the scale")
  parser.add_option("-l", "--lower-bound", dest="lbound", type="float",
                    help="set up the lower bound for the scale")
  parser.add_option("-o", "--offset", dest="offset", type="float", default = None,
                    help="set up the offset for the scale")
  parser.add_option("", "--offset-field", dest="offsetf", type="string",
                    help="set up the offset for the scale")
  parser.add_option("-m", "--colour-map", dest="cmap",
                    help="set up the colour map (jet, gray or stress)")
  parser.add_option("-t", "--title", dest="title", type="string",
                    help="set up the title (use by default the run name of the edf file)")
  parser.add_option("-n", "--normalise", dest="normalise", type="float",
                    help="normalise the EDF values with the given argument")
  parser.add_option("-f", "--format", dest="format", type="string", default="%.2e",
                    help="format to display the colour scale values")
  parser.add_option("-r", "--rectangle", dest="rect", type="string",
                    help="highlight an area (format is x,y-w,h-colour)")
  parser.add_option("-w", "--window", dest="win", type="string",
                    help="load only a subset (format is x,y-w,h)")
  parser.add_option("", "--legend", dest="legend", type="string",
                    help="legend label for the colour scale")
  parser.add_option("", "--factor", dest="factor", type="float",
                    help="factor to apply to data")
  parser.add_option("", "--integral", dest="integral", action="store_true",
                    help="compute integrated intensity")
  parser.add_option("", "--mask", dest="mask", type="string",
                    help="mask with provided edf file")
  parser.add_option("", "--mask-integral", dest="mask_integral", type="float", default = "0.25",
                    help="part of total masking value to keep")
  parser.add_option("", "--mask-threshold", dest="mask_threshold", type="float",
                    help="threshold for the mask")
  parser.add_option("", "--save-as", dest="save_as", type="string",
                    help="save the data in a new EDF file")
  parser.add_option("", "--detector", dest="detector", type="string",
                    help="name of the detector")
  parser.add_option("", "--transpose", dest="transpose", action="store_true",
                    help="transpose the image data")
  
  return parser

if __name__ == "__main__":
  parser = parse()
  (options, args) = parser.parse_args()

  if options.cmap == "gray":
    cmap = grayScale
  elif options.cmap  == "stress":
    cmap = stressScale
  elif options.cmap  == "xray":
    cmap = xrayScale
  elif options.cmap  == "viridis":
    cmap = viridisScale
  else:
    options.cmap = "jet"
    cmap = jetScale
  rect = options.rect
  rectColor = 0xFF000055
  if options.rect is not None:
    rect = {}
    vals = options.rect.split("-", 3)
    xy = vals[0].split(",", 2)
    wh = vals[1].split(",", 2)
    rect = {"x": int(xy[0]), "y": int(xy[1]),
            "width": int(wh[0]), "height": int(wh[1])}
    rectColor = int(vals[2], 16)
  win = options.win
  if options.win is not None:
    win = {}
    vals = options.win.split("-", 2)
    xy = vals[0].split(",", 2)
    wh = vals[1].split(",", 2)
    win = {"x": int(xy[0]), "y": int(xy[1]),
           "width": int(wh[0]), "height": int(wh[1])}

  raw = RawImage(store = True, transpose = options.transpose).read(args[0], options.index, options.group, options.detector, win)
  if options.normalise is not None:
    raw.setStored(True)
    d = raw.getData()
    d /= options.normalise
  if options.offsetf is not None:
    options.offset = float(raw.dict[options.offsetf])
  if options.factor is not None and options.offset is not None:
    options.offset *= options.factor
  if options.integral:
    d = raw.getData()
    print(d.sum())
    sys.exit(0)

  if options.mask is not None:
    other = RawImage(transpose = options.transpose).read(options.mask)
    if options.mask_threshold is not None:
      raw.mask(other, None, options.mask_threshold)
    else:
      raw.mask(other, (options.mask_integral, options.mask_integral * 1.05))

  if options.scale == "linear":
    logScale = False
  else:
    logScale = True
  if options.export:
    if raw.prefix is None or len(raw.prefix) < 6:
      out = "".join(os.path.basename(args[0]).split('.')[:-1])
      title = out.replace("_", " ")
    else:
      out = raw.prefix
      title = raw.prefix.replace("_", " ")
    if options.title is not None:
      title = options.title if len(options.title) > 0 else None
    print("export to: ", out + ".png")
    raw.exportPDF(out + ".pdf",
                  scale = (options.lbound, options.ubound),
                  title = title,
                  logScale = logScale,
                  offset = options.offset,
                  colorMap = cmap,
                  legend = options.legend,
                  format = options.format,
                  factor = options.factor)
    sys.exit(0)

  if options.dump:
    out = "".join(os.path.basename(args[0]).split('.')[:-1])
    print("export to: ", out + ".png")
    if raw.prefix is None or len(raw.prefix) < 6:
      title = out
    else:
      title = raw.prefix
    scale_ = [options.lbound, options.ubound, options.offset]
    rawPix = raw.getPixbuf(scale = scale_,
                           logScale = logScale,
                           colorMap = cmap,
                           factor = options.factor,
                           rect = rect, rectColor = rectColor)
    rawPix.savev(out + ".png", "png", (), ())
    sys.exit(0)

  if options.save_as is not None:
    raw.save(options.save_as)
    sys.exit(0)

  import gi
  gi.require_version('Gtk', '3.0')
  from gi.repository import Gtk, GLib
  from gui_display import EDF_display
  
  window = Gtk.ApplicationWindow()
  window.connect("delete_event", Gtk.main_quit)
  window.connect("destroy", Gtk.main_quit)

  display = EDF_display()
  window.add(display)

  dark = None
  flat = None
  if len(args) > 2:
    dark = RawImage(transpose = options.transpose).read(args[1])
  if len(args) > 3:
    flat = RawImage(transpose = options.transpose).read(args[2])

  if dark is not None and flat is not None:
    display.set(EDF_display.KIND_RAW, raw)

    corr = RawImage(transpose = options.transpose)
    corr.add(dark)
    corr.multiply(-1.)
    corr.add(raw)
    corr.store()
    display.set(EDF_display.KIND_DARK, corr)

    resc = RawImage(transpose = options.transpose)
    resc.add(corr)

    tmp = RawImage(transpose = options.transpose)
    tmp.add(dark)
    tmp.multiply(-1.)
    tmp.add(flat)
    tmp.store(kind = "flatminusdark")

    resc.divide(tmp)
    display.set(EDF_display.KIND_FLAT, resc)
  elif dark is not None:
    corr = RawImage(transpose = options.transpose)
    corr.add(dark)
    corr.multiply(-1.)
    corr.add(raw)
    display.set(EDF_display.KIND_RAW, raw)
    display.set(EDF_display.KIND_DARK, corr)
  else:
    display.set(EDF_display.KIND_RAW, raw)
  display.setLabel(args[0])

  if options.scale == "linear":
    display.setLogscale(False)
  else:
    display.setLogscale(True)
  if options.lbound is not None:
    display.scaleMin.set_text("%g" % options.lbound)
  if options.ubound is not None:
    display.scaleMax.set_text("%g" % options.ubound)
  display.offset = options.offset
  display.setFactor(options.factor)

  display.setCMap(options.cmap)

  GLib.idle_add(display.show)

  window.set_title("%s - %04d" % (raw.prefix, raw.run))
  window.show_all()

  Gtk.main()

