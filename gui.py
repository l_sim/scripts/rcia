#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2012-2020 Damien Caliste <damien.caliste@cea.fr>,
#                         Thu Nhi Tran Thi <thu-nhi.tran-thi@esrf.fr>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA

import os, time, re, threading
import edf, rcv
from gui_model import EDF_model
from gui_display import EDF_display
import multiprocessing
import queue
from gi.repository import Gtk, GObject
#from meliae import scanner

class EDF_gui(Gtk.HPaned):
  COL_KIND_LABEL     = 0
  COL_KIND_SENSITIVE = 1

  def __init__(self, model, factor = None):
    super(EDF_gui, self).__init__()

    self.model = model

    # Generate a model for the data kind.
    lst = Gtk.ListStore(GObject.TYPE_STRING, GObject.TYPE_BOOLEAN)
    lst.set(lst.append(), EDF_gui.COL_KIND_LABEL, "raw")
    lst.set(lst.append(), EDF_gui.COL_KIND_LABEL, "dark corr.")
    lst.set(lst.append(), EDF_gui.COL_KIND_LABEL, "flat corr.")
    lst.set(lst.append(), EDF_gui.COL_KIND_LABEL, "norm.")

    # The left part of the pane.
    book = Gtk.Notebook()
    book.set_tab_pos(Gtk.PositionType.RIGHT)
    self.pack1(book, False, False)

    vbox = Gtk.VBox()
    self.vboxFile = vbox

    hbox2 = Gtk.HBox()
    vbox.pack_start(hbox2, False, False, 0)
    wd = Gtk.FileChooserDialog(title = "Choose a directory with EDF data", parent = None, \
                               action = Gtk.FileChooserAction.SELECT_FOLDER)
    wd.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.REJECT,
                   Gtk.STOCK_OPEN, Gtk.ResponseType.ACCEPT)
    wd.set_current_folder(os.curdir)
    wd.connect("response", self.chooseDir)
    dirSel = Gtk.FileChooserButton(dialog = wd)
    dirSel.set_action(Gtk.FileChooserAction.SELECT_FOLDER)
    hbox2.pack_start(dirSel, True, True, 0)
    entry = Gtk.Entry()
    entry.set_text("*\.edf")
    entry.set_tooltip_text("Use Python regular expressions.")
    entry.connect("activate", self.applyFilter, entry)
    #hbox2.pack_start(entry, False, False, 0)
    bt = Gtk.Button(label="Filter")
    bt.connect("clicked", self.applyFilter, entry)
    #hbox2.pack_start(bt, False, False, 0)

    scroll = Gtk.ScrolledWindow()
    scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
    scroll.set_shadow_type(Gtk.ShadowType.ETCHED_IN)
    scroll.set_size_request(450, -1)
    vbox.pack_start(scroll, True, True, 0)

    (view, renderMean) = self.model.getView()
    renderMean.connect("toggled", self.onUseForMean)
    scroll.add(view)

    # First line of action buttons
    hbox2 = Gtk.HBox()    
    vbox.pack_start(hbox2, False, False, 0)

    # The select all button.
    img = Gtk.Image()
    img.set_from_icon_name("edit-select-all", Gtk.IconSize.SMALL_TOOLBAR)
    wd = Gtk.Button()
    wd.add(img)
    wd.connect("clicked", self.selectAll)
    hbox2.pack_start(wd, False, False, 0)
    
    ## img = Gtk.Image()
    ## img.set_from_stock(Gtk.STOCK_SAVE, Gtk.ICON_SIZE_SMALL_TOOLBAR)
    ## self.btSaveCorr = Gtk.Button()
    ## self.btSaveCorr.add(img)
    ## self.btSaveCorr.set_sensitive(False)
    ## self.btSaveCorr.connect("clicked", self.saveAll)
    #hbox2.pack_end(self.btSaveCorr, False, False, 0)

    # The correction button
    table = Gtk.Table(n_rows = 2, n_columns = 2)
    hbox2.pack_start(table, False, False, 0)

    table.attach(Gtk.Label(label = "Dark:"), 0, 1, 0, 1)
    cbDark = model.getCbDark()
    cbDark.connect("changed", self.selectDir)
    table.attach(cbDark, 1, 2, 0, 1)

    table.attach(Gtk.Label(label = "Flat:"), 0, 1, 1, 2)
    cbFlat = model.getCbFlat()
    cbFlat.connect("changed", self.selectDir)
    table.attach(cbFlat, 1, 2, 1, 2)

    vbox2 = Gtk.VBox()
    hbox2.pack_start(vbox2, False, False, 0)
    ## hbox3 = Gtk.HBox()
    #vbox2.pack_start(hbox3, False, False, 0)
    ## hbox3.pack_start(Gtk.Label("shift:"), True, True, 0)
    ## self.shift = Gtk.SpinButton(Gtk.Adjustment(lower = 0,
    ##                                            upper = 50,
    ##                                            step_incr = 1,
    ##                                            page_incr = 5.),
    ##                                      digits = 0)
    ## hbox3.pack_start(self.shift, False, False, 0)
    self.btCorr = Gtk.Button(label="Apply\ncorrection")
    self.btCorr.connect("clicked", self.applyCorrection)
    self.btCorr.set_sensitive(False)
    vbox2.pack_end(self.btCorr, True, True, 0)

    # The normalize button.
    self.btNorm = Gtk.Button(label="Normalise")
    self.btNorm.connect("clicked", self.applyNormalisation)
    self.btNorm.set_sensitive(False)
    hbox2.pack_end(self.btNorm, False, False, 0)
    
    ## hbox2.pack_end(Gtk.HSeparator(), False, False, 10)

    vbox.pack_start(Gtk.HSeparator(), False, False, 2)

    # Second line of action buttons
    hbox2 = Gtk.HBox()    
    vbox.pack_start(hbox2, False, False, 0)

    vbox2 = Gtk.VBox()
    hbox2.pack_start(vbox2, True, False, 0)
    vbox2.pack_start(Gtk.Label(label ="sample ω range"), False, False, 0)
    hbox3 = Gtk.HBox()    
    vbox2.pack_start(hbox3, False, False, 0)
    hbox3.pack_start(Gtk.Label(label = "from:"), False, False, 0)
    self.angOrig = Gtk.SpinButton.new(Gtk.Adjustment(lower = -180,
                                                 upper = 180,
                                                 step_increment = 0.005,
                                                 page_increment = 5.), 1.,
                                    digits = 10)
    hbox3.pack_end(self.angOrig, False, False, 0)
    hbox3 = Gtk.HBox()    
    vbox2.pack_start(hbox3, False, False, 0)
    hbox3.pack_start(Gtk.Label(label = "step:"), False, False, 0)
    self.angStep = Gtk.SpinButton.new(Gtk.Adjustment(lower = -5,
                                                 upper = 5,
                                                 step_increment = 0.00005,
                                                 page_increment = 0.001), 1.,
                                  digits = 10)
    self.angStep.set_value(0.00025)
    hbox3.pack_end(self.angStep, False, False, 0)

    hbox2.pack_start(Gtk.HSeparator(), False, False, 10)

    vbox2 = Gtk.VBox()
    hbox2.pack_start(vbox2, True, False, 0)
    hbox3 = Gtk.HBox()    
    vbox2.pack_start(hbox3, False, False, 0)
    hbox3.pack_start(Gtk.Label(label = "from:"), False, False, 0)
    self.cbKindRC = Gtk.ComboBox.new_with_model(lst)
    render = Gtk.CellRendererText()
    self.cbKindRC.pack_start(render, False)
    self.cbKindRC.add_attribute(render, "text", EDF_gui.COL_KIND_LABEL)
    self.cbKindRC.set_active(0)
    hbox3.pack_start(self.cbKindRC, True, True, 0)
    self.btRC = Gtk.Button(label = "Calculate rocking curves")
    self.btRC.set_sensitive(False)
    self.btRC.connect("clicked", self.calcRc)
    vbox2.pack_start(self.btRC, False, False, 0)

    hbox2.pack_start(Gtk.HSeparator(), False, False, 10)

    vbox2 = Gtk.VBox()
    hbox2.pack_end(vbox2, True, False, 0)
    hbox3 = Gtk.HBox()    
    vbox2.pack_start(hbox3, False, False, 0)
    hbox3.pack_start(Gtk.Label(label = "from:"), False, False, 0)
    self.cbKindMap = Gtk.ComboBox.new_with_model(lst)
    render = Gtk.CellRendererText()
    self.cbKindMap.pack_start(render, False)
    self.cbKindMap.add_attribute(render, "text", EDF_gui.COL_KIND_LABEL)
    self.cbKindMap.add_attribute(render, "sensitive", EDF_gui.COL_KIND_SENSITIVE)
    self.cbKindMap.set_active(0)
    hbox3.pack_start(self.cbKindMap, True, True, 0)
    self.btMap = Gtk.Button(label = "Generate Maps")
    self.btMap.set_sensitive(False)
    self.btMap.connect("clicked", self.calcMap)
    vbox2.pack_start(self.btMap, False, False, 0)

    self.bar = Gtk.ProgressBar()
    self.bar.set_show_text(True)
    self.bar.set_no_show_all(True)

    wd = Gtk.Label(label = "Files")
    wd.set_angle(90)
    vpane = Gtk.VBox()
    book.append_page(vpane, wd)
    vpane.pack_start(vbox, True, True, 0)
    vpane.pack_end(self.bar, False, False, 0)

    # The right part of the pane.
    self.display = EDF_display()
    self.display.setLogscale(True)
    self.display.setFactor(factor)
    self.pack2(self.display, True, True)
    view.get_selection().connect("changed", self.selectFile)

    wd = Gtk.Label(label = "Intensity profiles")
    wd.set_angle(90)
    book.append_page(self.display.getPlotProfile(), wd)

    wd = Gtk.Label(label = "Rocking curves")
    wd.set_angle(90)
    book.append_page(self.display.getPlot(), wd)

    book.connect("switch-page", self.onPage)

  def setLock(self, status):
    if not status:
      self.bar.show()
      self.bar.set_fraction(0)
    else:
      self.bar.hide()
    self.vboxFile.set_sensitive(status)
    self.get_child2().set_sensitive(status)
    while Gtk.events_pending():
      Gtk.main_iteration()

  def onPage(self, book, page, i):
    if i == 2:
      self.display.setAction(EDF_display.ACTION_ROCKING_CURVES)
    elif i == 1:
      self.display.setAction(EDF_display.ACTION_INTENSITY_PROFILES)
    else:
      self.display.setAction(EDF_display.ACTION_NONE)

  def setProgress(self, text, fraction = None):
    if fraction is None:
      self.bar.pulse()
    else:
      self.bar.set_fraction(fraction)
    self.bar.set_text(text)
    while Gtk.events_pending():
      Gtk.main_iteration()

  def chooseDir(self, widget, response_id):
    if (response_id != Gtk.RESPONSE_ACCEPT):
      return
    widget.hide()
    self.setLock(False)
    self.model.setDirectory(widget.get_filename())
    self.setLock(True)

  def applyFilter(self, widget, entry):
    self.model.setFilterPattern(entry.get_text())

  def updateCalcBt(self, iterFilter, updateLbl = False):
    if iterFilter is not None:
      (nmean, vmean, avgOnly) = self.model.getMean(iterFilter)
      if updateLbl:
        addLbl = ""
        if avgOnly:
          if not(vmean == 0.):
            self.btNorm.set_label("Compute\naverage\n" +
                                  " %d entries" % nmean)
          else:
            self.btNorm.set_label("Compute\naverage")
        else:
          if not(vmean == 0.):
            self.btNorm.set_label("Normalise\n" +
                                  " %d entries,\n %s = %g" % (nmean, self.model.normKey, vmean))
          else:
            self.btNorm.set_label("Normalise")
      avg = self.model.getAvg(iterFilter)
      norm = self.model.getNorm(iterFilter)
    else:
      nmean = 0
      avg = None
      norm = None
    # Update norm button
    self.btNorm.set_sensitive(nmean > 0)
    # Update corr button
    dark = self.model.getDark()
    flat = self.model.getFlat()
    sensitive = (dark is not None and nmean > 0)
    self.btCorr.set_sensitive(sensitive)
    ## self.btSaveCorr.set_sensitive(sensitive)
    # Update RC button
    self.btRC.set_sensitive(nmean > 0)
    # Update RC stepping
    raw  = self.model.getSelected(EDF_model.KIND_RAW)
    if raw is not None:
      if "dtyOrigin" in raw.counter:
        self.angOrig.set_value(raw.counter["dtyOrigin"])
      if "dtyStep" in raw.counter:
        self.angStep.set_value(raw.counter["dtyStep"])
    # Update map button
    rcRaw  = self.model.getSelectedRC(EDF_model.KIND_RAW, iterFilter)
    rcNorm = self.model.getSelectedRC(EDF_model.KIND_NORM, iterFilter)
    rcDark = self.model.getSelectedRC(EDF_model.KIND_DARK, iterFilter)
    rcFlat = self.model.getSelectedRC(EDF_model.KIND_FLAT, iterFilter)
    self.btMap.set_sensitive(rcRaw is not None or rcNorm is not None or
                             rcDark is not None or rcFlat is not None)
    model = self.cbKindMap.get_model()
    it = model.iter_children(None)
    model.set(it, EDF_gui.COL_KIND_SENSITIVE, rcRaw is not None)
    it = model.iter_next(it)
    model.set(it, EDF_gui.COL_KIND_SENSITIVE, rcDark is not None)
    it = model.iter_next(it)
    model.set(it, EDF_gui.COL_KIND_SENSITIVE, rcFlat is not None)
    it = model.iter_next(it)
    model.set(it, EDF_gui.COL_KIND_SENSITIVE, rcNorm is not None)

  def selectDir(self, cb):
    self.updateCalcBt(self.model.getView()[0].get_selection().get_selected()[1])
    
  def selectFile(self, selection):
    # Show image.
    self.display.setFromModel(self.model)
    self.display.show()
    # Update buttons.
    self.updateCalcBt(selection.get_selected()[1], updateLbl = True)

  def selectAll(self, bt):
    selection = self.model.getView()[0].get_selection()
    (model, iter) = selection.get_selected()
    if iter is None:
      return
    self.model.selectAll(iter, modulo = 1)
    # Update buttons.
    self.updateCalcBt(iter, updateLbl = True)

  def saveAll(self, bt, iterFilter = None):
    self.setLock(False)
    self.model.saveAll(iterFilter, infoFunc = self.setProgress)
    self.setLock(True)

  def calcRc(self, bt, step = 64, nproc = None):
    self.setLock(False)
    kind = self.cbKindRC.get_active()
    lbl = EDF_model.KIND_DICT_NAME[kind]
    datas = self.model.getCheckItems(kind)
    if self.display.showRectangle:
      (xStart, yStart, width, height) = self.display.getRectangle()
    else:
      (xStart, yStart, width, height) = (0, 0, datas[0].width, datas[0].height)
    if nproc is None or nproc > 1:
      q = multiprocessing.Queue()
      pool = multiprocessing.Pool(processes = nproc,
                                  initializer = rcSlabInit, initargs = (q, ))
      rcvs = pool.map_async(calcRcSlab, [(datas, xStart, i, width,
                                          min(yStart + height - i, step),
                                          self.angOrig.get_value(), self.angStep.get_value(),
                                          EDF_model.KIND_DICT_NAME[kind], None)
                                         for i in range(yStart, yStart + height, step)])
      nData = 0
      while not(rcvs.ready()):
        try:
          val = q.get(block = True, timeout = 1)
          nData += 1
          frac = float(nData) / (len(datas) * len(range(yStart, yStart + height, step)))
          self.setProgress(val + " (%6.2f%% done)" % (frac * 100.), fraction = frac)
        except queue.Empty:
          continue
      pool.close()
      rcvs = rcvs.get()
    else:
      rcvs = []
      calcRcSlab.q = None
      for i in range(yStart, yStart + height, step):
        rocks = calcRcSlab((datas, xStart, i, width, min(yStart + height - i, step),
                            self.angOrig.get_value(), self.angStep.get_value(),
                            EDF_model.KIND_DICT_NAME[kind], self.setProgress))
        rcvs.append(rocks)
    model.setRockingCurves(kind, rcvs)
    self.updateCalcBt(self.model.getView()[0].get_selection().get_selected()[1])
    self.display.setFromModel(self.model)
    self.setLock(True)

  def calcMap(self, bt):
    self.setLock(False)
    rc = self.model.getSelectedRC(self.cbKindMap.get_active())
    (mpInt, mpFwhm, mpPeak, mpMax) = rc.calcMap(infoFunc = self.setProgress, fitFunc = self.display.fitFunc)
    lbl = EDF_model.KIND_DICT_NAME[self.cbKindMap.get_active()]
    mpInt.save(os.path.join(mpInt.path, "%s_map_%s.edf" % (mpInt.prefix, lbl)))
    mpFwhm.save(os.path.join(mpFwhm.path, "%s_map_%s.edf" % (mpFwhm.prefix, lbl)))
    mpPeak.save(os.path.join(mpPeak.path, "%s_map_%s.edf" % (mpPeak.prefix, lbl)))
    mpMax.save(os.path.join(mpMax.path, "%s_map_%s.edf" % (mpMax.prefix, lbl)))
    self.setLock(True)

  def onUseForMean(self, cell, path):
    self.updateCalcBt(self.model.filter.get_iter(path), updateLbl = True)

  def applyNormalisation(self, bt, iterFilter = None):
    iterFilter = self.model.getView()[0].get_selection().get_selected()[1]
    if iterFilter is None:
      return
    (nmean, vmean, avgOnly) = self.model.getMean(iterFilter)
    self.setLock(False)
    if avgOnly:
      self.model.calcAverage(iterFilter, infoFunc = self.setProgress)
    else:
      self.model.applyNormalisation(iterFilter, infoFunc = self.setProgress)
    self.updateCalcBt(iterFilter)
    self.display.setFromModel(self.model)
    self.setLock(True)
  def applyCorrection(self, bt, iterFilter = None):
    self.setLock(False)
    self.model.applyCorrection(iterFilter, infoFunc = self.setProgress) #,
    #shift = self.shift.get_value())
    self.display.setFromModel(self.model)
    self.setLock(True)

def rcSlabInit(q):
  calcRcSlab.q = q

def calcRcSlab(args):
  (datas, xStart, yStart, width, height, angOrig, angStep, lbl, infoFunc) = args
  rocks = rcv.RockingCurve()
  if calcRcSlab.q is None:
    rocks.setFromList(datas, xStart, yStart, width, height, infoFunc)
  else:
    rocks.setFromList(datas, xStart, yStart, width, height, calcRcSlab.q.put)
  rocks.stepAng = angStep
  rocks.origAng = angOrig
  rocks.export(os.path.join(rocks.path,
                            "%s%04d_%s.rcv" % (rocks.prefix, yStart, lbl)),
               empty = True)
  return rocks

def mainBuild(model, options):
  window = Gtk.Window(type = Gtk.WindowType.TOPLEVEL)
  window.connect("delete_event", Gtk.main_quit)
  window.connect("destroy", Gtk.main_quit)

  pane = EDF_gui(model, options.factor)
  window.add(pane)

  window.show_all()
  return pane

def debug(model, window):
  ## This is a debug auto mode
  parent = model.filter.iter_children(None)
  iter = model.filter.iter_children(parent)
  model.view.get_selection().select_iter(iter)
  iter0 = iter
  for i in range(3):
    model.onUseForMean(None, model.filter.get_path(iter), force = True)
    iter = model.filter.iter_next(iter)
  applyNormalisation(None, model, window, iterFilter = iter0)
  while Gtk.events_pending():
    Gtk.main_iteration()

  parent = model.filter.iter_children(None)
  parent = model.filter.iter_next(parent)
  iter = model.filter.iter_children(parent)
  model.view.get_selection().select_iter(iter)
  iter0 = iter
  for i in range(3):
    model.onUseForMean(None, model.filter.get_path(iter), force = True)
    iter = model.filter.iter_next(iter)
  applyNormalisation(None, model, window, iterFilter = iter0)
  while Gtk.events_pending():
    Gtk.main_iteration()
  parent = model.filter.iter_children(None)
  parent = model.filter.iter_next(parent)
  iter0 = model.filter.iter_children(parent)
  applyCorrection(None, model, window, iterFilter = iter0)
  return False

from optparse import OptionParser, OptionGroup

def parse():
  parser = OptionParser("usage: edf.py [options] [files]")
  parser.add_option("-s", "--scale", dest="scale", type="string",
                    help="use [log] scale or [linear] scale")
  parser.add_option("-u", "--upper-bound", dest="ubound", type="float",
                    help="set up the upper bound for the scale")
  parser.add_option("-l", "--lower-bound", dest="lbound", type="float",
                    help="set up the lower bound for the scale")
  parser.add_option("-o", "--offset", dest="offset", type="float", default = "0.",
                    help="set up the offset for the scale")
  parser.add_option("-m", "--colour-map", dest="cmap",
                    help="set up the colour map (jet, gray or stress)")
  parser.add_option("-t", "--title", dest="title", type="string",
                    help="set up the title (use by default the run name of the edf file)")
  parser.add_option("-n", "--normalise", dest="normalise", type="float",
                    help="normalise the EDF values with the given argument")
  parser.add_option("-f", "--format", dest="format", type="string", default="%.2e",
                    help="format to display the colour scale values")
  parser.add_option("", "--legend", dest="legend", type="string",
                    help="legend label for the colour scale")
  parser.add_option("", "--factor", dest="factor", type="float",
                    help="factor to apply to data")
  parser.add_option("", "--norm-key", dest="normKey", type="string", default="srcur",
                    help="counter name to use to normalise the data")
  parser.add_option("-d", "--detector", dest="detector", type="string", default="pcogold",
                    help="name of the detector")
  
  return parser

if __name__ == "__main__":
  parser = parse()
  (options, args) = parser.parse_args()

  model = EDF_model(options.normKey, options.detector)
  pane = mainBuild(model, options)
  pane.display.rcWithGuess = False
  
  pane.setLock(False)
  model.setDirectory(os.curdir, lazy = True, infoFunc = pane.setProgress)
  #GObject.idle_add(debug, model, window,
  #                 priority = GObject.PRIORITY_LOW)
  #model.selectAll(iterFilter = model.filter.iter_children(None))
  #rocks = model.storeRockingCurves(iterFilter = model.filter.iter_children(None),
  #                                 xStart = 0, yStart = 128,
  #                                 width = 2048, height = 10,
  #                                 infoFunc = pane.setProgress)
  #rocks.export("TIV48A_072_E1_b_0000_raw.rcv")
  pane.setLock(True)
  Gtk.main()

